<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


//prefix listed Route
/*
 * 1 user
 * 2 buyer
 * 3 agent
 * 4 role
 * 5 department
 * 6 team
 * 7 order-status
 * 8 order-category
 * 9 order-sub-category
 * 10 order
 * 11 shipment
 * 12 production
 * 13 sub-contractor
 * 14 production-line
 * 15 budget
 * 16 color-lib
 * 17 size-lib
 * 18 order-size-n-color
 * 19 kd-program
 * 20 order-pending
 * 21 kd-parts-lib
 * 22 kd-issue
 * 23 supplier
 * */



Route::get('/clear', function() {
    $exitCode = Artisan::call('cache:clear');
    // return what you want
});

Route::get('/lara', function () {
    return view('welcome');
});

Route::get('/', 'HomeController@index')->name('homePage');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//___1
Route::prefix('user')->group(function (){
    Route::get('list', 'UserController@index')->name('user-list');
    Route::get('create', 'UserController@create')->name('user-create');
    Route::post('store', 'UserController@store')->name('user-store');
    Route::get('show', 'UserController@show')->name('user-show');
    Route::get('edit/{user}', 'UserController@edit')->name('user-edit');
    Route::patch('update/{user}', 'UserController@update')->name('user-update');
    Route::get('destroy', 'UserController@destroy')->name('user-destroy');
});

//___2
Route::prefix('buyer')->group(function (){
    $prefixName = 'buyer';
    $controllerName = 'Buyer';
    Route::get('list', $controllerName.'Controller@index')->name($prefixName.'-list');
    Route::get('create', $controllerName.'Controller@create')->name($prefixName.'-create');
    Route::post('store', $controllerName.'Controller@store')->name($prefixName.'-store');
    Route::get('show', $controllerName.'Controller@show')->name($prefixName.'-show');
    Route::get('edit/{'.$prefixName.'}', $controllerName.'Controller@edit')->name($prefixName.'-edit');
    Route::patch('update/{'.$prefixName.'}', $controllerName.'Controller@update')->name($prefixName.'-update');
    Route::get('destroy', $controllerName.'Controller@destroy')->name($prefixName.'-destroy');
});

//___3
Route::prefix('agent')->group(function (){
    $prefixName = 'agent';
    $controllerName = 'Agent';
    Route::get('list', $controllerName.'Controller@index')->name($prefixName.'-list');
    Route::get('create', $controllerName.'Controller@create')->name($prefixName.'-create');
    Route::post('store', $controllerName.'Controller@store')->name($prefixName.'-store');
    Route::get('show', $controllerName.'Controller@show')->name($prefixName.'-show');
    Route::get('edit/{'.$prefixName.'}', $controllerName.'Controller@edit')->name($prefixName.'-edit');
    Route::patch('update/{'.$prefixName.'}', $controllerName.'Controller@update')->name($prefixName.'-update');
    Route::get('destroy', $controllerName.'Controller@destroy')->name($prefixName.'-destroy');
});

//___4
Route::prefix('role')->group(function (){
    Route::get('list', 'RoleController@index')->name('role-list');
    Route::get('create', 'RoleController@create')->name('role-create');
    Route::post('store', 'RoleController@store')->name('role-store');
    Route::get('show', 'RoleController@show')->name('role-show');
    Route::get('edit/{role}', 'RoleController@edit')->name('role-edit');
    Route::patch('update/{role}', 'RoleController@update')->name('role-update');
    Route::get('destroy/{role}', 'RoleController@destroy')->name('role-destroy');
});

//___5
Route::prefix('department')->group(function (){
    Route::get('list', 'DepartmentController@index')->name('department-list');
    Route::get('create', 'DepartmentController@create')->name('department-create');
    Route::post('store', 'DepartmentController@store')->name('department-store');
    Route::get('show', 'DepartmentController@show')->name('department-show');
    Route::get('edit/{department}', 'DepartmentController@edit')->name('department-edit');
    Route::patch('update/{department}', 'DepartmentController@update')->name('department-update');
    Route::get('destroy/{department}', 'DepartmentController@destroy')->name('department-destroy');
});

//___6
Route::prefix('team')->group(function (){
    Route::get('list', 'TeamController@index')->name('team-list');
    Route::get('create', 'TeamController@create')->name('team-create');
    Route::post('store', 'TeamController@store')->name('team-store');
    Route::get('show', 'TeamController@show')->name('team-show');
    Route::get('edit/{team}', 'TeamController@edit')->name('team-edit');
    Route::patch('update/{team}', 'TeamController@update')->name('team-update');
    Route::get('destroy/{team}', 'TeamController@destroy')->name('team-destroy');
});

//___7
Route::prefix('order-status')->group(function (){
    Route::get('list', 'OrderStatusController@index')->name('order-status-list');
    Route::get('create', 'OrderStatusController@create')->name('order-status-create');
    Route::post('store', 'OrderStatusController@store')->name('order-status-store');
    Route::get('show', 'OrderStatusController@show')->name('order-status-show');
    Route::get('edit/{orderStatus}', 'OrderStatusController@edit')->name('order-status-edit');
    Route::patch('update/{orderStatus}', 'OrderStatusController@update')->name('order-status-update');
    Route::get('destroy/{orderStatus}', 'OrderStatusController@destroy')->name('order-status-destroy');
});

//___8
Route::prefix('order-category')->group(function (){
    $prefixName = 'order-category';
    $controllerName = Str::studly($prefixName);
    Route::get('list', $controllerName.'Controller@index')->name($prefixName.'-list');
    Route::get('create', $controllerName.'Controller@create')->name($prefixName.'-create');
    Route::post('store', $controllerName.'Controller@store')->name($prefixName.'-store');
    Route::get('show', $controllerName.'Controller@show')->name($prefixName.'-show');
    Route::get('edit/{'.Str::camel($prefixName).'}', $controllerName.'Controller@edit')->name($prefixName.'-edit');
    Route::patch('update/{'.Str::camel($prefixName).'}', $controllerName.'Controller@update')->name($prefixName.'-update');
    Route::get('destroy/{'.Str::camel($prefixName).'}', $controllerName.'Controller@destroy')->name($prefixName.'-destroy');
});

//___9
Route::prefix('order-sub-category')->group(function (){
    $prefixName = 'order-sub-category';
    $controllerName = Str::studly($prefixName);
    Route::get('list', $controllerName.'Controller@index')->name($prefixName.'-list');
    Route::get('create', $controllerName.'Controller@create')->name($prefixName.'-create');
    Route::post('store', $controllerName.'Controller@store')->name($prefixName.'-store');
    Route::get('show', $controllerName.'Controller@show')->name($prefixName.'-show');
    Route::get('edit/{'.Str::camel($prefixName).'}', $controllerName.'Controller@edit')->name($prefixName.'-edit');
    Route::patch('update/{'.Str::camel($prefixName).'}', $controllerName.'Controller@update')->name($prefixName.'-update');
    Route::get('destroy/{'.Str::camel($prefixName).'}', $controllerName.'Controller@destroy')->name($prefixName.'-destroy');
});

//___10
Route::prefix('order')->group(function (){
    $prefixName = 'order';
    $controllerName = Str::studly($prefixName);
    Route::get('list', $controllerName.'Controller@index')->name($prefixName.'-list');
    Route::get('create', $controllerName.'Controller@create')->name($prefixName.'-create');
    Route::post('store', $controllerName.'Controller@store')->name($prefixName.'-store');
    Route::get('show/{'.Str::camel($prefixName).'}', $controllerName.'Controller@show')->name($prefixName.'-show');
    Route::get('edit/{'.Str::camel($prefixName).'}', $controllerName.'Controller@edit')->name($prefixName.'-edit');
    Route::patch('update/{'.Str::camel($prefixName).'}', $controllerName.'Controller@update')->name($prefixName.'-update');
    Route::get('destroy/{'.Str::camel($prefixName).'}', $controllerName.'Controller@destroy')->name($prefixName.'-destroy');
// jewel
    Route::get('print/{'.Str::camel($prefixName).'}', $controllerName.'Controller@print')->name($prefixName.'-print');
});

//___11
Route::prefix('shipment')->group(function (){
    $prefixName = 'shipment';
    $controllerName = Str::studly($prefixName);
    Route::get('list', $controllerName.'Controller@index')->name($prefixName.'-list');
    Route::get('create', $controllerName.'Controller@create')->name($prefixName.'-create');
    Route::post('store', $controllerName.'Controller@store')->name($prefixName.'-store');
    Route::get('show/{'.Str::camel($prefixName).'}', $controllerName.'Controller@show')->name($prefixName.'-show');
    Route::get('edit/{'.Str::camel($prefixName).'}', $controllerName.'Controller@edit')->name($prefixName.'-edit');
    Route::patch('update/{'.Str::camel($prefixName).'}', $controllerName.'Controller@update')->name($prefixName.'-update');
    Route::get('destroy/{'.Str::camel($prefixName).'}', $controllerName.'Controller@destroy')->name($prefixName.'-destroy');
});

//___12
Route::prefix('production')->group(function (){
    $prefixName = 'production';
    $controllerName = Str::studly($prefixName);
    Route::get('list', $controllerName.'Controller@index')->name($prefixName.'-list');
    Route::get('create', $controllerName.'Controller@create')->name($prefixName.'-create');
    Route::post('store/{order}', $controllerName.'Controller@store')->name($prefixName.'-store');
    Route::get('show/{'.Str::camel($prefixName).'}', $controllerName.'Controller@show')->name($prefixName.'-show');
    Route::get('edit/{'.Str::camel($prefixName).'}', $controllerName.'Controller@edit')->name($prefixName.'-edit');
    Route::patch('update/{'.Str::camel($prefixName).'}', $controllerName.'Controller@update')->name($prefixName.'-update');
    Route::get('destroy/{'.Str::camel($prefixName).'}', $controllerName.'Controller@destroy')->name($prefixName.'-destroy');

    Route::get('date-wise-list', $controllerName.'Controller@dateWiseList')->name($prefixName.'-date-wise-list');

});

//___13
Route::prefix('sub-contractor')->group(function (){
    $prefixName = 'sub-contractor';
    $controllerName = Str::studly($prefixName);
    Route::get('list', $controllerName.'Controller@index')->name($prefixName.'-list');
    Route::get('create', $controllerName.'Controller@create')->name($prefixName.'-create');
    Route::post('store', $controllerName.'Controller@store')->name($prefixName.'-store');
    Route::get('show/{'.Str::camel($prefixName).'}', $controllerName.'Controller@show')->name($prefixName.'-show');
    Route::get('edit/{'.Str::camel($prefixName).'}', $controllerName.'Controller@edit')->name($prefixName.'-edit');
    Route::patch('update/{'.Str::camel($prefixName).'}', $controllerName.'Controller@update')->name($prefixName.'-update');
    Route::get('destroy/{'.Str::camel($prefixName).'}', $controllerName.'Controller@destroy')->name($prefixName.'-destroy');
});

//___14
Route::prefix('production-line')->group(function (){
    $prefixName = 'production-line';
    $controllerName = Str::studly($prefixName);
    Route::get('list', $controllerName.'Controller@index')->name($prefixName.'-list');
    Route::get('create', $controllerName.'Controller@create')->name($prefixName.'-create');
    Route::post('store', $controllerName.'Controller@store')->name($prefixName.'-store');
    Route::get('show/{'.Str::camel($prefixName).'}', $controllerName.'Controller@show')->name($prefixName.'-show');
    Route::get('edit/{'.Str::camel($prefixName).'}', $controllerName.'Controller@edit')->name($prefixName.'-edit');
    Route::patch('update/{'.Str::camel($prefixName).'}', $controllerName.'Controller@update')->name($prefixName.'-update');
    Route::get('destroy/{'.Str::camel($prefixName).'}', $controllerName.'Controller@destroy')->name($prefixName.'-destroy');
});

//___15
Route::prefix('budget')->group(function (){
    $prefixName = 'budget';
    $controllerName = Str::studly($prefixName);
    Route::get('list', $controllerName.'Controller@index')->name($prefixName.'-list');
    Route::get('create', $controllerName.'Controller@create')->name($prefixName.'-create');
    Route::post('store', $controllerName.'Controller@store')->name($prefixName.'-store');
    Route::get('show/{'.Str::camel($prefixName).'}', $controllerName.'Controller@show')->name($prefixName.'-show');
    Route::get('edit/{'.Str::camel($prefixName).'}', $controllerName.'Controller@edit')->name($prefixName.'-edit');
    Route::patch('update/{'.Str::camel($prefixName).'}', $controllerName.'Controller@update')->name($prefixName.'-update');
    Route::get('destroy/{'.Str::camel($prefixName).'}', $controllerName.'Controller@destroy')->name($prefixName.'-destroy');
});

//___16
Route::prefix('color-lib')->group(function (){
    $prefixName = 'color-lib';
    $controllerName = Str::studly($prefixName);
    Route::get('list', $controllerName.'Controller@index')->name($prefixName.'-list');
    Route::get('create', $controllerName.'Controller@create')->name($prefixName.'-create');
    Route::post('store', $controllerName.'Controller@store')->name($prefixName.'-store');
    Route::get('show/{'.Str::camel($prefixName).'}', $controllerName.'Controller@show')->name($prefixName.'-show');
    Route::get('edit/{'.Str::camel($prefixName).'}', $controllerName.'Controller@edit')->name($prefixName.'-edit');
    Route::patch('update/{'.Str::camel($prefixName).'}', $controllerName.'Controller@update')->name($prefixName.'-update');
    Route::get('destroy/{'.Str::camel($prefixName).'}', $controllerName.'Controller@destroy')->name($prefixName.'-destroy');
});

//___17
Route::prefix('size-lib')->group(function (){
    $prefixName = 'size-lib';
    $controllerName = Str::studly($prefixName);
    Route::get('list', $controllerName.'Controller@index')->name($prefixName.'-list');
    Route::get('create', $controllerName.'Controller@create')->name($prefixName.'-create');
    Route::post('store', $controllerName.'Controller@store')->name($prefixName.'-store');
    Route::get('show/{'.Str::camel($prefixName).'}', $controllerName.'Controller@show')->name($prefixName.'-show');
    Route::get('edit/{'.Str::camel($prefixName).'}', $controllerName.'Controller@edit')->name($prefixName.'-edit');
    Route::patch('update/{'.Str::camel($prefixName).'}', $controllerName.'Controller@update')->name($prefixName.'-update');
    Route::get('destroy/{'.Str::camel($prefixName).'}', $controllerName.'Controller@destroy')->name($prefixName.'-destroy');
});

//___18
Route::prefix('order-size-n-color')->group(function (){
    $prefixName = 'order-size-n-color';
    $controllerName = Str::studly($prefixName);
    Route::get('list', $controllerName.'Controller@index')->name($prefixName.'-list');
    Route::get('create', $controllerName.'Controller@create')->name($prefixName.'-create');
    Route::post('store', $controllerName.'Controller@store')->name($prefixName.'-store');
    Route::get('show/{'.Str::camel($prefixName).'}', $controllerName.'Controller@show')->name($prefixName.'-show');
    Route::get('edit/{'.Str::camel($prefixName).'}', $controllerName.'Controller@edit')->name($prefixName.'-edit');
    Route::patch('update/{'.Str::camel($prefixName).'}', $controllerName.'Controller@update')->name($prefixName.'-update');
    Route::get('destroy/{'.Str::camel($prefixName).'}', $controllerName.'Controller@destroy')->name($prefixName.'-destroy');
});

//___19
Route::prefix('kd-program')->group(function (){
    $prefixName = 'kd-program';
    $controllerName = Str::studly($prefixName);
    Route::get('list', $controllerName.'Controller@index')->name($prefixName.'-list');
    Route::get('create', $controllerName.'Controller@create')->name($prefixName.'-create');
    Route::post('store', $controllerName.'Controller@store')->name($prefixName.'-store');
    //Route::get('show/{'.Str::camel($prefixName).'}', $controllerName.'Controller@show')->name($prefixName.'-show');
    Route::get('show/{order}', $controllerName.'Controller@show')->name($prefixName.'-show'); //get order model
    Route::get('edit/{'.Str::camel($prefixName).'}', $controllerName.'Controller@edit')->name($prefixName.'-edit');
    Route::patch('update/{'.Str::camel($prefixName).'}', $controllerName.'Controller@update')->name($prefixName.'-update');
    Route::get('destroy/{'.Str::camel($prefixName).'}', $controllerName.'Controller@destroy')->name($prefixName.'-destroy');

    Route::get('date-wise-list', $controllerName.'Controller@dateWiseList')->name($prefixName.'-date-wise-list');
});

//___20
Route::prefix('order-pending')->group(function (){
    $prefixName = 'order-pending';
    $controllerName = Str::studly($prefixName);
    Route::get('list', $controllerName.'Controller@index')->name($prefixName.'-list');
    Route::get('create', $controllerName.'Controller@create')->name($prefixName.'-create');
    Route::post('store', $controllerName.'Controller@store')->name($prefixName.'-store');
    Route::get('show/{'.Str::camel($prefixName).'}', $controllerName.'Controller@show')->name($prefixName.'-show');
    Route::get('edit/{'.Str::camel($prefixName).'}', $controllerName.'Controller@edit')->name($prefixName.'-edit');
    Route::patch('update/{'.Str::camel($prefixName).'}', $controllerName.'Controller@update')->name($prefixName.'-update');
    Route::get('destroy/{'.Str::camel($prefixName).'}', $controllerName.'Controller@destroy')->name($prefixName.'-destroy');
});


//___21
Route::prefix('kd-parts-lib')->group(function (){
    $prefixName = 'kd-parts-lib';
    $controllerName = Str::studly($prefixName);
    Route::get('list', $controllerName.'Controller@index')->name($prefixName.'-list');
    Route::get('create', $controllerName.'Controller@create')->name($prefixName.'-create');
    Route::post('store', $controllerName.'Controller@store')->name($prefixName.'-store');
    Route::get('show/{'.Str::camel($prefixName).'}', $controllerName.'Controller@show')->name($prefixName.'-show');
    Route::get('edit/{'.Str::camel($prefixName).'}', $controllerName.'Controller@edit')->name($prefixName.'-edit');
    Route::patch('update/{'.Str::camel($prefixName).'}', $controllerName.'Controller@update')->name($prefixName.'-update');
    Route::get('destroy/{'.Str::camel($prefixName).'}', $controllerName.'Controller@destroy')->name($prefixName.'-destroy');
});


//___22
Route::prefix('kd-issue')->group(function (){
    $prefixName = 'kd-issue';
    $controllerName = Str::studly($prefixName);
    Route::get('list', $controllerName.'Controller@index')->name($prefixName.'-list');
    Route::get('create', $controllerName.'Controller@create')->name($prefixName.'-create');
    Route::post('store', $controllerName.'Controller@store')->name($prefixName.'-store');
    Route::get('show/{'.Str::camel($prefixName).'}', $controllerName.'Controller@show')->name($prefixName.'-show');
    Route::get('edit/{'.Str::camel($prefixName).'}', $controllerName.'Controller@edit')->name($prefixName.'-edit');
    Route::patch('update/{'.Str::camel($prefixName).'}', $controllerName.'Controller@update')->name($prefixName.'-update');
    Route::get('destroy/{'.Str::camel($prefixName).'}', $controllerName.'Controller@destroy')->name($prefixName.'-destroy');
});


//___23
Route::prefix('supplier')->group(function (){
    $prefixName = 'supplier';
    $controllerName = Str::studly($prefixName);
    Route::get('list', $controllerName.'Controller@index')->name($prefixName.'-list');
    Route::get('create', $controllerName.'Controller@create')->name($prefixName.'-create');
    Route::post('store', $controllerName.'Controller@store')->name($prefixName.'-store');
    Route::get('show/{'.Str::camel($prefixName).'}', $controllerName.'Controller@show')->name($prefixName.'-show');
    Route::get('edit/{'.Str::camel($prefixName).'}', $controllerName.'Controller@edit')->name($prefixName.'-edit');
    Route::patch('update/{'.Str::camel($prefixName).'}', $controllerName.'Controller@update')->name($prefixName.'-update');
    Route::get('destroy/{'.Str::camel($prefixName).'}', $controllerName.'Controller@destroy')->name($prefixName.'-destroy');
});
























