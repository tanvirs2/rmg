<?php

use Illuminate\Http\Request;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('team')->group(function (){
    //Route::get('get-datas/{department_id}', 'TeamController@getTeamsByDepartmentId')->name('api-team-data');
});

Route::prefix('department')->group(function (){
    Route::get('/', 'DepartmentController@departmentList')->name('api-departmentList');
    Route::get('/{department_id}', 'DepartmentController@getSingleDepartment')->name('api-getSingleDepartment');
    Route::get('/{department_id?}/teams', 'DepartmentController@getTeamsFromDepartment')->name('api-getTeamsFromDepartment');
});

/*
Route::prefix('user')->group(function (){
    Route::get('/filtered/{name?}/{data?}', 'UserController@getFilterUsers')->name('api-get-filter-users');
});*/

Route::prefix('utility')->group(function (){
    $prefixName = 'utility';
    $controllerName = Str::studly($prefixName);

    Route::get('searchData/{model?}/{name?}/{data?}',  $controllerName.'Controller@searchData')->name('api-get-search-data');
    Route::get('selectDropdown/{model?}/{belong?}',  $controllerName.'Controller@selectDropdownData')->name('api-get-select-dropdown-data');
});
