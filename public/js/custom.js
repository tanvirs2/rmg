function deleteThisData(link){
    swal({
        title: "Are you sure?",
        text: "",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    }).then((willDelete) => {
        if (willDelete) {
            swal("Poof! This data has been deleted!", {
                icon: "success",
            });
            window.location.href = link;
        }
    });

    return false;
}

function comingSoonAlert(text) {
    swal({
        title: text+" options Coming Soon ! ",
        text: "",
        icon: "warning",
    });
}


$(function () {
    $('#analytics-overview-date-range').datepicker({
        format: 'yyyy-mm-dd'
    });
    // Initialize the datatable.
    $('.summary-table').DataTable({
        paging: false
    });

    var detailsTable = $('.details-table').DataTable({
        dom: 't',
        info: false,
        bFilter: false,
        scrollX: true,
        paging: false
    });

    $('.filter-data-table').DataTable({
        paging: false,
        info: false,
        searching: false,

    });

    // main report table
    var oTable = $('.main-data-table').DataTable({
        dom: 't',
        searching: true,
        bFilter: false,
        paging: false,
        info: false,
        scrollCollapse: true,
        "scrollX": true,
        fixedHeader: true,
        //"order": [[ 1, 'asc' ]],
        fixedColumns:   {
            leftColumns: 0,
            rightColumns: 1
        }
    });   //pay attention to capital D, which is mandatory to retrieve "api" datatables' object, as @Lionel said

    oTable.on( 'order.dt search.dt', function () { //indexing first column
        oTable.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();

    $('#dataTableSearch').keyup(function(){
        oTable.search($(this).val()).draw() ;
    });

    $('.main-data-table tbody').on( 'click', 'tr', function () {
        $(this).toggleClass('tbl-row-selected');
    } );

    if (typeof dataTableFooterSum === "function") {
        dataTableFooterSum({
            table: oTable
        });
    }


    //extra-menu open on hover
    $('.nav-item.border-right.dropdown.extra-menu').hover(function() {
        $(this).find('.dropdown-menu').stop(true, true).delay(5).fadeIn(200);
    }, function() {
        $(this).find('.dropdown-menu').stop(true, true).delay(5).fadeOut(200);
    });
});

