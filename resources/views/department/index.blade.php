@extends('layouts.master')

@section('content')
    @include('layouts.partial.notice')

    <div class="main-content-container container-fluid px-4">
        <!-- Page Header -->
        <div class="page-header row no-gutters py-4">
            <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
                <span class="text-uppercase page-subtitle">Overview</span>
                <h3 class="page-title">Department List</h3>
            </div>
            <div class="float-right col-12 col-sm-8 text-center text-sm-right">
                <span class="text-uppercase page-subtitle">Action</span>
                <h3 class="page-title">
                    <a href="{{ route('department-create') }}" class="btn btn-md btn-white">
                        <i class="material-icons">playlist_add</i> Add
                    </a>
                </h3>
            </div>
        </div>
        <!-- End Page Header -->
        <!-- Default Light Table -->
        <div class="row">
            <div class="col">
                <div class="card card-small mb-4">
                    <div class="card-header border-bottom">
                        <h6 class="m-0">Department Table</h6>
                    </div>
                    <div class="card-body p-0 pb-3 text-center">
                        <table class="table mb-0">
                            <thead class="bg-light">
                            <tr>
                                <th scope="col" class="border-0"></th>
                                <th scope="col" class="border-0">Name</th>
                                <th scope="col" class="border-0">ShortName</th>
                                <th scope="col" class="border-0">Code</th>
                                <th scope="col" class="border-0">No of Users</th>
                                <th scope="col" class="border-0">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($departments as $department)
                            <tr>
                                <td></td>
                                <td>{{ ucwords($department->name) }}</td>
                                <td>{{ $department->short_name }}</td>
                                <td>{{ $department->code }}</td>

                                <td>{{ count($department->users) }}</td>
                                <td>
                                    <div class="btn-group btn-group-sm" role="group" aria-label="Table row actions">
                                        <a href="#" class="btn btn-white">
                                            <i class="material-icons"></i>
                                        </a>
                                        <a href="#" class="btn btn-white">
                                            <i class="material-icons"></i>
                                        </a>
                                        <a href="{{ route('department-edit', $department->id) }}" class="btn btn-white">
                                            <i class="material-icons"></i>
                                        </a>
                                        <a class="btn btn-white" onclick="return deleteThisData('{{ route('department-destroy', $department->id) }}')">
                                            <i class="material-icons"></i>
                                        </a>
                                    </div>
                                </td>
                            </tr>
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Default Dark Table -->
    </div>
@endsection