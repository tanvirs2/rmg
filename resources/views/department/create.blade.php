@extends('layouts.master')

@section('content')
    @include('layouts.partial.notice')

    <div class="main-content-container container-fluid px-4">
        <!-- Page Header -->
        <div class="page-header row no-gutters py-4">
            <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
                <span class="text-uppercase page-subtitle">Overview</span>
                <h3 class="page-title">{{ isset($department)?'Edit':'Add New' }} Department</h3>
            </div>
        </div>
        <!-- End Page Header -->

        <!-- Default Light Table -->
        <div class="row">

            <div class="col-lg-8">
                <div class="card card-small mb-4">
                    <div class="card-header border-bottom">
                        <h6 class="m-0">Department Details</h6>
                    </div>
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item p-3">
                            <div class="row">
                                <div class="col">

                                    @if(isset($department))
                                        <form action="{{ route('department-update', $department->id) }}" method="post" >
                                        @method('PATCH')
                                    @else
                                        <form action="{{ route('department-store') }}" method="post">
                                    @endif

                                    @csrf

                                        <div class="form-row">
                                            <div class="form-group col-md-6">
                                                <label for="name" class="input-required">Department Name</label>
                                                <input type="text"
                                                       class="form-control"
                                                       placeholder="Department Name"
                                                       name="name"
                                                       value="{{ isset($department->name)?$department->name:null }}{{ old('name') }}"
                                                >
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label for="code" class="input-required">Short Code</label>
                                                <input type="text"
                                                       class="form-control"
                                                       placeholder="Short Code"
                                                       name="short_name"
                                                       value="{{ isset($department->short_name)?$department->short_name:null }}{{ old('short_name') }}"
                                                >
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label for="code" class="input-required">Code</label>
                                                <input type="text"
                                                       class="form-control"
                                                       placeholder="Code"
                                                       name="code"
                                                       value="{{ isset($department->code)?$department->code:null }}{{ old('code') }}"
                                                >
                                            </div>
                                        </div>

                                        <button type="submit" class="btn btn-accent">{{ isset($department)?'Update':'Create' }}</button>
                                    </form>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- End Default Light Table -->
    </div>
@endsection