@extends('layouts.master')

@section('content')

    @include('layouts.partial.notice')

    <div class="main-content-container container-fluid px-4">
        <!-- Page Header -->
        <div class="page-header row no-gutters py-4">
            <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
                <span class="text-uppercase page-subtitle">Overview</span>
                <h3 class="page-title"> {{ isset($isSetModel)? 'Edit':'Place' }} {{ $pageData['pageName'] }}</h3>
            </div>
        </div>
        <!-- End Page Header -->
        <!-- Default Light Table -->

        @include('layouts.orderFilterForm')

        <div class="row">
            <div class="col-lg-3 col-md-12 mb-4">
                @if(request()->coming != 'fromList')
                <div class="card card-small mb-2">
                    <div class="card-header border-bottom">
                        <h6 class="m-0">Search by Order ID</h6>
                    </div>
                    <form action="">
                        <ul class="list-group list-group-flush">
                        <li class="list-group-item p-3">
                            <div class="row">
                                <div class="col">
                                    <div class="form-row">
                                        <div class="form-group col-md-8">
                                            <select class="select2-box-field-filter form-control" name="id" laravel-get-from="id" laravel-model="Order">
                                                <option value="{{ $id }}">{{ $id }}</option>
                                            </select>
                                        </div>
                                        <div class="form-group col-md-4">
                                            <button class="form-control btn btn-primary">
                                                Filter
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                    </form>
                </div>
                @endif
                <!-- Sales by Category -->
                @if(isset($order))
                    <div class="card card-small user-details mb-4 text-capitalize">
                        <h4 class="text-center m-0 mt-2">{{ $order->name }}</h4>
                        <p class="text-center text-light m-0 mb-2">{{ $order->buyer->name }} ({{ $order->buyer->agent->name ?? 'None' }})</p>
                        {{--<ul class="user-details__social user-details__social--primary d-table mx-auto mb-4">
                            <li class="mx-1"><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                            <li class="mx-1"><a href="#"><i class="fab fa-twitter"></i></a></li>
                            <li class="mx-1"><a href="#"><i class="fab fa-github"></i></a></li>
                            <li class="mx-1"><a href="#"><i class="fab fa-slack"></i></a></li>
                        </ul>--}}
                        <div class="user-details__user-data border-top border-bottom p-4">
                            <div class="row mb-3">
                                <div class="col w-50">
                                    <span>Create</span>
                                    <span>{{ $order->user->name }}</span>
                                </div>
                                <div class="col w-50">
                                    <span>Sale's</span>
                                    <span>{{ $order->sales_user->name }}</span>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="col w-50">
                                    <span>Reporting</span>
                                    <span>{{ $order->reporting_user->name }}</span>
                                </div>
                                <div class="col w-50">
                                    <span>Qty</span>
                                    <span>{{ $order->quantity->sum('quantity') }}</span>
                                </div>
                            </div>

                            <div class="row mb-3">
                                <div class="col w-50">
                                    <span>Style</span>
                                    <span>{{ $order->style }}</span>
                                </div>
                                <div class="col w-50">
                                    <span>Category</span>
                                    <span>{{ $order->subCategory->orderCategory->name ?? null }}</span>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col w-50">
                                    <span>Shipment</span>
                                    <span>{{ $order->date_of_ship }}</span>
                                </div>
                                <div class="col w-50">
                                    <span>Type</span>
                                    <span>{{ $order->subCategory->name ?? null }}</span>
                                </div>

                            </div>
                        </div>
                        <div class="card-header p-0">
                            <div class="user-details__bg">
                                <img src="{{ asset('profile-pic/system/up-user-details-background.jpg') }}" alt="User Details Background Image">
                            </div>
                        </div>
                        <div class="card-body p-0">
                            <div class="user-details__avatar mx-auto ">
                                <img class="rounded" src="{{ $order->getGarmentsPicture() }}" alt="User Avatar">
                            </div>
                            {{--<div class="user-details__tags p-4">
                                <span class="badge badge-pill badge-light text-light text-uppercase mb-2 border">User Experience</span>
                                <span class="badge badge-pill badge-light text-light text-uppercase mb-2 border">UI Design</span>
                                <span class="badge badge-pill badge-light text-light text-uppercase mb-2 border">React JS</span>
                                <span class="badge badge-pill badge-light text-light text-uppercase mb-2 border">HTML &amp; CSS</span>
                                <span class="badge badge-pill badge-light text-light text-uppercase mb-2 border">JavaScript</span>
                                <span class="badge badge-pill badge-light text-light text-uppercase mb-2 border">Bootstrap 4</span>
                            </div>--}}
                        </div>
                    </div>
            @endif
            <!-- End Sales by Category -->
            </div>
            <div class="col col-lg-9 col-md-12 mb-4">
                <!-- Sales Report -->
                <div class="card card-small production-entry">
                    <div class="card-header border-bottom">
                        <h6 class="m-0">{{ $pageData['pageName'] }} Entry</h6>
                        <div class="block-handle"></div>
                    </div>
                    @if(isset($order))
                    <form action="{{ route($pageData['routeFirstName'].'-store', $order->id??'') }}" method="post">
                        @csrf

                        <input type="hidden" name="order_id" value="{{ $order->id }}">

                        <div class="card-body pt-0">


                            @foreach($order->kdProgramWithParts() as $kdParts)
                                <div class="row">
                                    <div class="col col-lg-8 col-md-12">
                                        <div class="card mb-1 inner-tbl">
                                            <div class="card-body p-0">
                                                <h5>{{ $kdParts->kdParts->name }}</h5>
                                                <table class="table table-bordered table-responsive table-hover table-striped table-sm mb-0">
                                                    <thead>
                                                    <tr>
                                                        <th scope="col" class="border-bottom-0">#</th>
                                                        <th scope="col" class="border-bottom-0">Color</th>
                                                        <th scope="col" class="border-bottom-0">Qty</th>
                                                        <th scope="col" class="border-bottom-0">YarnRcv2</th>
                                                        <th scope="col" class="border-bottom-0">YarnIss4</th>
                                                        <th scope="col" class="border-bottom-0">Knit6</th>
                                                        <th scope="col" class="border-bottom-0">YarnReturn7</th>
                                                        <th scope="col" class="border-bottom-0">D.Qty9</th>
                                                        <th scope="col" class="border-bottom-0">D.Return10</th>
                                                        <th scope="col" class="border-bottom-0">FiniRcv13</th>
                                                        <th scope="col" class="border-bottom-0">FavIss15</th>
                                                    </tr>
                                                    </thead>

                                                    <?php $i = -1;
                                                    $orderQuantity = $order->quantity->sum('quantity');
                                                    ?>
                                                    <tbody>
                                                    @foreach($order->orderColorAndSizeQtyArrFunc() as $color => $orderSizeAndColor)

                                                        <?php $i++; ?>

                                                        <tr>
                                                            <td class="">
                                                                <input type="radio">
                                                            </td>
                                                            <td>
                                                                <label>{{ $kdParts->kdParts->kdProgram[$i]->color->name }} {{--{{ $color }}--}}</label>
                                                            </td>

                                                            <td>
                                                                <label>{{ array_sum($orderSizeAndColor['qty']) }}</label>
                                                            </td>
                                                            <td>
                                                                <table class="table">
                                                                    <tr>
                                                                        <td>0</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Entry</td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td>
                                                                <table class="table">
                                                                    <tr>
                                                                        <td>0</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Entry</td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td>
                                                                <table class="table">
                                                                    <tr>
                                                                        <td>0</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Entry</td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td>
                                                                <table class="table">
                                                                    <tr>
                                                                        <td>0</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Entry</td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td>
                                                                <table class="table">
                                                                    <tr>
                                                                        <td>0</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Entry</td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td>
                                                                <table class="table">
                                                                    <tr>
                                                                        <td>0</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Entry</td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td>
                                                                <table class="table">
                                                                    <tr>
                                                                        <td>0</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Entry</td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td>
                                                                <table class="table">
                                                                    <tr>
                                                                        <td>0</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Entry</td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col col-lg-4 col-md-12">
                                        hello
                                    </div>
                                </div>


                            @endforeach
                        </div>
                    </form>
                    @endif
                </div>
                <!-- End Sales Report -->
            </div>

        </div>

        {{--@if(isset($isSetModel))
        <form action="{{ route($pageData['routeFirstName'].'-update', $isSetModel->id) }}" method="post" enctype="multipart/form-data">
            @method('PATCH')
        @else
        <form action="{{ route($pageData['routeFirstName'].'-store') }}" method="post" enctype="multipart/form-data">
        @endif
            @csrf

        </form>--}}
                    <!-- End Default Light Table -->
    </div>
@endsection

@section('vue-script')
    <script>
        var error = new Howl({
            src: ["{{ asset('sound/to-the-point.ogg') }}"]
        });

        $(function () {
            @if(isset($order))
                //..
            @endif
        });

    </script>

    <script>
        const data = {
            removeBtn: false,
            kdParts: [{parts: null}],
            kdDetailHeadings: [{ title: "", description: "" }],

            productionType: '{{ old('production_type') }}',
            prQuantityFld: 0,
            operation:'{{ old('operation') }}',

            testInput: {
                cutting:[],
                swing_in:[],
                swing_out:[],
                iron:[],
                packing:[],
            },

            @if($order)

            @foreach($order->quantity as $sNcWiseQty)
            test{{ $sNcWiseQty->id }}: false,
            @endforeach

            test{{old('order_quantity_id')}}: true,

            @endif
        };

        Vue.directive('select', {
            twoWay: true,
            bind: function (el, binding, vnode) {
                $(el).select2().on("select2:select", (e) => {
                    // v-model looks for
                    //  - an event named "change"
                    //  - a value with property path "$event.target.value"
                    el.dispatchEvent(new Event('change', { target: e.target }));
                });
            },
        });

        const app = new Vue({
            el: '#app',
            data,
            methods: {
                addHeadInputRow(elm){
                    this.kdDetailHeadings.push({ title: "", description: "" })
                },
                removeHeadInputRow(row){
                    let index = this.kdDetailHeadings.indexOf(row);
                    this.kdDetailHeadings.splice(index, 1);
                    //this.kdDetailHeadings.splice(this.kdDetailHeadings.indexOf(item), 1);
                },
                addRow(elm){
                    this.removeBtn = true;
                    this.kdParts.push({parts: this.kdParts.length})
                },
                removeRow(item, elm){
                    let rowCount = this.kdParts.length;
                    if (rowCount < 3) {
                        this.removeBtn = false;
                    }
                    if (rowCount > 1) {
                        this.kdParts.splice(this.kdParts.indexOf(item), 1);
                    }
                },


                @if($order)
                    @foreach($order->quantity as $sNcWiseQty)
                    testInpFun{{$sNcWiseQty->id}}(data = 0, fld) {
                        return data + (this.testInput[fld][{{$sNcWiseQty->id}}] || 0);
                    },
                @endforeach

                selectQtyRowFalse() {
                    @foreach($order->quantity as $sNcWiseQty)
                        this.test{{ $sNcWiseQty->id }} = false;
                    @endforeach
                },

                @endif

                prQuantitySumOnInp(field, fieldName, compare){
                    let userInpQtySum = Number(field) + Number(this.prQuantity[fieldName] || 0);
                    let dangerBtn = '';
                    if (compare < userInpQtySum) {
                        dangerBtn = 'btn-danger';
                        if (fieldName != 'cutting') {
                            error.play();
                        }
                    }

                    return { qty: userInpQtySum, status: dangerBtn };
                }
            },
            updated(){
                select2Loader();
            },
            mounted(){
                //..
            },
            computed: {
                addRowBtn() {
                    let localVar;
                    for(let item of this.kdParts){

                        if (item.parts) {
                            localVar = true;
                        } else {
                            localVar = false;
                            break;
                        }

                        let dblCount = 0;

                        for(let item2 of this.kdParts){

                            if (item.parts == item2.parts) {
                                ++dblCount;
                                if (dblCount == 1) {
                                    localVar = true;
                                } else {
                                    localVar = false;
                                    break;
                                }
                            }
                        }
                    }

                    return localVar;
                },
            }

        });
    </script>
@endsection

