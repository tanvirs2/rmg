@extends('layouts.master')

@section('content')

    @include('layouts.partial.notice')

    <div class="main-content-container container-fluid px-4">
        <!-- Page Header -->
        <div class="page-header row no-gutters py-4">
            <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
                <span class="text-uppercase page-subtitle">Overview</span>
                <h3 class="page-title">{{ $pageData['pageName'] }} List</h3>
            </div>
            <div class="float-right col-12 col-sm-8 text-center text-sm-right">
                <span class="text-uppercase page-subtitle">Action</span>
                <h3 class="page-title">
                    <a href="{{ route('order-status-create') }}" class="btn btn-md btn-white">
                        <i class="material-icons">playlist_add</i> Add
                    </a>
                </h3>
            </div>
        </div>
        <!-- End Page Header -->
        <!-- Default Light Table -->
        <div class="row">
            <div class="col">
                <div class="card card-small mb-4">
                    <div class="card-header border-bottom">
                        <h6 class="m-0">{{ $pageData['pageName'] }} Table</h6>
                    </div>
                    <div class="card-body p-0 pb-3 text-center">
                        <table class="table mb-0">
                            <thead class="bg-light">
                            <tr>
                                <th scope="col" class="border-0"></th>
                                <th scope="col" class="border-0">Name</th>
                                <th scope="col" class="border-0">Code</th>
                                <th scope="col" class="border-0">Forwarding {{ $pageData['pageName'] }}</th>
                                <th scope="col" class="border-0">Publicly {{ $pageData['pageName'] }}</th>
                                <th scope="col" class="border-0">Is Mail Send</th>
                                <th scope="col" class="border-0">Is SMS Send</th>
                                <th scope="col" class="border-0">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($orderStatuses as $status)
                            <tr>
                                <td></td>
                                <td>{{ $status->name }}</td>
                                <td>{{ $status->code }}</td>
                                <td>
                                    @foreach($status->nextStatuses as $nextStatus)
                                        <button type="button" class="mb-2 btn btn-sm btn-dark mr-1">{{ $nextStatus->name }}</button>
                                    @endforeach
                                </td>

                                <td>{{ isset($status->public_status)? ucwords($status->public_status->name):ucwords($status->name) }}</td>
                                <td>{{ $status->is_mail_send }}</td>
                                <td>{{ $status->is_sms_send }}</td>
                                <td>
                                    <div class="btn-group btn-group-sm" role="group" aria-label="Table row actions">
                                        <a href="#" class="btn btn-white">
                                            <i class="material-icons"></i>
                                        </a>
                                        <a href="#" class="btn btn-white">
                                            <i class="material-icons"></i>
                                        </a>
                                        <a href="{{ route('order-status-edit', $status->id) }}" class="btn btn-white">
                                            <i class="material-icons"></i>
                                        </a>
                                        <a class="btn btn-white" onclick="return deleteThisData('{{ route('order-status-destroy', $status->id) }}')">
                                            <i class="material-icons"></i>
                                        </a>
                                    </div>
                                </td>
                            </tr>
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Default Dark Table -->
    </div>
@endsection