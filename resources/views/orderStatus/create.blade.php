@extends('layouts.master')

@section('content')
    @include('layouts.partial.notice')

    <div class="main-content-container container-fluid px-4">
        <!-- Page Header -->
        <div class="page-header row no-gutters py-4">
            <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
                <span class="text-uppercase page-subtitle">Overview</span>
                <h3 class="page-title">{{ isset($department)?'Edit':'Add New' }} {{ $pageData['pageName'] }}</h3>
            </div>
        </div>
        <!-- End Page Header -->

        <!-- Default Light Table -->
        <div class="row">

            <div class="col-lg-8">
                <div class="card card-small mb-4">
                    <div class="card-header border-bottom">
                        <h6 class="m-0">Order {{ $pageData['pageName'] }} Details</h6>
                    </div>
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item p-3">
                            <div class="row">
                                <div class="col">

                                    @if(isset($getUser))
                                        <form action="{{ route('order-status-update', $getUser->id) }}" method="post">
                                        @method('PATCH')
                                    @else
                                        <form action="{{ route('order-status-store') }}" method="post">
                                    @endif

                                        @csrf

                                        <div class="form-row">
                                            <div class="form-group col-md-6">
                                                <label for="name" class="input-required">{{ $pageData['pageName'] }} Name</label>
                                                <input type="text"
                                                       autocomplete="off"
                                                       class="form-control"
                                                       id="name"
                                                       placeholder="{{ $pageData['pageName'] }} Name"
                                                       value="Sierra"
                                                       name="name"
                                                >
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label for="code" class="input-required">Code</label>
                                                <input type="text"
                                                       class="form-control"
                                                       id="code"
                                                       placeholder="Last Name"
                                                       value="1"
                                                       name="code"
                                                >
                                            </div>
                                        </div>

                                        <div class="form-row">
                                            <div class="form-group col-md-6">
                                                <label for="feInputState" class="input-required">{{ $pageData['pageName'] }}</label>
                                                <div class='card card-small mb-3'>
                                                    <div class="card-header border-bottom">
                                                        <h6 class="m-0">Forwarding {{ $pageData['pageName'] }}</h6>
                                                    </div>
                                                    <div class='card-body p-0'>
                                                        <ul class="list-group list-group-flush">
                                                            <li class="list-group-item px-3 pb-2">
                                                                @foreach($orderStatuses as $status)
                                                                    <div class="custom-control custom-checkbox mb-1">
                                                                        <input type="checkbox" value="{{ $status->id }}" name="operation[]" class="custom-control-input" id="opration{{ $status->id }}">
                                                                        <label class="custom-control-label" for="opration{{ $status->id }}">{{ $status->name }}</label>
                                                                    </div>
                                                                @endforeach
                                                            </li>

                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group col-md-6">
                                                <label class="input-required">Is Mail Send</label>
                                                <select name="dashboard" class="form-control">
                                                    <option value="no">No</option>
                                                    <option value="yes">Yes</option>
                                                </select>

                                                <label class="input-required">Is SMS Send</label>
                                                <select name="dashboard" class="form-control">
                                                    <option value="no">No</option>
                                                    <option value="yes">Yes</option>
                                                </select>

                                                <label class="input-required">Public Show {{ $pageData['pageName'] }}</label>
                                                <select name="dashboard" class="form-control">
                                                    <option value="0" selected>As it is</option>
                                                    @foreach($orderStatuses as $status)
                                                        <option value="{{ $status->id }}">{{ ucfirst($status->name) }}</option>
                                                    @endforeach
                                                </select>
                                            </div>

                                        </div>

                                        <button type="submit" class="btn btn-accent">Create</button>
                                    </form>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- End Default Light Table -->
    </div>
@endsection