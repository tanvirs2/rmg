<!doctype html>
<html class="no-js h-100" lang="en">
    {{--header--}}
    @include('layouts.partial.header')
    {{--header--}}

    <body class="h-100">

        <span id="app">

            @include('layouts.partial.top_nav')

        </span>

    </body>

    {{--footer 2--}}
    @include('layouts.partial.footer2')
    {{--footer 2--}}


    {{--script--}}
    @yield('jQuery-code')
    @yield('script')
    @yield('vue-script')
    {{--script--}}

</html>