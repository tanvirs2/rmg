<div class="row" id="search-area">
    <div class="col-lg-12 mb-1">
        <div class="card card-small">
            <ul class="list-group list-group-flush">
                <li class="list-group-item p-2">
                    <div class="row">
                        <div class="col-sm-12 col-md-12">

                            {{--search form--}}
                            <form action="#search-area">
                                <div class="row mb-2">
                                    <div class="col-md-1">
                                        <select class="select2-box-manual form-control yr-select">
                                            <option value="2017">2017</option>
                                            <option value="2018">2018</option>
                                            <option value="2019" selected>2019</option>
                                            <option value="2020">2020</option>
                                            <option value="2021">2021</option>
                                        </select>
                                    </div>

                                    <div class="col-md-1 p-0">
                                        <select class="select2-box-manual form-control" name="date_type">
                                            <option value="date_of_ship" {{ (Request::get('date_type') == 'date_of_ship') ? 'selected' : null }}>ShipDate</option>
                                            <option value="order_accept_date" {{ (Request::get('date_type') == 'order_accept_date') ? 'selected' : null }}>EntryDate</option>
                                        </select>
                                    </div>

                                    <div class="col-md-7 p-0">
                                        <button onclick="dateAppend('-01-01', '-01-31')" type="button" class="ml-1 btn btn-success btn-sm">Jan</button>
                                        <button onclick="dateAppend('-02-01', '-02-28')" type="button" class="btn btn-success btn-sm">Fab</button>
                                        <button onclick="dateAppend('-03-01', '-03-31')" type="button" class="btn btn-success btn-sm">Mar</button>
                                        <button onclick="dateAppend('-04-01', '-04-30')" type="button" class="btn btn-success btn-sm">Apr</button>
                                        <button onclick="dateAppend('-05-01', '-05-31')" type="button" class="btn btn-success btn-sm">May</button>
                                        <button onclick="dateAppend('-06-01', '-06-30')" type="button" class="btn btn-success btn-sm">Jun</button>
                                        <button onclick="dateAppend('-07-01', '-07-31')" type="button" class="btn btn-success btn-sm">Jul</button>
                                        <button onclick="dateAppend('-08-01', '-08-31')" type="button" class="btn btn-success btn-sm">Aug</button>
                                        <button onclick="dateAppend('-09-01', '-09-30')" type="button" class="btn btn-success btn-sm">Sep</button>
                                        <button onclick="dateAppend('-10-01', '-10-31')" type="button" class="btn btn-success btn-sm">Oct</button>
                                        <button onclick="dateAppend('-11-01', '-11-30')" type="button" class="btn btn-success btn-sm">Nov</button>
                                        <button onclick="dateAppend('-12-01', '-12-31')" type="button" class="btn btn-success btn-sm">Dec</button>
                                    </div>

                                    <div class="col-md-3">
                                        <div style="max-width: 240px" id="analytics-overview-date-range" class="input-daterange input-group input-group-sm ml-auto">
                                            <input value="{{ $from }}" autocomplete="off" type="text" class="input-sm form-control" name="from" placeholder="Start Date">
                                            <input value="{{ $to }}" autocomplete="off" type="text" class="input-sm form-control" name="to" placeholder="End Date">
                                            {{--<span class="input-group-append">
                                                <span class="input-group-text">
                                                  <i class="material-icons"></i>
                                                </span>
                                            </span>--}}
                                        </div>
                                    </div>

                                </div>
                                <div class="form-row">

                                    <div class="form-row col-md-2">
                                        <div class="form-group col-md-4">
                                            <select class="select2-box-field-filter form-control" name="id" laravel-get-from="id" laravel-model="Order">
                                                <option value="{{ $id }}">{{ $id }}</option>
                                            </select>
                                            <div class="filter-name">ID</div>
                                        </div>
                                        <div class="form-group col-md-8">
                                            <select class="select2-box form-control" name="buyer_id" laravel-model="Buyer" laravel-get-from="name" laravel-belongs="agent">
                                                <option value="{{ $buyer_id }}">{{ $buyer_id ? ucwords(App\Buyer::find($buyer_id)->name):null }}</option>
                                            </select>
                                            <div class="filter-name">Buyer Name</div>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-1">
                                        <select class="select2-box-field-filter form-control" name="name" laravel-model="Order" laravel-get-from="name" laravel-direct-value="true">
                                            <option value="{{ $name }}">{{ $name }}</option>
                                        </select>
                                        <div class="filter-name">Order Name</div>
                                    </div>
                                    <div class="form-group col-md-1">
                                        <select class="select2-box-field-filter form-control" name="style" laravel-model="Order" laravel-get-from="style" laravel-direct-value="true">
                                            <option value="{{ $style }}">{{ ucwords($style) }}</option>
                                        </select>
                                        <div class="filter-name">Style Name</div>
                                    </div>

                                    <div class="form-group col-md-1">
                                        <select class="select2-box-field-filter form-control" name="style_desc" laravel-model="Order" laravel-get-from="style_desc" laravel-direct-value="true">
                                            <option value="{{ $style_desc }}">{{ ucwords($style_desc) }}</option>
                                        </select>
                                        <div class="filter-name">Style Desc</div>
                                    </div>
                                    <div class="form-group col-md-1">
                                        <select class="select2-box-manual form-control" name="statuses">
                                            <option value="{{ $statuses }}">{{ $statuses ?? 'Select...' }}</option>
                                            <option value="Running">Running</option>
                                            <option value="Partial">Partial</option>
                                            <option value="ShipOut">ShipOut</option>
                                        </select>
                                        <div class="filter-name">Order Status</div>
                                    </div>
                                    <div class="form-group col-md-1">
                                        <select class="select2-box form-control" name="category_id" laravel-model="OrderCategory" laravel-get-from="name">
                                            <option value="{{ $category_id }}">{{ $category_id ? ucwords(App\OrderCategory::find($category_id)->name) : null }}</option>
                                        </select>
                                        <div class="filter-name">Category</div>
                                    </div>
                                    <div class="form-group col-md-1">
                                        <select class="select2-box form-control" name="sub_category_id" laravel-model="OrderSubCategory" laravel-get-from="name">
                                            <option value="{{ $sub_category_id }}">{{ $category_id ? ucwords(App\OrderSubCategory::find($sub_category_id)->name) : null }}</option>
                                        </select>
                                        <div class="filter-name">Sub Category</div>
                                    </div>
                                    <div class="form-group col-md-2">
                                        <button type="submit" class="mb-2 btn btn-primary">Search</button>

                                        <a href="{{ route($pageData['routeFirstName'].'-list') }}#search-area" class="mb-2 btn btn-dark">Reset</a>
                                    </div>
                                    <div class="form-group col-md-1 p-0">
                                        <button @click="doit" type="button" class="float-right btn btn-info">
                                            <i class="material-icons">save_alt</i> Export
                                        </button>
                                    </div>
                                    <div class="form-group col-md-1">
                                        <select name='per_page' class="form-control is-invalid" @change="per_page">
                                            @foreach($per_page_rows as $number)
                                                <option {{ ($per_page==$number) ? "selected" : null }}>{{ $number }}</option>
                                            @endforeach
                                        </select>
                                        <div class="invalid-feedback">Rows</div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>