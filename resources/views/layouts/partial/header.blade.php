
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Spinning Technology</title>
    <meta name="description" content="A high-quality &amp; free Bootstrap admin dashboard template pack that comes with lots of templates and components.">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('lib/fontawesome-free-5.4.1-web/css/all.min.css') }}" rel="stylesheet">
    <link href="{{ asset('dashboard/styles/was_cdn/icon.css') }}" rel="stylesheet">
    <link rel="stylesheet" id="main-stylesheet" data-version="1.1.0" href="{{ asset('dashboard/styles/shards-dashboards.1.2.0.min.css') }}">
    <link rel="stylesheet" href="{{ asset('dashboard/styles/extras.1.1.0.min.css') }}">
    {{--<link rel="stylesheet" href="{{ asset('lib/jquery-ui-1.12.1/jquery-ui.min.css') }}">--}}
    <link rel="stylesheet" href="{{ asset('dashboard/styles/custom.css') }}">
    <script>
        window.Laravel = { csrfToken: '{{ csrf_token() }}'};
        const appBaseUrl = '{{ URL::to('/') }}';

        window.apiGetSelectDropdownData = '{{ route('api-get-select-dropdown-data') }}';
        window.apiGetSearchData = '{{ route('api-get-search-data') }}';
    </script>
</head>