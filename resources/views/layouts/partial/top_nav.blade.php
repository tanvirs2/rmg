
<div class="color-switcher animated">
  <h5>Accent Color</h5>
  <ul class="accent-colors">
    <li class="accent-primary active" data-color="primary">
      <i class="material-icons">check</i>
    </li>
    <li class="accent-secondary" data-color="secondary">
      <i class="material-icons">check</i>
    </li>
    <li class="accent-success" data-color="success">
      <i class="material-icons">check</i>
    </li>
    <li class="accent-info" data-color="info">
      <i class="material-icons">check</i>
    </li>
    <li class="accent-warning" data-color="warning">
      <i class="material-icons">check</i>
    </li>
    <li class="accent-danger" data-color="danger">
      <i class="material-icons">check</i>
    </li>
  </ul>
  <div class="actions mb-4">
    <a class="mb-2 btn btn-sm btn-primary w-100 d-table mx-auto extra-action" href="https://designrevision.com/downloads/shards-dashboard-lite/">
      <i class="material-icons">cloud</i> Download</a>
    <a class="mb-2 btn btn-sm btn-white w-100 d-table mx-auto extra-action" href="https://designrevision.com/docs/shards-dashboard-lite">
      <i class="material-icons">book</i> Documentation</a>
  </div>
  <div class="social-wrapper">
    <div class="social-actions">
      <h5 class="my-2">Help us Grow</h5>
      <div class="inner-wrapper">
        <a class="github-button" href="https://github.com/DesignRevision/shards-dashboard" data-icon="octicon-star" data-show-count="true" aria-label="Star DesignRevision/shards-dashboard on GitHub">Star</a>
        <!-- <iframe style="width: 91px; height: 21px;"src="https://yvoschaap.com/producthunt/counter.html#href=https%3A%2F%2Fwww.producthunt.com%2Fr%2Fp%2F112998&layout=wide" width="56" height="65" scrolling="no" frameborder="0" allowtransparency="true"></iframe> -->
      </div>
    </div>
    <div id="social-share" data-url="https://designrevision.com/downloads/shards-dashboard-lite/" data-text="ðŸ”¥ Check out Shards Dashboard Lite, a free and beautiful Bootstrap 4 admin dashboard template!" data-title="share"></div>
    <div class="loading-overlay">
      <div class="spinner"></div>
    </div>
  </div>
  <div class="close">
    <i class="material-icons">close</i>
  </div>
</div>
{{--Comming soon--}}
{{--<div class="color-switcher-toggle animated pulse infinite">
  <i class="material-icons">settings</i>
</div>--}}
<div class="container-fluid ">
  <div class="row">
    <!-- Main Sidebar -->
    <aside class="main-sidebar col-12 col-md-3 col-lg-2 px-0">
      <div class="main-navbar">
        <nav class="navbar align-items-stretch navbar-light bg-white flex-md-nowrap border-bottom p-0">
          <a class="navbar-brand w-100 mr-0" href="#" style="line-height: 25px;">
            <div class="d-table m-auto">
              <img id="main-logo" class="d-inline-block align-top mr-1" style="max-width: 25px;" src="{{ asset('dashboard/') }}/images/logo.png" alt="Shards Dashboard">
              <span class="d-none d-md-inline ml-1">Spinning Technology</span>
            </div>
          </a>
          <a class="toggle-sidebar d-sm-inline d-md-none d-lg-none">
            <i class="material-icons">&#xE5C4;</i>
          </a>
        </nav>
      </div>
      <form action="#" class="main-sidebar__search w-100 border-right d-sm-flex d-md-none d-lg-none">
        <div class="input-group input-group-seamless ml-3">
          <div class="input-group-prepend">
            <div class="input-group-text">
              <i class="fas fa-search"></i>
            </div>
          </div>
          <input class="navbar-search form-control" type="text" placeholder="Search for something..." aria-label="Search"> </div>
      </form>
      <div class="nav-wrapper">

        <ul class="nav flex-column side-bar-height">
          <li class="nav-item">
            <a class="nav-link {{ Route::is('homePage')?'active':null }}" href="{{ route('homePage') }}">
              <i class="material-icons">av_timer</i>
              <span>Dashboard</span>
            </a>
          </li>
            {{--border_color--}}
            <h6 class="main-sidebar__nav-title">Order Management</h6>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                    <i class="material-icons">border_color</i>
                    <span>Entry</span>
                </a>
                <div class="dropdown-menu dropdown-menu-small">
                    <a class="dropdown-item {{ Route::is('order-create')?'active':null }}" href="{{ route('order-create') }}">
                        <i class="material-icons">add_shopping_cart</i>
                        <span>Order Place</span>
                    </a>
                    <a class="dropdown-item {{ Route::is('production-create')?'active':null }}" href="{{ route('production-create') }}">
                        <i class="material-icons">map</i>
                        <span>Production Place</span>
                    </a>
                    <a class="dropdown-item {{ Route::is('kd-program-create')?'active':null }}" href="{{ route('kd-program-create') }}">
                        <i class="material-icons">art_track</i>
                        <span>KD Place</span>
                    </a>
                    <a class="dropdown-item {{ Route::is('kd-issue-create')?'active':null }}" href="{{ route('kd-issue-create') }}">
                        <i class="material-icons">art_track</i>
                        <span>KD Issue Place</span>
                    </a>
                </div>
            </li>

            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                    <i class="material-icons">shopping_basket</i>
                    <span>Order</span>
                </a>
                <div class="dropdown-menu dropdown-menu-small">

                    <a class="dropdown-item {{ Route::is('order-list')?'active':null }}" href="{{ route('order-list') }}">
                        <i class="material-icons">shopping_cart</i>
                        <span>Order List</span>
                    </a>

                    <a class="dropdown-item {{ Route::is('budget-list')?'active':null }}" href="{{ route('budget-list') }}">
                        <i class="material-icons">attach_money</i>
                        <span>Budget</span>
                    </a>
                    <a class="dropdown-item {{ Route::is('order-pending-list')?'active':null }}" href="{{ route('order-pending-list') }}">
                        <i class="material-icons">remove_shopping_cart</i>
                        <span>Pending</span>
                    </a>
                    {{--<a class="dropdown-item {{ Route::is('order-createx')?'active':null }}" href="#">
                        <i class="material-icons">access_alarms</i>
                        <span>TNA</span>
                    </a>--}}

                </div>
            </li>

            <h6 class="main-sidebar__nav-title">Manufacturing</h6>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                    <i class="material-icons">confirmation_number</i>
                    <span>Production</span>
                </a>
                <div class="dropdown-menu dropdown-menu-small">
                    <a class="dropdown-item {{ Route::is('production-list')?'active':null }}" href="{{ route('production-list') }}">
                        <i class="material-icons">format_list_numbered</i>
                        <span>Order Wise</span>
                    </a>
                    <a class="dropdown-item {{ Route::is('production-date-wise-list')?'active':null }}" href="{{ route('production-date-wise-list') }}">
                        <i class="material-icons">ballot</i>
                        <span>Date Wise</span>
                    </a>
                </div>
            </li>

            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle " data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                    <i class="material-icons">gradient</i>
                    <span>Knitting & Dyeing</span>
                </a>
                <div class="dropdown-menu dropdown-menu-small">
                    <a class="dropdown-item {{ Route::is('kd-program-list')?'active':null }}" href="{{ route('kd-program-list') }}">
                        <i class="material-icons">format_list_numbered</i>
                        <span>Order Wise</span>
                    </a>
                    <a class="dropdown-item {{ Route::is('kd-program-date-wise-list')?'active':null }}" href="{{ route('kd-program-date-wise-list') }}">
                        <i class="material-icons">ballot</i>
                        <span>Date Wise</span>
                    </a>
                </div>
            </li>

          <h6 class="main-sidebar__nav-title">Management</h6>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
              <i class="material-icons">people</i>
              <span>Users</span>
            </a>
            <div class="dropdown-menu dropdown-menu-small">
              <a class="dropdown-item {{ Route::is('user-list')?'active':null }}" href="{{ route('user-list') }}">
                <i class="material-icons">person</i>
                <span>User List</span>
              </a>
              <a class="dropdown-item {{ Route::is('role-list')?'active':null }}" href="{{ route('role-list') }}">
                <i class="material-icons">how_to_reg</i>
                <span>User Roles</span>
              </a>
            </div>
          </li>

            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle " data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                    <i class="material-icons">vertical_split</i>
                    <span>Functions</span>
                </a>
                <div class="dropdown-menu dropdown-menu-small">
                    <a class="dropdown-item {{ Route::is('department-list')?'active':null }}" href="{{ route('department-list') }}">
                        <i class="material-icons">view_module</i>
                        <span>Departments</span>
                    </a>
                    <a class="dropdown-item {{ Route::is('team-list')?'active':null }}" href="{{ route('team-list') }}">
                        <i class="material-icons">line_style</i>
                        <span>Sections</span>
                    </a>
                </div>
            </li>
            <h6 class="main-sidebar__nav-title">Library</h6>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle " data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                    <i class="material-icons">view_module</i>
                    <span>Orders</span>
                </a>
                <div class="dropdown-menu dropdown-menu-small">
                    <a class="dropdown-item {{ Route::is('order-status-list')?'active':null }}" href="{{ route('order-status-list') }}">
                        <i class="material-icons">dvr</i>
                        <span>TNA List</span>
                    </a>
                    <a class="dropdown-item {{ Route::is('order-category-list')?'active':null }}" href="{{ route('order-category-list') }}">
                        <i class="material-icons">category</i>
                        <span>Categories</span>
                    </a>
                    <a class="dropdown-item {{ Route::is('order-sub-category-list')?'active':null }}" href="{{ route('order-sub-category-list') }}">
                        <i class="material-icons">art_track</i>
                        <span>Sub Categories</span>
                    </a>
                </div>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle " data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                    <i class="material-icons"></i>
                    <span>Buyers</span>
                </a>
                <div class="dropdown-menu dropdown-menu-small">
                    <a class="dropdown-item {{ Route::is('agent-list')?'active':null }}" href="{{ route('agent-list') }}#agent-list" id="agent-list">
                        <i class="material-icons">card_travel</i>
                        <span>Agents</span>
                    </a>
                    <a class="dropdown-item {{ Route::is('buyer-list')?'active':null }}" href="{{ route('buyer-list') }}#buyer-list" id="buyer-list">
                        <i class="material-icons">assignment_ind</i>
                        <span>Buyer List</span>
                    </a>
                </div>
            </li>

            <li class="nav-item">
                <a class="nav-link {{ Route::is('sub-contractor-list')?'active':null }}" href="{{ route('sub-contractor-list') }}#sub-contractor-list" id="sub-contractor-list">
                    <i class="material-icons">business_center</i>
                    <span>Sub Contractor</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{ Route::is('supplier-list')?'active':null }}" href="{{ route('supplier-list') }}#supplier-list" id="supplier-list">
                    <i class="material-icons">chrome_reader_mode</i>
                    <span>Supplier</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{ Route::is('production-line-list')?'active':null }}" href="{{ route('production-line-list') }}#production-line-list" id='production-line-list'>
                    <i class="material-icons">airline_seat_recline_extra</i>
                    <span>Production-Line</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{ Route::is('color-lib-list')?'active':null }}" href="{{ route('color-lib-list') }}#color-lib-list" id='color-lib-list'>
                    <i class="material-icons">brush</i>
                    <span>Color</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{ Route::is('size-lib-list')?'active':null }}" href="{{ route('size-lib-list') }}#size-lib-list" id='size-lib-list'>
                    <i class="material-icons">photo_size_select_large</i>
                    <span>Size</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{ Route::is('kd-parts-lib-list')?'active':null }}" href="{{ route('kd-parts-lib-list') }}#kd-parts-lib-list" id='kd-parts-lib-list'>
                    <i class="material-icons">table_chart</i>
                    <span>KD Parts</span>
                </a>
            </li>
          {{--

          <li class="nav-item">
            <a class="nav-link " href="tables.html">
              <i class="material-icons">supervisor_account</i>
              <span>Tables</span>
            </a>
          </li>

          <li class="nav-item">
            <a class="nav-link " href="errors.html">
              <i class="material-icons">error</i>
              <span>Errors</span>
            </a>
          </li>--}}
        </ul>
      </div>
    </aside>
    <!-- End Main Sidebar -->
    <main class="main-content col-lg-10 col-md-9 col-sm-12 p-0 offset-lg-2 offset-md-3">
      <div class="main-navbar sticky-top bg-white">
        <!-- Main Navbar -->
        <nav class="navbar align-items-stretch navbar-light flex-md-nowrap p-0">

            <ul class="navbar-nav border-left flex-row ">
                <li class="nav-item border-right dropdown notifications extra-menu">
                    <a class="nav-link nav-link-icon text-center orderList" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <div class="nav-link-icon__wrapper">
                            <i class="material-icons essential-menu">shopping_cart</i>
                        </div>
                    </a>
                    <div class="dropdown-menu dropdown-menu-small" aria-labelledby="dropdownMenuLink" style="right: auto;">
                        <a class="dropdown-item" href="{{ route('budget-list') }}">
                            <div class="notification__icon-wrapper">
                                <div class="notification__icon">
                                    <i class="material-icons">attach_money</i>
                                    {{--<i class="material-icons"></i>--}}
                                </div>
                            </div>
                            <div class="notification__content">
                                <span class="notification__category">Budget</span>
                                <p>Order Budget list</p>
                            </div>
                        </a>
                        <a class="dropdown-item" href="#">
                            <div class="notification__icon-wrapper">
                                <div class="notification__icon">
                                    <i class="material-icons">confirmation_number</i>
                                    {{--<i class="material-icons"></i>--}}
                                </div>
                            </div>
                            <div class="notification__content">
                                <span class="notification__category">Production</span>

                                <p>
                                    <button onclick='window.location.href="{{ route('production-list') }}"' class="btn btn-info btn-sm">Order Wise</button>
                                    <button onclick='window.location.href="{{ route('production-date-wise-list') }}"' class="btn btn-primary btn-sm">Date Wise</button>
                                </p>
                            </div>
                        </a>
                        <a class="dropdown-item" href="#">
                            <div class="notification__icon-wrapper">
                                <div class="notification__icon">
                                    <i class="material-icons">gradient</i>
                                    {{--<i class="material-icons"></i>--}}
                                </div>
                            </div>
                            <div class="notification__content">
                                <span class="notification__category">Knitting & Dyeing</span>
                                <p>
                                    <button onclick='window.location.href="{{ route('kd-program-list') }}"' class="btn btn-info btn-sm">Order Wise</button>
                                    <button onclick='window.location.href="{{ route('kd-program-date-wise-list') }}"' class="btn btn-primary btn-sm">Date Wise</button>
                                </p>
                                {{--<p>Last week your store’s sales count decreased by <span class="text-danger text-semibold">5.52%</span>. It could have been worse!</p>--}}
                            </div>
                        </a>
                        {{--<a class="dropdown-item notification__all text-center" href="#"> View all Notifications </a>--}}
                    </div>
                </li>
            </ul>

            <form action="#" class="main-navbar__search w-100 d-none d-md-flex d-lg-flex">
                <div class="input-group input-group-seamless ml-3">
                    <div class="input-group-prepend">
                        <div class="input-group-text">
                            <i class="fas fa-search"></i>
                        </div>
                    </div>
                    <input id="dataTableSearch" class="navbar-search form-control" type="text" placeholder="Search on main data..." aria-label="Search"> </div>
            </form>

          <ul class="navbar-nav border-left flex-row ">

            {{--Comment Notification Bell--}}

            {{--<li onmouseover="comingSoonAlert('Notification')" class="nav-item border-right dropdown notifications">
              <a class="nav-link nav-link-icon text-center" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <div class="nav-link-icon__wrapper">
                  <i class="material-icons">&#xE7F4;</i>
                  <span class="badge badge-pill badge-danger">2</span>
                </div>
              </a>
              <div class="dropdown-menu dropdown-menu-small" aria-labelledby="dropdownMenuLink">
                <a class="dropdown-item" href="#">
                  <div class="notification__icon-wrapper">
                    <div class="notification__icon">
                      <i class="material-icons">&#xE6E1;</i>
                    </div>
                  </div>
                  <div class="notification__content">
                    <span class="notification__category">Analytics</span>
                    <p>Your websiteâ€™s active users count increased by
                      <span class="text-success text-semibold">28%</span> in the last week. Great job!</p>
                  </div>
                </a>
                <a class="dropdown-item" href="#">
                  <div class="notification__icon-wrapper">
                    <div class="notification__icon">
                      <i class="material-icons">&#xE8D1;</i>
                    </div>
                  </div>
                  <div class="notification__content">
                    <span class="notification__category">Sales</span>
                    <p>Last week your storeâ€™s sales count decreased by
                      <span class="text-danger text-semibold">5.52%</span>. It could have been worse!</p>
                  </div>
                </a>
                <a class="dropdown-item notification__all text-center" href="#"> View all Notifications </a>
              </div>
            </li>--}}
            @if(Auth::user())
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle text-nowrap px-3" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                <img style="max-height: 50px" class="user-avatar rounded-circle mr-2" src="{{ Auth::user()->GetProfilePicture() }}" alt="User Avatar">
                <span class="d-none d-md-inline-block">{{ Auth::user()->name }}</span>
              </a>
              <div class="dropdown-menu dropdown-menu-small">
                <a class="dropdown-item" href="#">
                  <i class="material-icons">&#xE7FD;</i> Profile</a>
                <a class="dropdown-item" href="#">
                  <i class="material-icons">vertical_split</i> Test</a>
                <a class="dropdown-item" href="#">
                  <i class="material-icons">note_add</i> Test2</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item text-danger" href="{{ route('logout') }}"
                   onclick="event.preventDefault();
                   document.getElementById('logout-form').submit();
                    ">
                  <i class="material-icons text-danger">&#xE879;</i> Logout </a>

                  <form id="logout-form" action="{{ route('logout') }}" method="post" style="display: none">
                      @csrf
                  </form>
              </div>
            </li>
            @endif
          </ul>
          <nav class="nav">
            <a href="#" class="nav-link nav-link-icon toggle-sidebar d-md-inline d-lg-none text-center border-left" data-toggle="collapse" data-target=".header-navbar" aria-expanded="false" aria-controls="header-navbar">
              <i class="material-icons">&#xE5D2;</i>
            </a>
          </nav>
        </nav>
      </div>
      <!-- / .main-navbar -->
      {{-- Content --}}
        @yield('content', 'default')
      {{-- Content --}}


      {{-- footer --}}
        @include('layouts.partial.footer')
      {{-- footer --}}

    </main>
  </div>
</div>

