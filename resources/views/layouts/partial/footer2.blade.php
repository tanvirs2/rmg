
<script src="{{ asset('js/app.js') }}"></script>
{{--<script src="{{ asset('dashboard/scripts/was_cdn/jquery-3.3.1.min.js') }}"></script>
<script src="{{ asset('dashboard/scripts/was_cdn/popper.min.js') }}"></script>
<script src="{{ asset('dashboard/scripts/was_cdn/bootstrap.min.js') }}"></script>--}}
<script src="{{ asset('dashboard/scripts/was_cdn/shards.min.js') }}"></script>
<script src="{{ asset('dashboard/scripts/shards-dashboards-customized.1.1.0.min.js') }}"></script>


{{--<script src="{{ asset('lib/sweetalert/dist/sweetalert.min.js') }}"></script>--}}
{{--<script src="{{ asset('lib/jquery-ui-1.12.1/jquery-ui.min.js') }}"></script>--}}
{{--<script src="{{ asset('lib/DataTables/DataTables-1.10.18/js/jquery.dataTables.min.js') }}"></script>--}}
<script src="{{ asset('js/custom.js') }}"></script>



<script>

    $(function () {

        //extra-menu click to Orders View
        $('.nav-item.border-right.dropdown .orderList').click(function() {
            window.location.href = '{{ route('order-list') }}';
        });

        //CSS Menu Active involk function
        $(".dropdown-item.active").closest('li').addClass('active');
        //$(".dropdown-item.active").closest('li').css('background', 'red');
        $(".dropdown-item.active").closest('div').addClass('show');

    });

    function dateAppend(from, to) {
        let yr = $('.yr-select').val();
        $('[placeholder="Start Date"]').val(yr + from);
        $('[placeholder="End Date"]').val(yr + to);
        $('.yr-select').closest('form').submit();
    }
</script>

