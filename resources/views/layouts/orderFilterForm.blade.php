<div class="row" id="search-area">
    <div class="col-lg-12 mb-1">
        <div class="card card-small">
            <ul class="list-group list-group-flush">
                <li class="list-group-item p-2">
                    <div class="row">
                        <div class="col-sm-12 col-md-12">

                            {{--search form--}}
                            <form action="#search-area">
                                <div class="form-row">
                                    <div class="form-row col-md-1">
                                        <select class="select2-box-manual form-control yr-select">
                                            <option value="2017">2017</option>
                                            <option value="2018">2018</option>
                                            <option value="2019" selected>2019</option>
                                            <option value="2020">2020</option>
                                            <option value="2021">2021</option>
                                        </select>
                                    </div>
                                    <div class="form-row col-md-8">
                                        <div class="form-group col-md-1">
                                            <button onclick="dateAppend('-01-01', '-01-31')" type="button" class="mb-2 btn btn-success">Jan</button>
                                        </div>
                                        <div class="form-group col-md-1">
                                            <button onclick="dateAppend('-02-01', '-02-28')" type="button" class="mb-2 btn btn-success">Fab</button>
                                        </div>
                                        <div class="form-group col-md-1">
                                            <button onclick="dateAppend('-03-01', '-03-31')" type="button" class="mb-2 btn btn-success">Mar</button>
                                        </div>
                                        <div class="form-group col-md-1">
                                            <button onclick="dateAppend('-04-01', '-04-30')" type="button" class="mb-2 btn btn-success">Apr</button>
                                        </div>
                                        <div class="form-group col-md-1">
                                            <button onclick="dateAppend('-05-01', '-05-31')" type="button" class="mb-2 btn btn-success">May</button>
                                        </div>
                                        <div class="form-group col-md-1">
                                            <button onclick="dateAppend('-06-01', '-06-30')" type="button" class="mb-2 btn btn-success">Jun</button>
                                        </div>
                                        <div class="form-group col-md-1">
                                            <button onclick="dateAppend('-07-01', '-07-31')" type="button" class="mb-2 btn btn-success">Jul</button>
                                        </div>
                                        <div class="form-group col-md-1">
                                            <button onclick="dateAppend('-08-01', '-08-31')" type="button" class="mb-2 btn btn-success">Aug</button>
                                        </div>
                                        <div class="form-group col-md-1">
                                            <button onclick="dateAppend('-09-01', '-09-30')" type="button" class="mb-2 btn btn-success">Sep</button>
                                        </div>
                                        <div class="form-group col-md-1">
                                            <button onclick="dateAppend('-10-01', '-10-31')" type="button" class="mb-2 btn btn-success">Oct</button>
                                        </div>
                                        <div class="form-group col-md-1">
                                            <button onclick="dateAppend('-11-01', '-11-30')" type="button" class="mb-2 btn btn-success">Nov</button>
                                        </div>
                                        <div class="form-group col-md-1">
                                            <button onclick="dateAppend('-12-01', '-12-31')" type="button" class="mb-2 btn btn-success">Dec</button>
                                        </div>
                                    </div>

                                    <div class="form-row col-md-3">
                                        <div style="max-width: 240px" id="analytics-overview-date-range" class="input-daterange input-group input-group-sm ml-auto">
                                            <input value="{{ $from }}" autocomplete="off" type="text" class="input-sm form-control" name="from" placeholder="Start Date">
                                            <input value="{{ $to }}" autocomplete="off" type="text" class="input-sm form-control" name="to" placeholder="End Date">
                                            {{--<span class="input-group-append">
                                                <span class="input-group-text">
                                                  <i class="material-icons"></i>
                                                </span>
                                            </span>--}}
                                        </div>
                                    </div>

                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-1">
                                        <select class="select2-box form-control" name="buyer_id" laravel-model="Buyer" laravel-get-from="name" laravel-belongs="agent">
                                            <option value="{{ $buyer_id }}">{{ $buyer_id ? ucwords(App\Buyer::find($buyer_id)->name):null }}</option>
                                        </select>
                                        <div class="filter-name">Buyer Name</div>
                                    </div>
                                    <div class="form-group col-md-1">
                                        <select class="select2-box-field-filter form-control" name="name" laravel-model="Order" laravel-get-from="name" laravel-direct-value="true">
                                            <option value="{{ $name }}">{{ $name }}</option>
                                        </select>
                                        <div class="filter-name">Order Name</div>
                                    </div>
                                    <div class="form-group col-md-1">
                                        <select class="select2-box-field-filter form-control" name="style" laravel-model="Order" laravel-get-from="style" laravel-direct-value="true">
                                            <option value="{{ $style }}">{{ ucwords($style) }}</option>
                                        </select>
                                        <div class="filter-name">Style Name</div>
                                    </div>

                                    <div class="form-group col-md-1">
                                        <select class="select2-box-field-filter form-control" name="style_desc" laravel-model="Order" laravel-get-from="style_desc" laravel-direct-value="true">
                                            <option value="{{ $style_desc }}">{{ ucwords($style_desc) }}</option>
                                        </select>
                                        <div class="filter-name">Style Desc</div>
                                    </div>
                                    <div class="form-group col-md-1">
                                        <select class="select2-box-manual form-control" name="statuses">
                                            <option value="{{ $statuses }}">{{ $statuses ?? 'Select...' }}</option>
                                            <option value="Running">Running</option>
                                            <option value="Partial">Partial</option>
                                            <option value="ShipOut">ShipOut</option>
                                        </select>
                                        <div class="filter-name">Order Status</div>
                                    </div>
                                    <div class="form-group col-md-1">
                                        <select class="select2-box form-control" name="category_id" laravel-model="OrderCategory" laravel-get-from="name">
                                            <option value="{{ $category_id }}">{{ $category_id ? ucwords(App\OrderCategory::find($category_id)->name) : null }}</option>
                                        </select>
                                        <div class="filter-name">Category</div>
                                    </div>
                                    <div class="form-group col-md-1">
                                        <select class="select2-box form-control" name="sub_category_id" laravel-model="OrderSubCategory" laravel-get-from="name">
                                            <option value="{{ $sub_category_id }}">{{ $category_id ? ucwords(App\OrderSubCategory::find($sub_category_id)->name) : null }}</option>
                                        </select>
                                        <div class="filter-name">Sub Category</div>
                                    </div>
                                    <div class="form-group col-md-2">
                                        <button type="submit" name="order_filter_form" value="submit" class="mb-2 btn btn-primary">Search</button>

                                        <a href="{{ route($pageData['routeFirstName'].'-list') }}#search-area" class="mb-2 btn btn-dark">Reset</a>
                                    </div>

                                </div>
                            </form>




                            <!-- Transaction History Table -->
                            <table class="filter-data-table d-none text-capitalize" id="main-table">
                                <thead>
                                <tr>
                                    <th></th>
                                    <th>ID</th>
                                    <th>Buyer</th>
                                    <th>OrderName</th>
                                    <th>Style</th>
                                    <th>StyleDesc</th>
                                    <th>Image</th>
                                    <th>Shipment</th>
                                    <th style="background: #ffd3b0;color: #000;">RemDay</th>
                                    <th>OrderQty</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($orders as $order)
                                    @php
                                        $pieChartMaterials[$order->buyer->name]['quantity'][] = $order->quantity->sum('quantity');
                                        $pieChartMaterials[$order->buyer->name]['value'][] = $order->quantity->sum('quantity')*$order->unit_price;

                                        $dateOfShip = $order->date_of_ship;
                                        $dateForHumans = Carbon::parse($dateOfShip)->diffForHumans(Carbon::now());
                                        $diff = -Carbon::parse($dateOfShip)->diffInDays(Carbon::now(), false)
                                    @endphp
                                    <tr>
                                        {{--Select--}}              <td> @if($id) <a class="btn btn-primary bg-primary text-dark" href="#">Selected</a> @else <a class="btn btn-primary bg-white text-dark" href="create?id={{ $order->id }}">Select</a> @endif </td>
                                        {{--Job ID--}}              <td>{{ $order->id }}</td>
                                        {{--Byuer--}}               <td>{{ $order->buyer->name }}</td>
                                        {{--order name--}}          <td>{{ $order->name }}</td>
                                        {{--style name--}}          <td>{{ $order->style }}</td>
                                        {{--style desc--}}          <td>{{ $order->style_desc }}</td>
                                        {{--getGarmentsPicture--}}  <td><img class="zoom" width="50" src="{{ $order->getGarmentsPicture() }}" alt=""></td>
                                        {{--Shipment Date--}}       <td>{{ $dateOfShip }}</td>
                                        {{--Rem Day--}}             <td> <div class="badge {{ ($diff<=10)?'badge-danger':'badge-secondary' }}">{{ $diff }}</div><br> {{ $dateForHumans }}</td>
                                        {{--Order Qty--}}           <td>{{ $order->quantity->sum('quantity') }}</td>

                                    </tr>
                                @endforeach
                                </tbody>

                            </table>
                            <!-- End Transaction History Table -->



                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>