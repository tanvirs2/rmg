@extends('layouts.master')

@section('content')

    @include('layouts.partial.notice')

    <div class="main-content-container container-fluid px-4">
        <!-- Page Header -->
        <div class="page-header row no-gutters py-4">
            <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
                <span class="text-uppercase page-subtitle">Overview</span>
                <h3 class="page-title">{{ $pageData['pageName'] }} List</h3>
            </div>
            <div class="float-right col-12 col-sm-8 text-center text-sm-right">
                <span class="text-uppercase page-subtitle">Action</span>
                <h3 class="page-title">
                    <a href="{{ route($pageData['routeFirstName'].'-create') }}" class="btn btn-md btn-white">
                        <i class="material-icons">person</i> Add
                    </a>
                </h3>
            </div>
        </div>
        <!-- End Page Header -->
        <div class="row">
            <div class="col-lg-12 mb-1">
                <div class="card card-small">
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item p-2">
                            <div class="row">
                                <div class="col-sm-12 col-md-12">
                                    <form>
                                        <div class="form-row">
                                            <div class="form-group col-md-4">
                                                <select class="select2-box-field-filter form-control" name="name" laravel-model="{{ $pageData['pageName'] }}" laravel-get-from="number" laravel-direct-value="true">
                                                    <option value="{{ $name }}">{{ $name }}</option>
                                                </select>
                                                <div class="filter-name">{{ $pageData['pageName'] }} Number</div>
                                            </div>
                                            <div class="form-group col-md-4">
                                                <select name='status' class="select2-box-manual form-control is-invalid">
                                                    <option value="active" {{ ($status=='active')?'selected':null }}>Active</option>
                                                    <option value="inactive" {{ ($status==='inactive')?'selected':null }}>Inactive</option>
                                                    <option value="pending" {{ ($status=='pending')?'selected':null }}>Pending</option>
                                                </select>
                                                <div class="invalid-feedback">Status</div>
                                            </div>

                                            <div class="form-group col-md-2">
                                                <button type="submit" class="mb-2 btn btn-primary">Search</button>

                                                <a href="{{ route($pageData['routeFirstName'].'-list') }}" class="mb-2 btn btn-dark">Reset</a>
                                            </div>
                                            <div class="form-group col-md-1 p-0">
                                                <button @click="doit" type="button" class="float-right btn btn-info">
                                                    <i class="material-icons">save_alt</i> Export
                                                </button>
                                            </div>
                                            <div class="form-group col-md-1">
                                                <select name='per_page' class="form-control is-invalid" @change="per_page">
                                                    @foreach($per_page_rows as $number)
                                                        <option {{ ($per_page==$number) ? "selected" : null }}>{{ $number }}</option>
                                                    @endforeach
                                                </select>
                                                <div class="invalid-feedback">Rows</div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12 mb-3">
                <div class="card card-small">
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item p-2">
                            <div class="row">
                                <div class="col-md-5">
                                    <b>Total : </b> <span>{{ $count }} {{ $pageData['pageName'] }} Found</span>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <!-- Default Light Table -->
        <div class="row">
            <div class="col">
                <div class="card card-small mb-4">
                    <div class="card-header border-bottom">
                        {{--From Controller--}}
                        <h6 class="m-0">Active {{ Str::plural($pageData['pageName']) }}</h6>
                    </div>
                    <div class="card-body p-0 pb-3 text-center">
                        <table class="table mb-0" id="main-table" >
                            <thead class="bg-light">
                            <tr>
                                <th scope="col" class="border-0">Sl</th>
                                <th scope="col" class="border-0">Number</th>
                                <th scope="col" class="border-0">Status</th>
                                <th scope="col" class="border-0">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($mainDatas as $mainData)
                            <tr>
                                <td>{{ $pageData['no']++ }}</td>
                                <td>{{ ucwords($mainData->number) }}</td>
                                <td>{{ $mainData->status }}</td>
                                <td>
                                    <div class="btn-group btn-group-sm" role="group" aria-label="Table row actions">
                                        <a href="#" class="btn btn-white">
                                            <i class="material-icons"></i>
                                        </a>
                                        <a href="#" class="btn btn-white">
                                            <i class="material-icons"></i>
                                        </a>
                                        <a href="{{ route($pageData['routeFirstName'].'-edit', $mainData->id) }}" class="btn btn-white">
                                            <i class="material-icons"></i>
                                        </a>
                                        <a class="btn btn-white" onclick="return deleteThisData('{{ route($pageData['routeFirstName'].'-destroy', $mainData->id) }}')">
                                            <i class="material-icons"></i>
                                        </a>
                                    </div>
                                </td>
                            </tr>
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
                {{ $mainDatas->links() }}
            </div>
        </div>
        <!-- End Default Dark Table -->
    </div>
@endsection

@section('vue-script')
    <script>

        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth()+1; //January is 0!
        var yyyy = today.getFullYear();

        if(dd<10) {
            dd = '0'+dd
        }

        if(mm<10) {
            mm = '0'+mm
        }

        today =  yyyy + '-' + mm + '-' + dd;

        const vm = new Vue({
            el: '#app',
            methods: {
                per_page(event) {
                    event.target.form.submit();
                },

                doit(){
                    var DefaultTable = document.getElementById('main-table');

                    var instance = new TableExport(DefaultTable, {
                        filename: '{{ $pageData['pageName'] }}-'+ today,
                        exportButtons: false,
                        ignoreCols: {{ $pageData['ignoreColsInExport'] }},
                    });

                    var exportData = instance.getExportData()['main-table']['xlsx'];
                    //console.log(exportData);
                    instance.export2file(exportData.data, exportData.mimeType, exportData.filename, exportData.fileExtension);
                }
            }
        })
    </script>
@endsection