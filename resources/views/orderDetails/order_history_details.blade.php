<div class="row text-capitalize">
    <div class="col-md-12">
        <div class="card card-small mb-4">
            <div class="card-header border-bottom">
                <h6 class="m-0">Order</h6>
            </div>
            <div class="card-body p-0">
                <div class="container-fluid px-0">
                    <table class="details-table">
                        <thead>
                        <tr>
                            <th>SL</th>
                            <th>Buyer</th>
                            <th>OrderName</th>
                            <th>Style</th>
                            <th>StyleDesc</th>
                            <th>Image</th>
                            <th>Shipment</th>
                            <th>OrderQty</th>
                            <th>UnitPrice</th>
                            {{--<th>Curr TNA</th>--}}
                            <th>Status</th>
                            {{--<th>CM</th>--}}
                            <th>SMV</th>
                        </tr>
                        </thead>
                        <tbody>
                        @php
                            $sl = 0;
                        @endphp
                        @foreach($order->orderHistories as $orderHistory)
                            <tr>
                                {{--SL--}}                  <td>{{ ++$sl }}</td>
                                {{--Byuer--}}               <td>{{ $orderHistory->buyer->name }}</td>
                                {{--order name--}}          <td>{{ $orderHistory->name }}</td>
                                {{--style name--}}          <td>{{ $orderHistory->style }}</td>
                                {{--style desc--}}          <td>{{ $orderHistory->style_desc }}</td>
                                {{--getGarmentsPicture--}}  <td><img class="zoom" width="50" src="{{ $orderHistory->getGarmentsPicture() }}" alt=""></td>
                                {{--Shipment Date--}}       <td>{{ $orderHistory->date_of_ship }}</td>
                                {{--Order Qty--}}           <td>{{ $orderHistory->quantity }}</td>
                                {{--Unit Price--}}          <td>{{ $orderHistory->unit_price }}</td>
                                {{--Order TNA Status--}}{{--        <td>{{ $order->status->name }}</td>--}}
                                {{--Order Status--}}        <td style="@if($orderHistory->statuses=='ShipOut') background: #794d53; color:#fff; @elseif($orderHistory->statuses=='Partial') background: #797750; color:#fff; @endif">{{ $orderHistory->statuses }}</td>
                                {{--CM from budget table--}}   {{--<td>{{ $order->budget->cm ?? null }}</td>--}}
                                {{--SMV--}}                 <td>{{ $orderHistory->smv }}</td>
                                {{--Sales--}}               {{--<td>{{ ucfirst($order->sales_user->name) }}</td>--}}

                            </tr>
                        @endforeach
                        </tbody>
                        <tfoot>
                        <tr>
                            <th>.</th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            {{--<th></th>
                            <th></th>--}}
                            <th></th>
                            <th></th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
        <div class="card card-small mb-4">
            <div class="card-header border-bottom">
                <h6 class="m-0">Shipment</h6>
            </div>
            <div class="card-body p-0">
                <div class="container-fluid px-0">
                    <table class="details-table ">
                        <thead class="py-2 bg-light text-semibold border-bottom">
                        <tr>
                            <th>SL</th>
                            <th>Quantity</th>
                            <th>Date</th>
                            <th class="text-center">Status</th>
                            <th>Processed By</th>
                            <th>action date</th>
                            <th class="text-right">History</th>
                        </tr>
                        </thead>
                        <tbody>
                        @php
                            $sl = 0;
                        @endphp
                        @foreach($order->shipmentHistory as $shipment)
                            <tr>
                                <td>
                                    {{ ++$sl }}
                                </td>
                                <td>
                                    {{ $shipment->quantity }}
                                </td>
                                <td>
                                    {{ $shipment->date }}
                                </td>
                                <td class="lo-stats__status">{{--Status--}}
                                    <div class="d-table mx-auto">
                                        <span class="badge badge-pill badge-success">{{ $shipment->status }}</span>
                                    </div>
                                </td>
                                <td>
                                    {{ $shipment->user->name }}
                                </td>
                                <td>
                                    {{ $shipment->updated_at }}
                                </td>
                                <td class="lo-stats__actions">
                                    <div class="btn-group btn-group-sm" role="group" aria-label="Table row actions">
                                                <span class="badge badge-info">
                                                    {{ $shipment->history_type }}
                                                </span>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                        <tfoot>
                        <tr>
                            <th>.</th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
        <div class="card card-small mb-4">
            <div class="card-header border-bottom">
                <h6 class="m-0">Budget</h6>
            </div>
            <div class="card-body p-0">
                <div class="container-fluid px-0">
                    <table class="details-table ">
                        <thead class="py-2 bg-light text-semibold border-bottom">
                        <tr>
                            <th>SL</th>
                            <th>Quantity</th>
                            <th>Date</th>
                            <th class="text-center">Status</th>
                            <th>Processed By</th>
                            <th>action date</th>
                            <th class="text-right">History</th>
                        </tr>
                        </thead>
                        <tbody>
                        @php
                            $sl = 0;
                        @endphp
                        @foreach($order->budgetHistories as $budgetHistory)
                            <tr>
                                <td>
                                    {{ ++$sl }}
                                </td>
                                <td>
                                    {{ $budgetHistory->quantity }}
                                </td>
                                <td>
                                    {{ $budgetHistory->date }}
                                </td>
                                <td>
                                    {{ $budgetHistory->date }}
                                </td>
                                <td>
                                    {{ $budgetHistory->user->name }}
                                </td>
                                <td>
                                    {{ $budgetHistory->updated_at }}
                                </td>
                                <td class="lo-stats__actions">
                                    <div class="btn-group btn-group-sm" role="group" aria-label="Table row actions">
                                                <span class="badge badge-info">
                                                    {{ $budgetHistory->history_type }}
                                                </span>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                        <tfoot>
                        <tr>
                            <th>.</th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
        <!-- End Weekly Performance -->
        <!-- User Activity -->
        <div class="card card-small user-activity mb-4">
            <div class="card-header border-bottom">
                <h6 class="m-0">User Activity</h6>
                <div class="block-handle"></div>
            </div>
            <div class="card-body p-0">
                <div class="user-activity__item pr-3 py-3">
                    <div class="user-activity__item__icon">
                        <i class="material-icons"></i>
                    </div>
                    <div class="user-activity__item__content">
                        <span class="text-light">23 Minutes ago</span>
                        <p>Assigned himself to the <a href="#">Shards Dashboards</a> project.</p>
                    </div>
                    <div class="user-activity__item__action ml-auto">
                        <button class="ml-auto btn btn-sm btn-white">View Project</button>
                    </div>
                </div>
                <div class="user-activity__item pr-3 py-3">
                    <div class="user-activity__item__icon">
                        <i class="material-icons"></i>
                    </div>
                    <div class="user-activity__item__content">
                        <span class="text-light">2 Hours ago</span>
                        <p>Marked <a href="#">7 tasks</a> as <span class="badge badge-pill badge-outline-success">Complete</span> inside the <a href="#">DesignRevision</a> project.</p>
                    </div>
                </div>
                <div class="user-activity__item pr-3 py-3">
                    <div class="user-activity__item__icon">
                        <i class="material-icons"></i>
                    </div>
                    <div class="user-activity__item__content">
                        <span class="text-light">3 Hours and 10 minutes ago</span>
                        <p>Added <a href="#">Jack Nicholson</a> and <a href="#">3 others</a> to the <a href="#">DesignRevision</a> team.</p>
                    </div>
                    <div class="user-activity__item__action ml-auto">
                        <button class="ml-auto btn btn-sm btn-white">View Team</button>
                    </div>
                </div>
                <div class="user-activity__item pr-3 py-3">
                    <div class="user-activity__item__icon">
                        <i class="material-icons"></i>
                    </div>
                    <div class="user-activity__item__content">
                        <span class="text-light">2 Days ago</span>
                        <p>Opened <a href="#">3 issues</a> in <a href="#">2 projects</a>.</p>
                    </div>
                </div>
                <div class="user-activity__item pr-3 py-3">
                    <div class="user-activity__item__icon">
                        <i class="material-icons"></i>
                    </div>
                    <div class="user-activity__item__content">
                        <span class="text-light">2 Days ago</span>
                        <p>Added <a href="#">3 new tasks</a> to the <a href="#">DesignRevision</a> project:</p>
                        <ul class="user-activity__item__task-list mt-2">
                            <li>
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="user-activity-task-1">
                                    <label class="custom-control-label" for="user-activity-task-1">Fix blog pagination issue.</label>
                                </div>
                            </li>
                            <li>
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="user-activity-task-2">
                                    <label class="custom-control-label" for="user-activity-task-2">Remove extra padding from blog post container.</label>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="user-activity__item pr-3 py-3">
                    <div class="user-activity__item__icon">
                        <i class="material-icons"></i>
                    </div>
                    <div class="user-activity__item__content">
                        <span class="text-light">2 Days ago</span>
                        <p>Marked <a href="#">3 tasks</a> as <span class="badge badge-pill badge-outline-danger">Invalid</span> inside the <a href="#">Shards Dashboards</a> project.</p>
                    </div>
                </div>
            </div>
            <div class="card-footer border-top">
                <button class="btn btn-sm btn-white d-table mx-auto">Load More</button>
            </div>
        </div>
    </div>
</div>