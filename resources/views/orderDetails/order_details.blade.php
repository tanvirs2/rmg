{{--<div class="row">
    <div class="col-lg-12 mb-4">
    </div>
</div>--}}


<!-- The Modal -->
<div class="modal fade" id="operation-modal">
    <div class="modal-dialog">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Shipment</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <form action="{{ route('shipment-store') }}" method="post">
                    @csrf
                    <div class="form-row">
                        <input type="hidden" name="order_id" value="{{ $order->id }}">
                        <div class="form-group col-md-6">
                            <label>Quantity</label>
                            <input name="quantity" type="number" placeholder="Quantity" value="" class="form-control" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label>Date</label>
                            <input name="date" type="date" placeholder="Date" class="form-control">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label>Status</label>
                            <select name="status" class="form-control" required>
                                <option disabled selected>Choose...</option>
                                <option value="Partial">Partial</option>
                                <option value="ShipOut">ShipOut</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <button class="btn btn-primary">Save</button>
                        </div>
                    </div>
                </form>
            </div>

            <!-- Modal footer -->
            {{--<div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>--}}

        </div>
    </div>
</div>
<!-- End Modal -->


    <div class="row text-capitalize">
        <div class="col-sm-12 col-lg-4">
            <!-- User Details Card -->
            <div class="card card-small user-details mb-4 text-capitalize">
                <div class="card-header p-0">
                    <div class="user-details__bg">
                        <img src="{{ asset('profile-pic/system/up-user-details-background.jpg') }}" alt="User Details Background Image">
                    </div>
                </div>
                <div class="card-body p-0">
                    <div class="user-details__avatar mx-auto ">
                        <img class="rounded" src="{{ $order->getGarmentsPicture() }}" alt="User Avatar">
                    </div>
                    <h4 class="text-center m-0 mt-2">{{ $order->name }}</h4>
                    <p class="text-center text-light m-0 mb-2">{{ $order->buyer->name }} ({{ $order->buyer->agent->name ?? 'None' }})</p>
                    {{--<ul class="user-details__social user-details__social--primary d-table mx-auto mb-4">
                        <li class="mx-1"><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                        <li class="mx-1"><a href="#"><i class="fab fa-twitter"></i></a></li>
                        <li class="mx-1"><a href="#"><i class="fab fa-github"></i></a></li>
                        <li class="mx-1"><a href="#"><i class="fab fa-slack"></i></a></li>
                    </ul>--}}
                    <div class="user-details__user-data border-top border-bottom p-4">
                        <div class="row mb-3">
                            <div class="col w-50">
                                <span>Create</span>
                                <span>{{ isset($order->user) ? $order->user->name : 'none' }}</span>
                            </div>
                            <div class="col w-50">
                                <span>Sale's</span>
                                <span>{{ isset($order->sales_user) ? $order->sales_user->name : 'none' }}</span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col w-50">
                                <span>Reporting to</span>
                                <span>{{ isset($order->reporting_user) ? $order->reporting_user->name : 'none' }}</span>
                            </div>

                        </div>
                    </div>
                    {{--<div class="user-details__tags p-4">
                        <span class="badge badge-pill badge-light text-light text-uppercase mb-2 border">User Experience</span>
                        <span class="badge badge-pill badge-light text-light text-uppercase mb-2 border">UI Design</span>
                        <span class="badge badge-pill badge-light text-light text-uppercase mb-2 border">React JS</span>
                        <span class="badge badge-pill badge-light text-light text-uppercase mb-2 border">HTML &amp; CSS</span>
                        <span class="badge badge-pill badge-light text-light text-uppercase mb-2 border">JavaScript</span>
                        <span class="badge badge-pill badge-light text-light text-uppercase mb-2 border">Bootstrap 4</span>
                    </div>--}}
                </div>
            </div>
            <!-- End User Details Card -->
            <!-- Contact User -->
            <div class="card card-small mb-4">
                <div class="card-header border-bottom">
                    <h6 class="m-0">Send Message</h6>
                    <div class="block-handle"></div>
                </div>
                <div class="card-body">
                    <form action="">
                        <textarea name="" id="" cols="30" rows="6" class="form-control mb-3" style="min-height: 150px;"></textarea>
                        <button type="submit" class="btn btn-accent btn-sm">Send Message</button>
                    </form>
                </div>
            </div>
            <!-- End Contact User -->
            <!-- User Teams -->
            {{--<div class="card card-small user-teams mb-4">
                <div class="card-header border-bottom">
                    <h6 class="m-0">Teams</h6>
                    <div class="block-handle"></div>
                </div>
                <div class="card-body p-0">
                    <div class="container-fluid">
                        <div class="row px-3">
                            <div class="user-teams__image col-2 col-sm-1 col-lg-2 p-0 my-auto">
                                <img class="rounded" src="images/user-profile/team-thumb-1.png">
                            </div>
                            <div class="col user-teams__info pl-3">
                                <h6 class="m-0">Team Edison</h6>
                                <span class="text-light">21 Members</span>
                            </div>
                        </div>
                        <div class="row px-3">
                            <div class="user-teams__image col-2 col-sm-1 col-lg-2 p-0 my-auto">
                                <img class="rounded" src="images/user-profile/team-thumb-2.png">
                            </div>
                            <div class="col user-teams__info pl-3">
                                <h6 class="m-0">Team Shelby</h6>
                                <span class="text-light">21 Members</span>
                            </div>
                        </div>
                        <div class="row px-3">
                            <div class="user-teams__image col-2 col-sm-1 col-lg-2 p-0 my-auto">
                                <img class="rounded" src="images/user-profile/team-thumb-3.png">
                            </div>
                            <div class="col user-teams__info pl-3">
                                <h6 class="m-0">Team Dante</h6>
                                <span class="text-light">21 Members</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>--}}
            <!-- End User Teams -->
        </div>

        <div class="col-lg-8">

            <div class="btn-group btn-group-lg mb-4" role="group" aria-label="Table row actions">
                <a href="#" onclick="printOrderInfo({{ $order->id }})" class="btn btn-white">
                    <i class="material-icons">print</i>
                </a>
                <button type="button" class="btn btn-white" data-toggle="modal" data-target="#operation-modal">
                    <i class="material-icons">local_shipping</i>
                </button>

                <a href="{{ route('order-edit',[$order->id, 'edit'=>'order']) }}" class="btn btn-white">
                    <i class="material-icons">&#xE254;</i>
                </a>
                <button onclick="deleteThisData('{{ route('order-destroy',$order->id) }}')" type="button" class="btn btn-white">
                    <i class="material-icons">&#xE872;</i>
                </button>
            </div>
            <!-- User Statistics -->
            <div class="card card-small user-stats mb-4">
                <div class="card-body">
                    <div class="row">
                        <div class="col-6 col-sm-3 text-center">
                            <h4 class="m-0">{{ $order->quantity->sum('quantity') }}</h4>
                            <span class="text-light text-uppercase">Order Quantity</span>
                        </div>
                        <div class="col-6 col-sm-3 text-center">
                            <h4 class="m-0">{{ $order->cutting->sum('quantity') }}</h4>
                            <span class="text-light text-uppercase">Production In</span>
                        </div>
                        <div class="col-6 col-sm-3 text-center">
                            <h4 class="m-0">{{ $order->packing->sum('quantity') }}</h4>
                            <span class="text-light text-uppercase">Ready For Shipment</span>
                        </div>
                        <div class="col-6 col-sm-3 text-center">
                            <h4 class="m-0">{{ round(($order->packing->sum('quantity')/$order->quantity->sum('quantity'))*100, 2) }}%</h4>
                            <span class="text-light text-uppercase">Completed</span>
                        </div>
                    </div>
                </div>
                <div class="card-footer py-0">
                    <div class="row">
                        <div class="col-12 col-sm-6 border-top pb-3 pt-2 border-right">
                            <div class="progress-wrapper">
                                <span class="progress-label">Workload</span>
                                <div class="progress progress-sm">
                                    <div class="progress-bar progress-bar-striped" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%;">
                                        <span class="progress-value">80%</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 border-top pb-3 pt-2">
                            <div class="progress-wrapper">
                                <span class="progress-label">Performance</span>
                                <div class="progress progress-sm">
                                    <div class="progress-bar progress-bar-striped bg-success" role="progressbar" aria-valuenow="92" aria-valuemin="0" aria-valuemax="100" style="width: 92%;">
                                        <span class="progress-value">92%</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card card-small mb-4">
                <div class="card-header border-bottom ">
                    <h6 class="m-0"><b class="badge badge-secondary " style="font-size: 1rem"><span>{{ ucwords($order->name) }} </span> - <span>ID: {{ $order->id }}</span></b> - Current status of this order</h6>
                </div>
                <div class="card-body p-0">
                    <div class="container-fluid px-0">
                        <table class="table mb-0">
                            <thead class="py-2 bg-light text-semibold border-bottom">
                            <tr>
                                <th>Details</th>
                                <th></th>
                                <th></th>
                                <th class="text-center">CurrentTNA</th>
                                <th class="text-center">ShipQty</th>
                                <th class="text-center">Short</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td class="lo-stats__image">
                                    <img class="border rounded" src="{{ $order->getGarmentsPicture() }}">
                                </td>
                                <td class="lo-stats__order-details">{{--sm Details--}}
                                    <span><b class="text-primary">Quantity: {{ $order->quantity->sum('quantity') }}</b></span>
                                    <span></span>
                                </td>
                                <td>UnitPrice:
                                    {{ $order->unit_price }}
                                </td>
                                <td class="lo-stats__status">{{--Status--}}
                                    <div class="d-table mx-auto">
                                        <span class="badge badge-pill badge-success">{{ $order->status->name ?? null }}</span>
                                    </div>
                                </td>
                                <td class="lo-stats__items text-center">
                                    {{ $shipmentQty = $order->shipment->sum('quantity') }}
                                </td>
                                <td class="text-center text-success">
                                    <span class="badge badge-danger">
                                    {{ $shipmentQty*$order->unit_price }} -
                                    {{ $totalValue = $order->quantity->sum('quantity')*$order->unit_price }}
                                    </span>
                                    <br>
                                    {{ $shipmentQty?$shipmentQty*$order->unit_price-$totalValue:null }}
                                </td>

                            </tr>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <!-- Weekly Performance -->
            <div class="card card-small mb-4">
                <div class="card-header border-bottom ">
                    <h6 class="m-0">Shipment Operations</h6>
                </div>
                <div class="card-body p-0">
                    <div class="container-fluid px-0">
                        <table class="details-table ">
                            <thead class="py-2 bg-light text-semibold border-bottom">
                            <tr>
                                <th>SL</th>
                                <th>Quantity</th>
                                <th>Date</th>
                                <th class="text-center">Status</th>
                                <th>Processed By</th>
                                <th>entry date</th>
                                <th class="text-right">Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($order->shipment as $shipment)
                                <tr>
                                    <td>
                                        {{ ++$pageData['no'] }}
                                    </td>
                                    <td>
                                        {{ $shipment->quantity }}
                                    </td>
                                    <td>
                                        {{ $shipment->date }}
                                    </td>
                                    <td class="lo-stats__status">{{--Status--}}
                                        <div class="d-table mx-auto">
                                            <span class="badge badge-pill badge-success">{{ $shipment->status }}</span>
                                        </div>
                                    </td>
                                    <td>
                                        {{ $shipment->user->name }}
                                    </td>
                                    <td>
                                        {{ $shipment->created_at }}
                                    </td>
                                    <td class="lo-stats__actions">
                                        <div class="btn-group btn-group-sm" role="group" aria-label="Table row actions">
                                            <button type="button" class="btn btn-white">
                                                <i class="material-icons">&#xE254;</i>
                                            </button>
                                            <button type="button" class="btn btn-white">
                                                <i class="material-icons">cancel_presentation</i>
                                            </button>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <!-- End User Activity -->
        </div>
    </div>