<?php

$unique_productions = App\Production::where('order_id', $order->id)->where('history_type', 'Running')->distinct()->get(['order_id', 'date']);

?>
<div class="row">
    <div class="col-lg col-md-6 col-sm-6 mb-4">
        <div class="stats-small stats-small--1 card card-small">
            <div class="card-body p-0 d-flex">
                <div class="d-flex flex-column m-auto">
                    <div class="stats-small__data text-center">
                        <span class="stats-small__label text-uppercase">Cutting</span>
                        <h6 class="stats-small__value count my-3">{{ $order->cutting->sum('quantity') }}</h6>
                    </div>
                    <div class="stats-small__data">
                        <span class="stats-small__percentage stats-small__percentage--increase">{{ round(($order->cutting->sum('quantity')/$order->quantity->sum('quantity'))*100, 2) }}%</span>
                    </div>
                </div>
                <canvas height="120" class="blog-overview-stats-small-1"></canvas>
            </div>
        </div>
    </div>
    <div class="col-lg col-md-6 col-sm-6 mb-4">
        <div class="stats-small stats-small--1 card card-small">
            <div class="card-body p-0 d-flex">
                <div class="d-flex flex-column m-auto">
                    <div class="stats-small__data text-center">
                        <span class="stats-small__label text-uppercase">Swing In</span>
                        <h6 class="stats-small__value count my-3">{{ $order->swingIn->sum('quantity') }}</h6>
                    </div>
                    <div class="stats-small__data">
                        <span class="stats-small__percentage stats-small__percentage--increase">{{ round(($order->swingIn->sum('quantity')/$order->quantity->sum('quantity'))*100, 2) }}%</span>
                    </div>
                </div>
                <canvas height="120" class="blog-overview-stats-small-2"></canvas>
            </div>
        </div>
    </div>
    <div class="col-lg col-md-4 col-sm-6 mb-4">
        <div class="stats-small stats-small--1 card card-small">
            <div class="card-body p-0 d-flex">
                <div class="d-flex flex-column m-auto">
                    <div class="stats-small__data text-center">
                        <span class="stats-small__label text-uppercase">Swing Out</span>
                        <h6 class="stats-small__value count my-3">{{ $order->swingOut->sum('quantity') }}</h6>
                    </div>
                    <div class="stats-small__data">
                        <span class="stats-small__percentage stats-small__percentage--decrease">{{ round(($order->swingOut->sum('quantity')/$order->quantity->sum('quantity'))*100, 2) }}%</span>
                    </div>
                </div>
                <canvas height="120" class="blog-overview-stats-small-3"></canvas>
            </div>
        </div>
    </div>
    <div class="col-lg col-md-4 col-sm-6 mb-4">
        <div class="stats-small stats-small--1 card card-small">
            <div class="card-body p-0 d-flex">
                <div class="d-flex flex-column m-auto">
                    <div class="stats-small__data text-center">
                        <span class="stats-small__label text-uppercase">Iron</span>
                        <h6 class="stats-small__value count my-3">{{ $order->iron->sum('quantity') }}</h6>
                    </div>
                    <div class="stats-small__data">
                        <span class="stats-small__percentage stats-small__percentage--increase">{{ round(($order->iron->sum('quantity')/$order->quantity->sum('quantity'))*100, 2) }}%</span>
                    </div>
                </div>
                <canvas height="120" class="blog-overview-stats-small-4"></canvas>
            </div>
        </div>
    </div>
    <div class="col-lg col-md-4 col-sm-12 mb-4">
        <div class="stats-small stats-small--1 card card-small">
            <div class="card-body p-0 d-flex">
                <div class="d-flex flex-column m-auto">
                    <div class="stats-small__data text-center">
                        <span class="stats-small__label text-uppercase">Packaging</span>
                        <h6 class="stats-small__value count my-3">{{ $order->packing->sum('quantity') }}</h6>
                    </div>
                    <div class="stats-small__data">
                        <span class="stats-small__percentage stats-small__percentage--decrease">{{ round(($order->packing->sum('quantity')/$order->quantity->sum('quantity'))*100, 2) }}%</span>
                    </div>
                </div>
                <canvas height="120" class="blog-overview-stats-small-5"></canvas>
            </div>
        </div>
    </div>
</div>
<!-- End Small Stats Blocks -->
<div class="row">
    <!-- Users Stats -->
    <div class="col-lg-8 col-md-12 col-sm-12 mb-4">
        <div class="card card-small">
            <div class="card-header border-bottom">
                <h6 class="m-0">Entry Wise</h6>
            </div>
            <div class="card-body pt-0">
                <table class="main-data-table d-none text-capitalize">
                    <thead>
                    <tr>
                        <th>SL</th>
                        <th>Date</th>
                        <th>Type</th>
                        <th>Quantity</th>
                        <th>Action</th>
                        <th class="d-none"></th> {{--For production id--}}
                    </tr>
                    </thead>
                    <tbody>
                    @if(isset($order->production))
                        @foreach($order->production as $entryProduction)
                            <tr>

                                {{--SL--}}                  <td>1</td>
                                {{--Production Date--}}     <td>{{ $entryProduction->date }}</td>
                                {{--Cutting--}}             <td>{{ $entryProduction->production_type }}</td>
                                {{--Swing_in--}}            <td>{{ $entryProduction->quantity }}</td>
                                <td>
                                    <div class="btn-group btn-group-sm" role="group" aria-label="Table row actions">
                                        {{--<button type="button" class="btn btn-white">
                                            <i class="material-icons">&#xE5CA;</i>
                                        </button>

{{--                                     <button type="button" data-toggle="popover" title="Popover title" class="btn btn-danger" data-toggle="popover" title="Popover title">popover</button>--}}

                                        <button data-toggle="popover" title="Production Edit" class="btn btn-white">
                                            <i class="material-icons"></i>
                                        </button>
                                    </div>
                                </td>
                                <td class="d-none">{{ $entryProduction->id }}</td>{{--For production id--}}
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                    <tfoot>
                    <tr>
                        <th>SL</th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th class="d-none"></th>{{--For production id--}}
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
    <!-- End Users Stats -->
    <!-- Users By Device Stats -->
    <div class="col-lg-4 col-md-6 col-sm-12 mb-4">
        <div class="card card-small h-100">
            <div class="card-header border-bottom">
                <h6 class="m-0">Users by device</h6>
            </div>
            <div class="card-body d-flex py-0">
                <canvas height="220" class="blog-users-by-device m-auto"></canvas>
            </div>
            <div class="card-footer border-top">
                <div class="row">
                    <div class="col">
                        <select class="custom-select custom-select-sm" style="max-width: 130px;">
                            <option selected>Last Week</option>
                            <option value="1">Today</option>
                            <option value="2">Last Month</option>
                            <option value="3">Last Year</option>
                        </select>
                    </div>
                    <div class="col text-right view-report">
                        <a href="#">Full report &rarr;</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Users By Device Stats -->
    <!-- New Draft Component -->
</div>
<div class="row">
    <!-- Users Stats -->
    <div class="col-lg-8 col-md-12 col-sm-12 mb-4">
        <div class="card card-small">
            <div class="card-header border-bottom">
                <h6 class="m-0">Date Wise</h6>
            </div>
            <div class="card-body pt-0">
                <table class="main-data-table d-none text-capitalize">
                    <thead>
                    <tr>
                        <th>SL</th>
                        <th>Date</th>
                        <th>Cutting</th>
                        <th>SwingIn</th>
                        <th>SwingOut</th>
                        <th>Iron</th>
                        <th>Poly</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(isset($unique_productions))
                        @foreach($unique_productions as $production)
                            <tr>
                                {{--SL--}}                  <td>1</td>
                                {{--Production Date--}}     <td>{{ $production->date }}</td>
                                {{--Cutting--}}             <td>{{ $order->prodCutting($production->date) }}</td>
                                {{--Swing_in--}}            <td>{{ $order->prodSwingIn($production->date) }}</td>
                                {{--Swing_out--}}           <td>{{ $order->prodSwingOut($production->date) }}</td>
                                {{--Iron--}}                <td>{{ $order->prodIron($production->date) }}</td>
                                {{--Packing--}}             <td>{{ $order->prodPacking($production->date) }}</td>
                                <td>
                                    <div class="btn-group btn-group-sm" role="group" aria-label="Table row actions">
                                        {{--<button type="button" class="btn btn-white">
                                            <i class="material-icons">&#xE5CA;</i>
                                        </button>

{{--                                     <button type="button" data-toggle="popover" title="Popover title" class="btn btn-danger" data-toggle="popover" title="Popover title">popover</button>--}}

                                        <button class="btn btn-white">
                                            <i class="material-icons"></i>
                                        </button>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                    <tfoot>
                    <tr>
                        <th>SL</th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
    <!-- End Users Stats -->
    <!-- Users By Device Stats -->
    <div class="col-lg-4 col-md-6 col-sm-12 mb-4">
        <div class="card card-small h-100">
            <div class="card-header border-bottom">
                <h6 class="m-0">Users by device</h6>
            </div>
            <div class="card-body d-flex py-0">
                <canvas height="220" class="blog-users-by-device m-auto"></canvas>
            </div>
            <div class="card-footer border-top">
                <div class="row">
                    <div class="col">
                        <select class="custom-select custom-select-sm" style="max-width: 130px;">
                            <option selected>Last Week</option>
                            <option value="1">Today</option>
                            <option value="2">Last Month</option>
                            <option value="3">Last Year</option>
                        </select>
                    </div>
                    <div class="col text-right view-report">
                        <a href="#">Full report &rarr;</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Users By Device Stats -->
    <!-- New Draft Component -->
</div>

{{--PopoverContent--}}
<div id="popoverContent" class="d-none">
    <div class="input-group">
        <input style="width: 60%;" type="date" class="form-control" placeholder="Date">

        <select style="width: 40%;" name="type" class="form-control">
            <option>Select...</option>
            <option value="cutting">Cutting</option>
            <option value="swing_in">Swing In</option>
            <option value="swing_out">Swing Out</option>
            <option value="iron">Iron</option>
            <option value="packing">Packing</option>
        </select>

        <input type="number" step="any" class="form-control" placeholder="Quantity">

        <div class="input-group-append" id="button-addon1">
            <button class="btn btn-outline-primary" type="button" data-toggle="popover" data-placement="bottom"
                    data-html="true" data-title="Search">
                <i class="material-icons">save</i>
            </button>
        </div>
    </div>
</div>

<form id="production-edit-form" action="{{ route($pageData['routeFirstName'].'-update', '::prId') }}" method="post" class="d-none">
    @method('PATCH')
    @csrf
    <input type="date" name="date">
    <input type="text" name="type">
    <input type="number" name="qty">
</form>



@section('jQuery-code')
<script>
    $(function () {
    /*popover entry field*/
        $('[data-toggle="popover"]').click(function () {
            $('[data-toggle="popover"]').popover('hide');
        });

        $('[data-toggle="popover"]').popover({
            container: 'body',
            html: true,
            placement: 'bottom',
            sanitize: false,
            content: function() {
                return $('#popoverContent').html()
            }
        });

        $('[data-toggle="popover"]').on('shown.bs.popover', function () {
            let trIndex = $(this).closest('tr').index()+2;
            let trTd = $("#DataTables_Table_0 tr").eq(trIndex).find('td');

            let date = trTd.eq(1).text();
            let type = trTd.eq(2).text();
            let qty = trTd.eq(3).text();
            let id = trTd.eq(5).text();

            let url = $('#production-edit-form').attr('action');
            url = url.replace('::prId', id);
            $('#production-edit-form').attr('action', url);

            $('.popover-body').find('input[type="date"]').val(date);
            $('.popover-body').find('input[type="text"]').val(type);
            $('.popover-body').find('input[type="number"]').val(qty);
        });

        $(document).on('click', '.popover-body button', function () {

            let date = $(this).closest('.popover-body').find('input[type="date"]').val();
            let type = $(this).closest('.popover-body').find('[name="type"]').val();
            let qty = $(this).closest('.popover-body').find('input[type="number"]').val();

            $('#production-edit-form').find('input[type="date"]').val(date);
            $('#production-edit-form').find('input[type="text"]').val(type);
            $('#production-edit-form').find('input[type="number"]').val(qty);

            $('#production-edit-form').submit();
        });
    /*end popover entry field*/
    });
</script>
@endsection