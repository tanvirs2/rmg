@extends('layouts.master')

@section('content')

    @include('layouts.partial.notice')

    <div class="main-content-container container-fluid px-4">
        <!-- Page Header -->
        <div class="page-header row no-gutters py-4">
            <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
                <span class="text-uppercase page-subtitle">Overview</span>
                <h3 class="page-title">{{ $pageData['pageName'] }} List</h3>
            </div>
            <div class="float-right col-12 col-sm-8 text-center text-sm-right">
                <span class="text-uppercase page-subtitle">Action</span>
                <h3 class="page-title">
                    <a href="{{ route($pageData['routeFirstName'].'-create') }}" class="btn btn-md btn-white">
                        <i class="material-icons">person</i> Add
                    </a>
                </h3>
            </div>
        </div>
        <!-- End Page Header -->
        <div class="row">
            <div class="col-lg-12 mb-1">
                <div class="card card-small">
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item p-2">
                            <div class="row">
                                <div class="col-sm-12 col-md-12">
                                    <form>
                                        <div class="form-row">
                                            <div class="form-group col-md-3">
                                                <select class="select2-box-field-filter form-control" name="name" laravel-model="{{ $pageData['pageName'] }}" laravel-get-from="name" laravel-direct-value="true">
                                                    <option value="{{ $name }}">{{ $name }}</option>
                                                </select>
                                                <div class="filter-name">{{ $pageData['pageName'] }} Name</div>
                                            </div>
                                            <div class="form-group col-md-2">
                                                <select class="select2-box-field-filter form-control" name="phone" laravel-model="{{ $pageData['pageName'] }}" laravel-get-from="phone" laravel-direct-value="true">
                                                    <option value="{{ $phone }}">{{ $phone }}</option>
                                                </select>
                                                <div class="filter-name">Phone</div>
                                            </div>
                                            <div class="form-group col-md-3">
                                                <select class="select2-box-field-filter form-control" name="phone" laravel-model="{{ $pageData['pageName'] }}" laravel-get-from="email" laravel-direct-value="true">
                                                    <option value="{{ $email }}">{{ $email }}</option>
                                                </select>
                                                <div class="filter-name">Email</div>
                                            </div>
                                            <div class="form-group col-md-2">
                                                <button type="submit" class="mb-2 btn btn-primary">Search</button>

                                                <a href="{{ route($pageData['routeFirstName'].'-list') }}" class="mb-2 btn btn-dark">Reset</a>
                                            </div>
                                            <div class="form-group col-md-1 p-0">
                                                <button @click="doit" type="button" class="float-right btn btn-info">
                                                    <i class="material-icons">save_alt</i> Export
                                                </button>
                                            </div>
                                            <div class="form-group col-md-1">
                                                <select name='per_page' class="form-control is-invalid" @change="per_page">
                                                    @foreach($per_page_rows as $number)
                                                        <option {{ ($per_page==$number) ? "selected" : null }}>{{ $number }}</option>
                                                    @endforeach
                                                </select>
                                                <div class="invalid-feedback">Rows</div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12 mb-3">
                <div class="card card-small">
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item p-2">
                            <div class="row">
                                <div class="col-md-2">
                                    <b>Total : </b> <span>{{ $count }} {{ $pageData['pageName'] }} Found</span>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <!-- Default Light Table -->
        <div class="row">
            <div class="col">
                <div class="card card-small mb-4">
                    <div class="card-header border-bottom">
                        {{--From Controller--}}
                        <h6 class="m-0">Active {{ $pageData['pageName'] }}s</h6>
                    </div>
                    <div class="card-body p-0 pb-3 text-center">
                        <table class="table mb-0" id="main-table" >
                            <thead class="bg-light">
                            <tr>
                                <th scope="col" class="border-0">Sl</th>
                                <th scope="col" class="border-0">Image</th>
                                <th scope="col" class="border-0">Name</th>
                                <th scope="col" class="border-0">Phone</th>
                                <th scope="col" class="border-0">Email</th>
                                <th scope="col" class="border-0">Address</th>
                                <th scope="col" class="border-0">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($mainDatas as $mainData)
                            <tr>
                                <td>{{ $pageData['no']++ }}</td>
                                <td><img width="50" src="{{ $mainData->GetProfilePicture() }}" alt=""></td>
                                <td>{{ ucwords($mainData->name) }}</td>
                                <td>{{ $mainData->phone }}</td>
                                <td>{{ $mainData->email }}</td>
                                <td>{{ $mainData->address }}</td>
                                <td>
                                    <div class="btn-group btn-group-sm" role="group" aria-label="Table row actions">
                                        <a href="#" class="btn btn-white">
                                            <i class="material-icons"></i>
                                        </a>
                                        <a href="#" class="btn btn-white">
                                            <i class="material-icons"></i>
                                        </a>
                                        <a href="{{ route($pageData['routeFirstName'].'-edit', $mainData->id) }}" class="btn btn-white">
                                            <i class="material-icons"></i>
                                        </a>
                                        <a class="btn btn-white" onclick="return deleteThisData('{{ route($pageData['routeFirstName'].'-destroy', $mainData->id) }}')">
                                            <i class="material-icons"></i>
                                        </a>
                                    </div>
                                </td>
                            </tr>
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
                {{ $mainDatas->links() }}
            </div>
        </div>
        <!-- End Default Dark Table -->
    </div>
@endsection

@section('vue-script')
    @include('js/script')
@endsection