<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
<head>
    <meta charset="utf-8"> <!-- utf-8 works for most cases -->
    <meta name="viewport" content="width=device-width"> <!-- Forcing initial-scale shouldn't be necessary -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge"> <!-- Use the latest (edge) version of IE rendering engine -->
    <meta name="x-apple-disable-message-reformatting">  <!-- Disable auto-scale in iOS 10 Mail entirely -->
    <title></title> <!-- The title tag shows in email notifications, like Android 4.4. -->

    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700" rel="stylesheet">

    <!-- CSS Reset : BEGIN -->
    <style>
        .border-tbl td, .border-tbl th{
            padding: 0;
            margin: auto;
            text-align: center;
        }

        .border-tbl h3 {
            font-size: 13px !important;
            height: 10px !important;
            padding: 0
        }


        /* What it does: Remove spaces around the email design added by some email clients. */
        /* Beware: It can remove the padding / margin and add a background color to the compose a reply window. */
        html,
        body {
            margin: 0 auto !important;
            padding: 0 !important;
            height: 100% !important;
            width: 100% !important;
            background: #f1f1f1;
        }

        /* What it does: Stops email clients resizing small text. */
        * {
            -ms-text-size-adjust: 100%;
            -webkit-text-size-adjust: 100%;
        }

        /* What it does: Centers email on Android 4.4 */
        div[style*="margin: 16px 0"] {
            margin: 0 !important;
        }

        /* What it does: Stops Outlook from adding extra spacing to tables. */
        table,
        td {
            mso-table-lspace: 0pt !important;
            mso-table-rspace: 0pt !important;
        }

        /* What it does: Fixes webkit padding issue. */
        table {
            border-spacing: 0 !important;
            border-collapse: collapse !important;
            table-layout: fixed !important;
            margin: 2px auto !important;
        }

        /* What it does: Uses a better rendering method when resizing images in IE. */
        img {
            -ms-interpolation-mode:bicubic;
        }

        /* What it does: Prevents Windows 10 Mail from underlining links despite inline CSS. Styles for underlined links should be inline. */
        a {
            text-decoration: none;
        }

        /* What it does: A work-around for email clients meddling in triggered links. */
        *[x-apple-data-detectors],  /* iOS */
        .unstyle-auto-detected-links *,
        .aBn {
            border-bottom: 0 !important;
            cursor: default !important;
            color: inherit !important;
            text-decoration: none !important;
            font-size: inherit !important;
            font-family: inherit !important;
            font-weight: inherit !important;
            line-height: inherit !important;
        }

        /* What it does: Prevents Gmail from displaying a download button on large, non-linked images. */
        .a6S {
            display: none !important;
            opacity: 0.01 !important;
        }

        /* What it does: Prevents Gmail from changing the text color in conversation threads. */
        .im {
            color: inherit !important;
        }

        /* If the above doesn't work, add a .g-img class to any image in question. */
        img.g-img + div {
            display: none !important;
        }

        /* What it does: Removes right gutter in Gmail iOS app: https://github.com/TedGoas/Cerberus/issues/89  */
        /* Create one of these media queries for each additional viewport size you'd like to fix */

        /* iPhone 4, 4S, 5, 5S, 5C, and 5SE */
        @media only screen and (min-device-width: 320px) and (max-device-width: 374px) {
            u ~ div .email-container {
                min-width: 320px !important;
            }
        }
        /* iPhone 6, 6S, 7, 8, and X */
        @media only screen and (min-device-width: 375px) and (max-device-width: 413px) {
            u ~ div .email-container {
                min-width: 375px !important;
            }
        }
        /* iPhone 6+, 7+, and 8+ */
        @media only screen and (min-device-width: 414px) {
            u ~ div .email-container {
                min-width: 414px !important;
            }
        }

    </style>

    <!-- CSS Reset : END -->

    <!-- Progressive Enhancements : BEGIN -->
    <style>

        .primary{
            background: #f85e9f;
        }
        .bg_white{
            background: #ffffff;
        }
        .bg_light{
            /*background: #fafafa;*/
        }
        .bg_black{
            background: #000000;
        }
        .bg_dark{
            background: rgba(0,0,0,.8);
        }
        .email-section{
            padding:2.5em;
        }

        /*BUTTON*/
        .btn{
            padding: 5px 15px;
            display: inline-block;
        }
        .btn.btn-primary{
            border-radius: 5px;
            background: #f85e9f;
            color: #ffffff;
        }
        .btn.btn-white{
            border-radius: 5px;
            background: #ffffff;
            color: #000000;
        }
        .btn.btn-white-outline{
            border-radius: 5px;
            background: transparent;
            border: 1px solid #fff;
            color: #fff;
        }
        .btn.btn-black-outline{
            border-radius: 0px;
            background: transparent;
            border: 2px solid #000;
            color: #000;
            font-weight: 700;
        }

        h1,h2,h3,h4,h5,h6{
            font-family: 'Lato', sans-serif;
            color: #000000;
            margin-top: 0;
            font-weight: 400;
        }

        body{
            font-family: 'Lato', sans-serif;
            font-weight: 400;
            font-size: 15px;
            line-height: 1.8;
            color: rgba(0,0,0,.4);
        }

        a{
            color: #f85e9f;
        }

        table{
        }
        /*LOGO*/

        .logo h1{
            margin: 0;
        }
        .logo h1 a{
            color: #000000;
            font-size: 20px;
            font-weight: 700;
            text-transform: uppercase;
            font-family: 'Lato', sans-serif;
            border: 2px solid #000;
            padding: .2em 1em;
        }

        .navigation{
            padding: 0;
            /*padding: 1em 0;*/
            /*background: rgba(0,0,0,1);*/
            border-top: 1px solid rgba(0,0,0,.05);
            border-bottom: 1px solid rgba(0,0,0,.05);
        }
        .navigation li{
            list-style: none;
            display: inline-block;;
            margin-left: 5px;
            margin-right: 5px;
            font-size: 13px;
            font-weight: 500;
            text-transform: uppercase;
            letter-spacing: 2px;
        }
        .navigation li a{
            color: rgba(0,0,0,1);
        }

        /*HERO*/
        .hero{
            position: relative;
            z-index: 0;
        }

        .hero .text{
            color: rgba(0,0,0,.3);
        }
        .hero .text h2{
            color: #000;
            font-size: 30px;
            margin-bottom: 0;
            font-weight: 300;
        }
        .hero .text h2 span{
            font-weight: 600;
            color: #f85e9f;
        }


        /*HEADING SECTION*/
        .heading-section{
        }
        .heading-section h2{
            color: #000000;
            font-size: 28px;
            margin-top: 0;
            line-height: 1.4;
            font-weight: 400;
        }
        .heading-section .subheading{
            margin-bottom: 20px !important;
            display: inline-block;
            font-size: 13px;
            text-transform: uppercase;
            letter-spacing: 2px;
            color: rgba(0,0,0,.4);
            position: relative;
        }
        .heading-section .subheading::after{
            position: absolute;
            left: 0;
            right: 0;
            bottom: -10px;
            content: '';
            width: 100%;
            height: 2px;
            background: #f85e9f;
            margin: 0 auto;
        }

        .heading-section-white{
            color: rgba(255,255,255,.8);
        }
        .heading-section-white h2{
            font-family: "Dosis", Arial, Helvetica, sans-serif;
            line-height: 1;
            padding-bottom: 0;
        }
        .heading-section-white h2{
            color: #ffffff;
        }
        .heading-section-white .subheading{
            margin-bottom: 0;
            display: inline-block;
            font-size: 13px;
            text-transform: uppercase;
            letter-spacing: 2px;
            color: rgba(255,255,255,.4);
        }


        ul.social{
            padding: 0;
        }
        ul.social li{
            display: inline-block;
            margin-right: 10px;
        }

        /*FOOTER*/

        .footer{
            border-top: 1px solid rgba(0,0,0,.05);
            color: rgba(0,0,0,.5);
        }
        .footer .heading{
            color: #000;
            font-size: 20px;
        }
        ul{
            margin: 0;
        }
        .footer ul{
            margin: 0;
            padding: 0;
        }
        .footer ul li{
            list-style: none;
            margin-bottom: 10px;
        }
        .footer ul li a{
            color: rgba(0,0,0,1);
        }


        @media screen and (max-width: 500px) {


        }


    </style>


</head>

<body width="100%" style="margin: 0; padding: 0 !important; mso-line-height-rule: exactly; background-color: #222222;">
<div style="width: 100%; background-color: #f1f1f1;">
    <div style="display: none; font-size: 1px;max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden; mso-hide: all; font-family: sans-serif;">
        &zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;
    </div>
    <div style="max-width: 700px; margin: 0 auto;" class="email-container">
        <!-- BEGIN BODY -->
        <table align="center" role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" style="margin: auto;">
            <tr>
                <td valign="top" class="bg_white" style="/*padding: 1em 2.5em 0 2.5em;*/">
                    <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td class="" style="text-align: center;">
                                <h1 style="margin-bottom: 0">West Apparels Ltd.</h1>
                                <small>FABRICS KNITTING & DYEING PROGRAM</small>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr><!-- end tr -->
            <tr>
                <td valign="top" class="bg_white" style="padding: 0;">
                    <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td width="60%" class="logo" style="text-align: center; color: black">
                                <ul class="navigation">
                                    <li><b>KDProgram : </b></li>
                                    <li><b>RM{{ $order->created_at->format('y') }}/{{ $order->created_at->format('m') }}/{{  $order->id }}</b></li>
                                </ul>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr><!-- end tr -->

            <tr>
                <td class="bg_white">
                    <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
                        <tr>
                            <td class="bg_light email-section" style="padding: 0; width: 100%;">
                                <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td valign="middle" width="50%">
                                            <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
                                                <tr>
                                                    <td class="text-services" style="text-align: left; padding: 0 20px; color: black">
                                                        <div class="heading-section">

                                                            <table>
                                                                <tr>
                                                                    <th>Buyer : </th>
                                                                    <td>{{ $order->buyer->name }}</td>
                                                                </tr>
                                                                <tr>
                                                                    <th>Product : </th>
                                                                    <td>{{ $order->style_desc }}</td>
                                                                </tr>
                                                                <tr>
                                                                    <th>Order No : </th>
                                                                    <td>{{ $order->name }}</td>
                                                                </tr>
                                                                <tr>
                                                                    <th>Quantity : </th>
                                                                    <td>{{ $orderQtyTot = $order->quantity->sum('quantity') }}</td>
                                                                </tr>
                                                            </table>

                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td valign="middle" width="50%">
                                            <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
                                                <tr>
                                                    <td>
                                                        <img src="{{ $order->getGarmentsPicture() }}" alt="" style="height: 150px; margin: auto; display: block;">
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr><!-- end: tr -->
                        <tr>
                            <td class="bg_light email-section" style="padding: 0; width: 100%;">
                                <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td valign="middle" width="100%" colspan="2">
                                            <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
                                                <tr>
                                                    <td class="text-services" style="text-align: left; padding: 10px 30px;">
                                                        <div class="heading-section">

                                                            <table class="border-tbl" border="1" size="100%" style="width: 100%; color: black">
                                                                <tr>
                                                                    <th colspan="3" align="middle" style="background: #dfdfdf"><h3>Color/Size wise Order Qty</h3></th>
                                                                </tr>
                                                                <tr>
                                                                    <th>Color</th>
                                                                    <th>Gmts Size</th>
                                                                    <th>Total Qty</th>
                                                                </tr>
                                                                @foreach($order->orderColorAndSizeQtyArrFunc() as $color => $orderSizeAndColor)
                                                                <tr>
                                                                    <td>{{ $color }}</td>
                                                                    <td align="middle">
                                                                        <table border="1" style="font-size: 10px">
                                                                            <tr>
                                                                                @foreach($orderSizeAndColor['sz'] as $sz)
                                                                                    <th>{{ $sz }}</th>
                                                                                @endforeach
                                                                            </tr>
                                                                            <tr>
                                                                                @foreach($orderSizeAndColor['qty'] as $qty)
                                                                                    <td>{{ $qty }}</td>
                                                                                @endforeach
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                    <td>{{ array_sum($orderSizeAndColor['qty']) }}</td>
                                                                </tr>
                                                                @endforeach
                                                                <tr>
                                                                    <th></th>
                                                                    <th></th>
                                                                    <th>{{ $orderQtyTot }} PCS</th>
                                                                </tr>
                                                            </table>

                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr><!-- end: tr -->
                        <tr>
                            <td class="bg_light email-section" style="padding: 0; width: 100%;">
                                @foreach($order->kdProgramWithParts() as $kdParts)

                                <table class="border-tbl" border="1" size="100%" style="width: 100%; color: black">
                                    <th colspan="8" align="middle" style="background: #dfdfdf"><h3 style="font-size: 18px">{{ $kdParts->kdParts->name }}</h3></th>
                                    <tr>
                                        <th>Color</th>
                                        <th>Fabric</th>
                                        <th>DIA</th>
                                        <th>Consum <small>(kg)</small></th>
                                        <th>FinFavQty <small>(kg)</small></th>
                                        <th>G.FavQty <small>(kg)</small></th>
                                        <th>Remark</th>
                                        <th>Lab</th>
                                    </tr>
                                    @php
                                        $finishFavArr = [];
                                        $grayFavArr = [];
                                    @endphp

                                    @foreach($order->kdProgramWithPartsDetails($kdParts->kdParts->id) as $kdProgram)
                                        @php
                                            //$i++;
                                            //$kdProgram = $kdParts->kdParts->kdProgram[$i]; //kd_program instance
                                            //$kdIssues = $kdProgram->kdIssues; //kd_issue instance

                                            $orderQty = $order->quantity->where('color_id', $kdProgram->color_id)->sum('quantity');
                                            $finishFav = round(($kdProgram->finish_fab/12)*$orderQty, 3);
                                            $finishFavArr[] = $finishFav;
                                            $finalFinishFavArr[] = $finishFav;

                                            $grayFav = round($finishFav+($kdProgram->gray_fab/100)*$finishFav, 3);
                                            $grayFavArr[] = $grayFav;
                                            $finalGrayFavArr[] = $grayFav;
                                        @endphp
                                    <tr>
                                        <td>{{ $kdProgram->color->name }}</td>
                                        <td align="middle">{{ $order->kdProgramWithFabricDetails($kdParts->kdParts->id)->fabric ?? null }} : {{ $order->kdProgramWithFabricDetails($kdParts->kdParts->id)->fabric_desc ?? null }}</td>
                                        <td>{{ $kdProgram->dia }}</td>
                                        <td>{{ $kdProgram->finish_fab }}</td>
                                        <td>{{ $finishFav }}</td>
                                        <td>{{ $grayFav }}</td>
                                        <td>{{ $kdProgram->remarks }}</td>
                                        <td>{{ $kdProgram->lab }}</td>
                                        {{--<td>{{ $order->kdProgramWithFabricDetails($kdParts->kdParts->id)->remarks ?? null }}</td>
                                        <td>{{ $order->kdProgramWithFabricDetails($kdParts->kdParts->id)->lab ?? null }}</td>--}}
                                    </tr>
                                    @endforeach
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>{{--{{ $order->kdProgramWithPartsDetails($kdParts->kdParts->id)->sum('dia') }}--}}</td>
                                        <td>{{ $order->kdProgramWithPartsDetails($kdParts->kdParts->id)->sum('finish_fab') }}</td>
                                        <td>{{ array_sum($finishFavArr) }}</td>
                                        <td>{{ array_sum($grayFavArr) }}</td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                </table>
                                @endforeach
                            </td>
                        </tr><!-- end: tr -->
                        @if(isset($order->kdExtra))
                        <tr>
                            <td class="bg_light email-section" style="padding: 0; width: 100%;">
                                <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td valign="middle" width="100%" colspan="2">
                                            <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
                                                <tr>
                                                    <td class="text-services" style="text-align: left; padding: 5px 10px;">
                                                        <div class="heading-section">

                                                            <table class="border-tbl" border="1" size="100%" style="width: 100%; color: black">
                                                                <tr>
                                                                    <th colspan="7" align="middle" style="background: #dfdfdf"><h3>Solid flat knit Collar</h3></th>
                                                                </tr>
                                                                <tr>
                                                                    <th>Color</th>
                                                                    <th colspan="2">Size</th>
                                                                    <th>CM Fini</th>
                                                                    <th>Consum <small>(kg)</small></th>
                                                                    <th>FinFavQty <small>(kg)</small></th>
                                                                    <th>G.FavQty <small>(kg)</small></th>
                                                                </tr>
                                                                @php
                                                                    $finishFavArr = [];
                                                                    $grayFavArr = [];
                                                                @endphp

                                                                @foreach($order->orderColorAndSizeQtyArrFunc() as $color => $orderSizeAndColor)
                                                                    @php
                                                                        $kdExtra = $order->kdExtra->where('color_id', $orderSizeAndColor['color_id'])->where('type', 'collar')->first();

                                                                        $orderQty = $order->quantity->where('color_id', $orderSizeAndColor['color_id'])->sum('quantity');
                                                                        $finishFav = round(($kdExtra->finish_fab/12)*$orderQty, 3);

                                                                        $finishFavArr[] = $finishFav;
                                                                        $finalFinishFavArr[] = $finishFav;
                                                                        $grayFav = round($finishFav+($kdExtra->gray_fab/100)*$finishFav, 3);
                                                                        $grayFavArr[] = $grayFav;
                                                                        $finalGrayFavArr[] = $grayFav;

                                                                    @endphp
                                                                    <tr>
                                                                        <td>{{ $color }}</td>
                                                                        <td align="middle" colspan="2">
                                                                            <table border="1" style="font-size: 10px">
                                                                                <tr>
                                                                                    @foreach($orderSizeAndColor['sz'] as $sz)
                                                                                        <th>{{ $sz }}</th>
                                                                                    @endforeach
                                                                                </tr>
                                                                                <tr>
                                                                                    @foreach($orderSizeAndColor['qty'] as $qty)
                                                                                        <td>{{ $qty * $kdExtra->req }}</td>
                                                                                    @endforeach
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                        <td>{{ array_sum($orderSizeAndColor['qty']) * $kdExtra->req }}</td>
                                                                        <td>{{ $kdExtra->finish_fab }}</td>
                                                                        <td>{{ $finishFav }}</td>
                                                                        <td>{{ $grayFav }}</td>
                                                                    </tr>

                                                                @endforeach
                                                                <tr>
                                                                    <th></th>
                                                                    <th colspan="2"></th>
                                                                    <th>{{ $orderQtyTot }} PCS</th>
                                                                    <th></th>
                                                                    <td>{{ array_sum($finishFavArr) }}</td>
                                                                    <td>{{ array_sum($grayFavArr) }}</td>
                                                                </tr>
                                                            </table>

                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr><!-- end: tr -->
                        @endif

                        @if(isset($order->kdExtra))
                        <tr>
                            <td class="bg_light email-section" style="padding: 0; width: 100%;">
                                <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>
                                        <td valign="middle" width="100%" colspan="2">
                                            <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
                                                <tr>
                                                    <td class="text-services" style="text-align: left; padding: 5px 10px;">
                                                        <div class="heading-section">

                                                            <table class="border-tbl" border="1" size="100%" style="width: 100%; color: black">
                                                                <tr>
                                                                    <th colspan="7" align="middle" style="background: #dfdfdf"><h3>Solid flat knit Cuff</h3></th>
                                                                </tr>
                                                                <tr>
                                                                    <th>Color</th>
                                                                    <th colspan="2">Size</th>
                                                                    <th>CM Fini</th>
                                                                    <th>Consum <small>(kg)</small></th>
                                                                    <th>FinFavQty <small>(kg)</small></th>
                                                                    <th>G.FavQty <small>(kg)</small></th>

                                                                </tr>
                                                            @php
                                                                $finishFavArr = [];
                                                                $grayFavArr = [];
                                                            @endphp

                                                            @foreach($order->orderColorAndSizeQtyArrFunc() as $color => $orderSizeAndColor)
                                                                @php
                                                                    $kdExtra = $order->kdExtra->where('color_id', $orderSizeAndColor['color_id'])->where('type', 'cuff')->first();

                                                                    $orderQty = $order->quantity->where('color_id', $orderSizeAndColor['color_id'])->sum('quantity');
                                                                    $finishFav = round(($kdExtra->finish_fab/12)*$orderQty, 3);

                                                                    $finishFavArr[] = $finishFav;
                                                                    $finalFinishFavArr[] = $finishFav;
                                                                    $grayFav = round($finishFav+($kdExtra->gray_fab/100)*$finishFav, 3);
                                                                    $grayFavArr[] = $grayFav;
                                                                    $finalGrayFavArr[] = $grayFav;

                                                                    @endphp
                                                                    <tr>
                                                                        <td>{{ $color }}</td>
                                                                        <td align="middle" colspan="2">
                                                                            <table border="1" style="font-size: 10px">
                                                                                <tr>
                                                                                    @foreach($orderSizeAndColor['sz'] as $sz)
                                                                                        <th>{{ $sz }}</th>
                                                                                    @endforeach
                                                                                </tr>
                                                                                <tr>
                                                                                    @foreach($orderSizeAndColor['qty'] as $qty)
                                                                                        <td>{{ $qty * $kdExtra->req }}</td>
                                                                                    @endforeach
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                        <td>{{ array_sum($orderSizeAndColor['qty']) * $kdExtra->req }}</td>
                                                                        <td>{{ $kdExtra->finish_fab }}</td>
                                                                        <td>{{ $finishFav }}</td>
                                                                        <td>{{ $grayFav }}</td>

                                                                    </tr>
                                                                @endforeach
                                                                <tr>
                                                                    <th></th>
                                                                    <th colspan="2"></th>
                                                                    <th>{{ $orderQtyTot }} PCS</th>
                                                                    <td></td>
                                                                    <td>{{ array_sum($finishFavArr) }}</td>
                                                                    <td>{{ array_sum($grayFavArr) }}</td>
                                                                </tr>
                                                            </table>

                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr><!-- end: tr -->
                        @endif
                        <tr>
                            <td class="bg_light email-section" style="padding: 0; width: 100%;">
                                <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tr>

                                        <td valign="middle" width="50%">
                                            <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
                                                <tr>
                                                    <td class="text-services" style="text-align: left; padding: 10px 30px;">
                                                        <table class="border-tbl" border="1" size="100%" style="width: 100%; color: black">

                                                            <tr>
                                                                <th>Total FiniFabReq</th>
                                                                <th>{{ array_sum($finalFinishFavArr) }}</th>
                                                                <th>Mrk
                                                                    {{ $mrkQty = round(($orderQtyTot * isset($order->budget) ? $order->budget->yarn_consumption : 0)/12, 3) }}
                                                                </th>
                                                            </tr>
                                                            <tr>
                                                                <th>Total GrayFabReq</th>
                                                                <th>{{ array_sum($finalGrayFavArr) }}</th>
                                                                <th>
                                                                    @if(array_sum($finalGrayFavArr) > $mrkQty)
                                                                        <b>OVER</b>
                                                                    @else
                                                                        Under
                                                                    @endif
                                                                </th>
                                                            </tr>

                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr><!-- end: tr -->
                    </table>
                </td>
            </tr><!-- end:tr -->
            <tr>
                <td class="bg_white" style="padding: 2em 2em; color: #0a0c0d">
                    <table class="border-tbl">
                        <tr>
                            <td width="48%" style="padding-top: 20px; text-align: left;">
                                Merchandiser
                            </td>
                            <td width="40%" style="padding-top: 20px; text-align: left;">
                                AGM-(M/M)
                            </td>
                            <td width="40%" style="padding-top: 20px; text-align: right;">
                                Approved
                            </td>
                        </tr>
                    </table>
                </td>
            </tr><!-- end tr -->
            <!-- 1 Column Text + Button : END -->
        </table>
        <table align="center" role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" style="margin: auto;">
            <tr>
                <td valign="middle" class="bg_white footer">

                </td>
            </tr><!-- end: tr -->

        </table>

    </div>
</div>
</body>
</html>