@extends('layouts.master')

@section('content')

    @include('layouts.partial.notice')

    <div class="main-content-container container-fluid px-4">
        <!-- Page Header -->
        <div class="page-header row no-gutters py-4">
            <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
                <span class="text-uppercase page-subtitle">Overview</span>
                <h3 class="page-title"> {{ isset($isSetSupplier)? 'Edit':'' }} {{ $pageData['pageName'] }} Profile</h3>
            </div>
        </div>
        <!-- End Page Header -->
        <!-- Default Light Table -->
        <div class="row">
            <div class="col-lg-4">
                <div class="card card-small mb-4 pt-3">
                    <div class="card-header border-bottom text-center">
                        <div class="mb-3 mx-auto">
                            <img class="rounded-circle" :src="userImg"
                                 alt="{{ $pageData['pageName'] }} Avatar" width="210"></div>
                        <h4 class="mb-0">@{{ fullName }}</h4>
                        <span class="text-muted d-block mb-2">@{{ userRole }}</span>

                    </div>
                    <ul class="list-group list-group-flush">

                        <li class="list-group-item p-4">
                            <strong class="text-muted d-block mb-2">About {{ $pageData['pageName'] }}</strong>
                            <span>
                                @{{ aboutUser }}
                            </span>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-8">
                <div class="card card-small mb-4">
                    <div class="card-header border-bottom">
                        <h6 class="m-0">Account Details</h6>
                    </div>
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item p-3">
                            <div class="row">
                                <div class="col">

                                    @if(isset($isSetSupplier))
                                        <form action="{{ route($pageData['routeFirstName'].'-update', $isSetSupplier->id) }}" method="post" enctype="multipart/form-data">
                                        @method('PATCH')
                                    @else
                                        <form action="{{ route($pageData['routeFirstName'].'-store') }}" method="post" enctype="multipart/form-data">
                                    @endif

                                        @csrf

                                        <div class="form-row">
                                            <div class="form-group col-md-6">
                                                <label for="feFirstName">Full Name</label>
                                                <input v-model="fullName" name="name" type="text" class="form-control" id="feFirstName"
                                                       placeholder="Full Name" value="">
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label for="feLastName">Phone Number</label>
                                                <input name="phone" type="text" class="form-control" id="feLastName"
                                                       placeholder="Phone Number" value="{{ isset($isSetSupplier->phone)?$isSetSupplier->phone:old('phone') }}">
                                            </div>
                                        </div>

                                        <div class="form-row">

                                            <div class="form-group col-md-4">
                                                <label for="feEmailAddress">Email</label>
                                                <input name="email" type="email" class="form-control" id="feEmailAddress" placeholder="Email" value="{{ isset($isSetSupplier)?$isSetSupplier->email:'' }}">
                                            </div>
                                            <div class="form-group col-md-4">
                                                <label>Status</label>
                                                <select name="status" class="form-control">
                                                    <option disabled selected="">Choose...</option>
                                                    <option value="1">Active</option>
                                                    <option value="0">Inactive</option>
                                                    <option value="2">Pending</option>
                                                </select>
                                            </div>
                                            <div class="form-group col-md-4">
                                                <label>Reporting To</label>
                                                <select name="reporting_to" class="form-control">
                                                    <option value="" disabled selected>None</option>
                                                    @foreach($users as $user)
                                                        <option value="{{ $user->id }}" @if(isset($isSetSupplier)) {{ ($isSetSupplier->reporting_to == $user->id)?'selected':'' }} @endif>{{ $user->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>

                                        </div>

                                        <div class="form-row">
                                            <div class="form-group col-md-4">
                                                <label for="feProPic">Profile Picture</label>
                                                <input name="profile_img" type="file" @change="readURL" class="form-control" id="feProPic" placeholder="Password">

                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="feInputAddress">Address</label>
                                            <input value="@if(isset($isSetSupplier)) {{ $isSetSupplier->address }} @endif" name="address" type="text" class="form-control" placeholder="1234 Dhaka, Gazipur">
                                        </div>

                                        <div class="form-row">
                                            <div class="form-group col-md-12">
                                                <label for="feDescription">About {{ $pageData['pageName'] }}</label>
                                                <textarea v-model="aboutUser" name="description" class="form-control" id="feDescription" rows="5">Lorem ipsum dolor sit amet consectetur adipisicing elit. Odio eaque, quidem, commodi soluta qui quae minima obcaecati quod dolorum sint alias, possimus illum assumenda eligendi cumque?</textarea>
                                            </div>
                                        </div>
                                        <button type="submit" class="btn btn-accent">Save</button>
                                    </form>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- End Default Light Table -->
    </div>
@endsection

@section('vue-script')
    <script>

        const data = {
            userImg: '{{ isset($isSetSupplier) ? $isSetSupplier->GetProfilePicture():asset('profile-pic/profile.jpg') }}',
            fullName: '{{ isset($isSetSupplier) ? $isSetSupplier->name:old('name') }}',
            aboutUser: '{{ isset($isSetSupplier) ? $isSetSupplier->description:'' }}',
        };

        const app = new Vue({
            el: '#app',
            data,
            methods: {

                readURL(input) { //img preview before upload
                    input = input.target;
                    if (input.files && input.files[0]) {
                        var reader = new FileReader();
                        reader.readAsDataURL(input.files[0]);
                        reader.onload = function(e) {
                            this.userImg = e.target.result;
                        }.bind(this);
                    }
                }
            },
            beforeMount(){
                //this.departmentChange()
            },
            computed: {
                //
            }

        });
    </script>
@endsection

