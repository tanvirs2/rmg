@extends('layouts.master')

@section('content')
    @include('layouts.partial.notice')

    <div class="main-content-container container-fluid px-4">
        <!-- Page Header -->
        <div class="page-header row no-gutters py-4">
            <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
                <span class="text-uppercase page-subtitle">Overview</span>
                <h3 class="page-title">{{ isset($isSetOrderSubCategory)?'Edit':'Add New' }} {{ $pageData['pageName'] }}</h3>
            </div>
        </div>
        <!-- End Page Header -->

        <!-- Default Light Table -->
        <div class="row">

            <div class="col-lg-8">
                <div class="card card-small mb-4">
                    <div class="card-header border-bottom">
                        <h6 class="m-0">{{ $pageData['pageName'] }} Details</h6>
                    </div>
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item p-3">
                            <div class="row">
                                <div class="col">

                                    @if(isset($isSetOrderSubCategory))
                                        <form action="{{ route($pageData['routeFirstName'].'-update', $isSetOrderSubCategory->id) }}" method="post" >
                                        @method('PATCH')
                                    @else
                                        <form action="{{ route($pageData['routeFirstName'].'-store') }}" method="post">
                                    @endif

                                    @csrf

                                            <div class="form-row">
                                                <div class="form-group col-md-6">
                                                    <label for="name" class="input-required">{{ $pageData['pageName'] }} Name</label>
                                                    <input type="text"
                                                           class="form-control"
                                                           placeholder="{{ $pageData['pageName'] }} Name"
                                                           name="name"
                                                           value="@isset($isSetOrderSubCategory) {{ $isSetOrderSubCategory->name }} @endisset{{ old('name') }}">
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label>Order Category</label>
                                                    <select name="order_category_id" class="form-control">
                                                        <option value="" disabled selected>Choose...</option>
                                                        @foreach($orderCategories as $orderCategory)
                                                            <option value="{{ $orderCategory->id }}" @isset($isSetOrderSubCategory) {{ ($isSetOrderSubCategory->orderCategory->id == $orderCategory->id)?'selected':'' }} @endisset>{{ ucfirst($orderCategory->name) }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label>Status</label>
                                                    <select name="status" class="form-control">
                                                        <option disabled selected>Choose...</option>
                                                        <option value="1" {{ isset($isSetOrderSubCategory)?(($isSetOrderSubCategory->status == 1)?'selected':null):null }}>Active</option>
                                                        <option value="0" {{ isset($isSetOrderSubCategory)?(($isSetOrderSubCategory->status == 0)?'selected':null):null }}>Inactive</option>
                                                        <option value="2" {{ isset($isSetOrderSubCategory)?(($isSetOrderSubCategory->status == 2)?'selected':null):null }}>Pending</option>
                                                    </select>
                                                </div>
                                            </div>

                                        <button type="submit" class="btn btn-accent">{{ isset($isSetOrderSubCategory)?'Update':'Create' }}</button>
                                    </form>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- End Default Light Table -->
    </div>
@endsection