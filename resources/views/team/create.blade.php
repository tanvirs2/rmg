@extends('layouts.master')

@section('content')
    @include('layouts.partial.notice')

    <div class="main-content-container container-fluid px-4">
        <!-- Page Header -->
        <div class="page-header row no-gutters py-4">
            <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
                <span class="text-uppercase page-subtitle">Overview</span>
                <h3 class="page-title">{{ isset($selectedTeam)?'Edit':'Add New' }} Section</h3>
            </div>
        </div>
        <!-- End Page Header -->

        <!-- Default Light Table -->
        <div class="row">

            <div class="col-lg-8">
                <div class="card card-small mb-4">
                    <div class="card-header border-bottom">
                        <h6 class="m-0">Section Details</h6>
                    </div>
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item p-3">
                            <div class="row">
                                <div class="col">

                                    @if(isset($selectedTeam))
                                        <form action="{{ route('team-update', $selectedTeam->id) }}" method="post" >
                                        @method('PATCH')
                                    @else
                                        <form action="{{ route('team-store') }}" method="post">
                                    @endif

                                    @csrf

                                        <div class="form-row">
                                            <div class="form-group col-md-6">
                                                <label for="name" class="input-required">Section Name</label>
                                                <input type="text"
                                                       class="form-control"
                                                       placeholder="Section Name"
                                                       name="name"
                                                       value="@isset($selectedTeam) {{ $selectedTeam->name }} @endisset{{ old('name') }}"
                                                >
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label>Department</label>
                                                <select name="department_id" class="form-control">
                                                    <option value="" disabled selected>Select Department</option>
                                                    @foreach($departments as $department)
                                                        <option value="{{ $department->id }}" @isset($selectedTeam) {{ ($selectedTeam->department->id == $department->id)?'selected':'' }} @endisset>{{ ucfirst($department->name) }}</option>
                                                    @endforeach
                                                </select>
                                            </div>


                                            <div class="form-group col-md-6">
                                                <label for="feInputState" class="input-required">Sections</label>
                                                <div class='card card-small mb-3'>
                                                    <div class="card-header border-bottom">
                                                        <h6 class="m-0">Subordinate Sections</h6>
                                                    </div>
                                                    <div class='card-body p-0'>
                                                        <ul class="list-group list-group-flush">
                                                            <li class="list-group-item px-3 pb-2">
                                                                @php($old_s_t_ids = isset($selectedTeam)?$selectedTeam->orderStatuses->pluck('id')->toArray() : old('subordinate_team_ids'))

                                                                @foreach($teams as $team)
                                                                    <div class="custom-control custom-checkbox mb-1">
                                                                        <input type="checkbox" value="{{ $team->id }}" @isset($old_s_t_ids) {{ in_array($team->id, $old_s_t_ids)?'checked':null }} @endisset name="subordinate_team_ids[]" class="custom-control-input" id="team{{ $team->id }}">
                                                                        <label class="custom-control-label" for="team{{ $team->id }}">{{ $team->name }}</label>
                                                                    </div>
                                                                @endforeach
                                                            </li>

                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group col-md-6">
                                                <label for="feInputState" class="input-required">Statuses</label>
                                                <div class='card card-small mb-3'>
                                                    <div class="card-header border-bottom">
                                                        <h6 class="m-0">Order Status Access</h6>
                                                    </div>
                                                    <div class='card-body p-0'>
                                                        <ul class="list-group list-group-flush">
                                                            <li class="list-group-item px-3 pb-2">

                                                                @php($old_s_t_ids = isset($selectedTeam)?$selectedTeam->subordinateTeams->pluck('id')->toArray() : old('subordinate_team_ids'))

                                                                @foreach($orderStatuses as $status)
                                                                    <div class="custom-control custom-checkbox mb-1">
                                                                        <input type="checkbox" value="{{ $status->id }}" name="order_status_ids[]" class="custom-control-input" id="status{{ $status->id }}">
                                                                        <label class="custom-control-label" for="status{{ $status->id }}">{{ $status->name }}</label>
                                                                    </div>
                                                                @endforeach
                                                            </li>

                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <button type="submit" class="btn btn-accent">{{ isset($department)?'Update':'Create' }}</button>
                                    </form>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- End Default Light Table -->
    </div>
@endsection