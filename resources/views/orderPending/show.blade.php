@extends('layouts.master')

@section('content')

    @include('layouts.partial.notice')

    <div class="main-content-container container-fluid px-4">
        <!-- Page Header -->
        <div class="page-header row no-gutters py-4">
            <div class="col-12 col-sm-4 text-center text-sm-left mb-4 mb-sm-0">
                <span class="text-uppercase page-subtitle">Overview</span>
                <h3 class="page-title">{{ $pageData['pageName'] }} Analytics {{ ucwords(request()->get('page')) }}</h3>
            </div>
            <div class="col-12 col-sm-4 d-flex align-items-center">
                <div class="btn-group btn-group-sm btn-group-toggle d-inline-flex mb-4 mb-sm-0 mx-auto" role="group" aria-label="Page actions">
                    <a href="{{ route($pageData['routeFirstName'].'-show', $order->id) }}"
                       class="btn btn-white {{ (request()->get('page') == null)?'active':null }}"> Order </a>
                    <a href="{{ route($pageData['routeFirstName'].'-show', [$order->id, 'page' => 'budget']) }}"
                       class="btn btn-white {{ (request()->get('page') == 'budget')?'active':null }}"> Budget </a>
                    <a href="{{ route($pageData['routeFirstName'].'-show', [$order->id, 'page' => 'manufacturing']) }}"
                       class="btn btn-white {{ (request()->get('page') == 'manufacturing')?'active':null }}"> Manufacturing </a>
                    <a href="{{ route($pageData['routeFirstName'].'-show', [$order->id, 'page' => 'history']) }}"
                       class="btn btn-white {{ (request()->get('page') == 'history')?'active':null }}"> History </a>
                </div>
            </div>
        </div>
        <!-- End Page Header -->
        <!-- Small Stats Blocks -->
        @if(request()->get('page') == 'manufacturing')
            @include('orderDetails.order_manufacturing_details')
        @elseif(request()->get('page') == 'budget')
            @include('orderDetails.order_budget_details')
        @elseif(request()->get('page') == 'history')
            @include('orderDetails.order_history_details')
        @else
            @include('orderDetails.order_details')
        @endif

    </div>
@endsection

@section('vue-script')
    <script>
        function simulateKey(iElement) {
            $( iElement ).focus();
            let e = jQuery.Event("keyup");
            e.which = 39;
            e.keyCode = 39;
            e.charCode = 0;
            $( iElement ).trigger(e);
        }

        $(function () {
            let laravelBelongsName;
            let $eventSelect = $('.select2-box').select2({
                placeholder: "Select...",
            });

            $eventSelect.on("select2:open", function () {

                let laravelModelName = $(this).attr('laravel-model');
                laravelBelongsName = $(this).attr('laravel-belongs') || '';
                let url = '{{ route('api-get-select-dropdown-data') }}/'+laravelModelName+'/'+laravelBelongsName;
                axios
                    .get(url)
                    .then(response => {
                        $(this).html('<option>');

                        (response.data).forEach(val => {
                                laravelBelongsName = snakeCase(laravelBelongsName);
                                let name = capitalizeFirstLetter(val.name);

                                let sanitizeVal = val[laravelBelongsName]?{id:val.id, text: name+' ('+capitalizeFirstLetter(val[laravelBelongsName].name)+')'}: {id:val.id, text:name};

                                $(this).append('<option value="'+sanitizeVal.id+'">'+sanitizeVal.text+'</option>');
                                simulateKey($('.select2-search__field'));
                            });

                        }
                    )
                ;
            });

        });

        const data = {
            userImg: '{{ isset($getUser) ? $getUser->GetProfilePicture():asset('profile-pic/garments/garment.png') }}',
            fullName: '{{ isset($getUser) ? $getUser->name:old('name') }}',
            buyerName: '{{ isset($getUser) ? $getUser->role->name:'' }}',
            aboutUser: '{{ isset($getUser) ? $getUser->description:'' }}',
            teams: [],
            teamsPhpView: true,
        };

        const app = new Vue({
            el: '#app',
            data,
            methods: {

            },
            mounted(){

            },
            computed: {


            }

        });
    </script>
@endsection

