@php

$pieChartMaterials = [];

@endphp

@extends('layouts.master')

@section('content')

    @include('layouts.partial.notice')

    <div class="main-content-container container-fluid px-4">
        <!-- Page Header -->
        <div class="page-header row no-gutters py-4">
            <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
                <span class="text-uppercase page-subtitle">Overview</span>
                <h3 class="page-title">{{ $pageData['pageName'] }} List</h3>
            </div>

        </div>
        <div class="row">
            <div class="col-lg-12 mb-3">
                <div id="accordion" style="cursor: pointer">
                    <div class="card card-small mb-1 rounded">
                        <div class="card-header border-bottom rounded">
                            <h6 class="m-0 card-link" data-toggle="collapse" href="#collapseThree">
                                Yearly Chart
                            </h6>
                            <div class="block-handle"></div>
                        </div>
                        <div id="collapseThree" class="collapse show" data-parent="#accordion">
                            <div class="card-body pt-0">
                                <ul class="list-group list-group-flush">
                                    <li class="list-group-item p-2">
                                        <div class="row" style="height: 400px;">
                                            <div class="col-md-12">
                                                <div id="columnChart"></div>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="card card-small mb-1 rounded">
                        <div class="card-header border-bottom rounded">
                            <h6 class="m-0 card-link" data-toggle="collapse" href="#collapseFour">
                                Pending Summery
                            </h6>
                            <div class="block-handle"></div>
                        </div>
                        <div id="collapseFour" class="collapse show" data-parent="#accordion">
                            <div class="card-body pt-0">
                                <ul class="list-group list-group-flush">
                                    <li class="list-group-item p-2">
                                        <div class="row">
                                            @php
                                                $grandTot = [];
                                            @endphp

                                            @foreach($allPendingOrder as $ym => $pendingOrds)
                                                <div class="col-12 col-md-4 col-lg-4 mb-4">
                                                    <div class="stats-small card card-small">
                                                        <div class="card-body px-0 pb-0"><div class="chartjs-size-monitor" style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;"><div class="chartjs-size-monitor-expand" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"><div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"></div></div><div class="chartjs-size-monitor-shrink" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"><div style="position:absolute;width:200%;height:200%;left:0; top:0"></div></div></div>
                                                            <div class="px-3">
                                                                <div class="">
                                                                    <span class="mb-1">{{ $ym }} [ {{ Carbon::parse($ym)->shortEnglishMonth }} ]</span>

                                                                    <?php
                                                                        $qtyArr = [];
                                                                        $valArr = [];
                                                                        foreach ($pendingOrds as $pendingOrd){
                                                                            $qty = $pendingOrd->quantity->sum('quantity');
                                                                            $qtyArr[] = $qty;
                                                                            $valArr[] = $pendingOrd->unit_price * $qty;

                                                                            $grandTot['qty'][] = $qty;
                                                                            $grandTot['val'][] = $pendingOrd->unit_price * $qty;
                                                                        }
                                                                    $grandTot['ord'][] = count($pendingOrds);
                                                                    ?>
                                                                    <table class="table table-striped table-bordered">
                                                                        <tr>
                                                                            <th>Ord</th>
                                                                            <th>Qty</th>
                                                                            <th>Value</th>
                                                                        </tr>
                                                                        <tr>
                                                                            <td><b class="text-danger">{{ count($pendingOrds) }}</b></td>
                                                                            <td>{{ array_sum($qtyArr) }}</td>
                                                                            <td>${{ array_sum($valArr) }}</td>
                                                                        </tr>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                            <canvas height="49" class="analytics-overview-stats-small-1 chartjs-render-monitor" width="247" style="display: block; width: 247px; height: 49px;"></canvas>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                                <div class="col-12 col-md-12 col-lg-12 mb-4">
                                                    <div class="stats-small card card-small">
                                                        <div class="card-body px-0 pb-0"><div class="chartjs-size-monitor" style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;"><div class="chartjs-size-monitor-expand" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"><div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"></div></div><div class="chartjs-size-monitor-shrink" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"><div style="position:absolute;width:200%;height:200%;left:0; top:0"></div></div></div>
                                                            <div class="px-3">
                                                                <div class="">
                                                                    <h4 class="mb-1">Grand Total</h4>
                                                                    <table class="table table-striped table-bordered">
                                                                        <tr>
                                                                            <th>Total Order</th>
                                                                            <th>Total Order Quantity</th>
                                                                            <th>Total Order Value</th>
                                                                        </tr>
                                                                        <tr>
                                                                            <td><b class="text-danger">{{ array_sum($grandTot['ord']) }}</b></td>
                                                                            <td>{{ array_sum($grandTot['qty']) }}</td>
                                                                            <td>${{ array_sum($grandTot['val']) }}</td>
                                                                        </tr>
                                                                    </table>
                                                                </div>
                                                            </div>

                                                            <div id="grandPending"></div>

                                                        </div>
                                                    </div>
                                                </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="card card-small mb-1 rounded">
                        <div class="card-header border-bottom rounded">
                            <h6 class="m-0 card-link" data-toggle="collapse" href="#collapseOne">
                                Chart
                            </h6>
                            <div class="block-handle"></div>
                        </div>
                        <div id="collapseOne" class="collapse" data-parent="#accordion">
                            <div class="card-body pt-0">
                                <ul class="list-group list-group-flush">
                                    <li class="list-group-item p-2">
                                        <div class="row" style="height: 400px;">
                                            <div class="col-md-6">
                                                <div id="container"></div>
                                            </div>
                                            <div class="col-md-6">
                                                <div id="container2"></div>
                                            </div>

                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="card card-small mb-1">
                        <div class="card-header border-bottom rounded">
                            <h6 class="m-0 collapsed card-link w-100" data-toggle="collapse" href="#collapseTwo">
                                Summery
                            </h6>
                            <div class="block-handle"></div>
                        </div>
                        <div id="collapseTwo" class="collapse" data-parent="#accordion">
                            <table class="summary-table text-capitalize">
                                <thead>
                                <tr>
                                    <th>SL</th>
                                    <th>Buyer</th>
                                    <th>Qty</th>
                                    <th>%</th>
                                    <th>Value</th>
                                    <th>%</th>
                                    <th>ShipQty</th>
                                    <th>%</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr v-for="(list, key, i) in buyerWiseSummaries">
                                    <td>@{{ ++i }}</td>
                                    <td>@{{ key }}</td>
                                    <td>@{{ list.quantity.reduce(getSum,0) }}</td>
                                    <td>@{{ roundWithFrac((list.quantity.reduce(getSum,0)/orderSummerySum('quantity'))*100) }}</td>
                                    <td>@{{ list.value.reduce(getSum,0) }}</td>
                                    <td>@{{ roundWithFrac((list.value.reduce(getSum,0)/orderSummerySum('value'))*100) }}</td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{--Star filter form--}}
        @include('layouts.tools.filterFormForReport')
        {{--End filter form--}}
        <div class="row">
            <div class="col-lg-12 mb-3">
                <div class="card card-small">
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item p-2">
                            <div class="row">
                                <div class="col-md-2">
                                    <b>Total : </b> <span>{{ $count }} {{ ($count>1)?Str::plural($pageData['pageName']):$pageData['pageName'] }} Found</span>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <div class="card card-small mb-4">
                    <div class="card-header border-bottom">
                        {{--From Controller--}}
                        <h6 class="m-0">Active {{ Str::plural($pageData['pageName']) }}</h6>
                    </div>
                    <div class="card-body p-0 text-center pre-x-scrollable">
                        <!-- Transaction History Table -->
                        <table class="main-data-table d-none text-capitalize" id="main-table">
                            <thead>
                            <tr>
                                <th>SL</th>
                                <th>ID</th>
                                <th>Buyer</th>
                                <th>OrderName</th>
                                <th>Style</th>
                                <th>StyleDesc</th>
                                <th>Image</th>
                                <th>Shipment</th>
                                <th style="background: #ffd3b0;color: #000;">RemDay</th>
                                <th>OrderQty</th>
                                <th>UnitPrice</th>
                                <th>TotValue</th>
                                <th style="background: #93a8ff;color: #000;">ShipQty</th>
                                <th>ShipValue</th>
                                <th>ShortShipVal</th>
                                <th>Curr TNA</th>
                                <th>Status</th>
                                <th>CM</th>
                                <th>SMV</th>

                                <th>Cutting</th>
                                <th>Cutting%</th>
                                <th>SwingIn</th>
                                <th>SwingOut</th>
                                <th>Iron</th>
                                <th>Poly</th>
                                {{--<th>LC/Sales</th>--}}
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($orders as $order)
                                @php
                                        $pieChartMaterials[$order->buyer->name]['quantity'][] = $order->quantity->sum('quantity');
                                        $pieChartMaterials[$order->buyer->name]['value'][] = $order->quantity->sum('quantity')*$order->unit_price;

                                        $dateOfShip = $order->date_of_ship;
                                        $dateForHumans = Carbon::parse($dateOfShip)->diffForHumans(Carbon::now());
                                        $diff = -Carbon::parse($dateOfShip)->diffInDays(Carbon::now(), false)
                                @endphp
                            <tr>
                                {{--SL--}}                  <td>1</td>
                                {{--Job ID--}}              <td>{{ $order->id }}</td>
                                {{--Byuer--}}               <td>{{ $order->buyer->name }}</td>
                                {{--order name--}}          <td>{{ $order->name }}</td>
                                {{--style name--}}          <td>{{ $order->style }}</td>
                                {{--style desc--}}          <td>{{ $order->style_desc }}</td>
                                {{--getGarmentsPicture--}}  <td><img class="zoom" width="50" src="{{ $order->getGarmentsPicture() }}" alt=""></td>
                                {{--Shipment Date--}}       <td>{{ $dateOfShip }}</td>
                                {{--Rem Day--}}             <td> <div class="badge {{ ($diff<=10)?'badge-danger':'badge-secondary' }}">{{ $diff }}</div><br> {{ $dateForHumans }}</td>
                                {{--Order Qty--}}           <td>{{ $order->quantity->sum('quantity') }}</td>
                                {{--Unit Price--}}          <td>{{ $order->unit_price }}</td>
                                {{--Total Price--}}         <td>{{ $totalValue = $order->quantity->sum('quantity')*$order->unit_price }}</td>
                                {{--Ship Qty--}}            <td>{{ $shipmentQty = $order->shipment->sum('quantity') }}</td>
                                {{--Ship value--}}          <td>{{ $shipmentQty*$order->unit_price }}</td>
                                {{--Short Ship value--}}    <td>{{ $shipmentQty?$shipmentQty*$order->unit_price-$totalValue:null }}</td>
                                {{--Order Status--}}        <td>{{ $order->status->name ?? null }}</td>
                                {{--Order Status--}}        <td style="@if($order->statuses=='ShipOut') background: #794d53; color:#fff; @elseif($order->statuses=='Partial') background: #797750; color:#fff; @endif">{{ $order->statuses }}</td>
                                {{--CM from budget table--}}<td>{{ $order->budget->cm ?? null }}</td>
                                {{--SMV--}}                 <td>{{ $order->smv }}</td>
                                {{--Sales--}}               {{--<td>{{ ucfirst($order->sales_user->name) }}</td>--}}

                                {{--Cutting--}}             <td>{{ $chartMaterials['cutting'][] = $order->cutting->sum('quantity') }}</td>
                                                                @php
                                                                    $ordQty = $order->quantity->sum('quantity');
                                                                    if ($ordQty != 0){
                                                                        $cutPerc = round(($order->cutting->sum('quantity') - $ordQty)/$ordQty*100, 2);
                                                                        $str = '%';
                                                                    } else {
                                                                        $cutPerc = null;
                                                                        $str = 'Order Qty is 0';
                                                                    }
                                                                @endphp
                                {{--Cutting--}}             <td class="@if($cutPerc > 5) bg-danger text-light @endif"> {{ $cutPerc }} {{ $str }}</td>
                                {{--Swing_in--}}            <td>{{ $chartMaterials['swingIn'][] = $order->swingIn->sum('quantity') }}</td>
                                {{--Swing_out--}}           <td>{{ $chartMaterials['swingOut'][] = $order->swingOut->sum('quantity') }}</td>
                                {{--Iron--}}                <td>{{ $chartMaterials['iron'][] = $order->iron->sum('quantity') }}</td>
                                {{--Packing--}}             <td>{{ $chartMaterials['packing'][] = $order->packing->sum('quantity') }}</td>

                                <td>
                                    <div class="btn-group btn-group-sm" role="group" aria-label="Table row actions">
                                        {{--<button type="button" class="btn btn-white">
                                            <i class="material-icons">&#xE5CA;</i>
                                        </button>--}}
                                        <a class="btn btn-white" href="{{ route($pageData['routeFirstName'].'-show', [$order->id, 'page' => 'history']) }}">
                                            <i class="material-icons">&#xE870;</i>
                                        </a>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                            </tbody>
                            <tfoot>
                            <tr>
                                <th>SL</th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th style="background: #93a8ff;color: #000;"></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                {{--<th>LC/Sales</th>--}}
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                            </tr>
                            </tfoot>
                        </table>
                        <!-- End Transaction History Table -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="promo-popup animated bounceIn" style="padding: 10px; border: 2px solid #898989; background: rgba(255,255,255,0.82); right: 10px;">

        <div>
            <a href="#accordion">
            <span class="close">
                <i class="material-icons">pie_chart</i>
            </span>
            </a>
        </div>


        <div>
            <a href="#search-area">
            <span class="close">
                <i class="material-icons">format_align_left</i>
            </span>
            </a>
        </div>


    </div>
@endsection

@section('vue-script')

    {{--start php code--}}
    <?php

    $orderModel = new App\Order();

    ?>
    {{--end php code--}}

    @include('js/script')
    <script>
        function dataTableFooterSum(obj) {
            let tbl = obj.table;
            [9, 10, 11, 12, 13, 14, 17, 18, 19, 21, 22, 23, 24].forEach(function (col) {
                $(tbl.column(col).footer()).html(
                    tbl.column(col).data().sum()
                );
            });
        }


        function piData(type) {
            let $pieChartMaterials = JSON.parse('{{ json_encode($pieChartMaterials) }}'.replace(/&quot;/g,'"'));

            return Object.keys($pieChartMaterials).map(function (k) {
                let val = $pieChartMaterials[k][type].reduce(function (qty, pilot) {
                    return qty + pilot
                },0);
                return {name: k, y:val};
            });
        }

        //summary-table
        vm.buyerWiseSummaries = JSON.parse('{{ json_encode($pieChartMaterials) }}'.replace(/&quot;/g,'"'));

        let piObj = {
            elm: 'container',
            seriesName: 'Quantity',
            heading: 'Buyer Wise Pending Quantity',
        };
        let piObj2 = {
            elm: 'container2',
            seriesName: 'Value',
            heading: 'Buyer Wise Pending Value',
        };

        Highcharts.chart('columnChart', {

            chart: {
                type: 'column'
            },

            title: {
                text: 'Monthly Order'
            },

            xAxis: {
                categories: [
                    @foreach($orderModel->pendingOrderYearWiseMonth(2019)[0] as $month => $monthlyOrder)
                        '{{ $month }}',
                    @endforeach
                ],
            },

            yAxis: [{
                title: {
                    text: 'Quantity axis'
                }
            },
                {
                    opposite: true,
                    title: {
                        text: 'Value axis'
                    }
                }],

            plotOptions: {
                series: {
                    borderWidth: 0,
                    /*dataLabels: {
                        enabled: true,
                        format: '{point.y:.f}'
                    }*/
                },
                column: {
                    pointPadding: 0.05,
                    borderRadius: 3
                }
            },

            series: [{
                name: 'Quantity',
                data: [
                    @foreach($orderModel->pendingOrderYearWiseMonth(2019)[0] as $month => $monthlyOrder)
                    {{ array_sum($orderModel->pendingOrderYearWiseMonth(2019)[1][$month] ?? []) }},
                    @endforeach
                ]
            }, {
                name: 'Value',
                data: [
                    @foreach($orderModel->pendingOrderYearWiseMonth(2019)[0] as $month => $monthlyOrder)
                    {{ array_sum($orderModel->pendingOrderYearWiseMonth(2019)[2][$month] ?? []) }},
                    @endforeach
                ],
                yAxis: 1
            }],
            credits: {
                enabled: false
            },

        });

        Highcharts.chart('grandPending', {
            chart: {
                type: 'areaspline'
            },
            title: {
                text: 'Pending Order Timeline'
            },
            legend: {
                layout: 'vertical',
                align: 'left',
                verticalAlign: 'top',
                x: 150,
                y: 100,
                floating: true,
                borderWidth: 1,
                backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
            },
            xAxis: {
                categories: [
                    @foreach($allPendingOrder as $ym => $pendingOrds)
                    '{{ $ym }}',
                    @endforeach
                ],
                plotBands: [{ // visualize the weekend
                    from: 4.5,
                    to: 6.5,
                    color: 'rgba(68, 170, 213, .2)'
                }]
            },
            yAxis: {
                title: {
                    text: 'Pending Quantity'
                }
            },
            tooltip: {
                shared: true,
            },
            credits: {
                enabled: false
            },
            plotOptions: {
                areaspline: {
                    fillOpacity: 0.5
                }
            },
            series: [{
                name: 'Quantity',
                data: [
                    @foreach($allPendingOrder as $ym => $pendingOrds)
                    <?php
                    $qtyArr = [];
                    foreach ($pendingOrds as $pendingOrd){
                        $qty = $pendingOrd->quantity->sum('quantity');
                        $qtyArr[] = $qty;
                    }
                    ?>
                    {{ array_sum($qtyArr) }},
                    @endforeach
                ]
            }, {
                name: 'Value',
                data: [
                    @foreach($allPendingOrder as $ym => $pendingOrds)
                    <?php
                    $valArr = [];
                    foreach ($pendingOrds as $pendingOrd){
                        $qty = $pendingOrd->quantity->sum('quantity');
                        $valArr[] = $pendingOrd->unit_price * $qty;
                    }
                    ?>
                    {{ array_sum($valArr) }},
                    @endforeach
                ]
            }]
        });

        chartNSummery(piObj, piData('quantity'));
        chartNSummery(piObj2, piData('value'));

        function chartNSummery($piObj, data) {
            // Build the chart
            Highcharts.chart($piObj.elm, {
                // Radialize the colors
                colors: Highcharts.map(Highcharts.getOptions().colors, function (color) {
                    return {
                        radialGradient: {
                            cx: 0.5,
                            cy: 0.3,
                            r: 0.7
                        },
                        stops: [
                            [0, color],
                            [1, Highcharts.Color(color).brighten(-0.3).get('rgb')] // darken
                        ]
                    };
                }),
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie'
                },
                title: {
                    text: $piObj.heading
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.y}</b>'
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: true,
                            format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                            style: {
                                color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                            },
                            connectorColor: 'silver'
                        }
                    }
                },
                series: [{
                    name: $piObj.seriesName,
                    data
                }],
                credits: {
                    enabled: false
                },
            });
        }
    </script>
@endsection