@php
use \Illuminate\Support\Carbon;
$chartMaterials = [];

@endphp

@extends('layouts.master')

@section('content')

    @include('layouts.partial.notice')

    <div class="main-content-container container-fluid px-4">
        <!-- Page Header -->
        <div class="page-header row no-gutters py-4">
            <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
                <span class="text-uppercase page-subtitle">Overview</span>
                <h3 class="page-title">{{ $pageData['pageName'] }} List</h3>
            </div>
            <div class="float-right col-12 col-sm-8 text-center text-sm-right">
                <span class="text-uppercase page-subtitle">Action</span>
                <h3 class="page-title">
                    <a href="{{ route($pageData['routeFirstName'].'-create') }}" class="btn btn-md btn-white">
                        <i class="material-icons">control_point</i> Add {{ $pageData['pageName'] }}
                    </a>
                </h3>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 mb-3">
                <div id="accordion" style="cursor: pointer">
                    <div class="card card-small mb-1 rounded">
                        <div class="card-header border-bottom rounded">
                            <h6 class="m-0 card-link" data-toggle="collapse" href="#collapseOne">
                                Chart
                            </h6>
                            <div class="block-handle"></div>
                        </div>
                        <div id="collapseOne" class="collapse show" data-parent="#accordion">
                            <div class="card-body pt-0">
                                <ul class="list-group list-group-flush">
                                    <li class="list-group-item p-2">
                                        <div class="row" style="height: 400px;">
                                            <div class="col-md-12">
                                                <div id="production-chart"></div>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        {{--Star filter form--}}
        @include('layouts.tools.filterFormForReport')
        {{--End filter form--}}
        <div class="row">
            <div class="col-lg-12 mb-3">
                <div class="card card-small">
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item p-2">
                            <div class="row">
                                <div class="col-md-2">
                                    <b>Total : </b> <span>{{ $count }} {{ ($count>1)?Str::plural($pageData['pageName']):$pageData['pageName'] }} Found</span>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <div class="card card-small mb-4">
                    <div class="card-header border-bottom">
                        {{--From Controller--}}
                        <h6 class="m-0">Active {{ Str::plural($pageData['pageName']) }}</h6>
                    </div>
                    <div class="card-body p-0 text-center pre-x-scrollable">
                        <!-- Transaction History Table -->
                        <table class="main-data-table d-none text-capitalize" id="main-table">
                            <thead>
                            <tr>
                                <th>SL</th>
                                <th>ID</th>
                                <th>Buyer</th>
                                <th>OrderName</th>
                                <th>Style</th>
                                <th>StyleDesc</th>
                                <th>Image</th>
                                <th>Shipment</th>
                                <th style="background: #ffd3b0;color: #000;">RemDay</th>
                                <th>OrderQty</th>
                                <th>UnitPrice</th>
                                <th>TotValue</th>
                                <th style="background: #93a8ff;color: #000;">ShipQty</th>
                                <th>ShipValue</th>
                                <th>ShortShipVal</th>
                                <th>Curr TNA</th>
                                <th>Status</th>
                                <th>CM</th>
                                <th>SMV</th>

                                <th>Cutting</th>
                                <th>SwingIn</th>
                                <th>SwingOut</th>
                                <th>Iron</th>
                                <th>Poly</th>
                                {{--<th>LC/Sales</th>--}}
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($orders as $order)
                                @php
                                        $chartMaterials['order_quantity'][] = $order->quantity->sum('quantity');
                                        $chartMaterials['shipment_quantity'][] = $order->shipment->sum('quantity');

                                        $dateOfShip = $order->date_of_ship;
                                        $dateForHumans = Carbon::parse($dateOfShip)->diffForHumans(Carbon::now());
                                        $diff = -Carbon::parse($dateOfShip)->diffInDays(Carbon::now(), false)
                                @endphp
                            <tr>
                                {{--SL--}}                  <td>1</td>
                                {{--Job ID--}}              <td>{{ $order->id }}</td>
                                {{--Byuer--}}               <td>{{ $order->buyer->name }}</td>
                                {{--order name--}}          <td>{{ $order->name }}</td>
                                {{--style name--}}          <td>{{ $order->style }}</td>
                                {{--style desc--}}          <td>{{ $order->style_desc }}</td>
                                {{--getGarmentsPicture--}}  <td><img class="zoom" width="50" src="{{ $order->getGarmentsPicture() }}" alt=""></td>
                                {{--Shipment Date--}}       <td>{{ $dateOfShip }}</td>
                                {{--Rem Day--}}             <td> <div class="badge {{ ($diff<=10)?'badge-danger':'badge-secondary' }}">{{ $diff }}</div><br> {{ $dateForHumans }}</td>
                                {{--Order Qty--}}           <td>{{ $order->quantity->sum('quantity') }}</td>
                                {{--Unit Price--}}          <td>{{ $order->unit_price }}</td>
                                {{--Total Price--}}         <td>{{ $totalValue = $order->quantity->sum('quantity')*$order->unit_price }}</td>
                                {{--Ship Qty--}}            <td>{{ $shipmentQty = $order->shipment->sum('quantity') }}</td>
                                {{--Ship value--}}          <td>{{ $shipmentQty*$order->unit_price }}</td>
                                {{--Short Ship value--}}    <td>{{ $shipmentQty?$shipmentQty*$order->unit_price-$totalValue:null }}</td>
                                {{--Order Status--}}        <td>{{ isset($order->status) ? $order->status->name:null }}</td>
                                {{--Order Status--}}        <td style="@if($order->statuses=='ShipOut') background: #794d53; color:#fff; @elseif($order->statuses=='Partial') background: #797750; color:#fff; @endif">{{ $order->statuses }}</td>
                                {{--CM from budget table--}}<td>{{ $order->budget->cm ?? null }}</td>
                                {{--SMV--}}                 <td>{{ $order->smv }}</td>
                                {{--Sales--}}               {{--<td>{{ ucfirst($order->sales_user->name) }}</td>--}}

                                {{--Cutting--}}             <td>{{ $chartMaterials['cutting'][] = $order->cutting->sum('quantity') }}</td>
                                {{--Swing_in--}}            <td>{{ $chartMaterials['swingIn'][] = $order->swingIn->sum('quantity') }}</td>
                                {{--Swing_out--}}           <td>{{ $chartMaterials['swingOut'][] = $order->swingOut->sum('quantity') }}</td>
                                {{--Iron--}}                <td>{{ $chartMaterials['iron'][] = $order->iron->sum('quantity') }}</td>
                                {{--Packing--}}             <td>{{ $chartMaterials['packing'][] = $order->packing->sum('quantity') }}</td>
                                <td>
                                    <div class="btn-group btn-group-sm" role="group" aria-label="Table row actions">
                                        {{--<button type="button" class="btn btn-white">
                                            <i class="material-icons">&#xE5CA;</i>
                                        </button>--}}
                                        <a class="btn btn-white" href="{{ route($pageData['routeFirstName'].'-show', [$order->id, 'page' => 'manufacturing']) }}">
                                            <i class="material-icons">&#xE870;</i>
                                        </a>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                            </tbody>
                            <tfoot>
                            <tr>
                                <th>SL</th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th style="background: #93a8ff;color: #000;"></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                {{--<th>LC/Sales</th>--}}
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                            </tr>
                            </tfoot>
                        </table>
                        <!-- End Transaction History Table -->
                    </div>
                </div>
                {{--{{ dd(array_sum($chartMaterials['order_quantity'])) }}--}}
                {{ $orders->links() }}
            </div>
        </div>
    </div>
    <div class="promo-popup animated bounceIn" style="padding: 10px; border: 2px solid #898989; background: rgba(255,255,255,0.82); right: 10px;">

        <div>
            <a href="#accordion">
            <span class="close">
                <i class="material-icons">pie_chart</i>
            </span>
            </a>
        </div>


        <div>
            <a href="#search-area">
            <span class="close">
                <i class="material-icons">format_align_left</i>
            </span>
            </a>
        </div>


    </div>
@endsection

@section('vue-script')
    <script>
        function dataTableFooterSum(obj) {
            let tbl = obj.table;
            [9, 10, 11, 12, 13, 14, 17, 18, 19, 20, 21, 22, 23]
                .forEach(function (col) {
                $(tbl.column(col).footer()).html(
                    tbl.column(col).data().sum()
                );
            });
        }



        Highcharts.chart('production-chart', {
            colors: Highcharts.map(Highcharts.getOptions().colors, function (color) {
                return {
                    radialGradient: {
                        cx: 0.5,
                        cy: 0.3,
                        r: 0.7
                    },
                    stops: [
                        [0, color],
                        [1, Highcharts.Color(color).brighten(-0.3).get('rgb')] // darken
                    ]
                };
            }),
            chart: {
                type: 'column'
            },
            title: {
                text: 'Production Chart'
            },
            xAxis: {
                type: 'category'
            },

            legend: {
                enabled: false
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        format: '{point.y}'
                    }
                }
            },

            tooltip: {
                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
            },

            "series": [
                {
                    "name": "Production",
                    "colorByPoint": false,
                    "data": [
                        {
                            "name": "OrderQty",
                            "y": {{ array_sum($chartMaterials['order_quantity']) }}
                        },
                        {
                            "name": "Ship Qty",
                            "y": {{ array_sum($chartMaterials['shipment_quantity']) }}
                        },
                        {
                            "name": "Cutting",
                            "y": {{ array_sum($chartMaterials['cutting']) }}
                        },
                        {
                            "name": "SwIng input",
                            "y": {{ array_sum($chartMaterials['swingIn']) }}
                        },
                        {
                            "name": "Sewing output",
                            "y": {{ array_sum($chartMaterials['swingOut']) }}
                        },
                        {
                            "name": "Iron",
                            "y": {{ array_sum($chartMaterials['iron']) }}
                        },
                        {
                            "name": "Poly",
                            "y": {{ array_sum($chartMaterials['packing']) }}
                        }
                    ]
                }
            ]
        });
    </script>
@endsection