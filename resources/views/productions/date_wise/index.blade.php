@php
use \Illuminate\Support\Carbon;
$pieChartMaterials = [];

@endphp

@extends('layouts.master')

@section('content')

    @include('layouts.partial.notice')

    <div class="main-content-container container-fluid px-4">
        <!-- Page Header -->
        <div class="page-header row no-gutters py-4">
            <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
                <span class="text-uppercase page-subtitle">Overview</span>
                <h3 class="page-title">{{ $pageData['pageName'] }} List</h3>
            </div>
            <div class="float-right col-12 col-sm-8 text-center text-sm-right">
                <span class="text-uppercase page-subtitle">Action</span>
                <h3 class="page-title">
                    <a href="{{ route($pageData['routeFirstName'].'-create') }}" class="btn btn-md btn-white">
                        <i class="material-icons">control_point</i> Add {{ $pageData['pageName'] }}
                    </a>
                </h3>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 mb-3">
                <div id="accordion" style="cursor: pointer">
                    <div class="card card-small mb-1 rounded">
                        <div class="card-header border-bottom rounded">
                            <h6 class="m-0 card-link" data-toggle="collapse" href="#collapseOne">
                                Chart
                            </h6>
                            <div class="block-handle"></div>
                        </div>
                        <div id="collapseOne" class="collapse show" data-parent="#accordion">
                            <div class="card-body pt-0">
                                <ul class="list-group list-group-flush">
                                    <li class="list-group-item p-2">
                                        <div class="row" style="height: 400px;">
                                            <div class="col-md-12">
                                                <div id="container"></div>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        {{--Star filter form--}}
        @include('layouts.tools.filterFormForReport')
        {{--End filter form--}}
        <div class="row">
            <div class="col-lg-12 mb-3">
                <div class="card card-small">
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item p-2">
                            <div class="row">
                                <div class="col-md-2">
                                    <b>Total : </b> <span>{{ $count }} {{ ($count>1)?Str::plural($pageData['pageName']):$pageData['pageName'] }} Found</span>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <div class="card card-small mb-4">
                    <div class="card-header border-bottom">
                        {{--From Controller--}}
                        <h6 class="m-0">Active {{ Str::plural($pageData['pageName']) }}</h6>
                    </div>
                    <div class="card-body p-0 text-center pre-x-scrollable">
                        <!-- Transaction History Table -->
                        <table class="main-data-table d-none text-capitalize" id="main-table">
                            <thead>
                            <tr>
                                <th>SL</th>
                                <th>ID</th>
                                <th>Buyer</th>
                                <th>OrderName</th>
                                <th>Style</th>
                                <th>StyleDesc</th>
                                <th>Image</th>
                                <th>Shipment</th>
                                <th style="background: #ffd3b0;color: #000;">RemDay</th>
                                <th>OrderQty</th>
                                <th>UnitPrice</th>
                                <th>TotValue</th>
                                <th>Curr TNA</th>
                                <th>Status</th>
                                <th>CM</th>
                                <th>SMV</th>

                                <th>Date</th>
                                <th>Cutting</th>
                                <th>SwingIn</th>
                                <th>SwingOut</th>
                                <th>Iron</th>
                                <th>Poly</th>
                                {{--<th>LC/Sales</th>--}}
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(isset($unique_productions))
                                @foreach($unique_productions as $production)
                                    @php
                                            $dateOfShip = $production->order->date_of_ship;
                                            $dateForHumans = Carbon::parse($dateOfShip)->diffForHumans(Carbon::now());
                                            $diff = -Carbon::parse($dateOfShip)->diffInDays(Carbon::now(), false)
                                    @endphp
                                <tr>
                                    {{--SL--}}                  <td>1</td>
                                    {{--Job ID--}}              <td>{{ $production->order->id }}</td>
                                    {{--Byuer--}}               <td>{{ $production->order->buyer->name }}</td>
                                    {{--order name--}}          <td>{{ $production->order->name }}</td>
                                    {{--style name--}}          <td>{{ $production->order->style }}</td>
                                    {{--style desc--}}          <td>{{ $production->order->style_desc }}</td>
                                    {{--getGarmentsPicture--}}  <td><img class="zoom" width="50" src="{{ $production->order->getGarmentsPicture() }}" alt=""></td>
                                    {{--Shipment Date--}}       <td>{{ $dateOfShip }}</td>
                                    {{--Rem Day--}}             <td> <div class="badge {{ ($diff<=10)?'badge-danger':'badge-secondary' }}">{{ $diff }}</div><br> {{ $dateForHumans }}</td>
                                    {{--Order Qty--}}           <td>{{ $production->order->quantity->sum('quantity') }}</td>
                                    {{--Unit Price--}}          <td>{{ $production->order->unit_price }}</td>
                                    {{--Total Price--}}         <td>{{ $totalValue = $production->order->quantity->sum('quantity')*$production->order->unit_price }}</td>
                                    {{--Order Status--}}        <td>{{ $production->order->status->name ?? null }}</td>
                                    {{--Order Status--}}        <td style="@if($production->order->statuses=='ShipOut') background: #794d53; color:#fff; @elseif($production->order->statuses=='Partial') background: #797750; color:#fff; @endif">{{ $production->order->statuses }}</td>
                                    {{--CM from budget table--}}<td>{{ $production->order->budget->cm ?? null }}</td>
                                    {{--SMV--}}                 <td>{{ $production->order->smv }}</td>
                                    {{--Sales--}}               {{--<td>{{ ucfirst($production->order->sales_user->name) }}</td>--}}

                                    {{--Production Date--}}     <td>{{ $production->date }}</td>
                                    {{--Cutting--}}             <td>{{ $production->order->prodCutting($production->date) }}</td>
                                    {{--Swing_in--}}            <td>{{ $production->order->prodSwingIn($production->date) }}</td>
                                    {{--Swing_out--}}           <td>{{ $production->order->prodSwingOut($production->date) }}</td>
                                    {{--Iron--}}                <td>{{ $production->order->prodIron($production->date) }}</td>
                                    {{--Packing--}}             <td>{{ $production->order->prodPacking($production->date) }}</td>
                                    <td>
                                        <div class="btn-group btn-group-sm" role="group" aria-label="Table row actions">
                                            {{--<button type="button" class="btn btn-white">
                                                <i class="material-icons">&#xE5CA;</i>
                                            </button>--}}
                                            <a class="btn btn-white" href="{{ route($pageData['routeFirstName'].'-show', [$production->order->id, 'page' => 'manufacturing']) }}">
                                                <i class="material-icons">&#xE870;</i>
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            @endif
                            </tbody>
                            <tfoot>
                            <tr>
                                <th>SL</th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                {{--<th>LC/Sales</th>--}}
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                            </tr>
                            </tfoot>
                        </table>
                        <!-- End Transaction History Table -->
                    </div>
                </div>
                {{--{{ $orders->links() }}--}}
            </div>
        </div>
    </div>
    <div class="promo-popup animated bounceIn" style="padding: 10px; border: 2px solid #898989; background: rgba(255,255,255,0.82); right: 10px;">

        <div>
            <a href="#accordion">
            <span class="close">
                <i class="material-icons">pie_chart</i>
            </span>
            </a>
        </div>


        <div>
            <a href="#search-area">
            <span class="close">
                <i class="material-icons">format_align_left</i>
            </span>
            </a>
        </div>


    </div>
@endsection

@section('vue-script')
    <script>
        function dataTableFooterSum(obj) {
            let tbl = obj.table;

            function columnIndex(colIndex) {
                return roundWithFrac(tbl.column(colIndex).data().sum());
            }

            [9, 10, 11, 14, 15, 17, 18, 19, 20, 21].forEach(function (colIndex) {
                $(tbl.column(colIndex).footer()).html(
                    columnIndex(colIndex)
                );
            });



            Highcharts.chart('container', {
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'Date Wise Production Chart',
                },
                subtitle: {
                    text: '<b> Value: '+columnIndex(11)+' | CM: '+columnIndex(14)+' </b>'
                },
                xAxis: {
                    type: 'category'
                },
                legend: {
                    enabled: false
                },
                plotOptions: {
                    series: {
                        borderWidth: 0,
                        dataLabels: {
                            enabled: true,
                            format: '{point.y}'
                        }
                    }
                },

                tooltip: {
                    headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                    pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
                },

                "series": [
                    {
                        "name": "Production",
                        "colorByPoint": false,
                        "data": [
                            {
                                "name": "Cut qty",
                                "y": columnIndex(17)
                            },
                            {
                                "name": "SwIng input",
                                "y": columnIndex(18)
                            },
                            {
                                "name": "Sewing output",
                                "y": columnIndex(19)
                            },
                            {
                                "name": "Iron qty",
                                "y": columnIndex(20)
                            },
                            {
                                "name": "Poly qty",
                                "y": columnIndex(21)
                            }
                        ]
                    }
                ]
            });
        }



    </script>

@endsection