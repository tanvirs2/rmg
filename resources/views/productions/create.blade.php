@extends('layouts.master')

@section('content')

    @include('layouts.partial.notice')

    <div class="main-content-container container-fluid px-4">
        <!-- Page Header -->
        <div class="page-header row no-gutters py-4">
            <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
                <span class="text-uppercase page-subtitle">Overview</span>
                <h3 class="page-title"> {{ isset($isSetModel)? 'Edit':'Place' }} {{ $pageData['pageName'] }}</h3>
            </div>
        </div>
        <!-- End Page Header -->
        <!-- Default Light Table -->

        @include('layouts.orderFilterForm')

        <div class="row">
            <div class="col-lg-4 col-md-12 mb-4">
                @if(request()->coming != 'fromList')
                <div class="card card-small mb-2">
                    <div class="card-header border-bottom">
                        <h6 class="m-0">Search by Order ID</h6>
                    </div>
                    <form action="">
                        <ul class="list-group list-group-flush">
                        <li class="list-group-item p-3">
                            <div class="row">
                                <div class="col">
                                    <div class="form-row">
                                        <div class="form-group col-md-1 sc-legend">
                                            ID
                                        </div>
                                        <div class="form-group col-md-8">
                                            <select class="select2-box-field-filter form-control" name="id" laravel-get-from="id" laravel-model="Order">
                                                <option value="{{ $id }}">{{ $id }}</option>
                                            </select>
                                        </div>
                                        <div class="form-group col-md-3">
                                            <button class="form-control btn btn-primary">
                                                Filter
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                    </form>
                </div>
                @endif
                <!-- Sales by Category -->
                @if(isset($order))
                    <div class="card card-small user-details mb-4 text-capitalize">
                        <div class="card-header p-0">
                            <div class="user-details__bg">
                                <img src="{{ asset('profile-pic/system/up-user-details-background.jpg') }}" alt="User Details Background Image">
                            </div>
                        </div>
                        <div class="card-body p-0">
                            <div class="user-details__avatar mx-auto ">
                                <img class="rounded" src="{{ $order->getGarmentsPicture() }}" alt="User Avatar">
                            </div>
                            <h4 class="text-center m-0 mt-2">{{ $order->name }}</h4>
                            <p class="text-center text-light m-0 mb-2">{{ $order->buyer->name }} ({{ $order->buyer->agent->name ?? 'None' }})</p>
                            {{--<ul class="user-details__social user-details__social--primary d-table mx-auto mb-4">
                                <li class="mx-1"><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                                <li class="mx-1"><a href="#"><i class="fab fa-twitter"></i></a></li>
                                <li class="mx-1"><a href="#"><i class="fab fa-github"></i></a></li>
                                <li class="mx-1"><a href="#"><i class="fab fa-slack"></i></a></li>
                            </ul>--}}
                            <div class="user-details__user-data border-top border-bottom p-4">
                                <div class="row mb-3">
                                    <div class="col w-50">
                                        <span>Create</span>
                                        <span>{{ $order->user->name }}</span>
                                    </div>
                                    <div class="col w-50">
                                        <span>Sale's</span>
                                        <span>{{ $order->sales_user->name }}</span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col w-50">
                                        <span>Reporting to</span>
                                        <span>{{ $order->reporting_user->name }}</span>
                                    </div>

                                </div>
                            </div>
                            {{--<div class="user-details__tags p-4">
                                <span class="badge badge-pill badge-light text-light text-uppercase mb-2 border">User Experience</span>
                                <span class="badge badge-pill badge-light text-light text-uppercase mb-2 border">UI Design</span>
                                <span class="badge badge-pill badge-light text-light text-uppercase mb-2 border">React JS</span>
                                <span class="badge badge-pill badge-light text-light text-uppercase mb-2 border">HTML &amp; CSS</span>
                                <span class="badge badge-pill badge-light text-light text-uppercase mb-2 border">JavaScript</span>
                                <span class="badge badge-pill badge-light text-light text-uppercase mb-2 border">Bootstrap 4</span>
                            </div>--}}
                        </div>
                    </div>
            @endif
            <!-- End Sales by Category -->
            </div>
            <div class="col col-lg-8 col-md-12 mb-4">
                <!-- Sales Report -->
                <div class="card card-small production-entry">
                    <div class="card-header border-bottom">
                        <h6 class="m-0">Production Entry</h6>
                        <div class="block-handle"></div>
                    </div>
                    <form action="{{ route($pageData['routeFirstName'].'-store', $order->id??'') }}" method="post">
                        @csrf
                        <div class="card-body pt-0">

                            <div class="row border-bottom py-2 bg-light">
                                <div class="col-12 col-sm-8 d-flex mb-2 mb-sm-0">
                                    <div class="production-entry-btn btn-group btn-group-sm btn-group-toggle d-flex my-auto mx-auto mx-sm-0">
                                        <label :class="['btn btn-white', { 'focus active': productionType === 'cutting' }]">
                                            <input type="radio" name="production_type" v-model="productionType" value="cutting" autocomplete="off" :checked="productionType === 'cutting'"> Cutting </label>
                                        <label :class="['btn btn-white', { 'focus active': productionType === 'swing_in' }]">
                                            <input type="radio" name="production_type" v-model="productionType" value="swing_in" autocomplete="off" :checked="productionType === 'swing_in'"> Swing In </label>
                                        <label :class="['btn btn-white', { 'focus active': productionType === 'swing_out' }]">
                                            <input type="radio" name="production_type" v-model="productionType" value="swing_out" autocomplete="off" :checked="productionType === 'swing_out'"> Swing Out </label>
                                        <label :class="['btn btn-white', { 'focus active': productionType === 'iron' }]">
                                            <input type="radio" name="production_type" v-model="productionType" value="iron" autocomplete="off" :checked="productionType === 'iron'"> Iron </label>
                                        <label :class="['btn btn-white', { 'focus active': productionType === 'packing' }]">
                                            <input type="radio" name="production_type" v-model="productionType" value="packing" autocomplete="off" :checked="productionType === 'packing'"> Packing </label>
                                    </div>
                                </div>
                            </div>


                            <div class="p-3">
                                @if(isset($order))

                                    <div class="form-row">
                                        <div class="col-12 col-sm-12 d-flex mb-2 mb-sm-0 pt-2 pb-4 w-100">
                                            <div class="card card-small overflow-hidden mb-4">
                                                <div class="card-header">
                                                    <h6 class="m-0">Size And Color wise entry</h6>
                                                </div>
                                                <div class="card-body p-0 text-center">
                                                    <table class="table table-bordered table-hover table-striped table-sm mb-0">
                                                        <thead class="">
                                                        <tr>
                                                            <th scope="col" class="border-bottom-0">#</th>
                                                            <th scope="col" class="border-bottom-0">Color</th>
                                                            <th scope="col" class="border-bottom-0">Size</th>
                                                            <th scope="col" class="border-bottom-0">Qty</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>

                                                        @foreach($order->quantity as $sNcWiseQty)
                                                            <tr>
                                                                <td>
                                                                    <input :disabled="productionType?false:true" type="radio" {{ (old('order_quantity_id') == $sNcWiseQty->id)? 'checked': null }} @click="selectQtyRowFalse(); test{{ $sNcWiseQty->id }} = true" class="btn" id="formsRadio{{ $sNcWiseQty->id }}" name="order_quantity_id" value="{{ $sNcWiseQty->id }}">
                                                                </td>
                                                                <td>
                                                                    <label for="formsRadio{{ $sNcWiseQty->id }}">{!! $sNcWiseQty->color->name ?? '<span class="text-danger">Not Set</span>' !!}</label>
                                                                </td>
                                                                <td>
                                                                    <label for="formsRadio{{ $sNcWiseQty->id }}">{!! $sNcWiseQty->size->name ?? '<span class="text-danger">Not Set</span>' !!}</label>
                                                                </td>
                                                                <td>
                                                                    <label for="formsRadio{{ $sNcWiseQty->id }}">{{ $sNcWiseQty->quantity }}</label>
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                        </tbody>
                                                        <tfoot class="">
                                                        <tr>
                                                            <th scope="col" class="border-bottom-0"></th>
                                                            <th scope="col" class="border-bottom-0"></th>
                                                            <th scope="col" class="border-bottom-0"></th>
                                                            <th scope="col" class="border-bottom-0">{{ $order->quantity->sum('quantity') }}</th>
                                                        </tr>
                                                        </tfoot>
                                                    </table>
                                                </div>
                                            </div>

                                            @foreach($order->quantity as $sNcWiseQty)
                                                <div v-if="test{{ $sNcWiseQty->id }}" class="production-entry-btn btn-group btn-group-sm btn-group-toggle d-flex my-auto mx-auto">
                                                    <label @click="productionType = 'cutting'" :class="['btn btn-info p-3', { 'focus active': productionType === 'cutting' }]">Cutting &nbsp; <hr><b class="text-white">{{ testInpFun<?= $sNcWiseQty->id ?> (<?= $sNcWiseQty->cutting->sum('quantity') ?>, 'cutting') }}</b></label>
                                                    <label @click="productionType = 'swing_in'" :class="['btn btn-info p-3', { 'focus active': productionType === 'swing_in' }]">SwingIn &nbsp; <hr><b class="text-white">{{ testInpFun<?= $sNcWiseQty->id ?> (<?= $sNcWiseQty->swingIn->sum('quantity') ?>, 'swing_in') }}</b></label>
                                                    <label @click="productionType = 'swing_out'" :class="['btn btn-info p-3', { 'focus active': productionType === 'swing_out' }]">SwingOut <hr><b class="text-white">{{ testInpFun<?= $sNcWiseQty->id ?> (<?= $sNcWiseQty->swingOut->sum('quantity') ?>, 'swing_out') }}</b></label>
                                                    <label @click="productionType = 'iron'" :class="['btn btn-info p-3', { 'focus active': productionType === 'iron' }]">&nbsp;&nbsp; Iron &nbsp;&nbsp; <hr><b class="text-white">{{ testInpFun<?= $sNcWiseQty->id ?> (<?= $sNcWiseQty->iron->sum('quantity') ?>, 'iron') }}</b></label>
                                                    <label @click="productionType = 'packing'" :class="['btn btn-info p-3', { 'focus active': productionType === 'packing' }]">Packing &nbsp; <hr><b class="text-white">{{ testInpFun<?= $sNcWiseQty->id ?> (<?= $sNcWiseQty->packing->sum('quantity') ?>, 'packing') }}</b></label>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                <div class="form-row">
                                    <div class="form-group col-md-3">
                                        <label>Date</label>
                                        <input name="date" type="date" class="form-control">
                                    </div>
                                    @if(isset($order))
                                    <div class="form-group col-md-6">
                                        <label>Quantity</label>
                                        @foreach($order->quantity as $sNcWiseQty)
                                            <input name="quantity" v-if="test{{ $sNcWiseQty->id }}" type="number" v-model.number="prQuantityFld = testInput[productionType][{{ $sNcWiseQty->id }}]" class="form-control" placeholder="Quantity">
                                        @endforeach

                                    </div>
                                    @endif
                                    <div class="form-group col-md-3" v-show="productionType == 'swing_in' || productionType == 'swing_out' ">
                                        <label>
                                            Line
                                            <a target="_blank" href="{{ route('production-line-create') }}">
                                                <i class="material-icons">control_point</i>
                                            </a>
                                        </label>
                                        <br>
                                        <select class="select2-box form-control" name="line_id" laravel-model="ProductionLine" laravel-get-from="number">
                                            {{--<option value="{{ $line_id }}">{{ $line_id ? ucwords(App\Buyer::find($buyer_id)->name):null }}</option>--}}
                                            <option value=""></option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col">
                                        <label>Operation</label>
                                        <br>
                                        {{--<select class="select2-box-manual form-control" name="operation">
                                            <option value="InHouse">InHouse</option>
                                            <option value="SubContract">SubContract</option>
                                        </select>--}}
                                        <div class="btn-group btn-group-sm btn-group-toggle d-flex my-auto mx-auto mx-sm-0">
                                            <label :class="['btn btn-white', { 'focus active': operation === 'InHouse' }]">
                                                <input type="radio" name="operation" v-model="operation" :checked="operation === 'InHouse'" value="InHouse"> InHouse </label>
                                            <label :class="['btn btn-white', { 'focus active': operation === 'SubContract' }]">
                                                <input type="radio" name="operation" v-model="operation" :checked="operation === 'SubContract'" value="SubContract"> SubContract </label>
                                        </div>
                                    </div>

                                    <div class="form-group col-md-7 sub-contract" v-show="operation == 'SubContract'">
                                        <label>
                                            Sub Contract
                                            <a target="_blank" href="{{ route('sub-contractor-create') }}">
                                                <i class="material-icons">control_point</i>
                                            </a>
                                        </label>
                                        <br>
                                        <select class="select2-box form-control" name="sub_contract_id" laravel-model="SubContractor" laravel-get-from="name">
                                            {{--<option value="{{ $line_id }}">{{ $line_id ? ucwords(App\Buyer::find($buyer_id)->name):null }}</option>--}}
                                            <option value=""></option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-row" v-if="prQuantityFld > 0">
                                    <div class="form-group col-md-6">
                                        <button class="btn btn-success col-md-6">Save</button>
                                    </div>
                                </div>
                                @endif
                            </div>
                        </div>
                    </form>
                </div>
                <!-- End Sales Report -->
            </div>

        </div>

        {{--@if(isset($isSetModel))
        <form action="{{ route($pageData['routeFirstName'].'-update', $isSetModel->id) }}" method="post" enctype="multipart/form-data">
            @method('PATCH')
        @else
        <form action="{{ route($pageData['routeFirstName'].'-store') }}" method="post" enctype="multipart/form-data">
        @endif
            @csrf

        </form>--}}
                    <!-- End Default Light Table -->
    </div>
@endsection

@section('vue-script')
    <script>
        var error = new Howl({
            src: ["{{ asset('sound/to-the-point.ogg') }}"]
        });

        /*let disabledElem = 'form input,form select,form button';
        function prodEntryEnable(disabledElem, bool){
            $('.production-entry').find(disabledElem).prop('disabled', bool);
        }*/
        $(function () {

            //prodEntryEnable(disabledElem, true);

            @if(isset($order))

                /*prodEntryEnable('.production-entry-btn input[type="radio"]', false);

                $('input[name="production_type"]').closest('form').submit(function (e) {
                    e.preventDefault();
                });

                $('input[name="production_type"]').change(function () {

                    prodEntryEnable(disabledElem, false);

                    if ($(this).val() == 'swing_in' || $(this).val() == 'swing_out'){
                        $('[name="line_id"]').closest('div').css('display', 'block');
                    } else {
                        $('[name="line_id"]').closest('div').css('display', 'none');
                    }
                    $(this).closest('form').unbind('submit');
                });
                $('input[name="operation"]').change(function () {
                    if ($(this).val() == 'SubContract'){
                        $('.sub-contract').css('display', 'block');
                    } else {
                        $('.sub-contract').closest('div').css('display', 'none');
                    }
                });*/
            @endif
        });

    </script>

    <script>
        const data = {

            productionType: '{{ old('production_type') }}',
            prQuantityFld: 0,
            operation:'{{ old('operation') }}',

            /*cutting: 0,
            swingIn: 0,
            swingOut: 0,
            iron: 0,
            packing: 0,*/
            testInput: {
                cutting:[],
                swing_in:[],
                swing_out:[],
                iron:[],
                packing:[],
            },

            @if($order)

            @foreach($order->quantity as $sNcWiseQty)
            test{{ $sNcWiseQty->id }}: false,
            @endforeach

            test{{old('order_quantity_id')}}: true,


            cutting: "{{ $order->cutting->sum('quantity') }}",
            swingIn: "{{ $order->swingIn->sum('quantity') }}",
            swingOut: "{{ $order->swingOut->sum('quantity') }}",
            iron: "{{ $order->iron->sum('quantity') }}",
            packing: "{{ $order->packing->sum('quantity') }}",
            @endif
        };

        const app = new Vue({
            el: '#app',
            data,
            methods: {
                @if($order)
                @foreach($order->quantity as $sNcWiseQty)
                testInpFun{{$sNcWiseQty->id}}(data = 0, fld) {
                    return data + (this.testInput[fld][{{$sNcWiseQty->id}}] || 0);
                },
                @endforeach

                selectQtyRowFalse() {
                    @foreach($order->quantity as $sNcWiseQty)
                        this.test{{ $sNcWiseQty->id }} = false;
                    @endforeach
                },

                @endif

                roundWithFrac(number){
                    return Math.round(number * 100)/100;
                },
                prQuantitySumOnInp(field, fieldName, compare){
                    let userInpQtySum = Number(field) + Number(this.prQuantity[fieldName] || 0);
                    let dangerBtn = '';
                    if (compare < userInpQtySum) {
                        dangerBtn = 'btn-danger';
                        if (fieldName != 'cutting') {
                            error.play();
                        }
                    }

                    return { qty: userInpQtySum, status: dangerBtn };
                }
            },
            updated(){
                console.log(this.operation);
            },
            mounted(){
                let productionEntryEnable = (boolVal) => {
                    document.querySelectorAll('.production-entry-btn').forEach((item)=>{
                        item.querySelectorAll('input').forEach((input)=>{
                            input.disabled = !boolVal;
                        })
                    });
                };

                productionEntryEnable(false);

                @if($order)
                    productionEntryEnable(true);
                @endif
            },
            computed: {
                cuttingQtyInpSum() {
                    return this.prQuantitySumOnInp(this.cutting, 'cutting', 0);
                },
                swingInQtyInpSum() {
                    return this.prQuantitySumOnInp(this.swingIn, 'swing_in', this.cuttingQtyInpSum.qty);
                },
                swingOutQtyInpSum() {
                    return this.prQuantitySumOnInp(this.swingOut, 'swing_out', this.swingInQtyInpSum.qty);
                },
                ironQtyInpSum() {
                    return this.prQuantitySumOnInp(this.iron, 'iron', this.swingOutQtyInpSum.qty);
                },
                packingQtyInpSum() {
                    return this.prQuantitySumOnInp(this.packing, 'packing', this.ironQtyInpSum.qty);
                },
            }

        });
    </script>
@endsection

