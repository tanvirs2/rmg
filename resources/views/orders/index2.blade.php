@extends('layouts.master')

@section('content')

    @include('layouts.partial.notice')

    <div class="main-content-container container-fluid px-4">
        <!-- Page Header -->
        <div class="page-header row no-gutters py-4">
            <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
                <span class="text-uppercase page-subtitle">Overview</span>
                <h3 class="page-title">{{ $pageData['pageName'] }} List</h3>
            </div>
            <div class="float-right col-12 col-sm-8 text-center text-sm-right">
                <span class="text-uppercase page-subtitle">Action</span>
                <h3 class="page-title">
                    <a href="{{ route($pageData['routeFirstName'].'-create') }}" class="btn btn-md btn-white">
                        <i class="material-icons">control_point</i> Add {{ $pageData['pageName'] }}
                    </a>
                </h3>
            </div>


        </div>
        <!-- End Page Header -->

        <div class="row">
            <div class="col-lg-12 mb-1">
                <div class="card card-small">
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item p-2">
                            <div class="row">
                                <div class="col-sm-12 col-md-12">
                                    <form>
                                        <div class="form-row">
                                            <div class="form-row col-md-9">
                                                <div class="form-group col-md-1">
                                                    <button type="button" class="mb-2 btn btn-success">Jan</button>
                                                </div>
                                                <div class="form-group col-md-1">
                                                    <button type="button" class="mb-2 btn btn-success">Fab</button>
                                                </div>
                                                <div class="form-group col-md-1">
                                                    <button type="button" class="mb-2 btn btn-success">Mar</button>
                                                </div>
                                                <div class="form-group col-md-1">
                                                    <button type="button" class="mb-2 btn btn-success">Apr</button>
                                                </div>
                                                <div class="form-group col-md-1">
                                                    <button type="button" class="mb-2 btn btn-success">May</button>
                                                </div>
                                                <div class="form-group col-md-1">
                                                    <button type="button" class="mb-2 btn btn-success">Jun</button>
                                                </div>
                                                <div class="form-group col-md-1">
                                                    <button type="button" class="mb-2 btn btn-success">Jul</button>
                                                </div>
                                                <div class="form-group col-md-1">
                                                    <button type="button" class="mb-2 btn btn-success">Aug</button>
                                                </div>
                                                <div class="form-group col-md-1">
                                                    <button type="button" class="mb-2 btn btn-success">Sep</button>
                                                </div>
                                                <div class="form-group col-md-1">
                                                    <button type="button" class="mb-2 btn btn-success">Oct</button>
                                                </div>
                                                <div class="form-group col-md-1">
                                                    <button type="button" class="mb-2 btn btn-success">Nov</button>
                                                </div>
                                                <div class="form-group col-md-1">
                                                    <button type="button" class="mb-2 btn btn-success">Dec</button>
                                                </div>

                                            </div>

                                            <div class="form-row col-md-3">
                                                <div class="form-group col-md-6">
                                                    <input value="{{ $name }}" name="id" type="text" class="form-control is-invalid" placeholder="From">
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <input value="{{ $name }}" name="id" type="text" class="form-control is-invalid" placeholder="To">
                                                </div>
                                            </div>

                                        </div>
                                        <div class="form-row">
                                            <div class="form-group col-md-1">
                                                <input value="{{ $name }}" name="id" type="text" class="form-control is-valid filter-input" placeholder="Job ID">
                                                <div class="valid-feedback">ID</div>
                                            </div>
                                            <div class="form-group col-md-1">
                                                <input value="{{ $name }}" name="buyer_id" type="text" class="form-control is-valid filter-input" placeholder="Name">
                                                <div class="valid-feedback">Buyer Name</div>
                                            </div>
                                            <div class="form-group col-md-1">
                                                <input value="{{ $name }}" name="name" type="text" class="form-control is-valid filter-input" placeholder="Name">
                                                <div class="valid-feedback">Order Name</div>
                                            </div>
                                            <div class="form-group col-md-1">
                                                <input value="{{ $name }}" name="buyer_id" type="text" class="form-control is-valid filter-input" placeholder="Name">
                                                <div class="valid-feedback">Style Name</div>
                                            </div>

                                            <div class="form-group col-md-1">
                                                <input value="{{ $phone }}" name="phone" type="text" class="form-control is-valid filter-input" placeholder="Phone Number">
                                                <div class="valid-feedback">Style Desc</div>
                                            </div>
                                            <div class="form-group col-md-1">
                                                <select name="team_id" class="form-control is-invalid">
                                                    <option value="">Choose...</option>
                                                    @foreach($departments as $department)
                                                        <optgroup label="{{ ucwords($department->name) }}">
                                                            @foreach($department->teams as $team)
                                                                <option value='{{ $team->id }}' {{ ($team->id==$team_id) ? "selected" : null }}>{{ ucwords($team->name) }}</option>
                                                            @endforeach
                                                        </optgroup>
                                                    @endforeach
                                                </select>
                                                <div class="invalid-feedback">Order Status</div>
                                            </div>
                                            <div class="form-group col-md-1">
                                                <select name="role_id" class="form-control is-invalid">
                                                    <option value="">Choose...</option>
                                                    @foreach($roles as $role)
                                                        <option value='{{ $role->id }}' {{ ($role->id==$role_id) ? "selected" : null }}>{{ ucwords($role->name) }}</option>
                                                    @endforeach
                                                </select>
                                                <div class="invalid-feedback">Category</div>
                                            </div>
                                            <div class="form-group col-md-1">
                                                <select name="role_id" class="form-control is-invalid">
                                                    <option value="">Choose...</option>
                                                    @foreach($roles as $role)
                                                        <option value='{{ $role->id }}' {{ ($role->id==$role_id) ? "selected" : null }}>{{ ucwords($role->name) }}</option>
                                                    @endforeach
                                                </select>
                                                <div class="invalid-feedback">Sub Category</div>
                                            </div>
                                            <div class="form-group col-md-2">
                                                <button type="submit" class="mb-2 btn btn-primary">Search</button>

                                                <a href="{{ route($pageData['routeFirstName'].'-list') }}" class="mb-2 btn btn-dark">Reset</a>
                                            </div>
                                            <div class="form-group col-md-1 p-0">
                                                <button @click="doit" type="button" class="float-right btn btn-info">
                                                    <i class="material-icons">save_alt</i> Export
                                                </button>
                                            </div>
                                            <div class="form-group col-md-1">
                                                <select name='per_page' class="form-control is-invalid" @change="per_page">
                                                    @foreach($per_page_rows as $number)
                                                        <option {{ ($per_page==$number) ? "selected" : null }}>{{ $number }}</option>
                                                    @endforeach
                                                </select>
                                                <div class="invalid-feedback">Rows</div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12 mb-3">
                <div class="card card-small">
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item p-2">
                            <div class="row">
                                <div class="col-md-2">
                                    <b>Total : </b> <span>{{ $count }} {{ ($count>1)?Str::plural($pageData['pageName']):$pageData['pageName'] }} Found</span>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <!-- Default Light Table -->
        <div class="row">
            <div class="col">
                <div class="card card-small mb-4">
                    <div class="card-header border-bottom">
                        {{--From Controller--}}
                        <h6 class="m-0">Active {{ Str::plural($pageData['pageName']) }}</h6>
                    </div>
                    <div class="card-body p-0 pb-3 text-center pre-x-scrollable">
                        <table class="table mb-0" id="main-table" >
                            <thead class="bg-light">
                            <tr>
                                <th scope="col" class="border-0">SL</th>
                                <th scope="col" class="border-0">ID</th>
                                <th scope="col" class="border-0">Buyer</th>
                                <th scope="col" class="border-0">OrderName</th>
                                <th scope="col" class="border-0">Style</th>
                                <th scope="col" class="border-0">StyleDesc</th>
                                <th scope="col" class="border-0">Image</th>
                                <th scope="col" class="border-0">RemDay</th>
                                <th scope="col" class="border-0">OrderQty</th>
                                <th scope="col" class="border-0">UnitPrice</th>
                                <th scope="col" class="border-0">TotValue</th>
                                <th scope="col" class="border-0">ShipQty</th>
                                <th scope="col" class="border-0">ShipValue</th>
                                <th scope="col" class="border-0">ShortShipVal</th>
                                <th scope="col" class="border-0">OrderSts</th>
                                <th scope="col" class="border-0">CM</th>
                                <th scope="col" class="border-0">SMV</th>
                                <th scope="col" class="border-0">LC/Sales</th>
                                <th scope="col" class="border-0">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($users as $user)
                                <tr>
                                    <td>{{ ++$pageData['no'] }}</td>
                                    <td>{{ $user->id }}</td>
                                    <td>{{ $user->name }}</td>
                                    <td>{{ $user->phone }}</td>
                                    <td>{{ $user->team->department->name }}</td>
                                    <td></td>
                                    <td><img width="50" src="{{ $user->GetProfilePicture() }}" alt=""></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td>{{ $user->team->name }}</td>
                                    <td>{{ $user->role->name }}</td>
                                    <td>{{ $user->raw_password }}</td>
                                    <td>
                                        <div class="btn-group btn-group-sm" role="group" aria-label="Table row actions">
                                            <a href="#" class="btn btn-white">
                                                <i class="material-icons"></i>
                                            </a>
                                            <a href="#" class="btn btn-white">
                                                <i class="material-icons"></i>
                                            </a>
                                            <a href="{{ route($pageData['routeFirstName'].'-edit', $user->id) }}" class="btn btn-white">
                                                <i class="material-icons"></i>
                                            </a>
                                            <a class="btn btn-white" onclick="return deleteThisData('{{ route($pageData['routeFirstName'].'-destroy', $user->id) }}')">
                                                <i class="material-icons"></i>
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
                {{ $users->links() }}
            </div>
        </div>
        <!-- End Default Dark Table -->
    </div>
@endsection

@section('vue-script')
    @include('js/script')
@endsection