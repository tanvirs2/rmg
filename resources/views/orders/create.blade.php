@extends('layouts.master')

@section('content')

    @include('layouts.partial.notice')

    <div class="main-content-container container-fluid px-4">
        <!-- Page Header -->
        <div class="page-header row no-gutters py-4">
            <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
                <span class="text-uppercase page-subtitle">Overview</span>
                <h3 class="page-title"> {{ isset($isSetModel)? 'Edit':'Place' }} {{ $pageData['pageName'] }}</h3>
            </div>
        </div>
        <!-- End Page Header -->
        <!-- Default Light Table -->

        @if(isset($isSetModel))
        <form action="{{ route($pageData['routeFirstName'].'-update', $isSetModel->id) }}" method="post" enctype="multipart/form-data">
            @method('PATCH')
        @else
        <form action="{{ route($pageData['routeFirstName'].'-store') }}" method="post" enctype="multipart/form-data">
        @endif
            @csrf
            <div class="row">
                <div class="col-lg-6">

                    <div class="card card-small mb-4">
                                    <div class="card-header border-bottom">
                                        <h6 class="m-0">{{ $pageData['pageName'] }} Details</h6>
                                    </div>
                                    <ul class="list-group list-group-flush">
                                        <li class="list-group-item p-3">
                                            <div class="row">
                                                <div class="col">
                                                    <div class="form-row">
                                                        <div class="card card-small mb-4 col-md-7">
                                                            <div class="card-header border-bottom text-center">
                                                                <div class="mx-auto">
                                                                    <img class="rounded-top" :src="userImg"
                                                                         alt="{{ $pageData['pageName'] }} Avatar"
                                                                         width="100"></div>
                                                                <h4 class="">@{{ fullName }}</h4>
                                                                <span class="text-muted d-block">@{{ buyerName }}</span>
                                                            </div>
                                                            <ul class="list-group list-group-flush">
                                                                <li class="list-group-item">
                                                                    <strong class="text-muted d-block mb-2">About {{ $pageData['pageName'] }}</strong>
                                                                    <span>
                                                                        @{{ aboutUser }}
                                                                    </span>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                        <div class="col-md-1">
                                                        </div>
                                                        <div class="card card-small mb-5 mt-4 p-2 col-md-4">
                                                            <label>Garments Picture</label>
                                                            <input name="image" type="file" @change="readURL"
                                                                   class="form-control">
                                                        </div>
                                                    </div>

                                                    <div class="form-row">
                                                        <div class="form-group col-md-6">
                                                            <label>{{ $pageData['pageName'] }}
                                                                Name</label>
                                                            <input v-model="fullName" name="name" type="text"
                                                                   autocomplete="off"
                                                                   class="form-control"
                                                                   placeholder="{{ $pageData['pageName'] }} Name"
                                                                   value="">
                                                        </div>

                                                        <div class="form-group col-md-6">
                                                            <label>Buyer Name
                                                                <a target="_blank" href="{{ route('buyer-create') }}">
                                                                    <i class="material-icons">control_point</i>
                                                                </a>
                                                            </label>
                                                            <select class="select2-box form-control" name="buyer_id" laravel-model="Buyer" laravel-get-from="name" laravel-belongs="agent">
                                                                <option value="{{ $isSetModel->buyer_id ?? null }}">{{ $isSetModel->buyer->name ?? null }}</option>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="form-row">
                                                        <div class="form-group col-md-4">
                                                            <label>Style Name</label>
                                                            <input name="style" type="text"
                                                                   class="form-control"
                                                                   placeholder="Style Name"
                                                                   value="{{ $isSetModel->style ?? null }}">
                                                        </div>
                                                        <div class="form-group col-md-8">
                                                            <label>Style Description</label>
                                                            <input value="{{ $isSetModel->style_desc ?? null }}"
                                                                   name="style_desc" type="text" class="form-control"
                                                                   placeholder="Style Description">
                                                        </div>
                                                    </div>

                                                    <div class="form-row">
                                                        <div class="form-group col-md-4">
                                                            <label>Order Accept Date</label>
                                                            <input name="order_accept_date" type="date" class="form-control"
                                                                   value="{{ $isSetModel->order_accept_date ?? null }}">
                                                        </div>
                                                        <div class="form-group col-md-4">
                                                            <label>Shipment Date</label>
                                                            <input name="date_of_ship" type="date" class="form-control" placeholder="Shipment Date"
                                                                   value="{{ $isSetModel->date_of_ship ?? null }}">
                                                        </div>

                                                        <div class="form-group col-md-4">
                                                            <label>Order Quantity</label>
                                                            <input v-model="orderQty"
                                                                   type="number"
                                                                   class="form-control"
                                                                   readonly
                                                                   data-toggle="modal" data-target="#quantity-modal"
                                                                   placeholder="Order Quantity">
                                                        </div>
                                                    </div>

                                                    <div class="form-row">
                                                        <div class="form-group col-md-4">
                                                            <label>Unit Price</label>
                                                            <input name="unit_price"
                                                                   value="{{ $isSetModel->unit_price ?? null }}"
                                                                   type="number" step="any"
                                                                   v-model="unit_price"
                                                                   class="form-control" placeholder="Unit Price">
                                                        </div>
                                                        <div class="form-group col-md-4">
                                                            <label>
                                                                Category
                                                                <a target="_blank" href="{{ route('order-sub-category-create') }}">
                                                                    <i class="material-icons">control_point</i>
                                                                </a>
                                                            </label>
                                                            <select class="select2-box form-control" name="sub_category_id" laravel-model="OrderSubCategory" laravel-get-from="name" laravel-belongs="orderCategory">
                                                                <option value="{{ $isSetModel->sub_category_id ?? null }}">{{ $isSetModel->subCategory->name ?? null }}</option>
                                                            </select>
                                                        </div>
                                                        <div class="form-group col-md-4">
                                                            <label>{{ $pageData['pageName'] }} TNA
                                                                <a target="_blank" href="{{ route('order-status-create') }}">
                                                                    <i class="material-icons">control_point</i>
                                                                </a>
                                                            </label>
                                                            <select class="select2-box form-control" name="status_id" laravel-model="OrderStatus" laravel-get-from="name">
                                                                <option value="{{ $isSetModel->status_id ?? null }}">{{ $isSetModel->status->name ?? null }}</option>
                                                            </select>
                                                        </div>
                                                    </div>



                                                    <div class="form-row">
                                                        <div class="form-group col-md-2">
                                                            <label>SMV</label>
                                                            <input name="smv"
                                                                   type="number"
                                                                   value="{{ $isSetModel->smv ?? null }}"
                                                                   class="form-control"
                                                                   placeholder="SMV">
                                                        </div>

                                                        <div class="form-group col-md-5">
                                                            <label>Sales Person
                                                                <a target="_blank" href="{{ route('user-create') }}">
                                                                    <i class="material-icons">control_point</i>
                                                                </a>
                                                            </label>
                                                            <select class="select2-box form-control" name="sales_user_id" laravel-model="User" laravel-get-from="name" laravel-belongs="team">
                                                                <option value="{{ $isSetModel->sales_user_id ?? null }}">{{ $isSetModel->sales_user->name ?? null }}</option>
                                                            </select>
                                                        </div>

                                                        <div class="form-group col-md-5">
                                                            <label>Reporting To
                                                                <a target="_blank" href="{{ route('user-create') }}">
                                                                    <i class="material-icons">control_point</i>
                                                                </a>
                                                            </label>
                                                            <select class="select2-box form-control" name="reporting_to_user_id" laravel-model="User" laravel-get-from="name" laravel-belongs="team">
                                                                <option value="{{ $isSetModel->reporting_to_user_id ?? null }}">{{ $isSetModel->reporting_user->name ?? null }}</option>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="form-row">
                                                        <div class="form-group col-md-12">
                                                            <label>About {{ $pageData['pageName'] }}</label>
                                                            <textarea v-model="aboutUser" name="description"
                                                                      class="form-control" rows="5">

                                                            </textarea>
                                                        </div>
                                                    </div>
                                                    <button type="submit" class="btn btn-accent">Save</button>

                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>

                </div>

                            {{--Budget--}}
                <div class="col-lg-6">
                    <div v-show="roundWithFrac(fob_Price/12)" class="card card-small mb-2 border border-primary" style="border: 1px solid black; min-width: 250px; position: fixed; right: 10px; bottom:30px; background: #ffffff; z-index: 9999; opacity: 0.7">
                        {{--<div class="card-header border-bottom">
                            <h6 class="m-0">Profit or Loss</h6>
                        </div>--}}
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item p-3">
                                <div class="row">
                                    <div class="col">
                                        <div class="form-row">
                                            <div class="form-group col-md-6">
                                                <label>Costing</label>
                                                <div class="form-control">
                                                    @{{ roundWithFrac(fob_Price/12) }}
                                                </div>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label>Profit</label>
                                                <div class="form-control">
                                                    <span v-if="unit_price > 0">
                                                        @{{ roundWithFrac(unit_price-(fob_Price/12)) }}
                                                    </span>

                                                    <span v-else>0</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="card card-small">
                                    <div class="card-header border-bottom">

                                        <h6 class="m-0">
                                            {{ isset($isSetModel)? 'Edit': null }} {{ $pageData['pageName'] }} Budget

                                            @if(request()->get('edit')=='order')
                                                <span class="custom-control custom-toggle custom-toggle-sm mb-1">
                                                    <input type="checkbox" id="customToggle1" name="budgetEditEnable" class="custom-control-input">
                                                    <label class="custom-control-label" for="customToggle1"></label>
                                                </span>
                                            @endif
                                        </h6>

                                    </div>
                                    <ul class="list-group list-group-flush">
                                        <li class="list-group-item p-3">
                                            <div class="row">
                                                <div class="col">
                                                    <div class="form-row bg-info">
                                                        <div class="form-group col-md-12">
                                                            <label class="text-dark">Yarn Consumption</label>
                                                            <input name="yarn_consumption" type="number" class="form-control"
                                                                   step="any"
                                                                   v-model="yarn_consumption"
                                                                   placeholder="Yarn Consumption"
                                                                   value="{{ $isSetModel->budget->yarn_consumption ?? null }}">
                                                        </div>
                                                    </div>
                                                    <div class="form-row">
                                                        <div class="form-group col-md-4">
                                                            <label>Yarn Price <small class="text-danger">(kg)</small></label>
                                                            <input name="yarn_price" type="number" class="form-control"
                                                                   step="any"
                                                                   value="{{ $isSetModel->budget->yarn_price ?? null }}"
                                                                   v-model="yarn_price"
                                                                   placeholder="Yarn Price"
                                                                   value="{{ isset($isSetModel->phone)?$isSetModel->phone:old('phone') }}">
                                                        </div>
                                                        <div class="form-group col-md-4">
                                                            <label>Knitting Price <small class="text-danger">(kg)</small></label>
                                                            <input name="knitting_price" type="number" class="form-control"
                                                                   step="any"
                                                                   v-model="knitting_price"
                                                                   placeholder="Knitting Price"
                                                                   value="{{ isset($isSetModel->phone)?$isSetModel->phone:old('phone') }}">
                                                        </div>
                                                        <div class="form-group col-md-4">
                                                            <label>Dyeing Price <small class="text-danger">(kg)</small></label>
                                                            <input name="dyeing_price" type="number" class="form-control"
                                                                   step="any"
                                                                   v-model="dyeing_price"
                                                                   placeholder="Dyeing Price"
                                                                   value="{{ isset($isSetModel->phone)?$isSetModel->phone:old('phone') }}">
                                                        </div>

                                                    </div>
                                                    <div class="form-row">
                                                        <div class="form-group col-md-6">
                                                            <label>AOP <small class="text-danger">(kg)</small></label>
                                                            {{--AOP = all over print--}}
                                                            {{--YD = Yarn Dyieing--}}
                                                            <input name="aop" type="number" class="form-control"
                                                                   step="any"
                                                                   v-model="aop"
                                                                   placeholder="AOP"
                                                                   value="{{ $isSetModel->budget->aop ?? null }}">
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <label>Yarn Dying <small class="text-danger">(kg)</small></label>
                                                            <input name="yd" type="number" class="form-control"
                                                                   step="any"
                                                                   v-model="yd"
                                                                   placeholder="YD"
                                                                   value="{{ $isSetModel->budget->yd ?? null }}">
                                                        </div>

                                                    </div>
                                                    <div class="form-row bg-light-gray">

                                                        <div class="form-group col-md-3">
                                                            <label>Accessories <small class="text-danger">(Dz)</small></label>
                                                            <input name="accessories" type="number" class="form-control"
                                                                   step="any"
                                                                   v-model="accessories"
                                                                   placeholder="Accessories">
                                                        </div>
                                                        <div class="form-group col-md-3">
                                                            <label>Test Cost <small class="text-danger">(Dz)</small></label>
                                                            <input name="test_cost" type="number" class="form-control"
                                                                   step="any"
                                                                   v-model="test_cost"
                                                                   placeholder="Test Cost"
                                                                   value="{{ $isSetModel->budget->test_cost ?? null }}">
                                                        </div>
                                                        <div class="form-group col-md-3">
                                                            <label>Print <small class="text-danger">(Dz)</small></label>
                                                            <input name="print" type="number" class="form-control"
                                                                   step="any"
                                                                   v-model="print"
                                                                   placeholder="Print"
                                                                   value="{{ $isSetModel->budget->print ?? null }}">
                                                        </div>
                                                        <div class="form-group col-md-3">
                                                            <label>Embroidery <small class="text-danger">(Dz)</small></label>
                                                            <input name="embroidery" type="number" class="form-control"
                                                                   step="any"
                                                                   v-model="embroidery"
                                                                   placeholder="Embroidery"
                                                                   value="{{ $isSetModel->budget->embroidery ?? null }}">
                                                        </div>

                                                    </div>

                                                    <div class="form-row bg-light-gray">
                                                        <div class="form-group col-md-4">
                                                            <label>Bank Charge <small class="text-danger">(Dz)</small></label>
                                                            <input name="bank_charge" type="number" class="form-control"
                                                                   step="any"
                                                                   v-model="bank_charge"
                                                                   placeholder="Bank Charge">
                                                        </div>
                                                        <div class="form-group col-md-4">
                                                            <label>Commission <small class="text-danger">(Dz)</small></label>
                                                            <input name="commission" type="number" class="form-control"
                                                                   step="any"
                                                                   v-model="commission"
                                                                   placeholder="Commission">
                                                        </div>
                                                        <div class="form-group col-md-4">
                                                            <label>CM <small class="text-danger">(Dz)</small></label>
                                                            <input name="cm" type="number" class="form-control"
                                                                   step="any"
                                                                   v-model="cm"
                                                                   placeholder="CM PerDz">
                                                        </div>

                                                    </div>

                                                    <div class="form-row">

                                                        <div class="form-group col-md-6">
                                                            <label>Finish Fabric Consumption</label>
                                                            <input name="finish_fab_consumption" type="number" class="form-control" placeholder="Finish Fabric Consumption"
                                                                   step="any"
                                                                   value="{{ $isSetModel->budget->finish_fab_consumption ?? null }}">
                                                        </div>


                                                    </div>

                                                    <div class="form-row bg-light-gray">

                                                        <div class="form-group col-md-3">
                                                            <label>Freight Charge</label>
                                                            <input name="freight_charge" type="number" class="form-control"
                                                                   step="any"
                                                                   placeholder="Freight Charge"
                                                                   value="{{ $isSetModel->budget->freight_charge ?? null }}">
                                                        </div>
                                                        <div class="form-group col-md-3">
                                                            <label>Others <small class="text-danger">(Dz)</small></label>
                                                            <input name="others" type="number" class="form-control"
                                                                   step="any"
                                                                   v-model="others"
                                                                   placeholder="Others"
                                                                   value="{{ $isSetModel->budget->others ?? null }}">
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            {{--LC = latter of contract--}}
                                                            <label>LC / Sales contract</label>
                                                            <input name="lc_or_sales_contract" type="number" class="form-control"
                                                                   step="any"
                                                                   placeholder="LC / Sales contract"
                                                                   value="{{ $isSetModel->budget->lc_or_sales_contract ?? null }}">
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-12">
                                                        <label>About Budget</label>
                                                        <textarea name="budget_description"
                                                                  class="form-control" rows="5">{{ $isSetModel->budget->description ?? null }}
                                                        </textarea>
                                                </div>

                                            </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                </div>
            </div>
            <!-- The Modal -->
            <div class="modal fade" id="quantity-modal" data-backdrop="static" data-keyboard="false">
                <div class="modal-dialog">
                    <div class="modal-content">

                        <!-- Modal Header -->
                        <div class="modal-header">
                            <h4 class="modal-title">Quantity</h4>
                        </div>

                        <!-- Modal body -->
                        <div class="modal-body p-2">
                            {{--<table class="table m-0" id="main-table" >
                                <thead class="bg-light">
                                <tr>
                                    <th scope="col" class="border-0">#</th>
                                    <th scope="col" class="border-0">Color</th>
                                    <th scope="col" class="border-0">Size</th>
                                    <th scope="col" class="border-0">Quantity</th>
                                </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            <i class="material-icons">control_point</i>
                                        </td>
                                        <td><input type="text"></td>
                                        <td><input type="text"></td>
                                        <td><input type="text"></td>
                                    </tr>
                                </tbody>
                            </table>--}}

                            <div class="form-row">
                                <div class="form-group col-md-1">
                                    <button v-if="addRowBtn" class="bg-success rounded-circle" type="button" @click="addRow"><i class="material-icons">control_point</i></button>
                                </div>
                                <div class="form-group col-md-4">
                                    <label>Color
                                        <a target="_blank" href="{{ route('color-lib-create') }}">
                                            <i class="material-icons">control_point</i>
                                        </a>
                                    </label>
                                </div>
                                <div class="form-group col-md-4">
                                    <label>Size
                                        <a target="_blank" href="{{ route('size-lib-create') }}">
                                            <i class="material-icons">control_point</i>
                                        </a>
                                    </label>
                                </div>
                                <div class="form-group col-md-3">
                                    <label>Quantity</label>
                                </div>
                            </div>

                            <div class="form-row qty-row" v-for="(sizeNColorRow, index) in sizeNColorRows">
                                <div class="form-group col-md-1">
                                    <button v-if="removeBtn" type="button" class="bg-danger rounded-circle" @click="removeRow(sizeNColorRow, $event)"><i class="material-icons">remove_circle</i></button>
                                </div>
                                <div class="form-group col-md-4">
                                    <select v-model="sizeNColorRow.clr" v-select="sizeNColorRow.clr" @change="clrSetNameDel(sizeNColorRow)" class="select2-box form-control" :name="'order_qty['+index+'][color_id]'" laravel-model="ColorLib" laravel-get-from="name">
                                        <option v-if="sizeNColorRow.clrName" :value="sizeNColorRow.clr">@{{ sizeNColorRow.clrName }}</option>
                                        <option v-else value=""></option>
                                    </select>
                                </div>
                                <div class="form-group col-md-4">
                                    <select v-model="sizeNColorRow.sz" v-select="sizeNColorRow.sz" @change="szSetNameDel(sizeNColorRow)" class="select2-box form-control" :name="'order_qty['+index+'][size_id]'" laravel-model="SizeLib" laravel-get-from="name">
                                        <option v-if="sizeNColorRow.szName" :value="sizeNColorRow.sz">@{{ sizeNColorRow.szName }}</option>
                                        <option v-else value=""></option>
                                    </select>
                                </div>
                                <div class="form-group col-md-3">
                                    <input :name="'order_qty['+index+'][qty]'"
                                           v-model="sizeNColorRow.qty"
                                           type="number"
                                           placeholder="Quantity" class="form-control">
                                </div>
                            </div>
                            <div v-if="!addRowBtn" class="row text-danger">
                                <div class="form-group col-md-11 offset-md-1">
                                    <p>Note : Something wrong !</p>
                                </div>
                            </div>
                        </div>

                        <!-- Modal footer -->
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default">Sum @{{ orderQty }}</button>
                            <button v-if="addRowBtn" type="button" class="btn btn-danger" data-dismiss="modal">OK</button>
                        </div>

                    </div>
                </div>
            </div>
            <!-- End Modal -->
        </form>
                    <!-- End Default Light Table -->
    </div>
@endsection

@section('vue-script')
    <script>
        @if(isset($isSetModel))
            $(function () {
                $('[name="budgetEditEnable"]').closest('.card-small').find('ul input').prop('disabled', true);

                $('[name="budgetEditEnable"]').click(function () {
                    if ($(this).is(':checked')) {
                        $(this).closest('.card-small').find('ul input').prop('disabled', false);
                    } else {
                        $(this).closest('.card-small').find('ul input').prop('disabled', true);
                    }

                });
            });
        @endif
    </script>

    <script>

        @if(isset($vueArr))
            let qtyJson = JSON.parse('{{ json_encode($vueArr) }}'.replace(/&quot;/g,'"'));
        @else
            let qtyJson = [{qty: null, clr: null, sz: null}];
        @endif

        const data = {
            userImg: '{{ isset($isSetModel) ? $isSetModel->getGarmentsPicture() : 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOAAAADgCAMAAAAt85rTAAAAY1BMVEX////e3t57e3uvr69+fn54eHisrKzQ0NC8vLy5ubnAwMDOzs62traxsbF3d3fS0tLFxcXk5OSLi4vw8PD09PTZ2dns7OyFhYXj4+P5+fmenp5xcXGKioqXl5eRkZGnp6dra2sH7R5VAAAOpUlEQVR4nO2da3uqvBKGISYooBwDAUR8//+v3DM5ETzVrl0t9srzYS2rYHMzk5kkE2gQeHl5eXl5eXl5eXl5eXl5eXl5eXl5eXl5eXl5eXl5eXl9gngL4v1vN+Nl4k0Oapow7Nr6L3K2uSsALf+YObv8UtstgIYt57z/A6R9eAU4c6I5wW8/2qKqC96FlJgf3T/rR3zWlui1CNrVH0fZyuY/qxSVh2X9Ob2z/Q6flLwiad7+dsuflAXMc7TO85wN/+2mP6fS8LU9523X5MoNLekC2f0h/wzAPlQ8qWlvz+u6DUMA1aT31HxGL+T5rfb2gNm2ZQecd0HDDwE0zQ9vfSrNCaRhnl8Blu9u6r+Jm/Z2j46C7omgYNAZ8EOiaP0dg6DjNp8GWHzT40oDWL+0XT+m8rsGCYtl1F27GtXeYvukQXp9Qvoheb7PC1BaFM8apJYngD4kDfZgPNXeJ08o9fHFzbSyPvHim+3tzAkfE0S1HqZBR4054UOCaGva+2yWMIDpZ3RBCPrf9DgTY9KXNuvnZNv7ZBDt0w8DNO3Nn/S42p7w2nb9lHrjoU9kib4FK5ffjbq/LBNjkq+7IAwJwrnPPnHC76rvW2ximWi1i8+uj+d5kSQAmJsT3tXQf1Hf1zBXDzExhKa9Tlarm+0lIQ8BL8FcuX7AnuNKBAoBG9PeOYiWaZJcjDPLLfJJwFQfX7y52U+Kt4YOhF5pDFIYQN5IFndc0yo8OKhdNSB32Axgb9qrxyV957Ao1dYpExidcf15sv01jjvq2yUdqMaRqG6vyhL11sLoxF83ReK+Z4PS6rJEe4kXdjKrqebupEu26Qyzw27Yw+c75y0nKK0uS9wABILOtL3FIowDk+x2IS8X7yS78GZQWomuAdEG4U4JDNKn5rX5vzCvtGIIu7k+4FMAGwNT25cPBGfksXpZrA6Q34ox0iCyvXWQ3uVyAVMNuL7ZYF/eAOSGCuJjYRzxNly8i8FqvNAfPzv5eKOufJRjloilsL1J/JVgANfKo4B2dVniGhCDaLvTgJAAdl8CNkHfmBNWWHi57IQlAJa66ZgGv+Tbc5gx6dfrS4Ogi04oJ0t7lDQI3z9WvEfAVP+0viAaXPkoBtFOtxcM0n4BCIgQYyzg+mIMRJQbWcK0F34IYc6wewS4wyCqX69yxekiUdS4ION6HNb+yq7J0wJC5Q1AsFqd6NfrXHFa+igwcdPe1NnW06s5Y7NNdvH+cDhYQMgSoTHxs4WM94ovsgSmQWOoXYG7l5aRsVc1622hOA8I1egT4mfX+d+rRaKQWUJa6HDQdophSlukTbcsOsA8OZR+iwM1dfBht8YsESx9FAEbTbcQku6KIm9wO9ocLDnHPquPWeuKkxtH0QbbazyHE+ZL6LhdO3fPZOWA/AKw2DwgNJxxAtZsmg6XtWP97gpXnKTcRIEdLXnINguvQ7xL0sJ49CrTIMrphNil9puvTWghUeaHdWaJwO2EmCVa1erv67DSIOomCgyi3eF5A2orao9d41Bbau6ECJj/owE3h9/muC/ro+hk6eYffXT/2xj3ZRMFBtEi+h5XtFEnRLvfxrivvnMAk8i22SBETzBH0WqzRDAnCgwTRYRCSKP51UOtNksEs4/i8Kvdn1HPQTnarDZLBLgFXaVBNb7kOB1K9ocnLae13iwR2ETRzdMEmOB2cj4U75/k3K9xQcaqnoPMQj2uV3RhmgDmY789r3LFyUp3QryX7Jan4ebzuu2K3T6Kzre76HnNQdRJFADZdWV576a5vi7Bb5NYGdRywv/nNQdRLGWHF5KYyHkLFMJQvk2LxHAi6AqX7V3Vl4AWswXM2/aEQBTCpBc76DlaX+VsqetC4YKzfZADcKGt69acJFDXhUIHr6zX3vwndMdHoSP+AThUf8ty7efdmntf13Qfenv1PS06YfmXTKfFHdP9dlteoxLT3p/qdRfq6zvjFi8vLy8vLy8vLy8vLy8vLy8vLy8vL6+3q0hTe+dUnrqPwSmbNM2v67M9vK23FdSLRxy2+FXz+fIhj5fPa+w7fE5e3uiN69x5zOOrKvn/Zdkx1q+n7Gi3jzebSZCMTsPlJpD6lGWDPiYDVeaD+AhfNe+vbwh8OCz21/TFMAmWZURM6leGIjNir9qZf6SUCn31TiQzOLEgjGWMEUKH5Qn1QIgBhHMpMUYbCaXM7n3t9wQ/de0SnighBL4S/jmrdyqKb8l3X7UzHwGJ+nUAyDRglFEybtJ0A61mp4WbOoC5BNS7QRWtBeQnQuGCOc3OKYEvPW0Oh/NJpAaQnPStNK+6CfZIBSWi0YDagumRskHeb1QO0KqD25McwITQaiQn9UMMjucAdoRMCTMfIgxegIO+iUnf9wSA7NX3VBzpdKZkkEYygPUErdPOVU6ECvfqOoAxIdFAKtlofmKn2H4C8BmJuowy0wnRolcw4UheD0imEIwkwQxgQp224A9uAHAAd9C8DaPy0EaQKM/IZA7D7+oFtZ16y6CDXtYYATB7vQVpvWeU8sAC9ufZgGDC0RhYyQE8A2ABPRjbDd+RA6BxyfpISQ0H2DdOhFRXG2Pf46JZy8FIUWCDTAuNOc9I4Fuj0zQHcADAUJCpk+dQns4WhO4HV6WAK6d8tAWvj65yalcRtuM16mXVfwQM4ozS1loQr6tzq0NEFtH+AhCzQ44hBN5MGal0n4NrBdG1FFQ5cNCM8g0QL6XkcWEFvjJIRS8FLOEXnS1gIyhzbnU4EJrdBOzBalv0030PMQU6U5pRDdgKQht1rEpBqTDO2FQTqCoUoM2DU/AiSUAI+JgqbgPu7wHWJyJyoAJn7OHNPtgSY8EU8hu69Y6RqVVvGMA8YxDUMhm3wvFdgNBWGFwMCrCcKNvMR0AmrG72QQXYZ9iLGY5NIJKOKqNEEF3wybcRDJPk05zwoikXbU7DSejADC7KXr0fWAHiFRbhXgFikJkHkRzCw8l9ipoFhMABgMFEMoifeCYAqpSJyZPAoDOzozd4Z47F4Nsz4DuiqP6d+0jnQQgrwu5DztFhnQQ2A0IMrEIZMA8bJloHEM7BoXZ2BELlq+gH1IwX6l8ADMIjHbWLBgX0jI1m6jGIuvf5O4ATw3RZQnKopHmsi0LXq0K867yODNeOUDvM+RXAIGLUDjzgcmd69JIwncmvASH0IyBH48jJAHpmoQDM+D2kZgADcZXp7Mot4PieRC9ftDjd0YAdtOa4Kdu2gxxBqsVcdAaEUImAPQxipK/KbI/ttWM/+FBAD5ZYDRJWRdfWbTM5FtyrvNi96v4fC4hzAzt0zAU0WpxO8B8bl7cFzIBbofoXNF0FEAMIVreD7DNgqQuUjvCdGZVB1EZRiG2jlDi/aCxzpEcNiMHTjo3DU6Zmpmy4WEtYWFCG13ZiJDHfAC96nIyYw3PIInrFoDxAWJXfyfTSQYheoxPh8CJAJuyUvBCCzlkpPJyoGJKrpZJ6oFR1MJhnyGzSJ8O5VICUwgihZoLO90RSISwtLw6DEMPZPMOqm4QRfZUFvby8vLy8vLw+Vp3zGPrO/SsSLbyvJ6jyT2LMAyn8+wOhfpFcPU+Mp4u/X8PDXP4Bh3wupIXwze4Z9k90vOTPppwzxkzNKyVzJS0oWEb0s7FFxjI2z3m7Sh8Wjm65zDSfsiyblzgaGG0SLJhVg5mU7I/sP3eC2RBog5T9jT8nOQk0q8/cWTeBibxeD4MZO5aN5nVLmNaq9aOuotdVvZhhkcUWM7DmpKYLRK+Q4hHHBaBzyI8DbgXWh4T2DGeJFxeJ9MoazO4m4aw7OYDkClBXzezbDU792rbZC2pqOFeAgpKzfCDpC56zChP2zWCvbc6oqYXk80rMiYhYrZ8pPQRswfAFIydDAICZ9M2U0EzNoW4AZq+61V6ubB6YWUDpmamF4jKEUN5ajmQKYZJre+dDwISRCHyamWhhAQPr5rcAX/X0ykaQihfU9Da5tidf8MouGxWUDf3BWSl9CDipqpldSnIB9WryOwF3jAxy3UBP5NNMNyc0L4L+zMB4WH42geMRID9iFSealywsYE6IjsRvdFFcnyzkmq8uTdbGjw5MmxJxoPtxYdcRHwJi96tlsVP7BALmfV83ky3J3QBkRV23L6ihlfDba6yuGB/F0ueIL4TNC2C7iqPzkqH/GnAgLJbJx2yaAEAyRNEgCI00860oqmpopzj4WUFEEIGMmET7H/goJop29tADk2tozkrgA8BWFQv5cqcJkbtRRvNYmRuAVO0uyS52rPzfwnoE/j/vb+ngaibS0yaFUwMNZl+e2cDxANBUzeBy6NViBJSLZhC+hvYOIKFmZe1n+ThkhQj/aFcFbqj3Vp11JdT0yhCOiXGUKOyA5wHgBveOwMERtZVd6AXoHeGBmg037wsyWEHHVVgcXB31GCLB/qg9DYXRBkeJeEzWfwGIK9tyWZfYnDCnCcisKhu8L01A1mPZ8XjMoOOx2Pw2Iprc1LyCoHKP2X4BiL0pk0fbqtkMWGd6FPE2QI6Fv5qDWmjYqN/EzLEjTMfQFrseHsJDYUY59wGh640dHtzvweDNErA/aid/G2Axj/rBEU0chZwxzCNPs4NG9U4VOBaAbvHLrZoJwmSFcQaMzUAXAd12vKwP4i4d7YgwyibaFmpPndkNMs3pLyG63O5Ol8hBPy8Iv6gDdzZVs0lXzVSi57zEoqqoDal59EevAFnifM1PCSICM2Vc3Kajc1AvyDz7K50BTAPZW5fbqQWkpnaCn4CHHs2AFXyChQqQnM7nAfoyUddHzhhNxUVV37CGpr/mBx8ogcNLU0viETP7IwIYexLTJ3B3iVNYVxYHC2bGRY1kn530MEheDhgqIHRDZGCFOXs2aT+MMzO/JbJy19gfSRb9IGAixpOdX6ZjVWnaRlSV0G9HYjzbie5urCakLYdRSLOWJzhLaYQBA4e581w1G6sRQ0oYnadqrIbNvNySCHNWJWfa4VTNX+MfCeLl5eXl5eXl5eXl5eXl5eXl5eXl5eXl5eX1w/ofLWA8kmdzDb0AAAAASUVORK5CYII=' }}',
            fullName: '{{ isset($isSetModel) ? $isSetModel->name:old('name') }}',
            buyerName: '{{ isset($isSetModel) ? $isSetModel->buyer->name:'' }}',
            aboutUser: '{{ isset($isSetModel) ? $isSetModel->description:'' }}',
            teams: [],

            sizeNColorRows: qtyJson,
            removeBtn: false,
            addBtn: true,
            dblCount: 0,
            modalCloseBtn: true,

            teamsPhpView: true,

            unit_price: '{{ $isSetModel->unit_price ?? null }}',

            yarn_price: '{{ $isSetModel->budget->yarn_price ?? null }}',
            knitting_price: '{{ $isSetModel->budget->knitting_price ?? null }}',
            dyeing_price: '{{ $isSetModel->budget->dyeing_price ?? null }}',
            aop: '{{ $isSetModel->budget->aop ?? null }}',
            yd: '{{ $isSetModel->budget->yd ?? null }}',

            /*Dz*/
            accessories: '{{ $isSetModel->budget->accessories ?? null }}',
            test_cost: '{{ $isSetModel->budget->test_cost ?? null }}',
            print: '{{ $isSetModel->budget->print ?? null }}',
            embroidery: '{{ $isSetModel->budget->embroidery ?? null }}',
            bank_charge: '{{ $isSetModel->budget->bank_charge ?? null }}',
            commission: '{{ $isSetModel->budget->commission ?? null }}',
            others: '{{ $isSetModel->budget->others ?? null }}',
            cm: '{{ $isSetModel->budget->cm ?? null }}',

            yarn_consumption: '{{ $isSetModel->budget->yarn_consumption ?? null }}',
        };

        Vue.directive('select', {
            twoWay: true,
            bind: function (el, binding, vnode) {
                $(el).select2().on("select2:select", (e) => {
                    // v-model looks for
                    //  - an event named "change"
                    //  - a value with property path "$event.target.value"
                    el.dispatchEvent(new Event('change', { target: e.target }));
                });
            },
        });

        const app = new Vue({
            el: '#app',
            data,
            updated() {
                select2Loader();
            },
            methods: {
                szSetNameDel(itm) {
                    if (itm.szName) {
                        delete itm.szName;
                    }
                },
                clrSetNameDel(itm) {
                    if (itm.clrName) {
                        delete itm.clrName;
                    }
                },
                removeRow(item, elm) {
                    let rowCount = this.sizeNColorRows.length;
                    if (rowCount < 3) {
                        this.removeBtn = false;
                    }
                    if (rowCount > 1) {
                        this.sizeNColorRows.splice(this.sizeNColorRows.indexOf(item), 1);
                    }
                },
                addRow(elm) {
                    this.removeBtn = true;
                    this.sizeNColorRows.push({ qty: null });
                },
                readURL(input) { //img preview before upload
                    input = input.target;
                    if (input.files && input.files[0]) {
                        var reader = new FileReader();
                        reader.readAsDataURL(input.files[0]);
                        reader.onload = function(e) {
                            this.userImg = e.target.result;
                        }.bind(this);
                    }
                },
                buyerNameF(){
                    $(() => {
                        $('[name="buyer_id"]').change((e) => { //for view role name on profile card
                            this.buyerName = e.target.options[e.target.value].text;
                        })
                    });
                },
                roundWithFrac(number){
                    return Math.round(number * 100)/100;
                }
            },
            mounted(){
                this.buyerNameF();
            },
            computed: {
                addRowBtn() {
                    let localVar;
                    for(let item of this.sizeNColorRows){

                        if ((item.clr || item.sz) && item.qty) {
                            localVar = true;
                        } else {
                            localVar = false;
                            break;
                        }

                        this.dblCount = 0;

                        for(let item2 of this.sizeNColorRows){

                            if ((item.clr == item2.clr) && (item.sz == item2.sz)) {
                                ++this.dblCount;
                                if (this.dblCount == 1) {
                                    localVar = true;
                                } else {
                                    localVar = false;
                                    break;
                                }
                            }
                        }
                    }

                    return localVar;
                },
                orderQty() {
                    return this.sizeNColorRows.reduce(function (sum, item) {
                        return sum + Number(item.qty);
                    }, 0);
                },

                fob_Price(){
                    return (
                        Number(this.yarn_price * (this.yarn_consumption || 1))+
                        Number(this.knitting_price * (this.yarn_consumption || 1))+
                        Number(this.dyeing_price * (this.yarn_consumption || 1))+
                        Number(this.aop * (this.yarn_consumption || 1))+
                        Number(this.yd * (this.yarn_consumption || 1))+
                        /*Dz*/
                        Number(this.accessories)+
                        Number(this.test_cost)+
                        Number(this.print)+
                        Number(this.embroidery)+
                        Number(this.bank_charge)+
                        Number(this.commission)+
                        Number(this.others)
                        +Number(this.cm)
                    )
                },

            }

        });

        /*function qtyValidateRow(elm) {
            let qtyRow = elm.closest('.modal-body').querySelectorAll('.qty-row');
            for(let rowElm of qtyRow){
                const colorSel = rowElm.querySelectorAll('select')[0];
                const sizeSel = rowElm.querySelectorAll('select')[1];
                let inp = rowElm.querySelector('input');
                const colorSelText = colorSel.options[colorSel.selectedIndex].text;
                const sizeSelText = sizeSel.options[sizeSel.selectedIndex].text;

                if (inp.value == '') {
                    app.modalCloseBtn = false;
                    break;
                } else {
                    app.modalCloseBtn = true;
                }

                app.dblCount = 0;

                for(let rowElm2 of qtyRow){
                    const colorSel2 = rowElm2.querySelectorAll('select')[0];
                    const sizeSel2 = rowElm2.querySelectorAll('select')[1];
                    const colorSelText2 = colorSel2.options[colorSel2.selectedIndex].text;
                    const sizeSelText2 = sizeSel2.options[sizeSel2.selectedIndex].text;

                    if ((colorSelText == colorSelText2) && (sizeSelText == sizeSelText2)) {
                        ++app.dblCount;
                        if (app.dblCount == 1) {
                            app.modalCloseBtn = true;
                        } else {
                            app.modalCloseBtn = false;
                            break;
                        }
                    }
                }
            }
        }*/


    </script>
@endsection

