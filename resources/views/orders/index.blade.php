@php
$pieChartMaterials = [];
@endphp

@extends('layouts.master')

@section('content')

    @include('layouts.partial.notice')

    <div class="main-content-container container-fluid px-4">
        <!-- Page Header -->
        <div class="page-header row no-gutters py-4">
            <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
                <span class="text-uppercase page-subtitle">Overview</span>
                <h3 class="page-title">{{ $pageData['pageName'] }} List</h3>
            </div>
            <div class="float-right col-12 col-sm-8 text-center text-sm-right">
                <span class="text-uppercase page-subtitle">Action</span>
                <h3 class="page-title">
                    <a href="{{ route($pageData['routeFirstName'].'-create') }}" class="btn btn-md btn-white">
                        <i class="material-icons">control_point</i> Add {{ $pageData['pageName'] }}
                    </a>
                </h3>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 mb-3">
                <div id="accordion" style="cursor: pointer">
                    <div class="card card-small mb-1 rounded">
                        <div class="card-header border-bottom rounded">
                            <h6 class="m-0 card-link" data-toggle="collapse" href="#collapseThree">
                                Yearly Chart
                            </h6>
                            <div class="block-handle"></div>
                        </div>
                        <div id="collapseThree" class="collapse show" data-parent="#accordion">
                            <div class="card-body pt-0">
                                <ul class="list-group list-group-flush">
                                    <li class="list-group-item p-2">
                                        <div class="row" style="height: 400px;">
                                            <div class="col-md-12">
                                                <div id="columnChart"></div>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="card card-small mb-1 rounded">
                        <div class="card-header border-bottom rounded">
                            <h6 class="m-0 card-link" data-toggle="collapse" href="#collapseOne">
                                Chart
                            </h6>
                            <div class="block-handle"></div>
                        </div>
                        <div id="collapseOne" class="collapse" data-parent="#accordion">
                            <div class="card-body pt-0">
                                <ul class="list-group list-group-flush">
                                    <li class="list-group-item p-2">
                                        <div class="row" style="height: 400px;">
                                            <div class="col-md-6">
                                                <div id="container"></div>
                                            </div>
                                            <div class="col-md-6">
                                                <div id="container2"></div>
                                            </div>

                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="card card-small mb-1">
                        <div class="card-header border-bottom rounded">
                            <h6 class="m-0 collapsed card-link w-100" data-toggle="collapse" href="#collapseTwo">
                                Summery
                            </h6>
                            <div class="block-handle"></div>
                        </div>
                        <div id="collapseTwo" class="collapse" data-parent="#accordion">
                            <table class="summary-table text-capitalize">
                                <thead>
                                <tr>
                                    <th>SL</th>
                                    <th>Buyer</th>
                                    <th>Qty</th>
                                    <th>%</th>
                                    <th>Value</th>
                                    <th>%</th>
                                    <th>ShipQty</th>
                                    <th>%</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr v-for="(list, key, i) in buyerWiseSummaries">
                                    <td>@{{ ++i }}</td>
                                    <td>@{{ key }}</td>
                                    <td>@{{ list.quantity.reduce(getSum,0) }}</td>
                                    <td>@{{ roundWithFrac((list.quantity.reduce(getSum,0)/orderSummerySum('quantity'))*100) }}</td>
                                    <td>@{{ list.value.reduce(getSum,0) }}</td>
                                    <td>@{{ roundWithFrac((list.value.reduce(getSum,0)/orderSummerySum('value'))*100) }}</td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{--Star filter form--}}
        @include('layouts.tools.filterFormForReport')
        {{--End filter form--}}
        <div class="row">
            <div class="col-lg-12 mb-3">
                <div class="card card-small">
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item p-2">
                            <div class="row">
                                <div class="col-md-2">
                                    <b>Total : </b> <span>{{ $count }} {{ ($count>1)?Str::plural($pageData['pageName']):$pageData['pageName'] }} Found</span>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <div class="card card-small mb-4">
                    <div class="card-header border-bottom">
                        {{--From Controller--}}
                        <h6 class="m-0">Active {{ Str::plural($pageData['pageName']) }}</h6>
                    </div>
                    <div class="card-body p-0 text-center pre-x-scrollable">
                        <!-- Transaction History Table -->
                        <table class="main-data-table d-none text-capitalize" id="main-table">
                            <thead>
                            <tr>
                                <th>SL</th>
                                <th>ID</th>
                                <th>Buyer</th>
                                <th>OrderName</th>
                                <th>Style</th>
                                <th>StyleDesc</th>
                                <th>Image</th>
                                <th>Shipment</th>
                                <th style="background: #ffd3b0;color: #000;">RemDay</th>
                                <th>OrderQty</th>
                                <th>UnitPrice</th>
                                <th>TotValue</th>
                                <th style="background: #93a8ff;color: #000;">ShipQty</th>
                                <th>ShipValue</th>
                                <th>ShortShipVal</th>
                                <th>Curr TNA</th>
                                <th>Status</th>
                                <th>CmDz</th>
                                <th>CM</th>
                                <th>SMV</th>
                                {{--<th>LC/Sales</th>--}}
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($orders as $order)
                                @php
                                        $pieChartMaterials[$order->buyer->name]['quantity'][] = $order->quantity->sum('quantity');
                                        $pieChartMaterials[$order->buyer->name]['value'][] = $order->quantity->sum('quantity')*$order->unit_price;

                                        $dateOfShip = $order->date_of_ship;
                                        $dateForHumans = Carbon::parse($dateOfShip)->diffForHumans(Carbon::now());
                                        $diff = -Carbon::parse($dateOfShip)->diffInDays(Carbon::now(), false)
                                @endphp
                            <tr>
                                {{--SL--}}                  <td>1</td>
                                {{--Job ID--}}              <td>{{ $order->id }}</td>
                                {{--Byuer--}}               <td>{{ $order->buyer->name }}</td>
                                {{--order name--}}          <td>{{ $order->name }}</td>
                                {{--style name--}}          <td>{{ $order->style }}</td>
                                {{--style desc--}}          <td>{{ $order->style_desc }}</td>
                                {{--getGarmentsPicture--}}  <td><img class="zoom" width="50" src="{{ $order->getGarmentsPicture() }}" alt=""></td>
                                {{--Shipment Date--}}       <td>{{ $dateOfShip }}</td>
                                {{--Rem Day--}}             <td> <div class="badge {{ ($diff<=10)?'badge-danger':'badge-secondary' }}">{{ $diff }}</div><br> {{ $dateForHumans }}</td>
                                {{--Order Qty--}}           <td>{{ $order->quantity->sum('quantity') }}</td>
                                {{--Unit Price--}}          <td>{{ $order->unit_price }}</td>
                                {{--Total Price--}}         <td>{{ $totalValue = $order->quantity->sum('quantity')*$order->unit_price }}</td>
                                {{--Ship Qty--}}            <td>{{ $shipmentQty = $order->shipment->sum('quantity') }}</td>
                                {{--Ship value--}}          <td>{{ $shipmentQty*$order->unit_price }}</td>
                                {{--Short Ship value--}}    <td>{{ $shipmentQty?$shipmentQty*$order->unit_price-$totalValue:null }}</td>
                                {{--Order Status--}}        <td>{{ $order->status->name ?? null }}</td>
                                {{--Order Status--}}        <td style="@if($order->statuses=='ShipOut') background: #794d53; color:#fff; @elseif($order->statuses=='Partial') background: #797750; color:#fff; @endif">{{ $order->statuses }}</td>
                                {{--CM from budget table--}}<td>{{ $cm = $order->budget->cm ?? null }}</td>
                                {{--CM from budget table--}}<td>{{ round($order->quantity->sum('quantity') / 12 * $cm, 2) }}</td>
                                {{--SMV--}}                 <td>{{ $order->smv }}</td>
                                {{--Sales--}}               {{--<td>{{ ucfirst($order->sales_user->name) }}</td>--}}

                                <td>
                                    <div class="btn-group btn-group-sm" role="group" aria-label="Table row actions">
                                        {{--<button type="button" class="btn btn-white">
                                            <i class="material-icons">&#xE5CA;</i>
                                        </button>--}}
                                        <a class="btn btn-white" href="{{ route($pageData['routeFirstName'].'-show', $order->id) }}">
                                            <i class="material-icons">&#xE870;</i>
                                        </a>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                            </tbody>
                            <tfoot>
                            <tr>
                                <th>SL</th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th style="background: #93a8ff;color: #000;"></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                {{--<th>LC/Sales</th>--}}
                                <th></th>
                            </tr>
                            </tfoot>
                        </table>
                        <!-- End Transaction History Table -->
                    </div>
                </div>
                {{--{{ $orders->links() }}--}}
            </div>
        </div>
    </div>
    <div class="promo-popup animated bounceIn" style="padding: 10px; border: 2px solid #898989; background: rgba(255,255,255,0.82); right: 10px;">

        <div>
            <a href="#accordion">
            <span class="close">
                <i class="material-icons">pie_chart</i>
            </span>
            </a>
        </div>


        <div>
            <a href="#search-area">
            <span class="close">
                <i class="material-icons">format_align_left</i>
            </span>
            </a>
        </div>

    </div>
@endsection

@section('vue-script')

{{--start php code--}}
    <?php

    $orderModel = new App\Order();

    ?>
{{--end php code--}}


    @include('js/script')
    <script>
        function dataTableFooterSum(obj) {
            let tbl = obj.table;
            [9, 11, 12, 13, 14, 18].forEach(function (col) {
                $(tbl.column(col).footer()).html(
                    roundWithFrac(tbl.column(col).data().sum())
                );
            });

            /*custom sum footer*/
            let totVal = $(tbl.column(11).footer()).text();
            let totOrdQty = $(tbl.column(9).footer()).text();
            let totCm = $(tbl.column(18).footer()).text();

            $(tbl.column(10).footer()).html(
                roundWithFrac(totVal/totOrdQty)
            );
            $(tbl.column(17).footer()).html(
                roundWithFrac(totCm/totOrdQty * 12)
            );
            /*custom sum footer*/
        }


        function piData(type) {
            let $pieChartMaterials = JSON.parse('{{ json_encode($pieChartMaterials) }}'.replace(/&quot;/g,'"'));

            return Object.keys($pieChartMaterials).map(function (k) {
                let val = $pieChartMaterials[k][type].reduce(function (qty, pilot) {
                    return qty + pilot
                },0);
                return {name: k, y:val};
            });
        }

        //summary-table
        vm.buyerWiseSummaries = JSON.parse('{{ json_encode($pieChartMaterials) }}'.replace(/&quot;/g,'"'));

        let piObj = {
            elm: 'container',
            seriesName: 'Quantity',
            heading: 'Buyer Wise Order Quantity',
        };
        let piObj2 = {
            elm: 'container2',
            seriesName: 'Value',
            heading: 'Buyer Wise Order Value',
        };

        Highcharts.chart('columnChart', {

            chart: {
                type: 'column'
            },

            title: {
                text: 'Monthly Order'
            },

            xAxis: {
                categories: [
                    @foreach($orderModel->orderYearWiseMonth(2019)[0] as $month => $monthlyOrder)
                    '{{ $month }}',
                    @endforeach
                ],
            },

            yAxis: [{
                title: {
                    text: 'Quantity axis'
                }
            },
            {
                opposite: true,
                title: {
                    text: 'Value axis'
                }
            }],

            plotOptions: {
                series: {
                    borderWidth: 0,
                    /*dataLabels: {
                        enabled: true,
                        format: '{point.y:.f}'
                    }*/
                },
                column: {
                    pointPadding: 0.05,
                    borderRadius: 3
                }
            },

            series: [{
                name: 'Quantity',
                data: [
                    @foreach($orderModel->orderYearWiseMonth(2019)[0] as $month => $monthlyOrder)
                    {{ array_sum($orderModel->orderYearWiseMonth(2019)[1][$month] ?? []) }},
                    @endforeach
                ]
            }, {
                name: 'Value',
                data: [
                    @foreach($orderModel->orderYearWiseMonth(2019)[0] as $month => $monthlyOrder)
                    {{ array_sum($orderModel->orderYearWiseMonth(2019)[2][$month] ?? []) }},
                    @endforeach
                ],
                yAxis: 1
            }],
            credits: {
                enabled: false
            },

        });

        chartNSummery(piObj, piData('quantity'));
        chartNSummery(piObj2, piData('value'));

        function chartNSummery($piObj, data) {
            // Build the chart
            Highcharts.chart($piObj.elm, {
                // Radialize the colors
                colors: Highcharts.map(Highcharts.getOptions().colors, function (color) {
                    return {
                        radialGradient: {
                            cx: 0.5,
                            cy: 0.3,
                            r: 0.7
                        },
                        stops: [
                            [0, color],
                            [1, Highcharts.Color(color).brighten(-0.3).get('rgb')] // darken
                        ]
                    };
                }),
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie'
                },
                title: {
                    text: $piObj.heading
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.y}</b>'
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: true,
                            format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                            style: {
                                color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                            },
                            connectorColor: 'silver'
                        }
                    }
                },
                series: [{
                    name: $piObj.seriesName,
                    data
                }],
                credits: {
                    enabled: false
                },
            });
        }
    </script>
@endsection