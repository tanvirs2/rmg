@extends('layouts.master')

@section('content')
    @include('layouts.partial.notice')
    <div class="main-content-container container-fluid px-4">
        <!-- Page Header -->
        <div class="page-header row no-gutters py-4">
            <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
                <span class="text-uppercase page-subtitle">Overview</span>
                <h3 class="page-title">{{ isset($role)?'Edit':'Add New' }} Role</h3>
            </div>
        </div>
        <!-- End Page Header -->

        <!-- Default Light Table -->
        <div class="row">

            <div class="col-lg-8">
                <div class="card card-small mb-4">
                    <div class="card-header border-bottom">
                        <h6 class="m-0">Role Details</h6>
                    </div>
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item p-3">
                            <div class="row">
                                <div class="col">
                                    @if(isset($getUser))
                                        <form action="{{ route('role-update', $getUser->id) }}" method="post">
                                        @method('PATCH')
                                    @else
                                        <form action="{{ route('role-store') }}" method="post">
                                    @endif
                                        @csrf

                                        <div class="form-row">
                                            <div class="form-group col-md-6">
                                                <label for="name" class="input-required">Role Name</label>
                                                <input type="text"
                                                       class="form-control"
                                                       id="name"
                                                       placeholder="Name"
                                                       value="{{ isset($role->name)?$role->name:null }}{{ old('name') }}"
                                                       name="name"
                                                >
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label for="code" class="input-required">Code</label>
                                                <input type="text"
                                                       class="form-control"
                                                       id="code"
                                                       placeholder="Last Name"
                                                       value="{{ isset($role->code)?$role->code:null }}{{ old('code') }}"
                                                       name="code"
                                                >
                                            </div>
                                        </div>

                                        <div class="form-row">
                                            <div class="form-group col-md-6">
                                                <label for="feInputState" class="input-required">Operation</label>
                                                <div class='card card-small mb-3'>
                                                    <div class="card-header border-bottom">
                                                        <h6 class="m-0">Select Any</h6>
                                                    </div>
                                                    <div class='card-body p-0'>
                                                        <ul class="list-group list-group-flush">
                                                            <li class="list-group-item px-3 pb-2">
                                                                @foreach($operations as $operation)
                                                                <div class="custom-control custom-checkbox mb-1">
                                                                    <input type="checkbox" @if(isset($role->operations)) {{ in_array($operation->id, $role->operations->pluck('id')->toArray())?'checked':null }} @endif value="{{ $operation->id }}" name="operation[]" class="custom-control-input" id="opration{{ $operation->id }}">

                                                                    <label class="custom-control-label" for="opration{{ $operation->id }}">{{ $operation->name }}</label>
                                                                </div>
                                                                @endforeach
                                                            </li>

                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group col-md-6">
                                                <label class="input-required">Dashboard</label>
                                                <select name="dashboard" class="form-control">
                                                    <option value="" disabled>Select Dashboard</option>
                                                    @foreach($dashboard_types as $dashboard_type)
                                                        <option @if(isset($role)) {{ ($role->dashboard==$dashboard_type)?'selected':'' }} @endif value="{{ $dashboard_type }}">{{ ucfirst($dashboard_type) }}</option>
                                                    @endforeach
                                                </select>
                                            </div>

                                        </div>

                                        <button type="submit" class="btn btn-accent">Create</button>
                                    </form>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- End Default Light Table -->
    </div>
@endsection