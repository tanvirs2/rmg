@extends('layouts.master')

@section('content')
    <div class="main-content-container container-fluid px-4">
        <!-- Page Header -->
        <div class="page-header row no-gutters py-4">
            <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
                <span class="text-uppercase page-subtitle">Overview</span>
                <h3 class="page-title">User Role List</h3>
            </div>
            <div class="float-right col-12 col-sm-8 text-center text-sm-right">
                <span class="text-uppercase page-subtitle">Action</span>
                <h3 class="page-title">
                    <a href="{{ route('role-create') }}" class="btn btn-md btn-white">
                        <i class="material-icons">person</i> Add
                    </a>
                </h3>
            </div>
        </div>
        <!-- End Page Header -->
        <!-- Default Light Table -->
        <div class="row">
            <div class="col">
                <div class="card card-small mb-4">
                    <div class="card-header border-bottom">
                        <h6 class="m-0">Active Users</h6>
                    </div>
                    <div class="card-body p-0 pb-3 text-center">
                        <table class="table mb-0">
                            <thead class="bg-light">
                            <tr>
                                <th scope="col" class="border-0"></th>
                                <th scope="col" class="border-0">Name</th>
                                <th scope="col" class="border-0">Code</th>
                                <th scope="col" class="border-0">Operation</th>
                                <th scope="col" class="border-0">Dashboard</th>
                                <th scope="col" class="border-0">No of Users</th>
                                <th scope="col" class="border-0">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($roles as $role)
                            <tr>
                                <td></td>
                                <td>{{ $role->name }}</td>
                                <td>{{ $role->code }}</td>
                                <td>
                                    @foreach($role->operations as $operation)
                                        <button type="button" class="mb-2 btn btn-sm btn-dark mr-1">{{ $operation->name }}</button>
                                    @endforeach
                                </td>
                                <td>{{ $role->dashboard }}</td>
                                <td>{{ count($role->users) }}</td>
                                <td>
                                    <div class="btn-group btn-group-sm" role="group" aria-label="Table row actions">
                                        <a href="#" class="btn btn-white">
                                            <i class="material-icons"></i>
                                        </a>
                                        <a href="#" class="btn btn-white">
                                            <i class="material-icons"></i>
                                        </a>
                                        <a href="{{ route('role-edit', $role->id) }}" class="btn btn-white">
                                            <i class="material-icons"></i>
                                        </a>
                                        <a class="btn btn-white" onclick="return deleteThisData('{{ route('role-destroy', $role->id) }}')">
                                            <i class="material-icons"></i>
                                        </a>
                                    </div>
                                </td>
                            </tr>
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Default Dark Table -->
    </div>
@endsection