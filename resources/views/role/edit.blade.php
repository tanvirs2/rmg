@extends('layouts.master')

@section('content')
    <div class="alert alert-success alert-dismissible fade show mb-0" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">×</span>
        </button>
        <i class="fa fa-check mx-2"></i>
        <strong>Success!</strong> Your profile has been updated!
    </div>

    <div class="main-content-container container-fluid px-4">
        <!-- Page Header -->
        <div class="page-header row no-gutters py-4">
            <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
                <span class="text-uppercase page-subtitle">Overview</span>
                <h3 class="page-title">Edit Role</h3>
            </div>
        </div>
        <!-- End Page Header -->

        <!-- Default Light Table -->
        <div class="row">

            <div class="col-lg-8">
                <div class="card card-small mb-4">
                    <div class="card-header border-bottom">
                        <h6 class="m-0">Role Details</h6>
                    </div>
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item p-3">
                            <div class="row">
                                <div class="col">
                                    <form action="{{ route('role-store') }}" method="post">
                                        @csrf
                                        <div class="form-row">
                                            <div class="form-group col-md-6">
                                                <label for="name" class="input-required">Role Name</label>
                                                <input type="text"
                                                       class="form-control"
                                                       id="name"
                                                       placeholder="First Name"
                                                       value="{{ $role->name }}"
                                                       name="name"
                                                >
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label for="code" class="input-required">Code</label>
                                                <input type="text"
                                                       class="form-control"
                                                       id="code"
                                                       placeholder="Last Name"
                                                       value="1"
                                                       name="code"
                                                >
                                            </div>
                                        </div>

                                        <div class="form-row">
                                            <div class="form-group col-md-6">
                                                <label for="feInputState" class="input-required">Operation</label>
                                                <div class='card card-small mb-3'>
                                                    <div class="card-header border-bottom">
                                                        <h6 class="m-0">Select Any</h6>
                                                    </div>
                                                    <div class='card-body p-0'>
                                                        <ul class="list-group list-group-flush">
                                                            <li class="list-group-item px-3 pb-2">
                                                                @foreach($operations as $operation)
                                                                <div class="custom-control custom-checkbox mb-1">
                                                                    <input type="checkbox" value="{{ $operation->id }}" name="operation[]" class="custom-control-input" id="opration{{ $operation->id }}">
                                                                    <label class="custom-control-label" for="opration{{ $operation->id }}">{{ $operation->name }}</label>
                                                                </div>
                                                                @endforeach
                                                            </li>

                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group col-md-6">
                                                <label for="dashboard" class="input-required">Dashboard</label>
                                                <select id="dashboard" name="dashboard" class="form-control">
                                                    <option value="" disabled selected>Select Dashboard</option>
                                                    @foreach($dashboard_types as $dashboard_type)
                                                        <option value="{{ $dashboard_type }}">{{ ucfirst($dashboard_type) }}</option>
                                                    @endforeach
                                                </select>
                                            </div>

                                        </div>

                                        <button type="submit" class="btn btn-accent">Create</button>
                                    </form>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- End Default Light Table -->
    </div>
@endsection