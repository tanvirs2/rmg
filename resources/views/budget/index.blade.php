@php
use \Illuminate\Support\Carbon;
$pieChartMaterials = [];

@endphp

@extends('layouts.master')

@section('content')

    @include('layouts.partial.notice')

    <div class="main-content-container container-fluid px-4">
        <!-- Page Header -->
        <div class="page-header row no-gutters py-4">
            <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
                <span class="text-uppercase page-subtitle">Overview</span>
                <h3 class="page-title">{{ $pageData['pageName'] }} List</h3>
            </div>
            {{--<div class="float-right col-12 col-sm-8 text-center text-sm-right">
                <span class="text-uppercase page-subtitle">Action</span>
                <h3 class="page-title">
                    <a href="{{ route($pageData['routeFirstName'].'-create') }}" class="btn btn-md btn-white">
                        <i class="material-icons">control_point</i> Add {{ $pageData['pageName'] }}
                    </a>
                </h3>
            </div>--}}
        </div>
        <div class="row">
            <div class="col-lg-12 mb-3">
                <div id="accordion" style="cursor: pointer">
                    <div class="card card-small mb-1 rounded">
                        <div class="card-header border-bottom rounded">
                            <h6 class="m-0 card-link" data-toggle="collapse" href="#collapseOne">
                                Chart
                            </h6>
                            <div class="block-handle"></div>
                        </div>
                        <div id="collapseOne" class="collapse show" data-parent="#accordion">
                            <div class="card-body pt-0">
                                <ul class="list-group list-group-flush">
                                    <li class="list-group-item p-2">
                                        <div class="row" style="height: 400px;">
                                            <div class="col-md-12">
                                                <div id="container"></div>
                                            </div>

                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="card card-small mb-1">
                        <div class="card-header border-bottom rounded">
                            <h6 class="m-0 collapsed card-link w-100" data-toggle="collapse" href="#collapseTwo">
                                Summery
                            </h6>
                            <div class="block-handle"></div>
                        </div>
                        <div id="collapseTwo" class="collapse" data-parent="#accordion">
                            <table class="summary-table text-capitalize">
                                <thead>
                                <tr>
                                    <th>Quantity</th>
                                    <th>Value</th>
                                    <th>%</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>Yarn</td>
                                    <td>@{{ Yarn }}</td>
                                    <td>@{{ (Yarn/js_orderVal*100).toFixed(3) }} %</td>
                                </tr>
                                <tr>
                                    <td>Knitting</td>
                                    <td>@{{ Knitting }}</td>
                                    <td>@{{ (Knitting/js_orderVal*100).toFixed(3) }} %</td>
                                </tr>
                                <tr>
                                    <td>Dying</td>
                                    <td>@{{ Dying }}</td>
                                    <td>@{{ (Dying/js_orderVal*100).toFixed(3) }} %</td>
                                </tr>
                                <tr>
                                    <td>AOP</td>
                                    <td>@{{ AOP }}</td>
                                    <td>@{{ (AOP/js_orderVal*100).toFixed(3) }} %</td>
                                </tr>
                                <tr>
                                    <td>YD</td>
                                    <td>@{{ YD }}</td>
                                    <td>@{{ (YD/js_orderVal*100).toFixed(3) }} %</td>
                                </tr>
                                <tr>
                                    <td>Print</td>
                                    <td>@{{ Print }}</td>
                                    <td>@{{ (Print/js_orderVal*100).toFixed(3) }} %</td>
                                </tr>
                                <tr>
                                    <td>Embroidery</td>
                                    <td>@{{ Embroidery }}</td>
                                    <td>@{{ (Embroidery/js_orderVal*100).toFixed(3) }} %</td>
                                </tr>
                                <tr>
                                    <td>Accessories</td>
                                    <td>@{{ Accessories }}</td>
                                    <td>@{{ (Accessories/js_orderVal*100).toFixed(3) }} %</td>
                                </tr>
                                <tr>
                                    <td>Test</td>
                                    <td>@{{ Test }}</td>
                                    <td>@{{ (Test/js_orderVal*100).toFixed(3) }} %</td>
                                </tr>
                                <tr>
                                    <td>CM</td>
                                    <td>@{{ CM }}</td>
                                    <td>@{{ (CM/js_orderVal*100).toFixed(3) }} %</td>
                                </tr>
                                <tr>
                                    <td>Commercial</td>
                                    <td>@{{ Commercial }}</td>
                                    <td>@{{ (Commercial/js_orderVal*100).toFixed(3) }} %</td>
                                </tr>
                                <tr>
                                    <td>Buying </td>
                                    <td>@{{ Buying  }}</td>
                                    <td>@{{ (Buying /js_orderVal*100).toFixed(3) }} %</td>
                                </tr>
                                <tr>
                                    <td>Other</td>
                                    <td>@{{ Other }}</td>
                                    <td>@{{ (Other/js_orderVal*100).toFixed(3) }} %</td>
                                </tr>
                                <tr>
                                    <td>Profit </td>
                                    <td>@{{ Profit  }}</td>
                                    <td>@{{ (Profit/js_orderVal*100).toFixed(3) }} %</td>
                                </tr>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{--Star filter form--}}
        @include('layouts.tools.filterFormForReport')
        {{--End filter form--}}
        <div class="row">
            <div class="col-lg-12 mb-3">
                <div class="card card-small">
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item p-2">
                            <div class="row">
                                <div class="col-md-2">
                                    <b>Total : </b> <span>{{ $count }} {{ ($count>1)?Str::plural($pageData['pageName']):$pageData['pageName'] }} Found</span>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <div class="card card-small mb-4">
                    <div class="card-header border-bottom">
                        {{--From Controller--}}
                        <h6 class="m-0">Active {{ Str::plural($pageData['pageName']) }}</h6>
                    </div>
                    <div class="card-body p-0 text-center pre-x-scrollable">
                        <!-- Transaction History Table -->
                        <table class="main-data-table d-none text-capitalize" id="main-table">
                            <thead>
                            <tr>
                                <th>SL</th>
                                <th>ID</th>
                                <th>Buyer</th>
                                <th>OrderName</th>
                                <th>Style</th>
                                <th>StyleDesc</th>
                                <th>Image</th>
                                <th>Shipment</th>
                                <th style="background: #ffd3b0;color: #000;">RemDay</th>
                                <th>OrderQty</th>
                                <th>UnitPrice</th>
                                <th>TotValue</th>
                                <th style="background: #93a8ff;color: #000;">ShipQty</th>
                                <th>ShipValue</th>
                                <th>ShortShipVal</th>
                                <th>Curr TNA</th>
                                <th>Status</th>
                                <th>SMV</th>

                                <th class="bg-success">YrnConsum</th>
                                <th>YrnPrice<small class="text-danger"> (kg)</small></th>
                                <th>BgtYrnCst</th>
                                <th>KnttngPrice<small class="text-danger"> (kg)</small></th>
                                <th>BgtKntCst </th>
                                <th>Dyng<small class="text-danger"> (kg)</small></th>
                                <th>BgtDyngCst</th>
                                <th>AOP<small class="text-danger"> (kg)</small></th>
                                <th>BgtAOP</th>

                                <th>YD<small class="text-danger"> (kg)</small></th>
                                <th>BgtYD</th>

                                <th>CmPr<small class="text-danger"> (Dz)</small></th>
                                <th>BgtCm </th>
                                <th>Print<small class="text-danger"> (Dz)</small></th>
                                <th>BgtPrntCst</th>
                                <th>Embroidery<small class="text-danger"> (Dz)</small></th>
                                <th>BgtEmbroCst</th>
                                <th>Accessories<small class="text-danger"> (Dz)</small> </th>
                                <th>BgtAccCst </th>
                                <th>TstCst<small class="text-danger"> (Dz)</small> </th>
                                <th>BgtTstCst </th>
                                <th>Commer<small class="text-danger"> (Dz)</small></th>
                                <th>BgtCommr </th>
                                <th>Cmmssn<small class="text-danger"> (Dz)</small></th>
                                <th>BgtCmmssn </th>
                                <th>OtherCst<small class="text-danger"> (Dz)</small></th>
                                <th>BgtOtherCst </th>
                                <th>BgtCst </th>
                                <th>PrftOrLoss </th>
                                {{--<th>LC/Sales</th>--}}
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($orders as $order)
                                @php
                                    $orderQty = $order->quantity->sum('quantity');

                                    $pieChartMaterials[$order->buyer->name]['quantity'][] = $orderQty;
                                    $pieChartMaterials[$order->buyer->name]['value'][] = $orderQty*$order->unit_price;

                                    $dateOfShip = $order->date_of_ship;
                                    $dateForHumans = Carbon::parse($dateOfShip)->diffForHumans(Carbon::now());
                                    $diff = -Carbon::parse($dateOfShip)->diffInDays(Carbon::now(), false)
                                @endphp
                            <tr>
                                {{--SL--}}                  <td>1</td>
                                {{--Job ID--}}              <td>{{ $order->id }}</td>
                                {{--Byuer--}}               <td>{{ $order->buyer->name }}</td>
                                {{--order name--}}          <td>{{ $order->name }}</td>
                                {{--style name--}}          <td>{{ $order->style }}</td>
                                {{--style desc--}}          <td>{{ $order->style_desc }}</td>
                                {{--getGarmentsPicture--}}  <td><img class="zoom" width="50" src="{{ $order->getGarmentsPicture() }}" alt=""></td>
                                {{--Shipment Date--}}       <td>{{ $dateOfShip }}</td>
                                {{--Rem Day--}}             <td> <div class="badge {{ ($diff<=10)?'badge-danger':'badge-secondary' }}">{{ $diff }}</div><br> {{ $dateForHumans }}</td>
                                {{--Order Qty--}}           <td>{{ $orderQty }}</td>
                                {{--Unit Price--}}          <td>{{ $order->unit_price }}</td>
                                {{--Total Price--}}         <td>{{ $totalValue = $orderQty*$order->unit_price }}</td>
                                {{--Ship Qty--}}            <td>{{ $shipmentQty = $order->shipment->sum('quantity') }}</td>
                                {{--Ship value--}}          <td>{{ $shipmentQty * $order->unit_price }}</td>
                                {{--Short Ship value--}}    <td>{{ $shipmentQty ? $shipmentQty * $order->unit_price - $totalValue : null }}</td>
                                {{--Order Status--}}        <td>{{ $order->status->name ?? null }}</td>
                                {{--Order Status--}}        <td style="@if($order->statuses=='ShipOut') background: #794d53; color:#fff; @elseif($order->statuses=='Partial') background: #797750; color:#fff; @endif">{{ $order->statuses }}</td>
                                {{--SMV--}}                 <td>{{ $order->smv }}</td>

                                {{--yarn_consumption--}}    <td>{{ $yarn_consumption = $order->budget->yarn_consumption ?? null }}</td>
                                {{--yarn_price--}}          <td>{{ $yarn_price = $order->budget->yarn_price ?? null }}</td>
                                {{--yarn_price Bdgt Bdgt--}}<td>{{ round($yarn_price * $yarn_consumption * $orderQty / 12, 2) ?? null }}</td>

                                {{--knitting_price--}}      <td>{{ $knitting_price = $order->budget->knitting_price ?? null }}</td>
                                {{--knitting_price Bdgt--}} <td>{{ round($knitting_price * $yarn_consumption * $orderQty / 12, 2) ?? null }}</td>

                                {{--dyeing_price--}}        <td>{{ $dyeing_price = $order->budget->dyeing_price ?? null }}</td>
                                {{--dyeing_price Bdgt--}}   <td>{{ round($dyeing_price * $yarn_consumption * $orderQty / 12, 2) ?? null }}</td>

                                {{--aop--}}                 <td>{{ $aop = $order->budget->aop ?? null }}</td>
                                {{--aop Bdgt--}}            <td>{{ round($aop * $yarn_consumption * $orderQty / 12, 2) ?? null }}</td>
                                {{--yd--}}                 <td>{{ $yd = $order->budget->yd ?? null }}</td>
                                {{--yd Bdgt--}}            <td>{{ round($yd * $yarn_consumption * $orderQty / 12, 2) ?? null }}</td>

                                {{--SMV--}}                 <td>{{ $cm = $order->budget->cm ?? null }}</td>
                                {{--SMV--}}                 <td>{{ round($cm * $orderQty / 12, 2) ?? null }}</td>

                                {{--SMV--}}                 <td>{{ $print = $order->budget->print ?? null }}</td>
                                {{--SMV--}}                 <td>{{ round($print * $orderQty / 12, 2) ?? null }}</td>

                                {{--SMV--}}                 <td>{{ $embroidery = $order->budget->embroidery ?? null }}</td>
                                {{--SMV--}}                 <td>{{ round($embroidery * $orderQty / 12, 2) ?? null }}</td>

                                {{--SMV--}}                 <td>{{ $accessories = $order->budget->accessories ?? null }}</td>
                                {{--SMV--}}                 <td>{{ round($accessories * $orderQty / 12, 2) ?? null }}</td>

                                {{--SMV--}}                 <td>{{ $test_cost = $order->budget->test_cost ?? null }}</td>
                                {{--SMV--}}                 <td>{{ round($test_cost * $orderQty / 12, 2) ?? null }}</td>

                                {{--SMV--}}                 <td>{{ $bank_charge = $order->budget->bank_charge ?? null }}</td>
                                {{--SMV--}}                 <td>{{ round($bank_charge * $orderQty / 12, 2) ?? null }}</td>

                                {{--SMV--}}                 <td>{{ $commission = $order->budget->commission ?? null }}</td>
                                {{--SMV--}}                 <td>{{ round($commission * $orderQty / 12, 2) ?? null }}</td>

                                {{--SMV--}}                 <td>{{ $others = $order->budget->others ?? null }}</td>
                                {{--SMV--}}                 <td>{{ round($others * $orderQty / 12, 2) ?? null }}</td>

                                {{--SMV--}}                 <td>{{ $budgetCost = round(
                                                                     $yarn_price
                                                                    +$knitting_price
                                                                    +$dyeing_price
                                                                    +$aop
                                                                    +$yd
                                                                    +$cm
                                                                    +$print
                                                                    +$embroidery
                                                                    +$accessories
                                                                    +$test_cost
                                                                    +$bank_charge
                                                                    +$commission
                                                                    +$others
                                                                    , 2) }}
                                                            </td>
                                {{--SMV--}}                 <td>{{ round(($orderQty * $order->unit_price) - $budgetCost, 2) }}</td>
                                {{--Sales--}}               {{--<td>{{ ucfirst($order->sales_user->name) }}</td>--}}

                                <td>
                                    <div class="btn-group btn-group-sm" role="group" aria-label="Table row actions">
                                        {{--<button type="button" class="btn btn-white">
                                            <i class="material-icons">&#xE5CA;</i>
                                        </button>--}}
                                        <a class="btn btn-white" href="{{ route($pageData['routeFirstName'].'-show', [$order->id, 'page' => 'budget']) }}">
                                            <i class="material-icons">&#xE870;</i>
                                        </a>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                            </tbody>
                            <tfoot>
                            <tr>
                                <th>SL</th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th style="background: #93a8ff;color: #000;"></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                            </tr>
                            </tfoot>
                        </table>
                        <!-- End Transaction History Table -->
                    </div>
                </div>
                {{ $orders->links() }}
            </div>
        </div>
    </div>
    <div class="promo-popup animated bounceIn" style="padding: 10px; border: 2px solid #898989; background: rgba(255,255,255,0.82); right: 10px;">

        <div>
            <a href="#accordion">
            <span class="close">
                <i class="material-icons">pie_chart</i>
            </span>
            </a>
        </div>


        <div>
            <a href="#search-area">
            <span class="close">
                <i class="material-icons">format_align_left</i>
            </span>
            </a>
        </div>


    </div>
@endsection

@section('vue-script')
    <script>

        const data = {
            js_orderQty: 0,
            js_orderVal: 0,

            Yarn: 0,
            Knitting: 0,
            Dying: 0,
            AOP: 0,
            YD: 0,
            Print: 0,
            Embroidery: 0,
            Accessories: 0,
            Test: 0,
            CM: 0,
            Commercial: 0,
            Buying : 0,
            Other: 0,
            Profit :0
        };

        const app = new Vue({
            el: '#app',
            data
        });

        function dataTableFooterSum(obj) {
            let tbl = obj.table;

            function columnIndex(colIndex){
                return roundWithFrac(tbl.column(colIndex).data().sum());
            }

            [9, 10, 11, 12, 13, 14, 17, 18, 20, 22, 24, 26, 28, 30, 32, 34, 36, 38, 40, 42, 44, 46].forEach(function (colIndex) {
                $(tbl.column(colIndex).footer()).html(
                    columnIndex(colIndex)
                );
            });

            app.js_orderQty = columnIndex(9);
            app.js_orderVal = columnIndex(11);

            Highcharts.chart('container', {
                chart: {
                    type: 'pie',
                    options3d: {
                        enabled: true,
                        alpha: 60,
                        beta: 0
                    }
                },
                title: {
                    text: 'Budget Chart'
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        depth: 45,
                        dataLabels: {
                            enabled: true,
                            format: '{point.name} : {point.y}'
                        }
                    }
                },
                series: [{
                    type: 'pie',
                    name: 'Budget',
                    data: [
                        ['Yarn', app.Yarn = columnIndex(20)],
                        ['Knitting', app.Knitting = columnIndex(22)],
                        ['Dying', app.Dying = columnIndex(24)],
                        ['AOP', app.AOP = columnIndex(26)],
                        ['YD', app.YD = columnIndex(28)],
                        ['CM', app.CM = columnIndex(30)],
                        ['Print', app.Print = columnIndex(32)],
                        ['Embroidery', app.Embroidery = columnIndex(34)],
                        ['accessories', app.Accessories = columnIndex(36)],
                        ['Test', app.Test = columnIndex(38)],
                        ['Commercial', app.Commercial = columnIndex(40)],
                        ['Buying Commission', app.Buying  = columnIndex(42)],
                        ['Other', app.Other = columnIndex(44)],
                        ['Profit Or Loss', app.Profit  = columnIndex(46)]
                    ]
                }]
            });
        }


    </script>
@endsection