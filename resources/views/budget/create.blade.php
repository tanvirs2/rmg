@extends('layouts.master')

@section('content')

    @include('layouts.partial.notice')

    <div class="main-content-container container-fluid px-4">
        <!-- Page Header -->
        <div class="page-header row no-gutters py-4">
            <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
                <span class="text-uppercase page-subtitle">Overview</span>
                <h3 class="page-title"> {{ isset($isSetModel)? 'Edit':'Place' }} {{ $pageData['pageName'] }}</h3>
            </div>
        </div>
        <!-- End Page Header -->
        <!-- Default Light Table -->

        @if(isset($isSetModel))
        <form action="{{ route($pageData['routeFirstName'].'-update', $isSetModel->id) }}" method="post" enctype="multipart/form-data">
            @method('PATCH')
        @else
        <form action="{{ route($pageData['routeFirstName'].'-store') }}" method="post" enctype="multipart/form-data">
        @endif
            @csrf
            <div class="row">
                <div class="col-lg-6">

                    <div class="card card-small mb-4">
                                    <div class="card-header border-bottom">
                                        <h6 class="m-0">{{ $pageData['pageName'] }} Details</h6>
                                    </div>
                                    <ul class="list-group list-group-flush">
                                        <li class="list-group-item p-3">
                                            <div class="row">
                                                <div class="col">


                                                    <div class="form-row">
                                                        <div class="card card-small mb-4 col-md-7">
                                                            <div class="card-header border-bottom text-center">
                                                                <div class="mx-auto">
                                                                    <img class="rounded-top" :src="userImg"
                                                                         alt="{{ $pageData['pageName'] }} Avatar"
                                                                         width="100"></div>
                                                                <h4 class="">@{{ fullName }}</h4>
                                                                <span class="text-muted d-block">@{{ buyerName }}</span>
                                                            </div>
                                                            <ul class="list-group list-group-flush">
                                                                <li class="list-group-item">
                                                                    <strong class="text-muted d-block mb-2">About {{ $pageData['pageName'] }}</strong>
                                                                    <span>
                                                                        @{{ aboutUser }}
                                                                    </span>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                        <div class="col-md-1">
                                                        </div>
                                                        <div class="card card-small mb-5 mt-4 p-2 col-md-4">
                                                            <label>Garments Picture</label>
                                                            <input name="image" type="file" @change="readURL"
                                                                   class="form-control">
                                                        </div>
                                                    </div>


                                                    <div class="form-row">
                                                        <div class="form-group col-md-6">
                                                            <label>{{ $pageData['pageName'] }}
                                                                Name</label>
                                                            <input v-model="fullName" name="name" type="text"
                                                                   class="form-control"
                                                                   placeholder="{{ $pageData['pageName'] }} Name"
                                                                   value="" required>
                                                        </div>

                                                        <div class="form-group col-md-6">
                                                            <label>Buyer Name
                                                                <a target="_blank" href="{{ route('buyer-create') }}">
                                                                    <i class="material-icons">control_point</i>
                                                                </a>
                                                            </label>
                                                            <select class="select2-box form-control" name="buyer_id" laravel-model="Buyer" laravel-get-from="name" laravel-belongs="agent">
                                                                <option value="{{ $isSetModel->buyer_id ?? null }}">{{ $isSetModel->buyer->name ?? null }}</option>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="form-row">
                                                        <div class="form-group col-md-4">
                                                            <label>Style Name</label>
                                                            <input name="style" type="text"
                                                                   class="form-control"
                                                                   placeholder="Style Name"
                                                                   value="{{ $isSetModel->style ?? null }}">
                                                        </div>
                                                        <div class="form-group col-md-8">
                                                            <label>Style Description</label>
                                                            <input value="{{ $isSetModel->style_desc ?? null }}"
                                                                   name="style_desc" type="text" class="form-control"
                                                                   placeholder="Style Description">
                                                        </div>
                                                        <div class="form-group col-md-4">
                                                            <label>Order Accept Date</label>
                                                            <input name="order_accept_date" type="date" class="form-control"
                                                                   value="{{ $isSetModel->order_accept_date ?? null }}">
                                                        </div>
                                                        <div class="form-group col-md-4">
                                                            <label>Shipment Date</label>
                                                            <input name="date_of_ship" type="date" class="form-control" placeholder="Shipment Date"
                                                                   value="{{ $isSetModel->date_of_ship ?? null }}">
                                                        </div>

                                                        <div class="form-group col-md-4">
                                                            <label>Order Quantity</label>
                                                            <input name="quantity"
                                                                   type="number"
                                                                   class="form-control"
                                                                   value="{{ $isSetModel->quantity ?? null }}"
                                                                   placeholder="Order Quantity">
                                                        </div>

                                                        <div class="form-group col-md-4">
                                                            <label>Unit Price</label>
                                                            <input name="unit_price"
                                                                   value="{{ $isSetModel->unit_price ?? null }}"
                                                                   type="number" step="any"
                                                                   class="form-control" placeholder="Unit Price">
                                                        </div>
                                                        <div class="form-group col-md-4">
                                                            <label>
                                                                Category
                                                                <a target="_blank" href="{{ route('order-sub-category-create') }}">
                                                                    <i class="material-icons">control_point</i>
                                                                </a>
                                                            </label>
                                                            <select class="select2-box form-control" name="sub_category_id" laravel-model="OrderSubCategory" laravel-get-from="name" laravel-belongs="orderCategory">
                                                                <option value="{{ $isSetModel->sub_category_id ?? null }}">{{ $isSetModel->subCategory->name ?? null }}</option>
                                                            </select>
                                                        </div>
                                                        <div class="form-group col-md-4">
                                                            <label>{{ $pageData['pageName'] }} TNA
                                                                <a target="_blank" href="{{ route('order-status-create') }}">
                                                                    <i class="material-icons">control_point</i>
                                                                </a>
                                                            </label>
                                                            <select class="select2-box form-control" name="status_id" laravel-model="OrderStatus" laravel-get-from="name">
                                                                <option value="{{ $isSetModel->status_id ?? null }}">{{ $isSetModel->status->name ?? null }}</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-row">
                                                        <div class="form-group col-md-2">
                                                            <label>SMV</label>
                                                            <input name="smv"
                                                                   type="number"
                                                                   value="{{ $isSetModel->smv ?? null }}"
                                                                   class="form-control"
                                                                   placeholder="SMV">
                                                        </div>

                                                        <div class="form-group col-md-5">
                                                            <label>Sales Person
                                                                <a target="_blank" href="{{ route('user-create') }}">
                                                                    <i class="material-icons">control_point</i>
                                                                </a>
                                                            </label>
                                                            <select class="select2-box form-control" name="sales_user_id" laravel-model="User" laravel-get-from="name" laravel-belongs="team">
                                                                <option value="{{ $isSetModel->sales_user_id ?? null }}">{{ $isSetModel->sales_user->name ?? null }}</option>
                                                            </select>
                                                        </div>

                                                        <div class="form-group col-md-5">
                                                            <label>Reporting To
                                                                <a target="_blank" href="{{ route('user-create') }}">
                                                                    <i class="material-icons">control_point</i>
                                                                </a>
                                                            </label>
                                                            <select class="select2-box form-control" name="reporting_to_user_id" laravel-model="User" laravel-get-from="name" laravel-belongs="team">
                                                                <option value="{{ $isSetModel->reporting_to_user_id ?? null }}">{{ $isSetModel->reporting_user->name ?? null }}</option>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="form-row">
                                                        <div class="form-group col-md-12">
                                                            <label>About {{ $pageData['pageName'] }}</label>
                                                            <textarea v-model="aboutUser" name="description"
                                                                      class="form-control" rows="5">

                                                            </textarea>
                                                        </div>
                                                    </div>
                                                    <button type="submit" class="btn btn-accent">Save</button>

                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>

                </div>

                            {{--Budget--}}
                <div class="col-lg-6">
                    <div class="card card-small mb-2">
                        <div class="card-header border-bottom">
                            <h6 class="m-0">Profit or Loss</h6>
                        </div>
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item p-3">
                                <div class="row">
                                    <div class="col">
                                        <div class="form-row">
                                            <div class="form-group col-md-6">
                                                <label>FOB Price</label>
                                                <div class="form-control">
                                                    @{{ roundWithFrac(fob_Price) }}
                                                </div>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label>Confirm Price</label>
                                                <div class="form-control">
                                                    @{{ roundWithFrac(fob_Price/12) }}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="card card-small">
                                    <div class="card-header border-bottom">

                                        <h6 class="m-0">
                                            {{ isset($isSetModel)? 'Edit': null }} {{ $pageData['pageName'] }} Budget

                                            @if(request()->get('edit')=='order')
                                                <span class="custom-control custom-toggle custom-toggle-sm mb-1">
                                                    <input type="checkbox" id="customToggle1" name="budgetEditEnable" class="custom-control-input">
                                                    <label class="custom-control-label" for="customToggle1"></label>
                                                </span>
                                            @endif
                                        </h6>

                                    </div>
                                    <ul class="list-group list-group-flush">
                                        <li class="list-group-item p-3">
                                            <div class="row">
                                                <div class="col">
                                                    <div class="form-row">

                                                        <div class="form-group col-md-3">
                                                            <label>Yarn Price</label>
                                                            <input name="yarn_price" type="number" class="form-control"
                                                                   step="any"
                                                                   value="{{ $isSetModel->budget->yarn_price ?? null }}"
                                                                   v-model="yarn_price"
                                                                   placeholder="Yarn Price"
                                                                   value="{{ isset($isSetModel->phone)?$isSetModel->phone:old('phone') }}">
                                                        </div>
                                                        <div class="form-group col-md-3">
                                                            <label>Knitting Price</label>
                                                            <input name="knitting_price" type="number" class="form-control"
                                                                   step="any"
                                                                   v-model="knitting_price"
                                                                   placeholder="Knitting Price"
                                                                   value="{{ isset($isSetModel->phone)?$isSetModel->phone:old('phone') }}">
                                                        </div>
                                                        <div class="form-group col-md-3">
                                                            <label>Dyeing Price</label>
                                                            <input name="dyeing_price" type="number" class="form-control"
                                                                   step="any"
                                                                   v-model="dyeing_price"
                                                                   placeholder="Dyeing Price"
                                                                   value="{{ isset($isSetModel->phone)?$isSetModel->phone:old('phone') }}">
                                                        </div>
                                                        <div class="form-group col-md-3">
                                                            <label>AOP/YD</label>
                                                            <input name="aop_or_yd" type="number" class="form-control"
                                                                   step="any"
                                                                   placeholder="AOP/YD"
                                                                   value="{{ $isSetModel->budget->aop_or_yd ?? null }}">
                                                        </div>
                                                    </div>
                                                    <div class="form-row bg-light-gray">

                                                        <div class="form-group col-md-3">
                                                            <label>Accessories</label>
                                                            <input name="accessories" type="number" class="form-control"
                                                                   step="any"
                                                                   v-model="accessories"
                                                                   placeholder="Accessories">
                                                        </div>
                                                        <div class="form-group col-md-3">
                                                            <label>Test Cost</label>
                                                            <input name="test_cost" type="number" class="form-control"
                                                                   step="any"
                                                                   placeholder="Test Cost"
                                                                   value="{{ $isSetModel->budget->test_cost ?? null }}">
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <label>Print+Embroidery</label>
                                                            <input name="print_and_embroidery" type="number" class="form-control"
                                                                   step="any"
                                                                   placeholder="Print+Embroidery"
                                                                   value="{{ $isSetModel->budget->print_and_embroidery ?? null }}">
                                                        </div>

                                                    </div>

                                                    <div class="form-row">
                                                        <div class="form-group col-md-4">
                                                            <label>Bank Charge</label>
                                                            <input name="bank_charge" type="number" class="form-control"
                                                                   step="any"
                                                                   v-model="bank_charge"
                                                                   placeholder="Bank Charge">
                                                        </div>
                                                        <div class="form-group col-md-4">
                                                            <label>Commission</label>
                                                            <input name="commission" type="number" class="form-control"
                                                                   step="any"
                                                                   v-model="commission"
                                                                   placeholder="Commission">
                                                        </div>
                                                        <div class="form-group col-md-4">
                                                            <label>CM Per Dozen</label>
                                                            <input name="cm" type="number" class="form-control"
                                                                   step="any"
                                                                   v-model="cm"
                                                                   placeholder="CM PerDz">
                                                        </div>

                                                    </div>

                                                    <div class="form-row bg-light-gray">

                                                        <div class="form-group col-md-6">
                                                            <label>Finish Fabric Consumption</label>
                                                            <input name="finish_fab_consumption" type="number" class="form-control" placeholder="Finish Fabric Consumption"
                                                                   step="any"
                                                                   value="{{ $isSetModel->budget->finish_fab_consumption ?? null }}">
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <label>Yarn Consumption</label>
                                                            <input name="yarn_consumption" type="number" class="form-control"
                                                                   step="any"
                                                                   placeholder="Yarn Consumption"
                                                                   value="{{ $isSetModel->budget->yarn_consumption ?? null }}">
                                                        </div>

                                                    </div>

                                                    <div class="form-row">

                                                        <div class="form-group col-md-3">
                                                            <label>Freight Charge</label>
                                                            <input name="freight_charge" type="number" class="form-control"
                                                                   step="any"
                                                                   placeholder="Freight Charge"
                                                                   value="{{ $isSetModel->budget->freight_charge ?? null }}">
                                                        </div>
                                                        <div class="form-group col-md-3">
                                                            <label>Others</label>
                                                            <input name="others" type="number" class="form-control"
                                                                   step="any"
                                                                   placeholder="Others"
                                                                   value="{{ $isSetModel->budget->others ?? null }}">
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <label>LC / Sales contract</label>
                                                            <input name="lc_or_sales_contract" type="number" class="form-control"
                                                                   step="any"
                                                                   placeholder="LC / Sales contract"
                                                                   value="{{ $isSetModel->budget->lc_or_sales_contract ?? null }}">
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-md-12">
                                                        <label>About Budget</label>
                                                        <textarea name="budget_description"
                                                                  class="form-control" rows="5">{{ $isSetModel->budget->description ?? null }}
                                                        </textarea>
                                                </div>
                                            </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                </div>
            </div>
        </form>
                    <!-- End Default Light Table -->
    </div>
@endsection

@section('vue-script')
    <script>
        @if(isset($isSetModel))
            $(function () {
                $('[name="budgetEditEnable"]').closest('.card-small').find('ul input').prop('disabled', true);

                $('[name="budgetEditEnable"]').click(function () {
                    if ($(this).is(':checked')) {
                        $(this).closest('.card-small').find('ul input').prop('disabled', false);
                    } else {
                        $(this).closest('.card-small').find('ul input').prop('disabled', true);
                    }

                });
            });
        @endif
    </script>

    <script>
        const data = {
            userImg: '{{ isset($isSetModel) ? $isSetModel->getGarmentsPicture() : asset('profile-pic/garments/garment.png') }}',
            fullName: '{{ isset($isSetModel) ? $isSetModel->name:old('name') }}',
            buyerName: '{{ isset($isSetModel) ? $isSetModel->buyer->name:'' }}',
            aboutUser: '{{ isset($isSetModel) ? $isSetModel->description:'' }}',
            teams: [],
            teamsPhpView: true,

            yarn_price: '{{ $isSetModel->budget->yarn_price ?? null }}',
            knitting_price: '{{ $isSetModel->budget->knitting_price ?? null }}',
            dyeing_price: '{{ $isSetModel->budget->dyeing_price ?? null }}',
            accessories: '{{ $isSetModel->budget->accessories ?? null }}',
            bank_charge: '{{ $isSetModel->budget->bank_charge ?? null }}',
            commission: '{{ $isSetModel->budget->commission ?? null }}',
            cm: '{{ $isSetModel->budget->cm ?? null }}',
        };

        const app = new Vue({
            el: '#app',
            data,
            methods: {
                readURL(input) { //img preview before upload
                    input = input.target;
                    if (input.files && input.files[0]) {
                        var reader = new FileReader();
                        reader.readAsDataURL(input.files[0]);
                        reader.onload = function(e) {
                            this.userImg = e.target.result;
                        }.bind(this);
                    }
                },
                buyerNameF(){
                    $(() => {
                        $('[name="buyer_id"]').change((e) => { //for view role name on profile card
                            this.buyerName = e.target.options[e.target.value].text;
                        })
                    });
                },
                roundWithFrac(number){
                    return Math.round(number * 100)/100;
                }
            },
            mounted(){
                this.buyerNameF();
            },
            computed: {

            }

        });
    </script>
@endsection

