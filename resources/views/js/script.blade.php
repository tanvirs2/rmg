
{{--/**
 * Created by PhpStorm.
 * User: Tanvir
 * Date: 29-Dec-18
 * Time: 7:47 PM
 */--}}


<style>
 .ui-autocomplete {
  border-radius: 5px;
 }

 .ui-menu .ui-menu-item {
  border: 0.5px solid #e3e3e3;
 }

 .ui-menu .ui-menu-item .ui-menu-item-wrapper{
  border-radius: 3px;
 }
 .ui-menu .ui-menu-item .ui-menu-item-wrapper:hover {
  background: #76A4F9;
 }
</style>

<script>
    $(function () {
        $('input.filter-input').autocomplete({
            source: function (req, res) {
                let inputField = this.element[0].name;
                let url = '{{ route('api-get-search-data') }}/{{ Str::studly($pageData['pageName']) }}/'
                    + inputField + '/' + req.term;

                console.log(url);
                axios
                    .get(url)
                    .then(response => {
                            console.log(response.data);
                            res(response.data);
                        }
                    )
                ;
            }
        });
    });

    var data = {
     buyerWiseSummaries: []
    };

    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+1; //January is 0!
    var yyyy = today.getFullYear();

    if(dd<10) {
        dd = '0'+dd
    }

    if(mm<10) {
        mm = '0'+mm
    }

    today =  yyyy + '-' + mm + '-' + dd;

    const vm = new Vue({
        el: '#app',
        data,
        computed: {

        },
        methods: {
            orderSummerySum(type){
                return Object.keys(this.buyerWiseSummaries).map( (k) => {
                   return this.buyerWiseSummaries[k][type].reduce(this.getSum, 0);
                }).reduce(this.getSum,0);
            },
            getSum(total, num) {
                return total + num;
            },
            roundWithFrac(number){
                return Math.round(number * 100)/100;
            },
            per_page(event) {
                event.target.form.submit();
            },

            doit(){
                var DefaultTable = document.getElementById('main-table');

                var instance = new TableExport(DefaultTable, {
                    filename: '{{ $pageData['pageName'] }}-'+ today,
                    exportButtons: false,
                    ignoreCols: {{ $pageData['ignoreColsInExport'] }},
                });

                var exportData = instance.getExportData()['main-table']['xlsx'];
                instance.export2file(exportData.data, exportData.mimeType, exportData.filename, exportData.fileExtension);
            }
        }
    })
</script>