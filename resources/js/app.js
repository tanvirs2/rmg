
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
window.roundWithFrac = function(number){
    return Math.round(number * 100)/100;
};

window.capitalizeFirstLetter = function(string) {
    if (string.substring) {
        return string.charAt(0).toUpperCase() + string.slice(1);
    } else{
        return string;
    }
};
window.snakeCase = function(string) {
    return string.replace(/\.?([A-Z])/g, function (x,y){return "_" + y.toLowerCase()}).replace(/^_/, "");
};

require('./bootstrap');

window.Vue = require('vue');
window.XLSX = require('xlsx');
window.FileSaver = require('file-saverjs');
window.TableExport = require('tableexport');

window.Highcharts = require('highcharts');
window.Highcharts3D = require('highcharts/highcharts-3d.src');
Highcharts3D(Highcharts);

const {Howl, Howler} = require('howler');
window.Howl = Howl;
window.Howler = Howler;
import swal from 'sweetalert';
import 'select2';
require('./select2process');

require('./datatable_functions');

$(function () {
    setTimeout(function () {
        let height = $('.flex-column.side-bar-height').outerHeight(true) + 250;
        $('.flex-column.side-bar-height').css('height', height+'px');
    }, 100);
});

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

//Vue.component('example-component', require('./components/ExampleComponent.vue'));

