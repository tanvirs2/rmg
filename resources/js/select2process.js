window.simulateKey = (iElement) => {
    $( iElement ).focus();
    let e = jQuery.Event("keyup");
    e.which = 39;
    e.keyCode = 39;
    e.charCode = 0;
    $( iElement ).trigger(e);
};

$(function () {

    var alreadyAppendOption = 0;
    var sprintCount = 0;

    window.select2Loader = function() {
        $('.select2-box-manual').select2();
        $('.select2-box-field-filter').select2({
            placeholder: "Select...",
            ajax: {
                url: function (param) {
                    console.log(param);
                    let laravelModelName = $(this).attr('laravel-model');
                    let laravelGetValueFromName = $(this).attr('laravel-get-from');
                    return apiGetSearchData+'/'+laravelModelName+'/'+laravelGetValueFromName+'/'+param.term
                },
                processResults: function (datas) {
                    let res = datas.map((data) => {
                        return { id: data, text: capitalizeFirstLetter(data) }
                    });
                    return {
                        results: res
                    };
                },
            }
        });

        let laravelBelongsName;
        let $eventSelect = $('.select2-box').select2({
            placeholder: "Select...",
        });

        $eventSelect.on("select2:close", function (){
            alreadyAppendOption = 0;
        });

        $eventSelect.on("select2:open", function () {

            if (1 > alreadyAppendOption) {
                ++alreadyAppendOption;

                console.log(++sprintCount);

                let laravelModelName = $(this).attr('laravel-model');
                let laravelGetValueFromName = $(this).attr('laravel-get-from');
                let laravelDirectValue = $(this).attr('laravel-direct-value');
                let id;
                laravelBelongsName = $(this).attr('laravel-belongs') || '';
                let url = apiGetSelectDropdownData+'/'+laravelModelName+'/'+laravelBelongsName;

                $.ajax({
                    url,
                    success: data => {
                        //$(this).html('<option>');

                        (data).forEach(val => {
                            laravelBelongsName = snakeCase(laravelBelongsName);
                            let name = capitalizeFirstLetter(val[laravelGetValueFromName]);

                            id = laravelDirectValue ? name : val.id;

                            let sanitizeVal = val[laravelBelongsName]?{id, text: name+' ('+capitalizeFirstLetter(val[laravelBelongsName].name)+')'}: {id, text:name};

                            $(this).append('<option value="'+sanitizeVal.id+'">'+sanitizeVal.text+'</option>');

                            let map = {};
                            $(this).closest('div').find('select option').each(function () {
                                if (map[this.value]) {
                                    $(this).remove()
                                }
                                map[this.value] = true;
                            });
                            simulateKey($('.select2-search__field'));
                        });
                    }
                });
            }


        });
    };

    select2Loader()
});
