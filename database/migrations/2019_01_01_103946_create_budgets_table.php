<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBudgetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('budgets', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('history_type', ['Running', 'Edit', 'Delete', 'OrderDel'])->default('Running');
            $table->integer('order_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->float('yarn_price')->nullable()->unsigned();
            $table->float('knitting_price')->nullable()->unsigned();
            $table->float('dyeing_price')->nullable()->unsigned();
            $table->float('aop')->nullable()->unsigned();
            $table->float('yd')->nullable()->unsigned();
            /*Dz*/
            $table->float('accessories')->nullable()->unsigned();
            $table->float('test_cost')->nullable()->unsigned();
            $table->float('print')->nullable()->unsigned();
            $table->float('embroidery')->nullable()->unsigned();
            $table->float('bank_charge')->nullable()->unsigned();
            $table->float('commission')->nullable()->unsigned();
            $table->float('cm')->nullable()->unsigned();
            $table->float('finish_fab_consumption')->nullable()->unsigned();
            $table->float('yarn_consumption')->nullable()->unsigned();
            $table->float('freight_charge')->nullable()->unsigned();
            $table->float('lc_or_sales_contract')->nullable()->unsigned();
            $table->float('others')->nullable()->unsigned();
            $table->text('description')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('budgets');
    }
}
