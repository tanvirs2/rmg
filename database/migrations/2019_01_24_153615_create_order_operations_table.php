<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderOperationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_operations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('order_id')->unsigned();
            $table->integer('quantity')->unsigned()->nullable();
            $table->integer('operate_user_id')->unsigned();
            $table->integer('previous_status_id')->unsigned()->nullable();
            $table->integer('status_id')->unsigned()->nullable();
            $table->enum('type', ['tna', 'status', 'shipment_done'])->default('status');
            $table->enum('is_first', ['no', 'yes'])->default('no');
            $table->enum('is_complete', ['no', 'yes'])->default('no');
            $table->enum('tna_meet', ['no', 'yes'])->default('no');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_operations');
    }
}
