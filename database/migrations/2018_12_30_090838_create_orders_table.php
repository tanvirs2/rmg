<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('image')->nullable();
            $table->integer('buyer_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->integer('sales_user_id')->nullable()->unsigned();
            $table->integer('reporting_to_user_id')->nullable()->unsigned();
            $table->integer('sub_category_id')->nullable()->unsigned();
            $table->integer('status_id')->nullable()->unsigned();//process_id
            $table->enum('statuses', ['Running', 'Partial', 'ShipOut'])->default('Running');
            $table->integer('previous_status_id')->unsigned()->nullable();
            $table->string('style')->nullable();
            $table->string('style_desc')->nullable();
            $table->float('unit_price')->unsigned();
            //$table->integer('quantity')->unsigned();
            $table->date('date_of_ship');
            $table->date('order_accept_date');
            $table->text('description')->nullable();
            $table->float('smv')->nullable()->unsigned();
            //$table->float('cm_per_dozen');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
