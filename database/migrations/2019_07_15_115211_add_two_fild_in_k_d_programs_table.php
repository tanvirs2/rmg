<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTwoFildInKDProgramsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('k_d_programs', function (Blueprint $table) {
            $table->string('remarks')->nullable();
            $table->string('lab')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('k_d_programs', function (Blueprint $table) {
            $table->dropColumn('remarks');
            $table->dropColumn('lab');
        });
    }
}
