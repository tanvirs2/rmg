<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->string('raw_password');
            $table->string('phone',50)->nullable();
            $table->integer('reporting_to')->unsigned()->default(0);

            $table->integer('team_id')->unsigned();
            $table->integer('role_id')->unsigned();

            $table->enum('status',['active','inactive','pending'])->default('active');
            $table->string('image')->nullable();
            $table->tinyInteger('gender')->default(1);
            $table->text('address');
            $table->text('description');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
