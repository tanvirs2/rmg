<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyKdExtraTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('k_d_extras', function (Blueprint $table){
            $table->float('req')->nullable()->unsigned();
            $table->enum('type', ['collar', 'cuff'])->nullable();
            $table->dropColumn('collar', 'cuff');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('k_d_extras', function (Blueprint $table){
            $table->dropColumn('type');
            $table->float('cuff')->nullable()->unsigned();
            $table->float('collar')->nullable()->unsigned();
        });
    }
}
