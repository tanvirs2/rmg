<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_histories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('order_id')->unsigned();
            $table->string('name');
            $table->string('image')->nullable();
            $table->integer('buyer_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->integer('sales_user_id')->nullable()->unsigned();
            $table->integer('reporting_to_user_id')->nullable()->unsigned();
            $table->integer('sub_category_id')->nullable()->unsigned();
            $table->integer('status_id')->unsigned()->nullable();//process_id
            $table->enum('statuses', ['Running', 'Partial', 'ShipOut'])->default('Running');
            $table->integer('previous_status_id')->unsigned()->nullable();
            $table->string('style')->nullable();
            $table->string('style_desc')->nullable();
            $table->float('unit_price')->unsigned();
            //$table->integer('quantity')->unsigned();
            $table->date('date_of_ship');
            $table->date('order_accept_date');
            $table->text('description')->nullable();
            $table->integer('smv')->nullable()->unsigned();
            $table->enum('is_first', ['no', 'yes'])->default('no');
            $table->enum('is_complete', ['no', 'yes'])->default('no');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_histories');
    }
}
