<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCollarAndCuffFldInKDPrograms extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('k_d_programs', function (Blueprint $table) {
            //$table->float('collar_and_cuff')->nullable()->unsigned();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('k_d_programs', function (Blueprint $table) {
            //$table->dropColumn('collar_and_cuff')->unsigned();
        });
    }
}
