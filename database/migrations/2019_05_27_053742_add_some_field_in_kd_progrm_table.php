<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSomeFieldInKdProgrmTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('k_d_programs', function (Blueprint $table) {
            $table->integer('order_id')->unsigned();
            $table->integer('color_id')->unsigned();
            $table->integer('kd_parts_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->float('finish_fab')->unsigned();
            $table->float('gray_fab')->unsigned();
            $table->enum('history_type', ['Running', 'Edit', 'Delete', 'OrderDel'])->default('Running');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('k_d_programs', function (Blueprint $table) {
            $table->dropColumn('order_id');
            $table->dropColumn('order_qty_id');
            $table->dropColumn('kd_parts_id');
            $table->dropColumn('user_id');
            $table->dropColumn('finish_fab');
            $table->dropColumn('gray_fab');
            $table->dropColumn('history_type');
        });
    }
}
