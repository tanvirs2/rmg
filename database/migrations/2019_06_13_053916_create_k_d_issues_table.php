<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKDIssuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('k_d_issues', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('kd_program_id');
            $table->date('date');
            $table->float('value')->unsigned();
            $table->enum('issue_type', ['yarn_rcv', 'yarn_issue', 'knit', 'yarn_return', 'dyeing_qty', 'dyeing_return', 'finish_rcv', 'fav_issue']);
            $table->enum('history_type', ['Running', 'Edit', 'Delete', 'OrderDel'])->default('Running');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('k_d_issues');
    }
}
