<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFinishFabNGrayFabFieldInKDExtraTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('k_d_extras', function (Blueprint $table) {
            $table->float('finish_fab')->unsigned();
            $table->float('gray_fab')->unsigned();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('k_d_extras', function (Blueprint $table) {
            $table->dropColumn('finish_fab');
            $table->dropColumn('gray_fab');
        });
    }
}
