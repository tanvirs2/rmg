<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKDExtrasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('k_d_extras', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('kd_register_id')->unsigned();
            $table->integer('order_id')->unsigned();
            $table->integer('color_id')->unsigned();
            $table->float('collar')->nullable()->unsigned();
            $table->float('cuff')->nullable()->unsigned();
            $table->enum('history_type', ['Running', 'Edit', 'Delete', 'OrderDel'])->default('Running');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('k_d_extras');
    }
}
