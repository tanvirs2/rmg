<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_statuses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('code')->nullable();
            $table->enum('status', ['active','inactive','pending'])->default('active');
            $table->integer('public_show_status_id')->default(0);
            $table->enum('is_mail_send', ['yes','no'])->default('no');
            $table->enum('is_sms_send', ['yes','no'])->default('no');
            $table->integer('sms_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_statuses');
    }
}
