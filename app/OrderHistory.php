<?php

namespace App;

use Illuminate\Database\Eloquent\Model, URL;

class OrderHistory extends Model
{
    public function buyer()
    {
        return $this->belongsTo(Buyer::class);
    }
    public function getGarmentsPicture()
    {
        if ($this->image != '') {
            return URL::to('profile-pic/garments/'.$this->image);
        } else {
            return URL::to('profile-pic/garments/garment.png');
        }
    }
}
