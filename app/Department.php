<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    public function users()
    {
        return $this->hasManyThrough(User::class, Team::class);
    }
    public function teams()
    {
        return $this->hasMany(Team::class);
    }
}
