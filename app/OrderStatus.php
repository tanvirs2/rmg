<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderStatus extends Model
{
    public function public_status()
    {
        return $this->belongsTo(OrderStatus::class, 'public_show_status_id');
    }

    public function nextStatuses()
    {
        return $this->belongsToMany(OrderStatus::class, 'order_status_dependencies', 'status_id', 'next_status_id');
    }
}
