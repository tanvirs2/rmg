<?php

namespace App;

use Illuminate\Database\Eloquent\Model, Auth;

class Role extends Model
{
    public function users()
    {
        return $this->hasMany(User::class);
    }
    public function operations()
    {
        return $this->belongsToMany(Operation::class);
    }
    public function managed_user_team_ids()
    {
        $department_ids = $this->managed_user_department_ids();
        //dd(Team::whereIn('department_id',$department_ids)->pluck('id')->toArray());
        return Team::whereIn('department_id',$department_ids)->pluck('id')->toArray();
    }
    public function managed_user_department_ids()
    {
        //dd($this->name);
        $user = Auth::user();
        $department_ids = array();
        if($this->name=='super admin')
        {
            $department_ids = Department::pluck('id')->toArray();
            //dd($department_ids);
        }
        else
        {
            foreach($this->operations as $aO)
            {
                if($aO->name=='manage user all')
                {
                    $department_ids = Department::pluck('id')->toArray();
                }
                if($aO->name=='manage user own department only')
                {
                    $department_ids[] = $user->team->department->id;
                }
            }
        }
        return $department_ids;
    }
}
