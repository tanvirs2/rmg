<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderSizeNColor extends Model
{
    public function color()
    {
        return $this->belongsTo(ColorLib::class);
    }
    public function size()
    {
        return $this->belongsTo(SizeLib::class);
    }

    /*Start Production*/
    public function cutting()
    {
        return $this->hasMany(Production::class, 'order_qty_id')->where('production_type', 'cutting')->where('history_type', 'Running')->latest();
    }

    public function cuttingHistory()
    {
        return $this->hasMany(Production::class, 'order_qty_id')->where('production_type', 'cutting')->whereIn('history_type', ['Edit','Delete'])->latest();
    }

    public function swingIn()
    {
        return $this->hasMany(Production::class, 'order_qty_id')->where('production_type', 'swing_in')->where('history_type', 'Running')->latest();
    }

    public function swingInHistory()
    {
        return $this->hasMany(Production::class, 'order_qty_id')->where('production_type', 'swing_in')->whereIn('history_type', ['Edit','Delete'])->latest();
    }

    public function swingOut()
    {
        return $this->hasMany(Production::class, 'order_qty_id')->where('production_type', 'swing_out')->where('history_type', 'Running')->latest();
    }

    public function swingOutHistory()
    {
        return $this->hasMany(Production::class, 'order_qty_id')->where('production_type', 'swing_out')->whereIn('history_type', ['Edit','Delete'])->latest();
    }

    public function iron()
    {
        return $this->hasMany(Production::class, 'order_qty_id')->where('production_type', 'iron')->where('history_type', 'Running')->latest();
    }

    public function ironHistory()
    {
        return $this->hasMany(Production::class, 'order_qty_id')->where('production_type', 'iron')->whereIn('history_type', ['Edit','Delete'])->latest();
    }

    public function packing()
    {
        return $this->hasMany(Production::class, 'order_qty_id')->where('production_type', 'packing')->where('history_type', 'Running')->latest();
    }

    public function packingHistory()
    {
        return $this->hasMany(Production::class, 'order_qty_id')->where('production_type', 'packing')->whereIn('history_type', ['Edit','Delete'])->latest();
    }
    /*End Production*/

}
