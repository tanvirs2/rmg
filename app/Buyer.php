<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use URL;

class Buyer extends Model
{
    public function GetProfilePicture()
    {
        if ($this->image != '') {
            return URL::to('profile-pic/buyer/'.$this->image);
        } else {
            return URL::to('profile-pic/man.jpg');
        }
    }

    public function agent()
    {
        return $this->belongsTo(Agent::class);
    }

    public function order()
    {
        return $this->hasMany(Order::class);
    }
}
