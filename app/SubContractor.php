<?php

namespace App;

use Illuminate\Database\Eloquent\Model, URL;

class SubContractor extends Model
{
    public function GetProfilePicture()
    {
        if ($this->image != '') {
            return URL::to('profile-pic/sub-contractor/'.$this->image);
        } else {
            return URL::to('profile-pic/man.jpg');
        }
    }
}
