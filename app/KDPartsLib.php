<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KDPartsLib extends Model
{
    public function kdProgram()
    {
        return $this->hasMany(KDProgram::class, 'kd_parts_id');
    }
}
