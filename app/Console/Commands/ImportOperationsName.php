<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Operation;

class ImportOperationsName extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'system:import_operations_name';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import Operation data in this system as default';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Create a file in system That presents the App install Time and Date.
     *
     * @return void
     */
    public function appInstallDate()
    {
        //
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $operations = [
            'order create',
            'order edit',
            'order delete',
            'order view',
            'manage user all',
            'manage user own department only',
        ];

        $query = Operation::query();

        if ($query->first()) {
            //$this->appInstallDate();
            $query->truncate();
        }

        $bar = $this->output->createProgressBar(count($operations));

        foreach ($operations as $operation) {
            $obj = new Operation;
            $obj->name = $operation;
            $obj->save();

            $bar->advance();
            $this->info("\t".$operation);
        }
        //$bar->finish();
        $this->info("\nOperations data import successfully !");
    }
}
