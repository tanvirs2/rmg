<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Department, App\Role;

class DummyData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'system:import_dummy_data';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import Dummy data in this system as default';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $departments = [
            [
                'name' => 'merchandising',
                'short_name' => 'merchan',
                'code' => 100
            ],
            [
                'name' => 'production',
                'short_name' => 'prod',
                'code' => 101
            ],
            [
                'name' => 'commercial',
                'short_name' => 'cmrl',
                'code' => 102
            ],
            [
                'name' => 'accounts',
                'short_name' => 'accnt',
                'code' => 103
            ],
            [
                'name' => 'store',
                'short_name' => 'str',
                'code' => 104
            ],
        ];

        $query = Department::query();

        if ($query->first()) {
            //$this->appInstallDate();
            $query->truncate();
        }

        $bar = $this->output->createProgressBar(count($departments));

        foreach ($departments as $department) {
            $obj = new Department();
            $obj->name = $department['name'];
            $obj->short_name = $department['short_name'];
            $obj->code = $department['code'];
            $obj->save();

            $bar->advance();
            $this->info("\t".$department['name']);
        }


        $roles = [
            [
                'name' => 'super admin',
                'code' => 100,
                'dashboard' => 'super admin'
            ],
            [
                'name' => 'admin',
                'code' => 101,
                'dashboard' => 'admin'
            ],
            [
                'name' => 'merchandiser',
                'code' => 102,
                'dashboard' => 'admin'
            ],
            [
                'name' => 'accountant',
                'code' => 103,
                'dashboard' => 'admin'
            ],
            [
                'name' => 'store admin',
                'code' => 104,
                'dashboard' => 'admin'
            ],
        ];

        $query = Role::query();

        if ($query->first()) {
            //$this->appInstallDate();
            $query->truncate();
        }

        $bar = $this->output->createProgressBar(count($roles));

        foreach ($roles as $role) {
            $obj = new Role();
            $obj->name = $role['name'];
            $obj->code = $role['code'];
            $obj->dashboard = $role['dashboard'];
            $obj->save();

            $bar->advance();
            $this->info("\t".$role['name']);
        }

        //$bar->finish();
        $this->info("\nDepartment data import successfully !");
        $this->info("\nRole data import successfully !");
    }
}
