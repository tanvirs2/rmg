<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Production extends Model
{
    function order()
    {
        return $this->belongsTo(Order::class);
    }

    public static function runningProductions()
    {
        return self::where('history_type', 'Running');
    }

    /*size and color wise qty*/
    public function clrNSzWiseQtyCutting()
    {
        return $this->hasMany(Production::class)->where('production_type', 'cutting')->where('history_type', 'Running')->latest();
    }

    public function clrNSzWiseQtyCuttingHistory()
    {
        return $this->hasMany(Production::class)->where('production_type', 'cutting')->whereIn('history_type', ['Edit','Delete'])->latest();
    }

    public function clrNSzWiseQtySwingIn()
    {
        return $this->hasMany(Production::class)->where('production_type', 'swing_in')->where('history_type', 'Running')->latest();
    }

    public function clrNSzWiseQtySwingInHistory()
    {
        return $this->hasMany(Production::class)->where('production_type', 'swing_in')->whereIn('history_type', ['Edit','Delete'])->latest();
    }

    public function clrNSzWiseQtySwingOut()
    {
        return $this->hasMany(Production::class)->where('production_type', 'swing_out')->where('history_type', 'Running')->latest();
    }

    public function clrNSzWiseQtySwingOutHistory()
    {
        return $this->hasMany(Production::class)->where('production_type', 'swing_out')->whereIn('history_type', ['Edit','Delete'])->latest();
    }

    public function clrNSzWiseQtyIron()
    {
        return $this->hasMany(Production::class)->where('production_type', 'iron')->where('history_type', 'Running')->latest();
    }

    public function clrNSzWiseQtyIronHistory()
    {
        return $this->hasMany(Production::class)->where('production_type', 'iron')->whereIn('history_type', ['Edit','Delete'])->latest();
    }

    public function clrNSzWiseQtyPacking()
    {
        return $this->hasMany(Production::class)->where('production_type', 'packing')->where('history_type', 'Running')->latest();
    }

    public function clrNSzWiseQtyPackingHistory()
    {
        return $this->hasMany(Production::class)->where('production_type', 'packing')->whereIn('history_type', ['Edit','Delete'])->latest();
    }
    /*End and color wise qty*/
}
