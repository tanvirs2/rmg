<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use URL;

class User extends Authenticatable
{
    use Notifiable;
    use \Znck\Eloquent\Traits\BelongsToThrough;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function GetProfilePicture()
    {
        if ($this->image != '') {
            return URL::to('profile-pic/'.$this->image);
        }

        if ($this->gender == 1) {
            return URL::to('profile-pic/man.jpg');
        } elseif($this->gender == 0) {
            return URL::to('profile-pic/woman.jpg');
        } else {
            return '';
        }
    }

    public function role()
    {
        return $this->belongsTo(Role::class);
    }

    public function can_manage_user()
    {
        if($this->role->name == 'super admin')
        {
            return true;
        }
        foreach($this->role->operations as $oP)
        {
            if($oP->name=='manage user all' || $oP->name=='manage user own department only')
            {
                return true;
            }
        }
        return false;
    }
    public function team()
    {
        return $this->belongsTo(Team::class);
    }

    public function department()
    {
        return $this->belongsToThrough(Department::class, Team::class);
    }

    public function order()
    {
        return $this->hasMany(Order::class);
    }

}
