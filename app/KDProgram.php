<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KDProgram extends Model
{
    public function kdParts()
    {
        return $this->belongsTo(KDPartsLib::class);
    }

    public function quantity()
    {
        return $this->belongsTo(OrderSizeNColor::class, 'order_qty_id');
    }

    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    public function color()
    {
        return $this->belongsTo(ColorLib::class);
    }

    public function kdIssues()
    {
        return $this->hasMany(KDIssue::class, 'kd_program_id');
    }
}
