<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    public function department()
    {
        return $this->belongsTo(Department::class);
    }

    public function users()
    {
        return $this->hasMany(User::class);
    }

    public function orderStatuses()
    {
        return $this->belongsToMany(OrderStatus::class, 'teams_order_statuses', 'team_id', 'order_status_id');
    }

    public function subordinateTeams()
    {
        return $this->belongsToMany(Team::class, 'subordinate_teams', 'parent_team_id', 'team_id');
    }
}
