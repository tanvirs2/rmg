<?php

namespace App;

use Illuminate\Database\Eloquent\Model, URL;

class Supplier extends Model
{
    public function GetProfilePicture()
    {
        if ($this->image != '') {
            return URL::to('profile-pic/supplier/'.$this->image);
        } else {
            return URL::to('profile-pic/man.jpg');
        }
    }
}
