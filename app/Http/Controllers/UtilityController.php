<?php

namespace App\Http\Controllers;

use App\Order;
use Illuminate\Http\Request;

class UtilityController extends Controller
{
    public function searchData($model, $name, $data)
    {
        $model = 'App\\'.$model;
        return $model::where($name, 'like', $data.'%')->distinct()->pluck($name);
    }
    public function selectDropdownData($model, $belongs=false)
    {
        $model = 'App\\'.$model;
        return $belongs? $model::with($belongs)->get() : $model::get();
    }

    public function filterOrder($request)
    {
        $query = Order::query();

        $orders = [];

        $query = $query->with(['budget:order_id,cm', 'buyer', 'quantity', 'status', 'shipment']);
        $id = $request->get('id') ?? null;
        $buyer_id = $request->get('buyer_id') ?? null;
        $name = $request->get('name') ?? null;
        $style = $request->get('style') ?? null;
        $style_desc = $request->get('style_desc') ?? null;
        $statuses = $request->get('statuses') ?? null;
        $category_id = $request->get('category_id') ?? null;
        $sub_category_id = $request->get('sub_category_id') ?? null;
        $from = $request->get('from') ?? null;
        $to = $request->get('to') ?? null;

        $per_page=(int)$request->input('per_page');

        if($per_page<=0)
        {
            $per_page = 200;
        }

        if($request->filled('id')) {
            $query = $query->where('id', $request->get('id') );
            //$phone = $request->get('phone');
        }
        if($request->filled('buyer_id')) {
            $query = $query->where('buyer_id', $request->get('buyer_id') );
            //$phone = $request->get('phone');
        }
        if($request->filled('name')) {
            $query = $query->where('name', $request->get('name') );
            //$phone = $request->get('phone');
        }
        if($request->filled('style')) {
            $query = $query->where('style', $request->get('style') );
            //$phone = $request->get('phone');
        }
        if($request->filled('style_desc')) {
            $query = $query->where('style_desc', $request->get('style_desc') );
            //$phone = $request->get('phone');
        }
        if($request->filled('statuses')) {
            $query = $query->where('statuses', $request->get('statuses') );
            //$phone = $request->get('phone');
        }
        if($request->filled('sub_category_id')) {
            $query = $query->where('sub_category_id', $request->get('sub_category_id') );
            //$phone = $request->get('phone');
        }
        if($request->filled('from') && $request->filled('to')) {
            $query = $query->whereBetween('date_of_ship', [$request->get('from'), $request->get('to')] );
            //$phone = $request->get('phone');
        }

        if($request->filled('order_filter_form') || $request->filled('id')) {
            $orders = $query->paginate($per_page);
            $orders->setpath($request->fullUrl()."&per_page=$per_page");
        }



        $compact = compact(
            'id',
            'buyer_id',
            'name',
            'style',
            'style_desc',
            'statuses',
            'category_id',
            'sub_category_id',
            'from',
            'to',
            'per_page',
            'orders'
        );
        //dd($orders);

        return $compact;
    }
}
