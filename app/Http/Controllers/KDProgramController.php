<?php

namespace App\Http\Controllers;

use App\KDExtra;
use App\KDFabrics, Auth;
use App\KDProgram;
use App\KDRegister;
use App\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class KDProgramController extends Controller
{
    public $pageData = [];
    public $pageName = 'kd-program';
    public $viewFolder = 'kdProgram';

    public function __construct()
    {
        $this->pageData = [
            'no' => 0,
            'pageName' => Str::studly($this->pageName),
            'routeFirstName' => $this->pageName,
            'ignoreColsInExport' => '[0]',
        ];

        view()->share('pageData', $this->pageData);

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = Order::query();
        $query = $query->with(['budget:order_id,cm', 'buyer', 'status', 'shipment']);
        $id = $request->get('id') ?? null;
        $buyer_id = $request->get('buyer_id') ?? null;
        $name = $request->get('name') ?? null;
        $style = $request->get('style') ?? null;
        $style_desc = $request->get('style_desc') ?? null;
        $statuses = $request->get('statuses') ?? null;
        $category_id = $request->get('category_id') ?? null;
        $sub_category_id = $request->get('sub_category_id') ?? null;
        $from = $request->get('from') ?? null;
        $to = $request->get('to') ?? null;

        $per_page=(int)$request->input('per_page');

        if($per_page<=0)
        {
            $per_page = 20;
        }

        if($request->filled('id')) {
            $query = $query->where('id', $request->get('id') );
            //$phone = $request->get('phone');
        }
        if($request->filled('buyer_id')) {
            $query = $query->where('buyer_id', $request->get('buyer_id') );
            //$phone = $request->get('phone');
        }
        if($request->filled('name')) {
            $query = $query->where('name', $request->get('name') );
            //$phone = $request->get('phone');
        }
        if($request->filled('style')) {
            $query = $query->where('style', $request->get('style') );
            //$phone = $request->get('phone');
        }
        if($request->filled('style_desc')) {
            $query = $query->where('style_desc', $request->get('style_desc') );
            //$phone = $request->get('phone');
        }
        if($request->filled('statuses')) {
            $query = $query->where('statuses', $request->get('statuses') );
            //$phone = $request->get('phone');
        }
        if($request->filled('sub_category_id')) {
            $query = $query->where('sub_category_id', $request->get('sub_category_id') );
            //$phone = $request->get('phone');
        }
        if($request->filled('from') && $request->filled('to')) {
            $query = $query->whereBetween('date_of_ship', [$request->get('from'), $request->get('to')] );
            //$phone = $request->get('phone');
        }

        $orders = $query->paginate($per_page);
        $orders->setpath($request->fullUrl()."&per_page=$per_page");

        $compact = compact(
            'id',
            'buyer_id',
            'name',
            'style',
            'style_desc',
            'statuses',
            'category_id',
            'sub_category_id',
            'from',
            'to',
            'per_page',
            'orders'
        );

        return view($this->viewFolder.'.index', $compact)
            ->withCount($query->get()->count())
            ;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $filterOrder = (new UtilityController)->filterOrder($request);

        $id = $request->get('id') ?? null;

        $compact = compact(
            'id'
        );

        $compact += $filterOrder;

        $order = Order::find($id);


        /*$orderSizeAndColorArr = [];
        if ($order) {
            foreach($order->quantity as $sNcWiseQty){
                $orderSizeAndColorArr[$sNcWiseQty->color->name]['color_id'] = $sNcWiseQty->color_id;
                $orderSizeAndColorArr[$sNcWiseQty->color->name]['sz'][] = $sNcWiseQty->size->name;
                $orderSizeAndColorArr[$sNcWiseQty->color->name]['qty'][] = $sNcWiseQty->quantity;
            }
        }*/


        return view($this->viewFolder.'.create', $compact)
            ->withOrder($order)
            ;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $loggedUser = Auth::user();
        //dd($request['kdExtra']);
        //return '<h3>Processing...</h3>';

        /*if ($request->filled('knit_cuff')) {
            dd('knit_cuff');
        }
        if ($request->filled('knit_collar')) {
            dd('knit_collar');
        }*/

        //dd($request->all());

        if ($request->filled('kdExtra')) {
            foreach ($request['kdExtra'] as $type => $kdExtras) {

                foreach ($kdExtras as $color_id => $kdExtra) {
                    $kdEx = new KDExtra();
                    $kdEx->order_id = $request['order_id'];
                    $kdEx->color_id = $color_id;
                    $kdEx->req = $kdExtra['req'] ?? null;
                    $kdEx->type = $type;
                    $kdEx->finish_fab = $kdExtra['finish_fab'];
                    $kdEx->gray_fab = $kdExtra['gray_fab'];
                    $kdEx->save();
                }

            }
        }

        //dd($request->all());

        $kdRegis = new KDRegister();
        $kdRegis->order_id =             $request['order_id'];
        $kdRegis->user_id = $loggedUser->id;
        $kdRegis->knit_collar = $request['knit_collar'];
        $kdRegis->knit_cuff = $request['knit_cuff'];
        $kdRegis->save();

        //dd($request->all());

        $request->validate([
            //'fabDesc.*.*' => 'required',
            //'kdPart.*.*.*' => 'required|numeric',
        ]);

        $loggedUser = Auth::user();

        /*foreach ($request['fabDesc'] as $fabDesc) {
            $model = new KDFabrics();
            $model->user_id = $loggedUser->id;
            $model->order_id =             $request['order_id'];
            $model->fabric =             $fabDesc['name'];
            $model->fabric_desc =             $fabDesc['value'];
            $model->save();
        }*/

        foreach ($request['kdPart'] as $kdPartId => $kdPart) {

            if ($kdPartId != 'null') {
                $model = new KDFabrics();
                $model->user_id = $loggedUser->id;
                $model->order_id =             $request['order_id'];
                $model->fabric =             $kdPart['fab_name'];
                $model->fabric_desc =             $kdPart['fab_value'];
                $model->kd_parts_id =             $kdPartId;
                $model->save();

                foreach ($kdPart as $color_id => $kdProgram) {
                    if (is_array($kdProgram)) {
                        $model2 = new KDProgram();
                        $model2->user_id = $loggedUser->id;
                        $model2->order_id =             $request['order_id'];
                        $model2->kd_parts_id =             $kdPartId;
                        $model2->color_id =             $color_id;
                        $model2->finish_fab =             $kdProgram['finish_fab'];
                        $model2->gray_fab =             $kdProgram['gray_fab'];
                        $model2->dia =             $kdProgram['dia'];
                        $model2->remarks =             $kdProgram['remarks'];
                        $model2->lab =             $kdProgram['lab'];
                        $model2->save();
                    }
                }
            }

        }
        return redirect()->back()->with(['success'=>$this->pageData['pageName'].' Save Successfully.']);

        //dd($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\KDProgram  $kDProgram
     * @return \Illuminate\Http\Response
     */
    public function show(Order $order)
    {
        return view($this->viewFolder.'.print.index')
            ->withOrder($order)
            ;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\KDProgram  $kDProgram
     * @return \Illuminate\Http\Response
     */
    public function edit(KDProgram $kDProgram)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\KDProgram  $kDProgram
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, KDProgram $kDProgram)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\KDProgram  $kDProgram
     * @return \Illuminate\Http\Response
     */
    public function destroy(KDProgram $kDProgram)
    {
        //
    }
}
