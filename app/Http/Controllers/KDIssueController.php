<?php

namespace App\Http\Controllers;

use App\KDIssue;
use App\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class KDIssueController extends Controller
{
    public $pageData = [];
    public $pageName = 'kd-issue';
    public $viewFolder = 'kdIssue';

    public function __construct()
    {
        $this->pageData = [
            'no' => 0,
            'pageName' => Str::studly($this->pageName),
            'routeFirstName' => $this->pageName,
            'ignoreColsInExport' => '[0]',
        ];

        view()->share('pageData', $this->pageData);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $filterOrder = (new UtilityController)->filterOrder($request);

        $id = $request->get('id') ?? null;

        $compact = compact(
            'id'
        );

        $compact += $filterOrder;

        $order = Order::find($id);

        return view($this->viewFolder.'.create', $compact)
            ->withOrder($order)
            ;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\KDIssue  $kDIssue
     * @return \Illuminate\Http\Response
     */
    public function show(KDIssue $kDIssue)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\KDIssue  $kDIssue
     * @return \Illuminate\Http\Response
     */
    public function edit(KDIssue $kDIssue)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\KDIssue  $kDIssue
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, KDIssue $kDIssue)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\KDIssue  $kDIssue
     * @return \Illuminate\Http\Response
     */
    public function destroy(KDIssue $kDIssue)
    {
        //
    }
}
