<?php

namespace App\Http\Controllers;

use App\Buyer;
use App\Order;
use App\Production;
use App\Shipment;
use App\User;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard.index')
            ->withOrdersCount(Order::count())
            ->withUsersCount(User::count())
            ->withUsers(User::all())
            ->withBuyersCount(Buyer::count())
            ->withBuyers(Buyer::all())
            ->withProductionCount(Production::where([['history_type', 'Running'],['production_type', 'packing']])->sum('quantity'))
            ->withShipmentCount(Shipment::where('history_type', 'Running')->sum('quantity'))
            ;
    }
}
