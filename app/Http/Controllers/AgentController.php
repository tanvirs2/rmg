<?php

namespace App\Http\Controllers;

use App\Agent, App\User, App\Buyer, Image, Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;

class AgentController extends Controller
{
    public $_profile_pic_folder = 'profile-pic/agent';
    public $pageData = [];

    public function __construct()
    {
        $pageName = 'agent';
        $this->pageData = [
            'no' => 0,
            'pageName' => Str::studly($pageName),
            'routeFirstName' => $pageName,
            'ignoreColsInExport' => '[1, 7]',
        ];

        view()->share('pageData', $this->pageData);

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = Agent::query();
        $name = null;
        $phone = null;
        $email = null;

        $per_page=(int)$request->input('per_page');

        if($per_page<=0)
        {
            $per_page = 20;
        }

        if($request->filled('name')) {
            $query = $query->where('name', 'like', '%'.$request->get('name').'%');
            $name = $request->get('name');
        }
        if($request->filled('phone')) {
            $query = $query->where('phone', 'like', '%' . $request->get('phone') . '%');
            $phone = $request->get('phone');
        }
        if($request->filled('email')) {
            $query = $query->where('email', $request->get('email') );
            $email = $request->get('email');
        }

        $mainDatas = $query->paginate($per_page);
        $mainDatas->setpath($request->fullUrl()."&per_page=$per_page");

        $compact = compact(
            'name',
            'phone',
            'email',
            'per_page',
            'mainDatas'
        );

        return view('agents.index', $compact)
            ->withCount($query->get()->count())
            ;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('agents.create')
            ->withUsers(User::all())
            ;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->all());
        $loggedUseruser = Auth::user();
        if(!$loggedUseruser->can_manage_user())
        {
            return redirect()->back()->with(['fail'=>'Unauthorized Access.']);
        }
        $team_ids = $loggedUseruser->role->managed_user_team_ids();
        //dd($team_ids);
        $request->validate([
            'name' => 'required|max:255|unique:agents',
            'reporting_to' => 'integer',
            'image' => 'mimes:jpeg,jpg,png,JPEG,JPG,PNG | max:1000'
        ]);

        $mainData = new Agent();
        $mainData->name = $request['name'];
        $mainData->email = $request['email'];
        $mainData->phone = $request['phone'];

        if ($request->has('reporting_to')) {
            $mainData->reporting_to = $request['reporting_to'];
        }
        if ($request->has('status')) {
            $mainData->status = $request['status'];
        }

        $mainData->description = $request['description'];
        $mainData->address = $request['address'];

        if($request->hasFile('profile_img'))
        {
            $image = $request->file('profile_img');
            $mainData->image = $filename = time() . '.' . $image->getClientOriginalExtension();
            //$image->move($this->_profile_pic_folder, $filename);
            $image_resize = Image::make($image->getRealPath());
            $image_resize->widen(250);
            $image_resize->save(public_path($this->_profile_pic_folder.'/' .$filename));
        }
        $mainData->save();
        return redirect()->route($this->pageData['routeFirstName'].'-list')->with(['success'=>$this->pageData['pageName'].' Created Successfully.']);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Agent  $agent
     * @return \Illuminate\Http\Response
     */
    public function show(Agent $agent)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Agent  $agent
     * @return \Illuminate\Http\Response
     */
    public function edit(Agent $agent)
    {
        return $this->create()
            ->withIsSetAgent($agent)
            ;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Agent  $agent
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Agent $agent)
    {
        //dd($request->all());
        $loggedUseruser = Auth::user();
        if(!$loggedUseruser->can_manage_user())
        {
            return redirect()->back()->with(['fail'=>'Unauthorized Access.']);
        }
        $team_ids = $loggedUseruser->role->managed_user_team_ids();
        //dd($team_ids);
        $request->validate([
            'name' => [
                'required', 'max:255',
                Rule::unique('agents')->ignore($agent->id)
            ],
            'email' => [
                'required', 'email',
                Rule::unique('agents')->ignore($agent->id)
            ],
            'agent_id' => 'integer',
            'reporting_to' => 'integer',
            'image' => 'mimes:jpeg,jpg,png,JPEG,JPG,PNG | max:1000'
        ]);

        $mainData = $agent;
        $mainData->name = $request['name'];
        $mainData->email = $request['email'];
        $mainData->phone = $request['phone'];

        if ($request->has('agent_id')) {
            $mainData->agent_id = $request['agent_id'];
        }
        if ($request->has('reporting_to')) {
            $mainData->reporting_to = $request['reporting_to'];
        }
        if ($request->has('status')) {
            $mainData->status = $request['status'];
        }
        $mainData->description = $request['description'];
        $mainData->address = $request['address'];

        if($request->hasFile('profile_img'))
        {
            $image = $request->file('profile_img');
            $mainData->image = $filename = time() . '.' . $image->getClientOriginalExtension();
            //$image->move($this->_profile_pic_folder, $filename);
            $image_resize = Image::make($image->getRealPath());
            $image_resize->widen(250);
            $image_resize->save(public_path($this->_profile_pic_folder.'/' .$filename));
        }
        $mainData->save();
        return redirect()->route($this->pageData['routeFirstName'].'-list')->with(['success'=>$this->pageData['pageName'].' Updated Successfully.']);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Agent  $agent
     * @return \Illuminate\Http\Response
     */
    public function destroy(Agent $agent)
    {
        //
    }
}
