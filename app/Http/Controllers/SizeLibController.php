<?php

namespace App\Http\Controllers;

use App\SizeLib;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class SizeLibController extends Controller
{
    public $pageData = [];

    public function __construct()
    {
        $pageName = 'size-lib';
        $this->pageData = [
            'no' => 1,
            'viewFolder' => 'sizeLib.',
            'pageName' => Str::studly($pageName),
            'routeFirstName' => $pageName,
            'ignoreColsInExport' => '[1, 7]',
        ];

        view()->share('pageData', $this->pageData);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = SizeLib::query();
        $name = null;
        $status = null;

        $per_page=(int)$request->input('per_page');

        if($per_page<=0)
        {
            $per_page = 20;
        }

        if($request->filled('name')) {
            $query = $query->where('name', 'like', '%'.$request->get('name').'%');
            $name = $request->get('name');
        }
        if($request->filled('status')) {
            $query = $query->where('status', 'like', '%' . $request->get('status') . '%');
            $status = $request->get('status');
        }

        $mainDatas = $query->paginate($per_page);
        $mainDatas->setpath($request->fullUrl()."&per_page=$per_page");

        $compact = compact(
            'name',
            'status',
            'per_page',
            'mainDatas'
        );

        return view($this->pageData['viewFolder'].'index', $compact)
            ->withCount($query->get()->count())
            ;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view($this->pageData['viewFolder'].'create')
            ;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $loggedUseruser = Auth::user();
        if(!$loggedUseruser->can_manage_user())
        {
            return redirect()->back()->with(['fail'=>'Unauthorized Access.']);
        }
        $team_ids = $loggedUseruser->role->managed_user_team_ids();
        //dd($team_ids);
        $request->validate([
            'name' => 'required|max:255',
        ]);

        $mainData = new SizeLib();
        $mainData->name = $request['name'];

        if ($request->has('status')) {
            $mainData->status = $request['status'];
        }

        $mainData->save();
        return redirect()->route($this->pageData['routeFirstName'].'-list')->with(['success'=>$this->pageData['pageName'].' Created Successfully.']);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SizeLib  $sizeLib
     * @return \Illuminate\Http\Response
     */
    public function show(SizeLib $sizeLib)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SizeLib  $sizeLib
     * @return \Illuminate\Http\Response
     */
    public function edit(SizeLib $sizeLib)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SizeLib  $sizeLib
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SizeLib $sizeLib)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SizeLib  $sizeLib
     * @return \Illuminate\Http\Response
     */
    public function destroy(SizeLib $sizeLib)
    {
        //
    }
}
