<?php

namespace App\Http\Controllers;

use App\OrderSizeNColor;
use Illuminate\Http\Request;

class OrderSizeNColorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\OrderSizeNColor  $orderSizeNColor
     * @return \Illuminate\Http\Response
     */
    public function show(OrderSizeNColor $orderSizeNColor)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\OrderSizeNColor  $orderSizeNColor
     * @return \Illuminate\Http\Response
     */
    public function edit(OrderSizeNColor $orderSizeNColor)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\OrderSizeNColor  $orderSizeNColor
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, OrderSizeNColor $orderSizeNColor)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\OrderSizeNColor  $orderSizeNColor
     * @return \Illuminate\Http\Response
     */
    public function destroy(OrderSizeNColor $orderSizeNColor)
    {
        //
    }
}
