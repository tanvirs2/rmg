<?php

namespace App\Http\Controllers;

use App\Department;
use App\OrderStatus;
use App\Team;
use Illuminate\Http\Request;

class TeamController extends Controller
{
    public function getTeamsByDepartmentId($department_id)
    {
        return $department_id;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('team.index')
            ->withTeams(Team::all())
            ;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('team.create')
            ->withDepartments(Department::all())
            ->withTeams(Team::all())
            ->withOrderStatuses(OrderStatus::all())
            ;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|unique:teams',
            'department_id' => 'required',
            'subordinate_team_ids' => 'required',
            'order_status_ids' => 'required',
        ]);

        $team = new Team();
        $team->name = $request->name;
        $team->department_id = $request->department_id;
        $team->save();

        $team->orderStatuses()->sync($request->order_status_ids);
        $team->subordinateTeams()->sync($request->subordinate_team_ids);

        return redirect()->route('team-list')->with(['success'=>'Team Created Successfully.']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Team $team)
    {
        return $this->create()
            ->withSelectedTeam($team)
        ;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Department $department)
    {
        $request->validate([
            'name' => [
                'required',
                Rule::unique('departments')->ignore($department->id),
            ],
            'short_name' => [
                'required',
                Rule::unique('departments')->ignore($department->id),
            ],
            'code' => [
                'required',
                Rule::unique('departments')->ignore($department->id),
            ],
        ]);


        $department->name = $request->name;
        $department->short_name = $request->short_name;
        $department->code = $request->code;
        $department->save();

        return redirect()->route('department-list')->with(['success'=>'Department updated Successfully.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
