<?php

namespace App\Http\Controllers;

use App\OrderOperation;
use Illuminate\Http\Request;

class OrderOperationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\OrderOperation  $orderOperation
     * @return \Illuminate\Http\Response
     */
    public function show(OrderOperation $orderOperation)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\OrderOperation  $orderOperation
     * @return \Illuminate\Http\Response
     */
    public function edit(OrderOperation $orderOperation)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\OrderOperation  $orderOperation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, OrderOperation $orderOperation)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\OrderOperation  $orderOperation
     * @return \Illuminate\Http\Response
     */
    public function destroy(OrderOperation $orderOperation)
    {
        //
    }
}
