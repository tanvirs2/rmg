<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Department;
use Illuminate\Validation\Rule;

class DepartmentController extends Controller
{
    /*
     * Start API Route
     * */
    public function departmentList()
    {
        return Department::all();
    }
    public function getSingleDepartment($department_id)
    {
        return Department::find($department_id);
    }

    public function getTeamsFromDepartment($department_id)
    {
        return Department::find($department_id)->teams;
    }
    /*
     * End API Route
     * */


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('department.index')
            ->withDepartments(Department::all())
            ;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('department.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|unique:departments',
            'short_name' => 'required|unique:departments',
            'code' => 'required|unique:departments',
        ]);

        $department = new Department();

        $department->name = $request->name;
        $department->short_name = $request->short_name;
        $department->code = $request->code;
        $department->save();

        return redirect()->route('department-list')->with(['success'=>'Department Created Successfully.']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Department $department)
    {
        return view('department.create')
            ->withDepartment($department)
            ;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Department $department)
    {
        $request->validate([
            'name' => [
                'required',
                Rule::unique('departments')->ignore($department->id),
            ],
            'short_name' => [
                'required',
                Rule::unique('departments')->ignore($department->id),
            ],
            'code' => [
                'required',
                Rule::unique('departments')->ignore($department->id),
            ],
        ]);


        $department->name = $request->name;
        $department->short_name = $request->short_name;
        $department->code = $request->code;
        $department->save();

        return redirect()->route('department-list')->with(['success'=>'Department updated Successfully.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
