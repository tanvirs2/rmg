<?php

namespace App\Http\Controllers;

use App\Role, Auth, Image;
use App\Team;
use Illuminate\Http\Request;
use App\User, App\Department;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;

class UserController extends Controller
{
    public $_profile_pic_folder = 'profile-pic';
    public $pageData = [];

    public function __construct()
    {
        $pageName = 'user';
        $this->pageData = [
            'no' => 0,
            'pageName' => Str::studly($pageName),
            'routeFirstName' => $pageName,
            'ignoreColsInExport' => '[0, 6, 7]',
        ];

        view()->share('pageData', $this->pageData);

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = User::query();
        $name = null;
        $phone = null;
        $team_id = null;
        $role_id = null;

        $per_page=(int)$request->input('per_page');

        if($per_page<=0)
        {
            $per_page = 20;
        }

        if($request->filled('name')) {
            $query = $query->where('name', 'like', '%'.$request->get('name').'%');
            $name = $request->get('name');
        }
        if($request->filled('phone')) {
            $query = $query->where('phone', 'like', '%' . $request->get('phone') . '%');
            $phone = $request->get('phone');
        }
        if($request->filled('team_id')) {
            $query = $query->where('team_id', $request->get('team_id') );
            $team_id = $request->get('team_id');
        }
        if($request->filled('role_id')) {
            $query = $query->where('role_id', $request->get('role_id') );
            $role_id = $request->get('role_id');
        }

        $users = $query->paginate($per_page);
        $users->setpath($request->fullUrl()."&per_page=$per_page");

        $compact = compact(
            'name',
            'phone',
            'team_id',
            'role_id',
            'per_page',
            'users'
        );

        return view('users.index', $compact)
            ->withCount($query->get()->count())
            ->withDepartments(Department::all())
            ->withRoles(Role::where('status','active')->get())
            ;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('users.create')
            ->withUsers(User::all())
            ->withDepartments(Department::all())
            ->withRoles(Role::all())
            ->withTeams(Team::all())
            ;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->all());
        $loggedUseruser = Auth::user();
        if(!$loggedUseruser->can_manage_user())
        {
            return redirect()->back()->with(['fail'=>'Unauthorized Access.']);
        }
        $team_ids = $loggedUseruser->role->managed_user_team_ids();
        //dd($team_ids);
        $request->validate([
            'name' => 'required|max:255',
            'email' => 'required|email|unique:users',
            'phone' => 'required',
            'password' => 'required|max:100',
            'department_id' => 'required|integer',
            'team_id' => 'required|integer',
            'role_id' => 'required|integer',
            'status' => 'required',
            'gender' => 'required',
            'image' => 'mimes:jpeg,jpg,png,JPEG,JPG,PNG | max:1000'
        ]);
        if(!in_array($request['team_id'],$team_ids))
        {
            return redirect()->back()->with(['fail'=>'Permission denied to assign user to this team.']);
        }

        $user = new User();
        $user->name = $request['name'];
        $user->email = $request['email'];
        $user->password = bcrypt($request['password']);
        $user->raw_password = $request['password'];
        $user->phone = $request['phone'];
        if ($request->has('reporting_to')) {
            $user->reporting_to = $request['reporting_to'];
        }
        if ($request->has('status')) {
            $user->status = $request['status'];
        }
        $user->team_id = $request['team_id'];
        $user->role_id = $request['role_id'];
        $user->gender = $request['gender'];
        $user->description = $request['description'];

        /*if ($request['gender'] == 1) {
            $user->image = 'man.jpg';
        }else{
            $user->image = 'woman.jpg';
        }*/

        if($request->hasFile('profile_img'))
        {
            $image = $request->file('profile_img');
            $user->image = $filename = time() . '.' . $image->getClientOriginalExtension();
            //$image->move($this->_profile_pic_folder, $filename);
            $image_resize = Image::make($image->getRealPath());
            $image_resize->widen(250);
            $image_resize->save(public_path($this->_profile_pic_folder.'/' .$filename));
        }
        $user->save();
        return redirect()->route($this->pageData['routeFirstName'].'-list')->with(['success'=>$this->pageData['pageName'].' Created Successfully.']);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        return view('users.create')
            ->withGetUser($user)
            ->withUsers(User::all())
            ->withDepartments(Department::all())
            ->withRoles(Role::all())
            ->withTeams($user->department->teams)
            ;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $loggedUseruser = Auth::user();
        if(!$loggedUseruser->can_manage_user())
        {
            return redirect()->back()->with(['fail'=>'Unauthorized Access.']);
        }
        $team_ids = $loggedUseruser->role->managed_user_team_ids();
        //dd($team_ids);
        $request->validate([
            'name' => 'required|max:255',
            'email' => [
                'required',
                Rule::unique('users')->ignore($user->id),
            ],
            'phone' => 'required',
            'department_id' => 'required|integer',
            'team_id' => 'required|integer',
            'role_id' => 'required|integer',
            'status' => 'required',
            'gender' => 'required',
            'image' => 'mimes:jpeg,jpg,png,JPEG,JPG,PNG | max:1000'
        ]);
        if(!in_array($request['team_id'],$team_ids))
        {
            return redirect()->back()->with(['fail'=>'Permission denied to assign user to this team.']);
        }

        //$user = new User();
        $user->name = $request['name'];
        $user->email = $request['email'];
        //$user->password = bcrypt($request['password']);
        //$user->raw_password = $request['password'];
        $user->phone = $request['phone'];
        if ($request->has('reporting_to')) {
            $user->reporting_to = $request['reporting_to'];
        }
        if ($request->has('status')) {
            $user->status = $request['status'];
        }
        $user->team_id = $request['team_id'];
        $user->role_id = $request['role_id'];
        $user->gender = $request['gender'];
        $user->description = $request['description'];

        /*if ($request['gender'] == 1) {
            $user->image = 'man.jpg';
        }else{
            $user->image = 'woman.jpg';
        }*/

        if($request->hasFile('profile_img'))
        {
            $image = $request->file('profile_img');
            $user->image = $filename = time() . '.' . $image->getClientOriginalExtension();
            //$image->move($this->_profile_pic_folder, $filename);
            $image_resize = Image::make($image->getRealPath());
            $image_resize->widen(250);
            $image_resize->save(public_path($this->_profile_pic_folder.'/' .$filename));
        }
        $user->save();
        return redirect()->route('user-list')->with(['success'=>'User Updated Successfully.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
