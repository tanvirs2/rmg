<?php

namespace App\Http\Controllers;

use App\Supplier, Auth, Image;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;

class SupplierController extends Controller
{
    public $_profile_pic_folder = 'profile-pic/supplier';
    public $pageData = [];
    public $pageName = 'supplier';
    public $viewFolder = 'supplier';

    public function __construct()
    {
        $this->pageData = [
            'no' => 0,
            'pageName' => Str::studly($this->pageName),
            'routeFirstName' => $this->pageName,
            'ignoreColsInExport' => '[0]',
        ];

        view()->share('pageData', $this->pageData);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = Supplier::query();
        $name = null;
        $phone = null;
        $email = null;

        $per_page=(int)$request->input('per_page');

        if($per_page<=0)
        {
            $per_page = 20;
        }

        if($request->filled('name')) {
            $query = $query->where('name', 'like', '%'.$request->get('name').'%');
            $name = $request->get('name');
        }
        if($request->filled('phone')) {
            $query = $query->where('phone', 'like', '%' . $request->get('phone') . '%');
            $phone = $request->get('phone');
        }
        if($request->filled('email')) {
            $query = $query->where('email', $request->get('email') );
            $email = $request->get('email');
        }

        $mainDatas = $query->paginate($per_page);
        $mainDatas->setpath($request->fullUrl()."&per_page=$per_page");

        $compact = compact(
            'name',
            'phone',
            'email',
            'per_page',
            'mainDatas'
        );

        return view($this->viewFolder.'.index', $compact)
            ->withCount($query->get()->count())
            ;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view($this->viewFolder.'.create')
            ->withUsers(User::all())
            ;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->all());
        $loggedUseruser = Auth::user();
        if(!$loggedUseruser->can_manage_user())
        {
            return redirect()->back()->with(['fail'=>'Unauthorized Access.']);
        }
        $team_ids = $loggedUseruser->role->managed_user_team_ids();
        //dd($team_ids);
        $request->validate([
            'name' => 'required|max:255|unique:suppliers',
            'reporting_to' => 'integer',
            'image' => 'mimes:jpeg,jpg,png,JPEG,JPG,PNG | max:1000'
        ]);

        $mainData = new Supplier();
        $mainData->name = $request['name'];
        $mainData->email = $request['email'];
        $mainData->phone = $request['phone'];

        if ($request->has('reporting_to')) {
            $mainData->reporting_to = $request['reporting_to'];
        }
        if ($request->has('status')) {
            $mainData->status = $request['status'];
        }

        $mainData->description = $request['description'];
        $mainData->address = $request['address'];

        if($request->hasFile('profile_img'))
        {
            $image = $request->file('profile_img');
            $mainData->image = $filename = time() . '.' . $image->getClientOriginalExtension();
            //$image->move($this->_profile_pic_folder, $filename);
            $image_resize = Image::make($image->getRealPath());
            $image_resize->widen(250);
            $image_resize->save(public_path($this->_profile_pic_folder.'/' .$filename));
        }
        $mainData->save();
        return redirect()->route($this->pageData['routeFirstName'].'-list')->with(['success'=>$this->pageData['pageName'].' Created Successfully.']);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Supplier  $supplier
     * @return \Illuminate\Http\Response
     */
    public function show(Supplier $supplier)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Supplier  $supplier
     * @return \Illuminate\Http\Response
     */
    public function edit(Supplier $supplier)
    {
        return $this->create()
            ->withIsSetSupplier($supplier)
            ;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Supplier  $supplier
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Supplier $supplier)
    {
        //dd($request->all());
        $loggedUseruser = Auth::user();
        if(!$loggedUseruser->can_manage_user())
        {
            return redirect()->back()->with(['fail'=>'Unauthorized Access.']);
        }
        $team_ids = $loggedUseruser->role->managed_user_team_ids();
        //dd($team_ids);
        $request->validate([
            'name' => [
                'required', 'max:255',
                Rule::unique('suppliers')->ignore($supplier->id)
            ],
            'email' => 'email',
            'reporting_to' => 'integer',
            'image' => 'mimes:jpeg,jpg,png,JPEG,JPG,PNG | max:1000'
        ]);

        $mainData = $supplier;
        $mainData->name = $request['name'];
        $mainData->email = $request['email'];
        $mainData->phone = $request['phone'];

        if ($request->has('reporting_to')) {
            $mainData->reporting_to = $request['reporting_to'];
        }
        if ($request->has('status')) {
            $mainData->status = $request['status'];
        }
        $mainData->description = $request['description'];
        $mainData->address = $request['address'];

        if($request->hasFile('profile_img'))
        {
            $image = $request->file('profile_img');
            $mainData->image = $filename = time() . '.' . $image->getClientOriginalExtension();
            //$image->move($this->_profile_pic_folder, $filename);
            $image_resize = Image::make($image->getRealPath());
            $image_resize->widen(250);
            $image_resize->save(public_path($this->_profile_pic_folder.'/' .$filename));
        }
        $mainData->save();
        return redirect()->route($this->pageData['routeFirstName'].'-list')->with(['success'=>$this->pageData['pageName'].' Updated Successfully.']);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Supplier  $supplier
     * @return \Illuminate\Http\Response
     */
    public function destroy(Supplier $supplier)
    {
        //
    }
}
