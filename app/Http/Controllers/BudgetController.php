<?php

namespace App\Http\Controllers;

use App\Budget;
use App\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class BudgetController extends Controller
{
    public $pageData = [];
    public $pageName;

    public function __construct()
    {
        $this->pageName = 'budget';
        $this->pageData = [
            'no' => 0,
            'pageName' => Str::studly($this->pageName),
            'routeFirstName' => $this->pageName,
            'ignoreColsInExport' => '[0]',
        ];

        view()->share('pageData', $this->pageData);

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //dd($request->all());
        $query = Order::query();
        $query = $query->with(['budget', 'buyer', 'status', 'shipment']);
        $id = $request->get('id') ?? null;
        $buyer_id = $request->get('buyer_id') ?? null;
        $name = $request->get('name') ?? null;
        $style = $request->get('style') ?? null;
        $style_desc = $request->get('style_desc') ?? null;
        $statuses = $request->get('statuses') ?? null;
        $category_id = $request->get('category_id') ?? null;
        $sub_category_id = $request->get('sub_category_id') ?? null;
        $from = $request->get('from') ?? null;
        $to = $request->get('to') ?? null;

        $per_page=(int)$request->input('per_page');

        if($per_page<=0)
        {
            $per_page = 20;
        }

        if($request->filled('id')) {
            $query = $query->where('id', $request->get('id') );
            //$phone = $request->get('phone');
        }
        if($request->filled('buyer_id')) {
            $query = $query->where('buyer_id', $request->get('buyer_id') );
            //$phone = $request->get('phone');
        }
        if($request->filled('name')) {
            $query = $query->where('name', $request->get('name') );
            //$phone = $request->get('phone');
        }
        if($request->filled('style')) {
            $query = $query->where('style', $request->get('style') );
            //$phone = $request->get('phone');
        }
        if($request->filled('style_desc')) {
            $query = $query->where('style_desc', $request->get('style_desc') );
            //$phone = $request->get('phone');
        }
        if($request->filled('statuses')) {
            $query = $query->where('statuses', $request->get('statuses') );
            //$phone = $request->get('phone');
        }
        if($request->filled('sub_category_id')) {
            $query = $query->where('sub_category_id', $request->get('sub_category_id') );
            //$phone = $request->get('phone');
        }
        if($request->filled('from') && $request->filled('to')) {
            $query = $query->whereBetween('date_of_ship', [$request->get('from'), $request->get('to')] );
            //$phone = $request->get('phone');
        }

        $orders = $query->paginate($per_page);
        $orders->setpath($request->fullUrl()."&per_page=$per_page");

        $compact = compact(
            'id',
            'buyer_id',
            'name',
            'style',
            'style_desc',
            'statuses',
            'category_id',
            'sub_category_id',
            'from',
            'to',
            'per_page',
            'orders'
        );

        return view($this->pageName.'.index', $compact)
            ->withCount($query->get()->count())
            ;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Budget  $budget
     * @return \Illuminate\Http\Response
     */
    public function show($order_id)
    {
        return view($this->pageName.'.show')
            ->withOrder(Order::find($order_id))
            ;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Budget  $budget
     * @return \Illuminate\Http\Response
     */
    public function edit(Budget $budget)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Budget  $budget
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Budget $budget)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Budget  $budget
     * @return \Illuminate\Http\Response
     */
    public function destroy(Budget $budget)
    {
        //
    }
}
