<?php

namespace App\Http\Controllers;

use App\Budget;
use App\Buyer;
use App\Department;
use App\Order;
use App\OrderHistory;
use App\OrderSizeNColor;
use Str;
use Image, Auth;
use App\OrderCategory;
use App\OrderStatus;
use App\OrderSubCategory;
use App\Role;
use App\Team;
use App\User;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    public $_profile_pic_folder = 'profile-pic/garments';
    public $pageData = [];
    public $pageName = 'Order';
    public $viewFolder = 'orders';

    public function __construct()
    {
        $pageName = 'order';
        $this->pageData = [
            'no' => 0,
            'pageName' => Str::studly($pageName),
            'routeFirstName' => $pageName,
            'ignoreColsInExport' => '[0]',
        ];

        view()->share('pageData', $this->pageData);

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        /*$msg = "New order place in Order List \n";

        $msg .= "\n \n ERROR";
        $msg .= "\n \n ERROR";
        $msg .= "\n \n ERROR";

        $headers = "From: newrmgerp@gmail.com";
        // send email
        mail("biplob@westapparelltd.com","My subject",$msg,$headers);
        mail("tanvir603@gmail.com","My subject",$msg,$headers);*/


        //dd($request->all());
        $query = Order::query();
        $query = $query->with(['budget:order_id,cm', 'buyer', 'quantity', 'status', 'shipment']);
        $id = $request->get('id') ?? null;
        $buyer_id = $request->get('buyer_id') ?? null;
        $name = $request->get('name') ?? null;
        $style = $request->get('style') ?? null;
        $style_desc = $request->get('style_desc') ?? null;
        $statuses = $request->get('statuses') ?? null;
        $category_id = $request->get('category_id') ?? null;
        $sub_category_id = $request->get('sub_category_id') ?? null;
        $from = $request->get('from') ?? null;
        $to = $request->get('to') ?? null;

        //$date_type = $request->get('date_type') ?? null;

        $per_page=(int)$request->input('per_page');

        if($per_page<=0)
        {
            $per_page = 20;
        }

        if($request->filled('id')) {
            $query = $query->where('id', $request->get('id') );
            //$phone = $request->get('phone');
        }
        if($request->filled('buyer_id')) {
            $query = $query->where('buyer_id', $request->get('buyer_id') );
            //$phone = $request->get('phone');
        }
        if($request->filled('name')) {
            $query = $query->where('name', $request->get('name') );
            //$phone = $request->get('phone');
        }
        if($request->filled('style')) {
            $query = $query->where('style', $request->get('style') );
            //$phone = $request->get('phone');
        }
        if($request->filled('style_desc')) {
            $query = $query->where('style_desc', $request->get('style_desc') );
            //$phone = $request->get('phone');
        }
        if($request->filled('statuses')) {
            $query = $query->where('statuses', $request->get('statuses') );
            //$phone = $request->get('phone');
        }
        if($request->filled('sub_category_id')) {
            $query = $query->where('sub_category_id', $request->get('sub_category_id') );
            //$phone = $request->get('phone');
        }
        if($request->filled('from') && $request->filled('to')) {
            $query = $query->whereBetween($request->get('date_type'), [$request->get('from'), $request->get('to')] );
            //$phone = $request->get('phone');
        }


        //$orders = $query->get();
        $orders = $query->orderBy('date_of_ship')->get();
        //dd($orders);
        //$orders->setpath($request->fullUrl()."&per_page=$per_page");

        $compact = compact(
            'id',
            'buyer_id',
            'name',
            'style',
            'style_desc',
            'statuses',
            'category_id',
            'sub_category_id',
            'from',
            'to',
            'per_page',
            'orders'
        );

        return view('orders.index', $compact)
            ->withCount($query->get()->count())
            ;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('orders.create')
            ;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request['order_qty']);
        //dd($request->all());
        /*echo '<pre>';
        print_r($request->all());
        echo '</pre>';*/
        //dd($request->all());
        $loggedUser = Auth::user();
        if(!$loggedUser->can_manage_user())
        {
            //return redirect()->back()->with(['fail'=>'Unauthorized Access.']);
        }
        $team_ids = $loggedUser->role->managed_user_team_ids();
        //dd($team_ids);
        $request->validate([
            //'name' => 'required|max:255',
            'buyer_id' => 'required|integer',
            //'style' => 'required',
            //'order_accept_date' => 'required',
            'date_of_ship' => 'required',
            //'quantity' => 'required|integer',
            'unit_price' => 'required|numeric',
            //'sub_category_id' => 'required|integer',
            //'status_id' => 'required|integer',
            //'sales_user_id' => 'required|integer',
            'image' => 'mimes:jpeg,jpg,png,JPEG,JPG,PNG|nullable|max:1000',

            //Budget
            "yarn_price" => 'numeric|nullable',
            "knitting_price" => 'numeric|nullable',
            "dyeing_price" => 'numeric|nullable',
            "aop" => 'numeric|nullable',
            "yd" => 'numeric|nullable',
            "accessories" => 'numeric|nullable',
            "test_cost" => 'numeric|nullable',
            "print" => 'numeric|nullable',
            "embroidery" => 'numeric|nullable',
            "bank_charge" => 'numeric|nullable',
            "commission" => 'numeric|nullable',
            "cm" => 'numeric|nullable',
            "finish_fab_consumption" => 'numeric|nullable',
            "yarn_consumption" => 'numeric|nullable',
            "freight_charge" => 'numeric|nullable',
            "others" => 'numeric|nullable',
            "lc_or_sales_contract" => 'numeric|nullable'
        ]);
        if(!in_array($loggedUser->team->id,$team_ids))
        {
            //return redirect()->back()->with(['fail'=>'Permission denied to assign user to this team.']);
        }

        $model = new Order();
        $model->user_id = $loggedUser->id;
        $model->name = $request['name'];
        $model->buyer_id = $request['buyer_id'];
        $model->style = $request['style'];
        $model->style_desc = $request['style_desc'];

        $model->order_accept_date = date('Y-m-d');
        if($request->filled('order_accept_date')) {
            $model->order_accept_date = $request['order_accept_date'];
        }

        $model->date_of_ship = $request['date_of_ship'];
        //$model->quantity = $request['quantity'];
        $model->unit_price = $request['unit_price'];
        $model->sub_category_id = $request['sub_category_id'];
        $model->status_id = $request['status_id'];
        $model->smv = $request['smv'];
        $model->sales_user_id = $request['sales_user_id'];
        if ($request->has('reporting_to_user_id')) {
            $model->reporting_to_user_id = $request['reporting_to_user_id'];
        }
        $model->description = $request['description'];

        if($request->hasFile('image'))
        {
            $image = $request->file('image');
            $model->image = $filename = time() . '.' . $image->getClientOriginalExtension();
            $image_resize = Image::make($image->getRealPath());
            $image_resize->widen(250);
            $image_resize->save(public_path($this->_profile_pic_folder.'/' .$filename));
        }
        $model->save();

        //Budget Save in DB
        $model2 = new Budget();
        $model2->order_id =             $model->id;
        $model2->yarn_price =           $request['yarn_price'];
        $model2->knitting_price =       $request['knitting_price'];
        $model2->dyeing_price =         $request['dyeing_price'];
        $model2->aop =            $request['aop'];
        $model2->yd =            $request['yd'];
        $model2->accessories =          $request['accessories'];
        $model2->test_cost =            $request['test_cost'];
        $model2->print = $request['print'];
        $model2->embroidery = $request['embroidery'];
        $model2->bank_charge =          $request['bank_charge'];
        $model2->commission =           $request['commission'];
        $model2->cm =                   $request['cm'];
        $model2->finish_fab_consumption = $request['finish_fab_consumption'];
        $model2->yarn_consumption =     $request['yarn_consumption'];
        $model2->freight_charge =       $request['freight_charge'];
        $model2->others =               $request['others'];
        $model2->lc_or_sales_contract = $request['lc_or_sales_contract'];
        $model2->description =          $request['budget_description'];
        $model2->save();


        foreach ($request['order_qty'] as $key => $qty) {
            //Order Quantity Save in DB
            $model3 = new OrderSizeNColor();
            $model3->user_id = $loggedUser->id;
            $model3->order_id =             $model->id;
            $model3->quantity =             $qty['qty'];
            $model3->color_id =             isset($qty['color_id'])?$qty['color_id']:null;
            $model3->size_id  =             isset($qty['size_id'])?$qty['size_id']:null;
            $model3->save();
        }


        /**************/
        // the message
        $msg = "New order place in Order List \n";

        $msg .= "Buyer: ".$model->buyer->name ."\n";
        $msg .= "Order No: $model->name \n";
        $msg .= "Style No: $model->style \n";
        $msg .= "Quantity: ".$model->quantity->sum('quantity') ."\n";
        $msg .= "Unit Price: $model->unit_price \n";
        $msg .= "Order Value: ". $model->unit_price * $model->quantity->sum('quantity') ."\n";
        $msg .= "Shipment: ". $model->date_of_ship ."\n";
        $msg .= "Order Place by: ".$model->user->name;

        $headers = "From: newrmgerp@gmail.com";
        // send email
        //mail("biplob@westapparelltd.com","New Order Place",$msg,$headers);
        //mail("tanvir603@gmail.com","New Order Place",$msg,$headers);
        /**************/

        return redirect()->route($this->pageData['routeFirstName'].'-list')->with(['success'=>$this->pageData['pageName'].' Created Successfully.']);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function show(Order $order)
    {
        return view('orders.show')
            ->withOrder($order)
            ;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function edit(Order $order)
    {
        //order quantity for vue
        $vueArr = [];

        foreach ($order->quantity as $key => $qty) {
            $vueArr[] = [
                'clrName' => isset($qty['color_id'])?$qty->color->name:'',
                'szName' => isset($qty['size_id'])?$qty->size->name:'',
                'clr' => isset($qty['color_id'])?$qty['color_id']:'',
                'sz' => isset($qty['size_id'])?$qty['size_id']:'',
                'qty' => $qty['quantity']
            ];
        }

        return $this->create()
            ->withIsSetModel($order)
            ->withVueArr($vueArr)
        ;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Order $order)
    {
        //dd($request['order_qty']);
        //budgetEditEnable
        $loggedUser = Auth::user();
        if(!$loggedUser->can_manage_user())
        {
            return redirect()->back()->with(['fail'=>'Unauthorized Access.']);
        }
        $team_ids = $loggedUser->role->managed_user_team_ids();
        //dd($team_ids);
        $request->validate([
            //'name' => 'required|max:255',
            'buyer_id' => 'required|integer',
            //'style' => 'required',
            //'order_accept_date' => 'required',
            'date_of_ship' => 'required',
            //'quantity' => 'required|integer',
            'unit_price' => 'required|numeric',
            //'sub_category_id' => 'required|integer',
            //'status_id' => 'required|integer',
            //'sales_user_id' => 'required|integer',
            'image' => 'mimes:jpeg,jpg,png,JPEG,JPG,PNG|nullable|max:1000',

            //Budget
            "yarn_price" => 'numeric|nullable',
            "knitting_price" => 'numeric|nullable',
            "dyeing_price" => 'numeric|nullable',
            "aop" => 'numeric|nullable',
            "yd" => 'numeric|nullable',
            "accessories" => 'numeric|nullable',
            "test_cost" => 'numeric|nullable',
            "print" => 'numeric|nullable',
            "embroidery" => 'numeric|nullable',
            "bank_charge" => 'numeric|nullable',
            "commission" => 'numeric|nullable',
            "cm" => 'numeric|nullable',
            "finish_fab_consumption" => 'numeric|nullable',
            "yarn_consumption" => 'numeric|nullable',
            "freight_charge" => 'numeric|nullable',
            "others" => 'numeric|nullable',
            "lc_or_sales_contract" => 'numeric|nullable'
        ]);
        if(!in_array($loggedUser->team->id,$team_ids))
        {
            return redirect()->back()->with(['fail'=>'Permission denied to assign user to this team.']);
        }

        /*order history*/
        $orderHistory = new OrderHistory();
        $orderHistory->user_id              = $order->user_id;
        $orderHistory->order_id             = $order->id;
        $orderHistory->name                 = $order->name;
        $orderHistory->buyer_id             = $order->buyer_id;
        $orderHistory->style                = $order->style;
        $orderHistory->style_desc           = $order->style_desc;
        $orderHistory->order_accept_date    = $order->order_accept_date;
        $orderHistory->date_of_ship         = $order->date_of_ship;
        //$orderHistory->quantity             = $order->quantity;
        $orderHistory->unit_price           = $order->unit_price;
        $orderHistory->sub_category_id      = $order->sub_category_id;
        $orderHistory->status_id            = $order->status_id;
        $orderHistory->smv                  = $order->smv;
        $orderHistory->sales_user_id        = $order->sales_user_id;
        $orderHistory->reporting_to_user_id = $order->reporting_to_user_id;
        $orderHistory->description          = $order->description;
        $orderHistory->image                = $order->image;
        $orderHistory->save();

        if ($request['budgetEditEnable']) {
            if ($order->budget) {
                $prevBudget = $order->budget;
                /*History Budget*/
                $budgetHistory = new Budget(); // new budget instance
                $budgetHistory->history_type            = 'Edit';
                $budgetHistory->order_id                = $prevBudget->order_id;
                $budgetHistory->yarn_price              = $prevBudget->yarn_price;
                $budgetHistory->knitting_price          = $prevBudget->knitting_price;
                $budgetHistory->dyeing_price            = $prevBudget->dyeing_price;
                $budgetHistory->aop               = $prevBudget->aop;
                $budgetHistory->yd               = $prevBudget->yd;
                $budgetHistory->accessories             = $prevBudget->accessories;
                $budgetHistory->test_cost               = $prevBudget->test_cost;
                $budgetHistory->print    = $prevBudget->print;
                $budgetHistory->embroidery    = $prevBudget->embroidery;
                $budgetHistory->bank_charge             = $prevBudget->bank_charge;
                $budgetHistory->commission              = $prevBudget->commission;
                $budgetHistory->cm                      = $prevBudget->cm;
                $budgetHistory->finish_fab_consumption  = $prevBudget->finish_fab_consumption;
                $budgetHistory->yarn_consumption        = $prevBudget->yarn_consumption;
                $budgetHistory->freight_charge          = $prevBudget->freight_charge;
                $budgetHistory->others                  = $prevBudget->others;
                $budgetHistory->lc_or_sales_contract    = $prevBudget->lc_or_sales_contract;
                $budgetHistory->description             = $prevBudget->budget_description;
                $budgetHistory->save();

                /*Update Budget*/
                $model2 = $order->budget;
                $model2->yarn_price =           $request['yarn_price'];
                $model2->knitting_price =       $request['knitting_price'];
                $model2->dyeing_price =         $request['dyeing_price'];
                $model2->aop =            $request['aop'];
                $model2->yd =            $request['yd'];
                $model2->accessories =          $request['accessories'];
                $model2->test_cost =            $request['test_cost'];
                $model2->print = $request['print'];
                $model2->embroidery = $request['embroidery'];
                $model2->bank_charge =          $request['bank_charge'];
                $model2->commission =           $request['commission'];
                $model2->cm =                   $request['cm'];
                $model2->finish_fab_consumption = $request['finish_fab_consumption'];
                $model2->yarn_consumption =     $request['yarn_consumption'];
                $model2->freight_charge =       $request['freight_charge'];
                $model2->others =               $request['others'];
                $model2->lc_or_sales_contract = $request['lc_or_sales_contract'];
                $model2->description =          $request['budget_description'];
                $model2->save();
            } else {
                $model2 = new Budget();
                $model2->order_id =             $order->id;
                $model2->yarn_price =           $request['yarn_price'];
                $model2->knitting_price =       $request['knitting_price'];
                $model2->dyeing_price =         $request['dyeing_price'];
                $model2->aop=            $request['aop'];
                $model2->yd =            $request['yd'];
                $model2->accessories =          $request['accessories'];
                $model2->test_cost =            $request['test_cost'];
                $model2->print = $request['print'];
                $model2->embroidery = $request['embroidery'];
                $model2->bank_charge =          $request['bank_charge'];
                $model2->commission =           $request['commission'];
                $model2->cm =                   $request['cm'];
                $model2->finish_fab_consumption = $request['finish_fab_consumption'];
                $model2->yarn_consumption =     $request['yarn_consumption'];
                $model2->freight_charge =       $request['freight_charge'];
                $model2->others =               $request['others'];
                $model2->lc_or_sales_contract = $request['lc_or_sales_contract'];
                $model2->description =          $request['budget_description'];
                $model2->save();
            }
        }

        /*order history*/

        $model = $order;
        $model->user_id = $loggedUser->id;
        $model->name = $request['name'];
        $model->buyer_id = $request['buyer_id'];
        $model->style = $request['style'];
        $model->style_desc = $request['style_desc'];
        $model->order_accept_date = $request['order_accept_date'];
        $model->date_of_ship = $request['date_of_ship'];
        //$model->quantity = $request['quantity'];
        $model->unit_price = $request['unit_price'];
        $model->sub_category_id = $request['sub_category_id'];
        $model->status_id = $request['status_id'];
        $model->smv = $request['smv'];
        $model->sales_user_id = $request['sales_user_id'];
        if ($request->has('reporting_to_user_id')) {
            $model->reporting_to_user_id = $request['reporting_to_user_id'];
        }
        $model->description = $request['description'];

        if($request->hasFile('image'))
        {
            $image = $request->file('image');
            $model->image = $filename = time() . '.' . $image->getClientOriginalExtension();
            $image_resize = Image::make($image->getRealPath());
            $image_resize->widen(250);
            $image_resize->save(public_path($this->_profile_pic_folder.'/' .$filename));
        }
        $model->save();

        $orderSizeNColors = OrderSizeNColor::where('order_id', $order->id)->get();

        foreach ($orderSizeNColors as $k=>$orderSizeNColor) {
            $orderSizeNColor->quantity = $request['order_qty'][$k]['qty'];
            $orderSizeNColor->color_id = $request['order_qty'][$k]['color_id'];
            $orderSizeNColor->size_id = $request['order_qty'][$k]['size_id'];
            $orderSizeNColor->save();
        }


        return redirect()->route('order-show', $order->id)->with(['success'=>$this->pageData['pageName'].' Updated Successfully.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order)
    {
        $order->deleteOrder();

        return redirect()->back();
    }

    /**
     * Print order information.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */

    public function print(Order $order)
    {
        //dd($order);
        return view($this->viewFolder.'.print.index')
            ->withOrder($order)
            ;
    }

}
