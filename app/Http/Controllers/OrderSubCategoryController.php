<?php

namespace App\Http\Controllers;

use App\OrderCategory;
use App\OrderSubCategory, Auth;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class OrderSubCategoryController extends Controller
{
    public $pageData = [];

    public function __construct()
    {
        $this->pageData = [
            'no' => 1,
            'pageName' => 'Order Sub Category',
            'routeFirstName' => 'order-sub-category',
            'ignoreColsInExport' => '[4]',
        ];

        view()->share('pageData', $this->pageData);

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = OrderSubCategory::query();
        $name = null;
        $status = null;

        $per_page=(int)$request->input('per_page');

        if($per_page<=0)
        {
            $per_page = 20;
        }

        if($request->filled('name')) {
            $query = $query->where('name', 'like', '%'.$request->get('name').'%');
            $name = $request->get('name');
        }
        if($request->filled('status')) {
            $query = $query->where('status', 'like', '%' . $request->get('status') . '%');
            $status = $request->get('status');
        }

        $mainDatas = $query->paginate($per_page);
        $mainDatas->setpath($request->fullUrl()."&per_page=$per_page");

        $compact = compact(
            'name',
            'status',
            'per_page',
            'mainDatas'
        );

        return view('orderSubCategories.index', $compact)
            ->withCount($query->get()->count())
            ;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('orderSubCategories.create')
            ->withOrderCategories(OrderCategory::all())
            ;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->all());
        $loggedUseruser = Auth::user();
        if(!$loggedUseruser->can_manage_user())
        {
            return redirect()->back()->with(['fail'=>'Unauthorized Access.']);
        }
        $team_ids = $loggedUseruser->role->managed_user_team_ids();
        //dd($team_ids);
        $request->validate([
            'name' => 'required|max:255|unique:order_sub_categories',
            'status' => 'integer',
            'order_category_id' => 'integer',
        ]);

        $mainData = new OrderSubCategory();
        $mainData->name = $request['name'];

        if ($request->has('status')) {
            $mainData->status = $request['status'];
        }
        if ($request->has('order_category_id')) {
            $mainData->order_category_id = $request['order_category_id'];
        }

        $mainData->save();
        return redirect()->route($this->pageData['routeFirstName'].'-list')->with(['success'=>$this->pageData['pageName'].' Created Successfully.']);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\OrderSubCategory  $orderSubCategory
     * @return \Illuminate\Http\Response
     */
    public function show(OrderSubCategory $orderSubCategory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\OrderSubCategory  $orderSubCategory
     * @return \Illuminate\Http\Response
     */
    public function edit(OrderSubCategory $orderSubCategory)
    {
        return $this->create()
            ->withIsSetOrderSubCategory($orderSubCategory)
            ;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\OrderSubCategory  $orderSubCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, OrderSubCategory $orderSubCategory)
    {
        //dd($request->all());
        $loggedUseruser = Auth::user();
        if(!$loggedUseruser->can_manage_user())
        {
            return redirect()->back()->with(['fail'=>'Unauthorized Access.']);
        }
        $team_ids = $loggedUseruser->role->managed_user_team_ids();
        //dd($team_ids);
        $request->validate([
            'name' => [
                'required',
                Rule::unique('order_sub_categories')->ignore($orderSubCategory->id)
            ],
            'status' => 'integer',
            'order_category_id' => 'integer',
        ]);

        $mainData = $orderSubCategory;
        $mainData->name = $request['name'];

        if ($request->has('status')) {
            $mainData->status = $request['status'];
        }
        if ($request->has('order_category_id')) {
            $mainData->order_category_id = $request['order_category_id'];
        }

        $mainData->save();
        return redirect()->route($this->pageData['routeFirstName'].'-list')->with(['success'=>$this->pageData['pageName'].' Updated Successfully.']);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\OrderSubCategory  $orderSubCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy(OrderSubCategory $orderSubCategory)
    {
        //
    }
}
