<?php

namespace App\Http\Controllers;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use App\Order, Auth;
use App\Production;
use Illuminate\Http\Request;

class ProductionController extends Controller
{
    public $_profile_pic_folder = 'profile-pic/garments';
    public $pageData = [];

    public function __construct()
    {
        $pageName = 'production';
        $this->pageData = [
            'no' => 0,
            'pageName' => Str::studly($pageName),
            'routeFirstName' => $pageName,
            'ignoreColsInExport' => '[0]',
        ];

        view()->share('pageData', $this->pageData);

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = Order::query();
        $query = $query->with(['budget:order_id,cm', 'buyer', 'status', 'shipment']);
        $id = $request->get('id') ?? null;
        $buyer_id = $request->get('buyer_id') ?? null;
        $name = $request->get('name') ?? null;
        $style = $request->get('style') ?? null;
        $style_desc = $request->get('style_desc') ?? null;
        $statuses = $request->get('statuses') ?? null;
        $category_id = $request->get('category_id') ?? null;
        $sub_category_id = $request->get('sub_category_id') ?? null;
        $from = $request->get('from') ?? null;
        $to = $request->get('to') ?? null;

        $per_page=(int)$request->input('per_page');

        if($per_page<=0)
        {
            $per_page = 20;
        }

        if($request->filled('id')) {
            $query = $query->where('id', $request->get('id') );
            //$phone = $request->get('phone');
        }
        if($request->filled('buyer_id')) {
            $query = $query->where('buyer_id', $request->get('buyer_id') );
            //$phone = $request->get('phone');
        }
        if($request->filled('name')) {
            $query = $query->where('name', $request->get('name') );
            //$phone = $request->get('phone');
        }
        if($request->filled('style')) {
            $query = $query->where('style', $request->get('style') );
            //$phone = $request->get('phone');
        }
        if($request->filled('style_desc')) {
            $query = $query->where('style_desc', $request->get('style_desc') );
            //$phone = $request->get('phone');
        }
        if($request->filled('statuses')) {
            $query = $query->where('statuses', $request->get('statuses') );
            //$phone = $request->get('phone');
        }
        if($request->filled('sub_category_id')) {
            $query = $query->where('sub_category_id', $request->get('sub_category_id') );
            //$phone = $request->get('phone');
        }
        if($request->filled('from') && $request->filled('to')) {
            $query = $query->whereBetween('date_of_ship', [$request->get('from'), $request->get('to')] );
            //$phone = $request->get('phone');
        }

        $orders = $query->paginate($per_page);
        $orders->setpath($request->fullUrl()."&per_page=$per_page");

        $compact = compact(
            'id',
            'buyer_id',
            'name',
            'style',
            'style_desc',
            'statuses',
            'category_id',
            'sub_category_id',
            'from',
            'to',
            'per_page',
            'orders'
        );

        return view('productions.index', $compact)
            ->withCount($query->get()->count())
            ;
    }

    public function dateWiseList(Request $request)
    {
        //dd(Production::find(1)->cutting());
        $query = Order::query();
        $query = $query->with(['budget:order_id,cm', 'buyer', 'status', 'shipment']);
        $id = $request->get('id') ?? null;
        $buyer_id = $request->get('buyer_id') ?? null;
        $name = $request->get('name') ?? null;
        $style = $request->get('style') ?? null;
        $style_desc = $request->get('style_desc') ?? null;
        $statuses = $request->get('statuses') ?? null;
        $category_id = $request->get('category_id') ?? null;
        $sub_category_id = $request->get('sub_category_id') ?? null;
        $from = $request->get('from') ?? null;
        $to = $request->get('to') ?? null;

        $per_page=(int)$request->input('per_page');

        if($per_page<=0)
        {
            $per_page = 20;
        }

        if($request->filled('id')) {
            $query = $query->where('id', $request->get('id') );
            //$phone = $request->get('phone');
        }
        if($request->filled('buyer_id')) {
            $query = $query->where('buyer_id', $request->get('buyer_id') );
            //$phone = $request->get('phone');
        }
        if($request->filled('name')) {
            $query = $query->where('name', $request->get('name') );
            //$phone = $request->get('phone');
        }
        if($request->filled('style')) {
            $query = $query->where('style', $request->get('style') );
            //$phone = $request->get('phone');
        }
        if($request->filled('style_desc')) {
            $query = $query->where('style_desc', $request->get('style_desc') );
            //$phone = $request->get('phone');
        }
        if($request->filled('statuses')) {
            $query = $query->where('statuses', $request->get('statuses') );
            //$phone = $request->get('phone');
        }
        if($request->filled('sub_category_id')) {
            $query = $query->where('sub_category_id', $request->get('sub_category_id') );
            //$phone = $request->get('phone');
        }
        if($request->filled('from') && $request->filled('to')) {
            $query = $query->whereBetween('date_of_ship', [$request->get('from'), $request->get('to')] );
            //$phone = $request->get('phone');
        }

        $orders = $query->paginate($per_page);
        $orders->setpath($request->fullUrl()."&per_page=$per_page");

        $order_ids = [];

        foreach ($query->get() as $ord) {
            $order_ids[] = $ord->id;
        }

        //dd($order_ids);

        //$productions = Production::runningProductions()->get();
        //$productions = Production::where('history_type', 'Running')->get();
        $unique_productions = Production::whereIn('order_id', $order_ids)->where('history_type', 'Running')->distinct()->get(['order_id', 'date']);
        //$productions = Production::where('history_type', 'Running')->groupBy('order_id')->get();


        /*$production_array = [];
        foreach ($productions as $production) {
            if ($production->id) {
            }
            $production_array[] = [
                'model' => $production,
            ];
        }*/

        //dd($productions);

        $compact = compact(
            'id',
            'buyer_id',
            'name',
            'style',
            'style_desc',
            'statuses',
            'category_id',
            'sub_category_id',
            'from',
            'to',
            'per_page'
        );

        if (count($unique_productions) > 0 ) {
            $compact += compact(
                'unique_productions'
            );
        }



        return view('productions.date_wise.index', $compact)
            ->withCount($query->get()->count())
            ;
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $id = $request->get('id') ?? null;

        $compact = compact(
            'id'
        );

        $filterOrder = (new UtilityController)->filterOrder($request);
        $compact += $filterOrder;

        return view('productions.create', $compact)
            ->withOrder(Order::find($id))
            ;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Order $order)
    {
        //dd($order);
        //dd($request->all());

        if (!$order) {
            return redirect()->back()->with(['fail'=>'Unauthorized Access.']);
        }
        $loggedUser = Auth::user();
        if(!$loggedUser->can_manage_user())
        {
            return redirect()->back()->with(['fail'=>'Unauthorized Access.']);
        }
        $team_ids = $loggedUser->role->managed_user_team_ids();

        if(!in_array($loggedUser->team->id,$team_ids))
        {
            return redirect()->back()->with(['fail'=>'Permission denied to assign user to this team.']);
        }

        //dd($team_ids);
        $request->validate([
            'production_type' => 'required',
            'quantity' => 'required|numeric',
            'line_id' => Rule::requiredIf($request['production_type'] == 'swing_in' || $request['production_type'] == 'swing_out'),
            'sub_contract_id' => Rule::requiredIf($request['operation'] == 'SubContract'),
            'date' => 'nullable|date',
        ]);

        $model = new Production();
        $model->order_id = $order->id;
        $model->user_id = $loggedUser->id;
        $model->production_type = $request['production_type'];
        $model->order_qty_id = $request['order_quantity_id'];
        $model->quantity = $request['quantity'];
        $model->operation = $request['operation'];
        $model->date = date('Y-m-d');

        if ($request->filled('line_id')) {
            $model->line_id = $request['line_id'];
        }
        if ($request->filled('date')) {
            $model->date = $request['date'];
        }
        if ($request->filled('sub_contract_id')) {
            $model->sub_contract_id = $request['sub_contract_id'];
        }

        $model->save();
        return back()->withInput()->with(['success'=>$this->pageData['pageName'].' Created Successfully.']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Production  $production
     * @return \Illuminate\Http\Response
     */
    public function show($order_id)
    {
        return view('productions.show')
            ->withOrder(Order::find($order_id))
            ;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Production  $production
     * @return \Illuminate\Http\Response
     */
    public function edit(Production $production)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Production  $production
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Production $production)
    {
        //dd($request->all());
        $production->date = $request->date;
        $production->production_type = $request->type;
        $production->quantity = $request->qty;
        $production->save();

        return back()->with(['success'=>$this->pageData['pageName'].' update Successfully.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Production  $production
     * @return \Illuminate\Http\Response
     */
    public function destroy(Production $production)
    {
        //
    }
}
