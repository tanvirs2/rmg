<?php
/**
 * Created by PhpStorm.
 * User: Tanvir
 * Date: 21-Nov-18
 * Time: 10:09 AM
 */

namespace App\Http\ViewComposer;

use Illuminate\View\View;

class UserComposer
{
    public function currentUser(View $view)
    {
        $dashboard_type = [
            'super admin',
            'admin'
        ];
        $per_page_rows = [1, 2, 5, 10, 20, 25, 50, 100, 200, 500];

        $view->with([
            'dashboard_types' => $dashboard_type,
            'per_page_rows' => $per_page_rows,
        ]);


    }
}