<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use URL;

class Agent extends Model
{
    public function GetProfilePicture()
    {
        if ($this->image != '') {
            return URL::to('profile-pic/agent/'.$this->image);
        } else {
            return URL::to('profile-pic/man.jpg');
        }
    }
}
