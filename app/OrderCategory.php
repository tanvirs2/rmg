<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderCategory extends Model
{
    public function getStatusNameFromNumber($statusNumber = false)
    {
        $statusNumber || $statusNumber = $this->status;
        switch ($statusNumber){
            case 0:
                return 'Inactive';
                break;
            case 1:
                return 'Active';
                break;
            case 2:
                return 'Pending';
        }
    }

    public function subCategories()
    {
        return $this->hasMany(OrderSubCategory::class);
    }
}
