<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderSubCategory extends Model
{
    public function getStatusNameFromNumber()
    {
        //function from 'OrderCategory'
        return (new OrderCategory())->getStatusNameFromNumber($this->status);
    }

    public function orderCategory()
    {
        return $this->belongsTo(OrderCategory::class);
    }
}
