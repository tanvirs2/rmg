<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use URL;

class Order extends Model
{
    /*Start Date Wise*/
    public function prodCutting($date)
    {
        return $this->hasMany(Production::class)->where(
            [
                ['date', $date],
                ['history_type', 'Running'],
                ['production_type', 'cutting']
            ]
        )->sum('quantity');
    }

    public function prodSwingIn($date)
    {
        return $this->hasMany(Production::class)->where(
            [
                ['date', $date],
                ['history_type', 'Running'],
                ['production_type', 'swing_in']
            ]
        )->sum('quantity');
    }


    public function prodSwingOut($date)
    {
        return $this->hasMany(Production::class)->where(
            [
                ['date', $date],
                ['history_type', 'Running'],
                ['production_type', 'swing_out']
            ]
        )->sum('quantity');
    }

    public function prodIron($date)
    {
        return $this->hasMany(Production::class)->where(
            [
                ['date', $date],
                ['history_type', 'Running'],
                ['production_type', 'iron']
            ]
        )->sum('quantity');
    }

    public function prodPacking($date)
    {
        return $this->hasMany(Production::class)->where(
            [
                ['date', $date],
                ['history_type', 'Running'],
                ['production_type', 'packing']
            ]
        )->sum('quantity');
    }
    /*End Date Wise*/

    public function orderYearWiseMonth($yr)
    {
        $orderPerMonth = [];
        $orderQtyTotVal = [];
        $orderQty = [];

        for ($i=1; $i<=12; $i++){
            $monthName = Carbon::create()->month($i)->shortEnglishMonth;
            $orderPerMonth[$monthName] = $this->whereYear('date_of_ship', $yr)
                ->whereMonth('date_of_ship', $i)
                ->with('quantity')
                ->get();

            foreach ($orderPerMonth[$monthName] as $ordQty) {
                $orderQtyTotVal[$monthName][] = $ordQty->unit_price * $ordQty->quantity->sum('quantity')  ?? [];
                $orderQty[$monthName][] = $ordQty->quantity->sum('quantity') ?? [];
            }
        }

        return [$orderPerMonth, $orderQty, $orderQtyTotVal];
    }

    public function pendingOrderYearWiseMonth($yr)
    {
        $orderPerMonth = [];
        $orderQtyTotVal = [];
        $orderQty = [];

        for ($i=1; $i<=12; $i++){
            $monthName = Carbon::create()->month($i)->shortEnglishMonth;
            $orderPerMonth[$monthName] = $this->whereYear('date_of_ship', $yr)
                ->whereMonth('date_of_ship', $i)
                ->where([['statuses', 'Partial'], ['date_of_ship', '<=', date('Y-m-d')]])
                ->with('quantity')
                ->get();

            foreach ($orderPerMonth[$monthName] as $ordQty) {
                $orderQtyTotVal[$monthName][] = $ordQty->unit_price * $ordQty->quantity->sum('quantity')  ?? [];
                $orderQty[$monthName][] = $ordQty->quantity->sum('quantity') ?? [];
            }
        }

        return [$orderPerMonth, $orderQty, $orderQtyTotVal];
    }

    public function kdExtra()
    {
        return $this->hasMany(KDExtra::class);
    }
    public function kdProgram()
    {
        return $this->hasMany(KDProgram::class);
    }

    public function kdProgramWithParts()
    {
        //return KDProgram::where('order_id', $this->id)->distinct()->get('kd_parts_id');
        return $this->hasMany(KDProgram::class)->distinct()->get('kd_parts_id');
    }

    public function kdProgramWithPartsDetails($kdPartsId)
    {
        return $this->kdProgram()->where('kd_parts_id', $kdPartsId)->get();
    }

    public function kdProgramWithFabricDetails($kdPartsId)
    {
        return $this->hasMany(KDFabrics::class)->where('kd_parts_id', $kdPartsId)->first();
    }

    public function kdRegister()
    {
        return $this->hasOne(KDRegister::class);
    }

    public function orderColorAndSizeQtyArrFunc()
    {
        $orderSizeAndColorArr = [];

        foreach($this->quantity as $sNcWiseQty){

            if ($sNcWiseQty->color) {

                $orderSizeAndColorArr[$sNcWiseQty->color->name]['color_id'] = $sNcWiseQty->color_id;

                if ($sNcWiseQty->size) {
                    $orderSizeAndColorArr[$sNcWiseQty->color->name]['sz'][] = $sNcWiseQty->size->name;
                }

                $orderSizeAndColorArr[$sNcWiseQty->color->name]['qty'][] = $sNcWiseQty->quantity;
            }
        }

        return $orderSizeAndColorArr;
    }
    public function kdFabric()
    {
        return $this->hasMany(KDFabrics::class);
    }

    public function quantity()
    {
        return $this->hasMany(OrderSizeNColor::class);
    }

    public function orderHistories()
    {
        return $this->hasMany(OrderHistory::class)->latest();
    }
    public function shipment()
    {
        return $this->hasMany(Shipment::class)->where('history_type', 'Running')->latest();
    }
    public function shipmentHistory()
    {
        return $this->hasMany(Shipment::class)->whereIn('history_type', ['Edit','Delete'])->latest();
    }
    public function getGarmentsPicture()
    {
        if ($this->image != '') {
            return URL::to('profile-pic/garments/'.$this->image);
        } else {
            return 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOAAAADgCAMAAAAt85rTAAAAY1BMVEX////e3t57e3uvr69+fn54eHisrKzQ0NC8vLy5ubnAwMDOzs62traxsbF3d3fS0tLFxcXk5OSLi4vw8PD09PTZ2dns7OyFhYXj4+P5+fmenp5xcXGKioqXl5eRkZGnp6dra2sH7R5VAAAOpUlEQVR4nO2da3uqvBKGISYooBwDAUR8//+v3DM5ETzVrl0t9srzYS2rYHMzk5kkE2gQeHl5eXl5eXl5eXl5eXl5eXl5eXl5eXl5eXl5eXl5eXl9gngL4v1vN+Nl4k0Oapow7Nr6L3K2uSsALf+YObv8UtstgIYt57z/A6R9eAU4c6I5wW8/2qKqC96FlJgf3T/rR3zWlui1CNrVH0fZyuY/qxSVh2X9Ob2z/Q6flLwiad7+dsuflAXMc7TO85wN/+2mP6fS8LU9523X5MoNLekC2f0h/wzAPlQ8qWlvz+u6DUMA1aT31HxGL+T5rfb2gNm2ZQecd0HDDwE0zQ9vfSrNCaRhnl8Blu9u6r+Jm/Z2j46C7omgYNAZ8EOiaP0dg6DjNp8GWHzT40oDWL+0XT+m8rsGCYtl1F27GtXeYvukQXp9Qvoheb7PC1BaFM8apJYngD4kDfZgPNXeJ08o9fHFzbSyPvHim+3tzAkfE0S1HqZBR4054UOCaGva+2yWMIDpZ3RBCPrf9DgTY9KXNuvnZNv7ZBDt0w8DNO3Nn/S42p7w2nb9lHrjoU9kib4FK5ffjbq/LBNjkq+7IAwJwrnPPnHC76rvW2ximWi1i8+uj+d5kSQAmJsT3tXQf1Hf1zBXDzExhKa9Tlarm+0lIQ8BL8FcuX7AnuNKBAoBG9PeOYiWaZJcjDPLLfJJwFQfX7y52U+Kt4YOhF5pDFIYQN5IFndc0yo8OKhdNSB32Axgb9qrxyV957Ao1dYpExidcf15sv01jjvq2yUdqMaRqG6vyhL11sLoxF83ReK+Z4PS6rJEe4kXdjKrqebupEu26Qyzw27Yw+c75y0nKK0uS9wABILOtL3FIowDk+x2IS8X7yS78GZQWomuAdEG4U4JDNKn5rX5vzCvtGIIu7k+4FMAGwNT25cPBGfksXpZrA6Q34ox0iCyvXWQ3uVyAVMNuL7ZYF/eAOSGCuJjYRzxNly8i8FqvNAfPzv5eKOufJRjloilsL1J/JVgANfKo4B2dVniGhCDaLvTgJAAdl8CNkHfmBNWWHi57IQlAJa66ZgGv+Tbc5gx6dfrS4Ogi04oJ0t7lDQI3z9WvEfAVP+0viAaXPkoBtFOtxcM0n4BCIgQYyzg+mIMRJQbWcK0F34IYc6wewS4wyCqX69yxekiUdS4ION6HNb+yq7J0wJC5Q1AsFqd6NfrXHFa+igwcdPe1NnW06s5Y7NNdvH+cDhYQMgSoTHxs4WM94ovsgSmQWOoXYG7l5aRsVc1622hOA8I1egT4mfX+d+rRaKQWUJa6HDQdophSlukTbcsOsA8OZR+iwM1dfBht8YsESx9FAEbTbcQku6KIm9wO9ocLDnHPquPWeuKkxtH0QbbazyHE+ZL6LhdO3fPZOWA/AKw2DwgNJxxAtZsmg6XtWP97gpXnKTcRIEdLXnINguvQ7xL0sJ49CrTIMrphNil9puvTWghUeaHdWaJwO2EmCVa1erv67DSIOomCgyi3eF5A2orao9d41Bbau6ECJj/owE3h9/muC/ro+hk6eYffXT/2xj3ZRMFBtEi+h5XtFEnRLvfxrivvnMAk8i22SBETzBH0WqzRDAnCgwTRYRCSKP51UOtNksEs4/i8Kvdn1HPQTnarDZLBLgFXaVBNb7kOB1K9ocnLae13iwR2ETRzdMEmOB2cj4U75/k3K9xQcaqnoPMQj2uV3RhmgDmY789r3LFyUp3QryX7Jan4ebzuu2K3T6Kzre76HnNQdRJFADZdWV576a5vi7Bb5NYGdRywv/nNQdRLGWHF5KYyHkLFMJQvk2LxHAi6AqX7V3Vl4AWswXM2/aEQBTCpBc76DlaX+VsqetC4YKzfZADcKGt69acJFDXhUIHr6zX3vwndMdHoSP+AThUf8ty7efdmntf13Qfenv1PS06YfmXTKfFHdP9dlteoxLT3p/qdRfq6zvjFi8vLy8vLy8vLy8vLy8vLy8vLy8vL6+3q0hTe+dUnrqPwSmbNM2v67M9vK23FdSLRxy2+FXz+fIhj5fPa+w7fE5e3uiN69x5zOOrKvn/Zdkx1q+n7Gi3jzebSZCMTsPlJpD6lGWDPiYDVeaD+AhfNe+vbwh8OCz21/TFMAmWZURM6leGIjNir9qZf6SUCn31TiQzOLEgjGWMEUKH5Qn1QIgBhHMpMUYbCaXM7n3t9wQ/de0SnighBL4S/jmrdyqKb8l3X7UzHwGJ+nUAyDRglFEybtJ0A61mp4WbOoC5BNS7QRWtBeQnQuGCOc3OKYEvPW0Oh/NJpAaQnPStNK+6CfZIBSWi0YDagumRskHeb1QO0KqD25McwITQaiQn9UMMjucAdoRMCTMfIgxegIO+iUnf9wSA7NX3VBzpdKZkkEYygPUErdPOVU6ECvfqOoAxIdFAKtlofmKn2H4C8BmJuowy0wnRolcw4UheD0imEIwkwQxgQp224A9uAHAAd9C8DaPy0EaQKM/IZA7D7+oFtZ16y6CDXtYYATB7vQVpvWeU8sAC9ufZgGDC0RhYyQE8A2ABPRjbDd+RA6BxyfpISQ0H2DdOhFRXG2Pf46JZy8FIUWCDTAuNOc9I4Fuj0zQHcADAUJCpk+dQns4WhO4HV6WAK6d8tAWvj65yalcRtuM16mXVfwQM4ozS1loQr6tzq0NEFtH+AhCzQ44hBN5MGal0n4NrBdG1FFQ5cNCM8g0QL6XkcWEFvjJIRS8FLOEXnS1gIyhzbnU4EJrdBOzBalv0030PMQU6U5pRDdgKQht1rEpBqTDO2FQTqCoUoM2DU/AiSUAI+JgqbgPu7wHWJyJyoAJn7OHNPtgSY8EU8hu69Y6RqVVvGMA8YxDUMhm3wvFdgNBWGFwMCrCcKNvMR0AmrG72QQXYZ9iLGY5NIJKOKqNEEF3wybcRDJPk05zwoikXbU7DSejADC7KXr0fWAHiFRbhXgFikJkHkRzCw8l9ipoFhMABgMFEMoifeCYAqpSJyZPAoDOzozd4Z47F4Nsz4DuiqP6d+0jnQQgrwu5DztFhnQQ2A0IMrEIZMA8bJloHEM7BoXZ2BELlq+gH1IwX6l8ADMIjHbWLBgX0jI1m6jGIuvf5O4ATw3RZQnKopHmsi0LXq0K867yODNeOUDvM+RXAIGLUDjzgcmd69JIwncmvASH0IyBH48jJAHpmoQDM+D2kZgADcZXp7Mot4PieRC9ftDjd0YAdtOa4Kdu2gxxBqsVcdAaEUImAPQxipK/KbI/ttWM/+FBAD5ZYDRJWRdfWbTM5FtyrvNi96v4fC4hzAzt0zAU0WpxO8B8bl7cFzIBbofoXNF0FEAMIVreD7DNgqQuUjvCdGZVB1EZRiG2jlDi/aCxzpEcNiMHTjo3DU6Zmpmy4WEtYWFCG13ZiJDHfAC96nIyYw3PIInrFoDxAWJXfyfTSQYheoxPh8CJAJuyUvBCCzlkpPJyoGJKrpZJ6oFR1MJhnyGzSJ8O5VICUwgihZoLO90RSISwtLw6DEMPZPMOqm4QRfZUFvby8vLy8vLw+Vp3zGPrO/SsSLbyvJ6jyT2LMAyn8+wOhfpFcPU+Mp4u/X8PDXP4Bh3wupIXwze4Z9k90vOTPppwzxkzNKyVzJS0oWEb0s7FFxjI2z3m7Sh8Wjm65zDSfsiyblzgaGG0SLJhVg5mU7I/sP3eC2RBog5T9jT8nOQk0q8/cWTeBibxeD4MZO5aN5nVLmNaq9aOuotdVvZhhkcUWM7DmpKYLRK+Q4hHHBaBzyI8DbgXWh4T2DGeJFxeJ9MoazO4m4aw7OYDkClBXzezbDU792rbZC2pqOFeAgpKzfCDpC56zChP2zWCvbc6oqYXk80rMiYhYrZ8pPQRswfAFIydDAICZ9M2U0EzNoW4AZq+61V6ubB6YWUDpmamF4jKEUN5ajmQKYZJre+dDwISRCHyamWhhAQPr5rcAX/X0ykaQihfU9Da5tidf8MouGxWUDf3BWSl9CDipqpldSnIB9WryOwF3jAxy3UBP5NNMNyc0L4L+zMB4WH42geMRID9iFSealywsYE6IjsRvdFFcnyzkmq8uTdbGjw5MmxJxoPtxYdcRHwJi96tlsVP7BALmfV83ky3J3QBkRV23L6ihlfDba6yuGB/F0ueIL4TNC2C7iqPzkqH/GnAgLJbJx2yaAEAyRNEgCI00860oqmpopzj4WUFEEIGMmET7H/goJop29tADk2tozkrgA8BWFQv5cqcJkbtRRvNYmRuAVO0uyS52rPzfwnoE/j/vb+ngaibS0yaFUwMNZl+e2cDxANBUzeBy6NViBJSLZhC+hvYOIKFmZe1n+ThkhQj/aFcFbqj3Vp11JdT0yhCOiXGUKOyA5wHgBveOwMERtZVd6AXoHeGBmg037wsyWEHHVVgcXB31GCLB/qg9DYXRBkeJeEzWfwGIK9tyWZfYnDCnCcisKhu8L01A1mPZ8XjMoOOx2Pw2Iprc1LyCoHKP2X4BiL0pk0fbqtkMWGd6FPE2QI6Fv5qDWmjYqN/EzLEjTMfQFrseHsJDYUY59wGh640dHtzvweDNErA/aid/G2Axj/rBEU0chZwxzCNPs4NG9U4VOBaAbvHLrZoJwmSFcQaMzUAXAd12vKwP4i4d7YgwyibaFmpPndkNMs3pLyG63O5Ol8hBPy8Iv6gDdzZVs0lXzVSi57zEoqqoDal59EevAFnifM1PCSICM2Vc3Kajc1AvyDz7K50BTAPZW5fbqQWkpnaCn4CHHs2AFXyChQqQnM7nAfoyUddHzhhNxUVV37CGpr/mBx8ogcNLU0viETP7IwIYexLTJ3B3iVNYVxYHC2bGRY1kn530MEheDhgqIHRDZGCFOXs2aT+MMzO/JbJy19gfSRb9IGAixpOdX6ZjVWnaRlSV0G9HYjzbie5urCakLYdRSLOWJzhLaYQBA4e581w1G6sRQ0oYnadqrIbNvNySCHNWJWfa4VTNX+MfCeLl5eXl5eXl5eXl5eXl5eXl5eXl5eXl5eX1w/ofLWA8kmdzDb0AAAAASUVORK5CYII=';
        }
    }
    public function buyer()
    {
        return $this->belongsTo(Buyer::class);
    }
    public function subCategory()
    {
        return $this->belongsTo(OrderSubCategory::class);
    }

    public function status()
    {
        return $this->belongsTo(OrderStatus::class);
    }

    public function sales_user()
    {
        return $this->belongsTo(User::class, 'sales_user_id');
    }
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function reporting_user()
    {
        return $this->belongsTo(User::class, 'reporting_to_user_id');
    }

    public function budget()
    {
        return $this->hasOne(Budget::class)->where('history_type', 'Running')->latest();
    }

    public function budgetHistories()
    {
        return $this->hasMany(Budget::class)->whereIn('history_type', ['Edit','Delete'])->latest();
    }

    public function production()
    {
        return $this->hasMany(Production::class)->where('history_type', 'Running')->latest();
    }

    public function cutting()
    {
        return $this->hasMany(Production::class)->where('production_type', 'cutting')->where('history_type', 'Running')->latest();
    }

    public function cuttingHistory()
    {
        return $this->hasMany(Production::class)->where('production_type', 'cutting')->whereIn('history_type', ['Edit','Delete'])->latest();
    }

    public function swingIn()
    {
        return $this->hasMany(Production::class)->where('production_type', 'swing_in')->where('history_type', 'Running')->latest();
    }

    public function swingInHistory()
    {
        return $this->hasMany(Production::class)->where('production_type', 'swing_in')->whereIn('history_type', ['Edit','Delete'])->latest();
    }

    public function swingOut()
    {
        return $this->hasMany(Production::class)->where('production_type', 'swing_out')->where('history_type', 'Running')->latest();
    }

    public function swingOutHistory()
    {
        return $this->hasMany(Production::class)->where('production_type', 'swing_out')->whereIn('history_type', ['Edit','Delete'])->latest();
    }

    public function iron()
    {
        return $this->hasMany(Production::class)->where('production_type', 'iron')->where('history_type', 'Running')->latest();
    }

    public function ironHistory()
    {
        return $this->hasMany(Production::class)->where('production_type', 'iron')->whereIn('history_type', ['Edit','Delete'])->latest();
    }

    public function packing()
    {
        return $this->hasMany(Production::class)->where('production_type', 'packing')->where('history_type', 'Running')->latest();
    }

    public function packingHistory()
    {
        return $this->hasMany(Production::class)->where('production_type', 'packing')->whereIn('history_type', ['Edit','Delete'])->latest();
    }

    public function deleteOrder()
    {
        //$this->delete();
        $this->shipment()->update(['history_type'=>'OrderDel']);
    }
}
